﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ThemeManagement.ascx.cs"
    Inherits="WebSitesCreative.Modules.Banner.ThemeManagement" %>
<div class="wsc_admin_content">
    <div class="wsc_admin_box">
        <div class="wsc_admin_box_head">
            <div class="wsc_admin_title">
                <%= GetLocalized("AdminTitle")%>
            </div>
        </div>
        <div class="wsc_admin_box_body">
            <div class="wsc_admin_fields">
                <table cellspacing="0" cellpadding="2" border="0" summary="Theme Management Design Table">
                    <tr>
                        <td class="SubHead" width="195">
                            <div class="hideWrap">
                                <a class="hideBtn" href="javascript://" onclick="hideShow(this);return false;">
                                    <asp:Label ID="lblThemeLocationModeText" runat="server" CssClass="wsc_setting_field"
                                        resourcekey="lblThemeLocationModeText"></asp:Label></a>
                                <div class="hideCont">
                                    <asp:Label ID="lblThemeLocationModeHelp" runat="server" CssClass="wsc_help_message"
                                        resourcekey="lblThemeLocationModeHelp"></asp:Label></div>
                            </div>
                        </td>
                        <td valign="bottom">
                            <asp:RadioButtonList runat="server" ID="rblThemeLocationMode" RepeatDirection="Horizontal"
                                AutoPostBack="True" CausesValidation="false" OnSelectedIndexChanged="rblThemeLocationMode_SelectedIndexChanged">
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <asp:PlaceHolder ID="phSkinThemes" runat="server">
                        <tr>
                            <td class="SubHead" width="150">
                                <div class="hideWrap">
                                    <a class="hideBtn" href="javascript://" onclick="hideShow(this);return false;">
                                        <asp:Label ID="lblSkinThemesText" runat="server" CssClass="wsc_setting_field" resourcekey="lblSkinThemesText"></asp:Label></a>
                                    <div class="hideCont">
                                        <asp:Label ID="lblSkinThemesHelp" runat="server" CssClass="wsc_help_message" resourcekey="lblSkinThemesHelp"></asp:Label>
                                    </div>
                                </div>
                            </td>
                            <td valign="bottom">
                                <asp:DropDownList ID="ddlSkinThemes" runat="server" AutoPostBack="true" CausesValidation="false"
                                    OnSelectedIndexChanged="ddlThemes_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="phModuleThemes" runat="server">
                        <tr>
                            <td class="SubHead" width="150">
                                <div class="hideWrap">
                                    <a class="hideBtn" href="javascript://" onclick="hideShow(this);return false;">
                                        <asp:Label ID="lblModuleThemesText" runat="server" CssClass="wsc_setting_field" resourcekey="lblModuleThemesText"></asp:Label></a>
                                    <div class="hideCont">
                                        <asp:Label ID="lblModuleThemesHelp" runat="server" CssClass="wsc_help_message" resourcekey="lblModuleThemesHelp"></asp:Label>
                                    </div>
                                </div>
                            </td>
                            <td valign="bottom">
                                <asp:DropDownList ID="ddlModuleThemes" runat="server" AutoPostBack="true" CausesValidation="false"
                                    OnSelectedIndexChanged="ddlThemes_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="phEmptyThemesFolder" runat="server">
                        <tr>
                            <td>
                                <asp:Label ID="lblEmptyThemesFolder" runat="server" resourcekey="lblEmptyThemesFolder"></asp:Label>
                            </td>
                        </tr>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="phXmlSettings" runat="server"></asp:PlaceHolder>
                    <tr>
                        <td colspan="2">
                            <div class="wsc_controls_group">
                                <asp:LinkButton CssClass="wsc_admin_button wsc_small wsc_green" ID="cmdApply" runat="server"
                                    OnClick="cmdApply_Click"><span><%= GetLocalized("cmdApply")%></span></asp:LinkButton>
                                <asp:LinkButton CssClass="wsc_admin_button wsc_small" ID="cmdPreview" runat="server"
                                    OnClick="cmdPreview_Click" CausesValidation="false"><span><%= GetLocalized("cmdPreview")%></span></asp:LinkButton>                            
                                <asp:LinkButton CssClass="wsc_admin_button wsc_small wsc_red" ID="cmdCancel" runat="server"
                                    OnClick="cmdCancel_Click" CausesValidation="false"><span><%= GetLocalized("cmdCancel")%></span></asp:LinkButton>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="wsc_preview_wrapper">
    <asp:PlaceHolder ID="phBanner" runat="server"></asp:PlaceHolder>
    <div class="clear_float">
    </div>
</div>
