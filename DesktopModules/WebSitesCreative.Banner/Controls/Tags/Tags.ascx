﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Tags.ascx.cs" Inherits="WebSitesCreative.Modules.Banner.Controls.Tags" %>
<div class="wsc_controls">
    <ul class="wsc_tags">
        <asp:Repeater runat="server" ID="rptTags">
            <ItemTemplate>
                <li>
                    <a><%# Eval("Name")%></a>
                    <span>(<%# Eval("Count") %>)</span>
                </li>
            </ItemTemplate>
        </asp:Repeater>
    </ul>
</div>

