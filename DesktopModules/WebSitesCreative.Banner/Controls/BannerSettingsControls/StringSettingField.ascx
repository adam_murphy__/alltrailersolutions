﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StringSettingField.ascx.cs"
    Inherits="WebSitesCreative.Modules.Banner.Controls.BannerSettingsControls.StringSettingField" %>
<tr>
    <td class="SubHead" width="150">
        <div class="hideWrap">
            <a class="hideBtn" href="javascript://" onclick="hideShow(this);return false;">
                <asp:Label ID="lblText" runat="server" CssClass="wsc_setting_field"></asp:Label>
            </a>
            <div class="hideCont">
                <asp:Label ID="lblHelp" runat="server" CssClass="wsc_help_message"></asp:Label>
            </div>
        </div>
    </td>
    <td valign="bottom">
        <asp:TextBox ID="tb" runat="server"></asp:TextBox>
    </td>
</tr>
