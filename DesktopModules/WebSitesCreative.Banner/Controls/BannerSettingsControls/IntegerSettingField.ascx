﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IntegerSettingField.ascx.cs"
    Inherits="WebSitesCreative.Modules.Banner.Controls.BannerSettingsControls.IntegerSettingField" %>
<%@ Import Namespace="WebSitesCreative.Modules.Banner.Components.Helpers" %>
<tr>
    <td class="SubHead" width="150">
        <div class="hideWrap">
            <a class="hideBtn" href="javascript://" onclick="hideShow(this);return false;">
                <asp:Label ID="lblText" runat="server" CssClass="wsc_setting_field"></asp:Label>
            </a>
            <div class="hideCont">
                <asp:Label ID="lblHelp" runat="server" CssClass="wsc_help_message"></asp:Label>
            </div>
        </div>
    </td>
    <td valign="bottom">
        <asp:TextBox ID="tb" runat="server"></asp:TextBox>
        <asp:RegularExpressionValidator ID="RegularExpressionValidatorIntControl" runat="server"
            ErrorMessage="this is not number" ValidationExpression="^\d{0,}$" ControlToValidate="tb"></asp:RegularExpressionValidator>
    </td>
</tr>
