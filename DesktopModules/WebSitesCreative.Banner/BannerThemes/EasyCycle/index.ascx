﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="../../Controls/BannerThemeControl/BannerThemeControl.ascx.cs"
    Inherits="WebSitesCreative.Modules.Banner.Controls.BannerThemeControl"  %>
<%@ Import Namespace="WebSitesCreative.Modules.Banner.Components.Helpers" %>
<%@ Register TagPrefix="cs" Namespace="WebSitesCreative.Modules.Banner.Controls"
    Assembly="WebSitesCreative.Modules.Banner" %>
<script runat="server" language="C#">
    protected void Page_Load(object sender, EventArgs e)
    {
        RegisterJQueryWSC();
        RegisterJS("scripts/jquery.cycle.all.js");
    }
</script>
<cs:InlineScript runat="server">
    <script type="text/javascript" language="javascript">
        jQueryWSC171(window).load(function () {
            jQueryWSC171('.slider<%=ModuleId%>').cycle({ fit: true, width:'<%= BannerSettings.Width %>px', height:"<%= BannerSettings.Height %>px", fx: '<%=GetSetting("effect") %>', speed: <%=GetSetting("cyclespeed") %> , timeout: <%=GetSetting("cycletimeout") %> , pause: <%= GetSetting("cyclepause").ToLower() %> });
        });
    </script>
</cs:InlineScript>
<div id="wrapper">
    <div class="slider-wrapper">        
        <div class="slider<%=ModuleId%>">
            <asp:Repeater ID="rptrImages" runat="server">
                <ItemTemplate>
                    <div class="slider-item">
                        <asp:PlaceHolder ID="pnlImageSlide" runat="server" Visible='<%# (bool)Eval("IsImageSlide") %>'>
                            <a href="<%# Eval("Url") %>">
                                <img src='<%# FilesHelper.GetFileUrl(ModuleId, Eval("ResizedImageName").ToString()) %>'
                                    width='<%=Width %>' title='<%# Eval("Title")%>' /></a> 
                        </asp:PlaceHolder>

                        <asp:PlaceHolder ID="pnlTextSlide" runat="server" Visible='<%# !((bool)Eval("IsImageSlide")) %>'>
                            <%# Eval("Text") %>
                        </asp:PlaceHolder>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</div>