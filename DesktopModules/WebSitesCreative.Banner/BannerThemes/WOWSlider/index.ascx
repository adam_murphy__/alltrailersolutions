﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="../../Controls/BannerThemeControl/BannerThemeControl.ascx.cs"
    Inherits="WebSitesCreative.Modules.Banner.Controls.BannerThemeControl" %>
<%@ Import Namespace="DotNetNuke.Common.Utilities" %>
<%@ Import Namespace="WebSitesCreative.Modules.Banner.Components.Helpers" %>
<%@ Import Namespace="WebSitesCreative.Modules.Banner.Components" %>
<%@ Register TagPrefix="cs" Namespace="WebSitesCreative.Modules.Banner.Controls"
    Assembly="WebSitesCreative.Modules.Banner" %>
<script runat="server" language="C#">
    protected void Page_Load(object sender, EventArgs e)
    {
        RegisterJS("scripts/" + GetSetting("wowslidereffects") + "/script.js");
        RegisterCss("styles/" + GetSetting("wowsliderstyles") + "/style.css");

        if (!IsPostBack)
        {
            BannerController controller = new BannerController();
            rptrBullets.DataSource = controller.GetSlides(ModuleId);
            rptrBullets.DataBind();
        }
    }
</script>
<cs:InlineScript runat="server">
    <script type="text/javascript" language="javascript">
    jQuery(document).ready(function(){
        jQuery('.wowslider-container.module<%=ModuleId%>').wowSlider({
                   effect: '<%=GetSetting("wowslidereffects") %>',
                   prev: '<%=GetSetting("wowsliderstyles") == "Block" ? "prev" : "" %>',
                   next: '<%=GetSetting("wowsliderstyles") == "Block" ? "next" : "" %>',
                   duration: <%=GetSetting("wowsliderduration") %> ,
                   delay: <%=GetSetting("wowsliderdelay") %> ,
                   outWidth: <%=Width %>,
                   outHeight: <%=Height %>,
                   width: <%=Width %>,
                   height: <%=Height %>,
                   autoPlay: <%=GetSetting("wowsliderautoPlay").ToLower() %>,
                   //Avoid stop on hover with Kenburns effect
                   stopOnHover: <%= (GetSetting("wowslidereffects") == "kenburns") ? "false" : GetSetting("wowsliderstopOnHover").ToLower() %>,
                   loop: false,
                   bullets: <%=GetSetting("wowsliderbullets").ToLower() %>,
                   caption: <%=GetSetting("wowslidercaption").ToLower() %>,
                   controls: <%=GetSetting("wowslidercontrols").ToLower() %>
        });
    });
    </script>
</cs:InlineScript>
<cs:InlineStyle runat="server">
    <style type="text/css">
        /*----all_style----*/
        .wowslider-container
        {
            margin: 0 auto;
            margin-top: <%=GetSetting("wowsliderhspace") %>px !important;
            margin-bottom: <%=GetSetting("wowsliderhspace") %>px !important;
        }
        
        .wowslider-container.crystal .ws_frame 
        {
            top: 0px;
            left: 0px;
            width: <%=Width - 16%>px;
            height: <%=Height - 16%>px;           
        }
        
        .ws_bulframe img
        {
            width: <%=Width /4%>px;
            height: <%=Height /4%>px;
        }
        .ws_images
        {
            width: <%=Width%>px;
            height: <%=Height%>px;
        }
        .wowslider-container
        {
            width: <%=Width %>px !important;
            height: <%=Height%>px !important;
        }
        .ws_bulframe div div
        {
            height: <%=Height /4%>px;
        }
        .ws_bulframe div
        {
            width: <%=Width /4%>px;
        }
        .ws_bulframe span
        {
            left: <%=Width /8%>px;
        }
        
       
    </style>
</cs:InlineStyle>


<div class='wowslider-container module<%=ModuleId%> <%=GetSetting("wowsliderstyles").ToLower()%>'>
    <div class="ws_images">
        <asp:Repeater ID="rptrImages" runat="server">
            <ItemTemplate>
                <asp:PlaceHolder runat="server" ID="pnlImageSlide" Visible='<%# (bool)Eval("IsImageSlide") %>'>
                    <a href="<%# Eval("Url") %>">
                        <img src='<%# FilesHelper.GetFileUrl(ModuleId, Eval("ResizedImageName").ToString()) %>'
                            alt='<%# HtmlUtils.StripTags(Eval("Title") as string, true)%>' title='<%# HtmlUtils.StripTags(Eval("Title") as String, true)%>' id="wows<%# Container.ItemIndex %>"
                             /></a>
                </asp:PlaceHolder>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    <%if (GetSetting("wowsliderbullets") == "True")
      {%>
    <div class="ws_bullets">
        <div>
            <asp:Repeater ID="rptrBullets" runat="server">
                <ItemTemplate>
                    <asp:PlaceHolder runat="server" ID="pnlImageSlide" Visible='<%# (bool)Eval("IsImageSlide") %>'>
                    <a href="#wows<%# Container.ItemIndex %>" title='<%# HtmlUtils.StripTags(Eval("Title") as string, true)%>'>
                        <img src='<%#FilesHelper.GetFileUrl(ModuleId, Eval("ResizedImageName").ToString())%>'
                            alt='<%# HtmlUtils.StripTags(Eval("Title") as string, true) %>' /></a>
                            </asp:PlaceHolder>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
    <%} %>
    <a class="ws_frame" href="#"></a>
    <div class="ws_shadow">
    </div>
</div>

<%--Hiding Kenburns image border jumping effect by overlaying it with css border --%>
<%if (GetSetting("wowslidereffects") == "kenburns")
  {%>
<style type="text/css">
    .wowslider-container.module<%=ModuleId%> .ws_frame
    {
        border: 1px solid black;
        display: block;
        left: 0px;
        opacity: 0.5;
        position: absolute;
        top: 0px;
        z-index: 100;
        width: <%=Width - 2%>px;
        height: <%=Height - 2%>px;
    }
</style>
<%} %>