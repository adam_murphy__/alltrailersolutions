﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditBanner.ascx.cs"
  Inherits="WebSitesCreative.Modules.Banner.EditBanner" %>
<%@ Import Namespace="WebSitesCreative.Modules.Banner.Components.Helpers" %>
<%@ Register TagPrefix="wsc" TagName="ImageChooser" Src="Controls/ImageChooser.ascx" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.UI.WebControls" Assembly="DotNetNuke" %>
<div class="wsc_admin_content">
  <div class="wsc_admin_box">
    <div class="wsc_admin_box_head">
      <div class="wsc_admin_title">
        Slides Management
      </div>
    </div>
    <div class="wsc_admin_box_body">
      <div class="wsc_admin_fields">
        <asp:MultiView ID="BannersMultiView" runat="server" ActiveViewIndex="0">
          <asp:View ID="ViewBanners" runat="server">
            <div class="wsc_controls_group">
              <asp:LinkButton CssClass="wsc_admin_button wsc_medium" ID="ButtonAdd" runat="server"
                OnClick="is_ButtonAdd_Click"><span class="wsc_plus_icon"><%= GetLocalized("is_ButtonAdd")%></span></asp:LinkButton>
              <asp:LinkButton CssClass="wsc_admin_button wsc_medium" ID="ButtonAddText" runat="server"
                OnClick="ts_ButtonAdd_Click"><span class="wsc_plus_icon"><%= GetLocalized("ts_ButtonAdd")%></span></asp:LinkButton>
            </div>
            <div class="wsc_slide_container">
                <asp:Repeater ID="rptImages" runat="server" OnItemDataBound="rptImages_ItemDataBound">
                  <ItemTemplate>
                    <div class="wsc_slide_box">
                      <asp:HiddenField runat="server" Value='<%# Eval("ItemId") %>' />
                      <div class="wsc_slide_head">
                        <div class="wsc_slide_title">
                          <asp:Label ID="BannerTitle" runat="server" resourcekey="BannerTitle">
                          </asp:Label>&nbsp;ss
                          <%# Eval("Title") %>
                        </div>
                        <div class="wsc_controls_box">
                          <a title="Drag-n-Drop" class="wsc_drag_icon"><%= GetLocalized("ButtonDrag") %></a>
                          <asp:LinkButton CssClass="wsc_delete_icon" ID="ButtonDelete" runat="server" CommandArgument='<%# Eval("ItemId") %>'
                            OnCommand="buttonDelete_Command" resourcekey="ButtonDelete"></asp:LinkButton>
                          <asp:LinkButton CssClass="wsc_edit_icon" ID="ButtonEdit" runat="server" CommandArgument='<%# Eval("ItemId") %>'
                            OnCommand="buttonEdit_Command" resourcekey="ButtonEdit"></asp:LinkButton>
                        </div>
                      </div>
                      <div class="wsc_slide_body" runat="server" visible='<%# Eval("IsImageSlide") %>'>
                        <div class="wsc_controls" runat="server" visible='<%# (bool)Eval("HasDescription")%>'>
                          <span>
                            <%# Eval("Description") %></span>
                        </div>
                        <div class="wsc_controls">
                          <asp:PlaceHolder runat="server" Visible='<%# ((bool)Eval("IsImageSlide")) %>'>
                            <div class="wsc_image_wrapper">
                              <asp:Image ID="BannerImage" Style="height: 150px" AlternateText='<%# Eval("Title") %>'
                                runat="server" ImageUrl='<%# FilesHelper.GetFileUrl(ModuleId, Eval("ImageName").ToString()) %>' />
                            </div>
                          </asp:PlaceHolder>
                          <asp:PlaceHolder runat="server" Visible='<%# !((bool)Eval("IsImageSlide")) %>'>
                            <div class="wsc_image_wrapper">
                              <img src='<%= ResolveUrl(WebSitesCreative.Modules.Banner.Components.Constants.ModuleFolderVirtualPath + "UI/images/text_slide.png") %>' />
                            </div>
                            <div class="text_slide_info" runat="server" visible='<%# !BannerSettings.SupportsTextSlides%>'>
                              <h4>
                                <%= GetLocalized("NotSupportedSlideType") %></h4>
                            </div>
                          </asp:PlaceHolder>
                          <div class="clear_float">
                          </div>
                        </div>
                        <div class="wsc_controls" runat="server" visible='<%# ((bool)Eval("IsImageSlide")) %>'>                        
                          <asp:Label ID="BannerUrl" runat="server" resourcekey="BannerUrl">
                          </asp:Label>
                          <a href="<%# Eval("Url") %>">
                            <%# Eval("Url") %></a>
                        </div>
                        <div class="wsc_controls" runat="server" visible='<%# Eval("HasTags") %>'>
                          <asp:Label runat="server" resourcekey="LabelTagsAdd"></asp:Label>
                          <ul class="wsc_slide_tags">
                            <asp:Repeater runat="server" DataSource='<%# Eval("SplittedTags") %>'>
                              <ItemTemplate>
                                <li>
                                  <%# Container.DataItem %></li>
                              </ItemTemplate>
                            </asp:Repeater>
                          </ul>
                        </div>
                      </div>
                      <div class="wsc_slide_body" runat="server" visible='<%# !(bool)Eval("IsImageSlide")%>'>
                        <span>
                          <%# Eval("Text")%></span>
                        <div class="wsc_controls" runat="server" visible='<%# Eval("HasTags") %>'>
                          <asp:Label runat="server" resourcekey="LabelTagsAdd"></asp:Label>
                          <ul class="wsc_slide_tags">
                            <asp:Repeater runat="server" DataSource='<%# Eval("SplittedTags") %>'>
                              <ItemTemplate>
                                <li>
                                  <%# Container.DataItem %></li>
                              </ItemTemplate>
                            </asp:Repeater>
                          </ul>
                        </div>
                        <h4 runat="server" visible="<%#!BannerSettings.SupportsTextSlides%>" style="color: red">
                          Current theme doesn't support this type of slides</h4>
                      </div>
                    </div>
                  </ItemTemplate>
                </asp:Repeater>
            </div>
            <script type="text/javascript">
                jQueryWSC171(document).ready(
                    function () {
                        $('#savingSpinner').hide();
                        $('.wsc_slide_container').sortable();
                    }
                );                
    
                function saveSlidesOrder(redirectToOnSuccess) {
                    var elements = $('.wsc_slide_box > input[type=hidden]');
                    var ids = $.map(elements, function (item) { return item.value; });                    
                    var baseUrl = $.ServicesFramework(<%=ModuleContext.ModuleId %>).getServiceRoot('WebSitesCreative.Banner');

                    $('#savingSpinner').show();
                    $.post(baseUrl + 'BannerAPI/SaveSlidesOrder', {moduleId: <%=ModuleContext.ModuleId %>, ids: ids})
                        .fail(function (data) {
                            if (data.status === 400) {
                                alert('ERROR: ' + JSON.parse(data.responseText).error);
                            } else {
                                alert(data.responseText);
                            }
                            $('#savingSpinner').hide();
                        })
                        .done(function( data ) {
                            $('#savingSpinner').hide();
                            window.location = redirectToOnSuccess;
                        });
                }
            </script>
            <div class="wsc_controls_group">
                <a class="wsc_admin_button wsc_small wsc_green" onclick="saveSlidesOrder('<%=DotNetNuke.Common.Globals.NavigateURL()%>')">
                    <span><%= GetLocalized("ButtonSaveSlidesOrder")%></span>
                </a>
                <span id="savingSpinner" class="spinner wsc_small" ><img src="/images/icon_wait.gif"/></span>
                <asp:LinkButton CssClass="wsc_admin_button wsc_small wsc_red" ID="ButtonReset" runat="server"
                    OnClick="ButtonReset_Click" CausesValidation="false"><span><%= GetLocalized("ButtonReset")%></span></asp:LinkButton>
            </div>
          </asp:View>
          <asp:View ID="is_View" runat="server">
            <asp:HiddenField ID="is_hfEditedItemId" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="is_hfItemOrder" runat="server"></asp:HiddenField>
            <script type="text/javascript" language="javascript">
              jQueryWSC171(document).ready(
                function () {
                  jQueryWSC171('#<%=is_TextBoxDescription.ClientID %>').redactor({ css: ['blank.css'], lang: 'en' });

                  jQueryWSC171("#is_Tags").tagit({
                    tagSource: <%=AllTags %>,
                    initialTags : <%=InitialTags %>,
                    triggerKeys: ['enter', 'comma', 'tab'],
                    select: true
                  });
                });
            </script>
            <div class="wsc_info_box">
              <div class="wsc_info_head">
                <asp:Label ID="is_HeadText" CssClass="wsc_info_title" runat="server" resourcekey="is_HeadText"></asp:Label>
              </div>
              <div class="wsc_info_body">
                <div>
                  <div class="wsc_controls" runat="server" id="is_TemplateContainer">
                    <div class="wsc_label">
                      <asp:Label ID="is_LabelTemplate" CssClass="SubHead" runat="server" resourcekey="is_LabelTemplate" />
                    </div>
                    <div class="wsc_controls">
                      <asp:DropDownList ID="is_DropDownListTemplate" runat="server" AutoPostBack="true"
                        CausesValidation="false" OnSelectedIndexChanged="is_SelectedTemplateChanged">
                      </asp:DropDownList>
                    </div>
                  </div>
                  <div class="wsc_label">
                    <asp:Label CssClass="SubHead" ID="is_LabelImage" runat="server" resourcekey="is_LabelImage"></asp:Label>
                  </div>
                  <div class="wsc_controls wsc_image_chooser">
                    <wsc:imagechooser runat="server" id="is_ImageChooser" isrequired="true">
                    </wsc:imagechooser>
                  </div>
                  <div class="wsc_label">
                    <asp:Label ID="is_LabelTitle" CssClass="SubHead" runat="server" resourcekey="is_LabelTitle"></asp:Label>
                  </div>
                  <div class="wsc_controls">
                    <asp:TextBox ID="is_TextBoxTitle" runat="server" CssClass="wsc_text_field"></asp:TextBox>
                  </div>
                  <div class="wsc_label">
                    <asp:Label ID="is_LabelDescription" CssClass="SubHead" runat="server" resourcekey="is_LabelDescription"></asp:Label>
                  </div>
                  <div class="wsc_controls">
                    <asp:TextBox CssClass="wsc_text_field" ID="is_TextBoxDescription" TextMode="MultiLine"
                      Width="600px" Height="100px" runat="server"></asp:TextBox>
                  </div>
                  <div class="wsc_label">
                    <asp:Label CssClass="SubHead" ID="is_LabelUrl" runat="server" resourcekey="is_LabelUrl"></asp:Label>
                  </div>
                  <div class="wsc_controls">
                    <asp:TextBox ID="is_TextBoxUrl" runat="server" CssClass="wsc_text_field"></asp:TextBox>
                  </div>
                  <div class="wsc_label">
                    <asp:Label ID="is_LabelTags" CssClass="SubHead" runat="server" resourcekey="is_LabelTags"></asp:Label>
                  </div>
                  <div class="wsc_controls">
                    <ul id="is_Tags" name="is_Tags">
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="wsc_controls_group">
              <asp:LinkButton CssClass="wsc_admin_button wsc_small" ID="is_ButtonSave" runat="server"
                CausesValidation="true" OnClick="is_ButtonSave_Click"><span><%= GetLocalized("ButtonSave")%></span></asp:LinkButton>
              <asp:LinkButton CssClass="wsc_admin_button wsc_small wsc_red" ID="is_ButtonCancel"
                CausesValidation="false" runat="server" OnClick="is_ButtonCancel_Click"><span><%= GetLocalized("ButtonCancel")%></span></asp:LinkButton>
            </div>
          </asp:View>
          <asp:View ID="ts_View" runat="server">
            <asp:HiddenField ID="ts_hfEditedItemId" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="ts_hfItemOrder" runat="server"></asp:HiddenField>
            <script type="text/javascript" language="javascript">
              jQueryWSC171(document).ready(
                function () {
                  jQueryWSC171("#ts_Tags").tagit({
                    tagSource: <%=AllTags %>,
                    initialTags : <%=InitialTags %>,
                    triggerKeys: ['enter', 'comma', 'tab'],
                    select: true
                  });
                });
            </script>
            <div class="wsc_info_box">
              <div class="wsc_info_head">
                <asp:Label ID="ts_HeadText" CssClass="wsc_info_title" runat="server" resourcekey="ts_HeadText"></asp:Label>
              </div>
              <div class="wsc_info_body">
                <div class="wsc_controls" runat="server" id="ts_TemplateContainer">
                  <div class="wsc_label">
                    <asp:Label ID="ts_LabelTemplate" CssClass="SubHead" runat="server" resourcekey="ts_LabelTemplate"></asp:Label>
                  </div>
                  <div class="wsc_controls">
                    <asp:DropDownList ID="ts_DropDownListTemplate" runat="server" AutoPostBack="true"
                      CausesValidation="false" OnSelectedIndexChanged="ts_SelectedTemplateChanged">
                    </asp:DropDownList>
                  </div>
                </div>
                <div class="wsc_label">
                  <asp:Label ID="ts_LabelTitle" CssClass="SubHead" runat="server" resourcekey="ts_LabelTitle"></asp:Label>
                </div>
                <div class="wsc_controls">
                  <asp:TextBox ID="ts_TextBoxTitle" CssClass="wsc_text_field" runat="server"></asp:TextBox>
                </div>
                <div class="wsc_label">
                  <asp:Label ID="ts_LabelText" CssClass="SubHead" runat="server" resourcekey="ts_LabelText"></asp:Label>
                </div>
                <div class="wsc_controls">
                  <dnn:DNNRichTextEditControl runat="server" ID="ts_TextBoxText" Width="600px" Height="400px"
                    TextRenderMode="Raw" HtmlEncode="False" defaultmode="Rich" choosemode="True" chooserender="False" />
                  <div class="clear_float"></div>
                </div>
                <div class="wsc_label">
                  <asp:Label ID="ts_LabelTags" CssClass="SubHead" runat="server" resourcekey="ts_LabelTags"></asp:Label>
                </div>
                <div class="wsc_controls">
                  <ul id="ts_Tags" name="ts_Tags">
                  </ul>
                </div>
              </div>
            </div>
            <div class="wsc_controls_group">
              <asp:LinkButton CssClass="wsc_admin_button wsc_small" ID="ts_ButtonSave" runat="server"
                OnClick="ts_ButtonSave_Click" CausesValidation="true"><span><%= GetLocalized("ButtonSave")%></span></asp:LinkButton>
              <asp:LinkButton CssClass="wsc_admin_button wsc_small wsc_red" ID="ts_ButtonCancel"
                runat="server" OnClick="ts_ButtonCancel_Click" CausesValidation="false"><span><%= GetLocalized("ButtonCancel")%></span></asp:LinkButton>
            </div>
          </asp:View>
        </asp:MultiView>
      </div>
    </div>
  </div>
  <asp:LinkButton CssClass="CommandButton wsc_admin_button wsc_gray" ID="cmdGoToView"
    runat="server" CausesValidation="False" OnClick="cmdGoToView_Click"><span class="wsc_door_icon"><%= GetLocalized("cmdGoToView")%></span></asp:LinkButton>
</div>
