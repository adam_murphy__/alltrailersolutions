﻿var Revindex = Revindex || {};
Revindex.Dnn = Revindex.Dnn || {};
Revindex.Dnn.RevindexStorefront = Revindex.Dnn.RevindexStorefront || {};

jQuery(document).ready(function($)
{
	// BUG: Allow AJAX to work correctly with Google Chrome and Apple Safari
	// http://forums.asp.net/t/1252014.aspx/3/10
	Sys.Browser.WebKit = {};
	if (navigator.userAgent.indexOf('WebKit/') > -1)
	{
		Sys.Browser.agent = Sys.Browser.WebKit;
		Sys.Browser.version = parseFloat(navigator.userAgent.match(/WebKit\/(\d+(\.\d+)?)/)[1]);
		Sys.Browser.name = 'WebKit';
	}

	Revindex.Web.FormTracker.ConfirmUnsavedChanges(".rvdsfContentContainer div.dnnForm:not(.rvdsfSearchContainer, .rvdsfListContainer)", "Your changes have not been saved.", ".rvdSaveAction");

    // Automatically scroll to content view if current page has 
    // acted on the Storefront module and there is no fragment targetted.
	if (window.location.href.toLowerCase().indexOf("rvdsfpt") >= 0 && window.location.href.indexOf("#") < 0)
	    document.getElementById("RvdsfContentAnchor").scrollIntoView();
});