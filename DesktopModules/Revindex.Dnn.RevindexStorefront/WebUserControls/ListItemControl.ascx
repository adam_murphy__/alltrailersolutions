﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ListItemControl.ascx.cs" Inherits="Revindex.Dnn.RevindexStorefront.WebUserControls.ListItemControl" %>
<%@ Register Src="~/Controls/labelcontrol.ascx" TagName="LabelControl" TagPrefix="dnn1" %>
<%@ Register Assembly="DotNetNuke.Web.Deprecated" Namespace="DotNetNuke.Web.UI.WebControls" TagPrefix="dnn2" %>
<div class="rvdFormListBox">
	<dnn2:DnnListBox ID="SelectionDnnListBox" runat="server" AutoPostBack="True" CausesValidation="False" OnSelectedIndexChanged="SelectionDnnListBox_SelectedIndexChanged" OnDeleted="SelectionDnnListBox_Deleted" AutoPostBackOnDelete="True" CheckBoxes="True" />
	<div class="rvdActions">
		<asp:LinkButton ID="AddSelectionDnnRadButton" runat="server" CausesValidation="False" Text="Add item" OnClick="AddSelectionLinkButton_Click" CssClass="dnnPrimaryAction rvdAddNewAction" />
	</div>
	<asp:Panel ID="EditSelectionPanel" runat="server">
		<div class="dnnFormItem">
			<dnn1:LabelControl ID="NameLabelControl" runat="server" Text="Name:" />
			<asp:TextBox ID="SelectionNameTextBox" runat="server"></asp:TextBox>
		</div>
		<asp:Panel runat="server" ID="SelectionValuePanel" CssClass="dnnFormItem">
			<dnn1:LabelControl ID="SelectionValueLabelControl" runat="server" Text="Value:" />
			<asp:TextBox ID="SelectionValueTextBox" runat="server"></asp:TextBox>
		</asp:Panel>
		<div class="rvdActions">
			<asp:LinkButton ID="SaveSelectionLinkButton" runat="server" Text="OK" OnClick="SaveSelectionLinkButton_Click" CssClass="dnnPrimaryAction rvdOKAction" />
		</div>
	</asp:Panel>
</div>
<script type="text/javascript">
    // BUG: RadListBox bug error in Chrome when scroll position is not a round number
    // http://www.telerik.com/forums/system-formatexception-78e82e51af27
    $(document).ready(function () {
        if (Telerik != undefined) {
            Telerik.Web.UI.RadListBox.prototype.saveClientState = function () {
                return "{" +
                            "\"isEnabled\":" + this._enabled +
                            ",\"logEntries\":" + this._logEntriesJson +
                           ",\"selectedIndices\":" + this._selectedIndicesJson +
                           ",\"checkedIndices\":" + this._checkedIndicesJson +
                           ",\"scrollPosition\":" + Math.round(this._scrollPosition) +
                       "}";
            }
        }
    });
</script>
