﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ViewSwitcher.ascx.cs"
    Inherits="WebSitesCreative.Modules.StyleSwitcher.ViewSwitcher" %>

<%@ Register TagPrefix="wsc" TagName="ThemeManagement" Src="~/DesktopModules/WebSitesCreative.StyleSwitcher/ThemeManagement.ascx" %>

<asp:UpdatePanel ID="upSwitcherButton" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div class="wsc_switcher_control">
            <!-- Button trigger modal -->
            <asp:LinkButton ID="SwitcherBtn" runat="server" CssClass="btn btn-primary btn-lg btn-wsc-switcher-modal-toggle" OnClick="SwitcherBtn_Click">
                <span class="glyphicon glyphicon-cog"></span>
            </asp:LinkButton>
            <asp:HiddenField ID="hfLoaded" runat="server" Value="false" />
        </div>      
    </ContentTemplate>
</asp:UpdatePanel>

<div class="modal fade wsc-style-switcher" id="SwitherModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <asp:UpdatePanel ID="upThemeManagement" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <wsc:ThemeManagement runat="server" ID="themeManagement" TriggerControlId="SwitcherBtn"></wsc:ThemeManagement>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>

<script type="text/javascript">
    (function () {
        var settings = {
            backgroundImage: '<%=GetSetting("BackgroundImage")%>',
            screenGradientType: '<%=GetSetting("ScreenGradientType")%>'
        }

        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(function (prm, args) {
            if (args._postBackElement !== $('.btn-wsc-switcher-modal-toggle')[0]) return;
            WSC_SWITCHER.show();
        });
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
            WSC_SWITCHER.init(settings)
        });
    })()
</script>