﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DropDownListSettingField.ascx.cs"
    Inherits="WebSitesCreative.Modules.StyleSwitcher.Controls.SwitcherSettingsControls.DropDownListSettingField" %>
<%@ Import Namespace="WebSitesCreative.Modules.StyleSwitcher.Components.Helpers" %>
<script type="text/javascript">
    (function ($) {

        /* Load after DOM is ready
        ---------------------------------------------------------------------- */
        $(function () {
            $("#<%=ddl.ClientID %>").change(function () {
                var src = '<%=ThemesHelper.GetThemeFileUrl("")%>' + $(this).val();
                $('#<%=ddl.ClientID + "img"%>').attr("src", src);
            });
            
        });
    })(jQuery)
</script>
<div class="panel panel-default">
    <div class="panel-heading">
        <asp:Label ID="lblText" runat="server"></asp:Label>
        <asp:Label ID="lblHelp" runat="server" Visible="false"></asp:Label>
    </div>
    <div class="panel-body">
        <div class="form-group">
            <asp:DropDownList ID="ddl" runat="server" CssClass="form-control">
            </asp:DropDownList>
        </div>
        <div class="form-group">
            <img id='<%=ddl.ClientID + "img" %>' src="<%= ThemesHelper.GetThemeFileUrl(ddl.SelectedValue) %>" />
        </div>
    </div>
</div>
