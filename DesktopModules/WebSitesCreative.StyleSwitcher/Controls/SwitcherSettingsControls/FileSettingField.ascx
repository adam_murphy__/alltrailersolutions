﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FileSettingField.ascx.cs"
    Inherits="WebSitesCreative.Modules.StyleSwitcher.Controls.SwitcherSettingsControls.FileSettingField" %>
<div class="panel panel-default">
    <div class="panel-heading">
        <asp:Label ID="lblText" runat="server"></asp:Label>
        <asp:Label ID="lblHelp" runat="server" Visible="false"></asp:Label>
    </div>
    <div class="panel-body">
        <div class="form-group">
            <label for="exampleInputFile">
                File input</label>
                <asp:FileUpload ID="uplFileUpload" runat="server"/>
        </div>
    </div>
</div>
