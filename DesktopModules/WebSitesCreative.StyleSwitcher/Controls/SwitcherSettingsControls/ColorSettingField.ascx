﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ColorSettingField.ascx.cs"
    Inherits="WebSitesCreative.Modules.StyleSwitcher.Controls.SwitcherSettingsControls.ColorSettingField" %>
<div class="panel panel-default">
    <div class="panel-heading">
        <asp:Label ID="lblText" runat="server"></asp:Label>
        <asp:Label ID="lblHelp" runat="server" Visible="false"></asp:Label>
    </div>
    <div class="panel-body">
        <asp:TextBox ID="tb" CssClass="pick-a-color form-control" runat="server"></asp:TextBox>
    </div>
</div>
