﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IntegerSettingField.ascx.cs"
    Inherits="WebSitesCreative.Modules.StyleSwitcher.Controls.SwitcherSettingsControls.IntegerSettingField" %>
<%@ Import Namespace="WebSitesCreative.Modules.StyleSwitcher.Components.Helpers" %>
<div class="panel panel-default">
    <div class="panel-heading">
        <asp:Label ID="lblText" runat="server" CssClass="wsc_setting_field"></asp:Label>
        <asp:Label ID="lblHelp" runat="server" CssClass="wsc_help_message"></asp:Label>
    </div>
    <div class="panel-body">
        <asp:TextBox ID="tb" runat="server"></asp:TextBox>
        <asp:RegularExpressionValidator ID="RegularExpressionValidatorIntControl" runat="server" ErrorMessage="this is not number" ValidationExpression="^\d{0,}$" ControlToValidate="tb"></asp:RegularExpressionValidator>
    </div>
</div>
