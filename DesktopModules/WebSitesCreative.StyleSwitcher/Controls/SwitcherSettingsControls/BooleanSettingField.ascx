﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BooleanSettingField.ascx.cs"
    Inherits="WebSitesCreative.Modules.StyleSwitcher.Controls.SwitcherSettingsControls.BooleanSettingField" %>
<tr>
    <td class="SubHead" width="150">
        <div class="hideWrap">
            <a class="hideBtn" href="javascript://" onclick="hideShow(this);return false;">
                <asp:Label ID="lblText" runat="server" CssClass="wsc_setting_field"></asp:Label></a>
            <div class="hideCont">
                <asp:Label ID="lblHelp" runat="server" CssClass="wsc_help_message"></asp:Label>
            </div>
        </div>
    </td>
    <td valign="bottom">
        <asp:CheckBox ID="chb" runat="server" />
    </td>
</tr>
