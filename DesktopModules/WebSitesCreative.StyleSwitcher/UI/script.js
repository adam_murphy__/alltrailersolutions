﻿var WSC_SWITCHER = (function () {

    var wscInitSwitcher = (function () {
        var Settings = [
                /*Accent Color
                ---------------------------------------------*/
                {
                    name: 'ColorScheme',
                    value: 'custom',
                    items: ['CustomColor']
                },

                /*Screen Background
                ---------------------------------------------*/
                {
                    name: 'BackgroundImage',
                    type: 'mix',
                    value: ['none.png', 'bg1', 'bg2', 'bg4', 'bg5', 'bg6', 'custom.png', 'SolidColor', 'gradient2', 'gradient3'],
                    items: [
                        ['none'],
                        ['none'],
                        ['none'],
                        ['none'],
                        ['none'],
                        ['none'],
                        ['CustomBackground'],
                        ['SolidBackgroundColor'],
                        ['ScreenGradientType', 'SolidBackgroundColor', 'SolidBackgroundColor2'],
                        ['ScreenGradientType', 'SolidBackgroundColor', 'SolidBackgroundColor2', 'SolidBackgroundColor3']
                    ]
                },
                {
                    name: 'BackgroundSlideshowOnOff',
                    value: '1',
                    items: ['BackgroundSlideshowTransition',
                            'BackgroundSlideshowPauseLength',
                            'BackgroundSlideshowTransitionSpeed',
                            'BackgroundSlideshowAmount',
                            'CustomBackgroundSlide'
                    ]
                },
                {
                    name: 'BackgroundSlideshowAmount',
                    type: 'section',
                    items: [['CustomBackgroundSlide1'], ['CustomBackgroundSlide2'], ['CustomBackgroundSlide3'], ['CustomBackgroundSlide4'], ['CustomBackgroundSlide5']
                    ]
                },

                /*Header and Menu
                ---------------------------------------------*/
                {
                    name: 'ChooseMenus',
                    value: 'wsc-slidemenu',
                    items: ['SlideMenuType']
                },
                {
                    name: 'TobBarOnOff',
                    value: 'wsc_tb_visible',
                    items: ['TobBarOnOffDevice',
                            'TopBarDisplay'
                    ]
                },
                {
                    name: 'TopBarDisplay',
                    value: 'tb_contacts',
                    items: ['TopBarAddress',
                            'TopBarPhone',
                            'TopBarEmail'
                    ]
                },
                {
                    name: 'LogoCustomDefault',
                    value: 'sm_logo_custom',
                    items: ['CustomLogoFile']
                },

                /*Mega Menu
                ---------------------------------------------*/
                {
                    name: 'MegaMenuEnable1',
                    value: '1',
                    items: ['MMTabName1',
                            'MegaMenuColumns1',
                            'MegaMenuWidth1',
                            'MegaMenuRealWidth1',
                            'MegaMenuContentAlign1'
                    ]
                },
                {
                    name: 'MegaMenuContentAlign1',
                    type: 'mix',
                    value: ['mega_cc_no', 'mega_cc_bg', 'mega_cc_left', 'mega_cc_right'],
                    items: [
                        ['none'],
                        ['CustomMegaMenuBgPic'],
                        ['MegaMenuCContentWidth1', 'MegaMenuContent1'],
                        ['MegaMenuCContentWidth1', 'MegaMenuContent1']
                    ]
                },
                {
                    name: 'MegaMenuEnable2',
                    value: '1',
                    items: ['MMTabName2',
                            'MegaMenuColumns2',
                            'MegaMenuWidth2',
                            'MegaMenuRealWidth2',
                            'MegaMenuContentAlign2'
                    ]
                },
                {
                    name: 'MegaMenuContentAlign2',
                    type: 'mix',
                    value: ['mega_cc_no', 'mega_cc_bg', 'mega_cc_left', 'mega_cc_right'],
                    items: [
                        ['none'],
                        ['CustomMegaMenuBgPic2'],
                        ['MegaMenuCContentWidth2', 'MegaMenuContent2'],
                        ['MegaMenuCContentWidth2', 'MegaMenuContent2']
                    ]
                },
                {
                    name: 'MegaMenuEnable3',
                    value: '1',
                    items: ['MMTabName3',
                            'MegaMenuColumns3',
                            'MegaMenuWidth3',
                            'MegaMenuRealWidth3',
                            'MegaMenuContentAlign3'
                    ]
                },
                {
                    name: 'MegaMenuContentAlign3',
                    type: 'mix',
                    value: ['mega_cc_no', 'mega_cc_bg', 'mega_cc_left', 'mega_cc_right'],
                    items: [
                        ['none'],
                        ['CustomMegaMenuBgPic3'],
                        ['MegaMenuCContentWidth3', 'MegaMenuContent3'],
                        ['MegaMenuCContentWidth3', 'MegaMenuContent3']
                    ]
                },
                /* Custom content alignment */

                /*Side Menu
                ---------------------------------------------*/
                {
                    name: 'SideMenuLogo',
                    value: 'sm_logo_custom',
                    items: ['CustomLogoSideMenuFile']
                },

                /*Sections
                ---------------------------------------------*/

                {
                    name: 'AdditonalPanesSections',
                    type: 'section',
                    items: [
                        ['AddSectionBackgroundStyle1'],
                        ['AddSectionBackgroundStyle2'],
                        ['AddSectionBackgroundStyle3'],
                        ['AddSectionBackgroundStyle4'],
                        ['AddSectionBackgroundStyle5']
                    ]
                },
                {
                    name: 'AddSectionBackgroundStyle1',
                    type: 'mix',
                    value: ['transparent', 'color', 'image', 'parallax'],
                    items: [
                        ['none'],
                        ['AddSectionBackgroundColor1'],
                        ['CustomParallaxBackground1', 'AddSectionImageVPosition1'],
                        ['CustomParallaxBackground1', 'AddSectionImageVPosition1']
                    ]
                },
                {
                    name: 'AddSectionBackgroundStyle2',
                    type: 'mix',
                    value: ['transparent', 'color', 'image', 'parallax'],
                    items: [
                        ['none'],
                        ['AddSectionBackgroundColor2'],
                        ['CustomParallaxBackground2', 'AddSectionImageVPosition2'],
                        ['CustomParallaxBackground2', 'AddSectionImageVPosition2']
                    ]
                },
                {
                    name: 'AddSectionBackgroundStyle3',
                    type: 'mix',
                    value: ['transparent', 'color', 'image', 'parallax'],
                    items: [
                        ['none'],
                        ['AddSectionBackgroundColor3'],
                        ['CustomParallaxBackground3', 'AddSectionImageVPosition3'],
                        ['CustomParallaxBackground3', 'AddSectionImageVPosition3']
                    ]
                },
                {
                    name: 'AddSectionBackgroundStyle4',
                    type: 'mix',
                    value: ['transparent', 'color', 'image', 'parallax'],
                    items: [
                        ['none'],
                        ['AddSectionBackgroundColor4'],
                        ['CustomParallaxBackground4', 'AddSectionImageVPosition4'],
                        ['CustomParallaxBackground4', 'AddSectionImageVPosition4']
                    ]
                },
                {
                    name: 'AddSectionBackgroundStyle5',
                    type: 'mix',
                    value: ['transparent', 'color', 'image', 'parallax'],
                    items: [
                        ['none'],
                        ['AddSectionBackgroundColor5'],
                        ['CustomParallaxBackground5', 'AddSectionImageVPosition5'],
                        ['CustomParallaxBackground5', 'AddSectionImageVPosition5']
                    ]
                },

                /*Typography
                ---------------------------------------------*/
                {
                    name: 'GoogleFontOnOff',
                    value: 'google_font_on',
                    items: ['GoogleFont']
                },
                {
                    name: 'ContentTypoIfCustom',
                    value: 'custom',
                    items: ['ContentFontSize',
                            'ContentFontWeight'
                    ]
                },
                {
                    name: 'H1TitleIfCustom',
                    value: 'custom',
                    items: ['H1FontSize',
                            'H1FontWeight'
                    ]
                },
                {
                    name: 'H2TitleIfCustom',
                    value: 'custom',
                    items: ['H2FontSize',
                            'H2FontWeight'
                    ]
                },
                {
                    name: 'H3TitleIfCustom',
                    value: 'custom',
                    items: ['H3FontSize',
                            'H3FontWeight'
                    ]
                },
                {
                    name: 'H4TitleIfCustom',
                    value: 'custom',
                    items: ['H4FontSize',
                            'H4FontWeight'
                    ]
                },
                {
                    name: 'H5TitleIfCustom',
                    value: 'custom',
                    items: ['H5FontSize',
                            'H5FontWeight'
                    ]
                },
                /*Social icons
                ---------------------------------------------*/
                {
                    name: 'SocialIconsAmount',
                    type: 'section',
                    items: [
                        ['FooterSocialIcon1', 'FooterSocialIconLink1'],
                        ['FooterSocialIcon2', 'FooterSocialIconLink2'],
                        ['FooterSocialIcon3', 'FooterSocialIconLink3'],
                        ['FooterSocialIcon4', 'FooterSocialIconLink4'],
                        ['FooterSocialIcon5', 'FooterSocialIconLink5'],
                        ['FooterSocialIcon6', 'FooterSocialIconLink6'],
                        ['FooterSocialIcon7', 'FooterSocialIconLink7'],
                        ['FooterSocialIcon8', 'FooterSocialIconLink8'],
                        ['FooterSocialIcon9', 'FooterSocialIconLink9']
                    ]
                },

                /*Christmas Snow
                ---------------------------------------------*/
                {
                    name: 'ChristmasSnowOnOff',
                    value: 'christmas_snow_on',
                    items: ['FlakeColor',
                            'NewFlakeOn',
                            'MinFlakeSize',
                            'MaxFlakeSize'
                    ]
                },

                /*Other
                ---------------------------------------------*/
                {
                    name: 'SquareCorners',
                    value: '0',
                    items: ['CornersRadius']
                }
        ];

        var initClasses = function () {
            Settings.forEach(function (obj) {
                $(".panel.panel-default .panel-heading [id*=" + obj.name + "_" + "]").parents(".panel.panel-default").addClass('panel-anchor');
                var dependent_items = obj.items;
                if (obj.items[0] instanceof Array) { dependent_items = Array.prototype.concat.apply([], obj.items); }
                dependent_items.forEach(function (item) {
                    $(".panel.panel-default .panel-heading [id*=" + item + "_" + "]").parents(".panel.panel-default").addClass(item + " panel-subitem");
                });
            });
        };

        var initView = function () {
            Settings.forEach(function (obj) {
                var checked_input = $(".panel .panel-body input[name='" + obj.name + "']:checked");

                switch (obj.type) {
                    case 'section':
                        var input_index = $(".panel .panel-body input[name='" + obj.name + "']").index(checked_input);
                        if (input_index > -1 && input_index <= obj.items.length) {
                            if ($(".panel .panel-body input[name='" + obj.name + "']").eq(0).attr('value') != 0) { input_index++; }
                            for (let i = input_index; i < obj.items.length; i++) {
                                var section_classes = obj.items[i];
                                section_classes.forEach(function (item) {
                                    $(".panel." + item).hide();
                                    var result = Settings.filter(function (tmp) { return tmp.name == item; });
                                    if (result && result.length > 0) {
                                        let dependent_items = result[0].items;
                                        dependent_items.forEach(function (item) { $(".panel." + item).hide(); });
                                    }
                                });
                            }
                        }
                        break;
                    case 'mix':
                        var value_index = obj.value.indexOf(checked_input.attr('value'));
                        hideSubItems(obj, value_index, false);
                        break;
                    default:
                        let current_value = checked_input.attr('value');
                        if (obj.value != current_value) {
                            hideSubItems(obj, undefined, false);
                        }
                        break;
                }

            });

        };

        var showSubItems = function (element, index) {
            let dependent_items = (element.type != 'section' && element.type != 'mix') ? element.items : element.items[index];

            if (typeof dependent_items !== 'undefined' && dependent_items.length > 0) {
                dependent_items.forEach(function (item) {
                    $(".panel." + item).addClass('active-item').slideDown();

                    var sub_item = Settings.filter(function (obj) { return obj.name == item; });
                    if (sub_item && sub_item.length > 0) {
                        let checked_item = $(".panel .panel-body input[name='" + sub_item[0].name + "']:checked");
                        switch (sub_item[0].type) {
                            case 'section':
                                let checked_index = $(".panel .panel-body input[name='" + sub_item[0].name + "']").index(checked_item);
                                for (let i = 0; i < sub_item[0].items.length; i++) {
                                    if (i <= checked_index) {
                                        showSubItems(sub_item[0], i);
                                    } else {
                                        hideSubItems(sub_item[0], i);
                                    }
                                }
                                break;
                            case 'mix':
                                var value_index = sub_item[0].value.indexOf(checked_item.attr('value'));
                                if (value_index > -1) {
                                    showSubItems(sub_item[0], value_index);
                                }
                                break;
                            default:
                                if (sub_item[0].value == checked_item.attr('value')) {
                                    showSubItems(sub_item[0]);
                                } else {
                                    hideSubItems(sub_item[0]);
                                }
                                break;
                        }

                    }
                });
            }

        };

        var hideSubItems = function (element, index, animation) {
            if (typeof animation === 'undefined') { animation = true; }
            let dependent_items = (element.type != 'section') ? element.items : element.items[index];
            if (element.type == 'mix') {
                dependent_items = getMixHiddenItems(element, index);
            }

            if (typeof dependent_items !== 'undefined' && dependent_items.length > 0) {
                dependent_items.forEach(function (item) {
                    if (animation) {
                        $(".panel." + item).removeClass('active-item').slideUp();
                    } else { $(".panel." + item).hide(); }

                    var sub_item = Settings.filter(function (obj) { return obj.name == item; });
                    if (sub_item && sub_item.length > 0) {
                        switch (sub_item[0].type) {
                            case 'section':
                                let temp = new Object();
                                temp.items = Array.prototype.concat.apply([], sub_item[0].items);
                                hideSubItems(temp, undefined, animation);
                                break;
                            case 'mix':
                                let mix_temp = new Object();
                                mix_temp.items = getAllMixItems(sub_item[0]);
                                hideSubItems(mix_temp, undefined, animation);
                                break;
                            default:
                                hideSubItems(sub_item[0]);
                                break;
                        }

                    }
                });
            }

        };

        var getAllMixItems = function (element) {
            var all_items = Array.prototype.concat.apply([], element.items);
            all_items = all_items.filter(function (item, index, inputArray) { return inputArray.indexOf(item) == index; });

            let i = all_items.indexOf("none");
            if (i != -1) { all_items.splice(i, 1); }

            return all_items;
        };

        var getMixHiddenItems = function (element, index) {
            var hidden_items = [];
            var all_items = getAllMixItems(element);
            if (typeof element.items[index] !== 'undefined') {
                var active_items = element.items[index];
                if (active_items[0] == 'none') {
                    hidden_items = all_items;
                } else {
                    for (let i = 0; i < all_items.length; i++) {
                        if (active_items.indexOf(all_items[i]) == -1) {
                            hidden_items.push(all_items[i]);
                        }
                    }
                }
            }
            return hidden_items;
        };

        return function () {
            initClasses();
            initView();

            $('.panel.panel-anchor .panel-body input').on('click', function () {
                var self = this;
                var result = Settings.filter(function (obj) { return obj.name == self.name; });
                if (result && result.length > 0) {
                    switch (result[0].type) {
                        case 'section':
                            let input_index = $(".panel .panel-body input[name='" + result[0].name + "']").index(self);
                            if (input_index > -1 && input_index <= result[0].items.length) {
                                if ($(".panel .panel-body input[name='" + result[0].name + "']").eq(0).attr('value') == 0) {
                                    input_index--;
                                }
                                if (self.value == 0) {
                                    let temp = new Object();
                                    temp.items = Array.prototype.concat.apply([], result[0].items);
                                    hideSubItems(temp);
                                } else {
                                    for (let i = 0; i < result[0].items.length; i++) {
                                        if (i <= input_index) {
                                            showSubItems(result[0], i);
                                        } else {
                                            hideSubItems(result[0], i);
                                        }
                                    }
                                }
                            }
                            break;
                        case 'mix':
                            var value_index = result[0].value.indexOf(self.value);
                            if (value_index > -1) {
                                var value_items = result[0].items[value_index];
                                if (value_items.length > 0) {

                                    if (value_items[0] == 'none') {
                                        hideSubItems(result[0], 0);
                                    } else {
                                        hideSubItems(result[0], value_index);
                                        showSubItems(result[0], value_index);
                                    }

                                }
                            }
                            break;
                        default:
                            if (self.value == result[0].value) {
                                showSubItems(result[0]);
                                $(self).parents('.panel-anchor').addClass('active-anchor');
                            } else {
                                hideSubItems(result[0]);
                                $(self).parents('.panel-anchor').removeClass('active-anchor');
                            }
                            break;
                    }

                }
            });
        }
    })();

    function saveActiveTab(e) {
        var value = $(e.target).attr('href').substr(1)  
        document.cookie = "activeTab=" + escape(value)
    }

    return {
        show: function () {
            if ($('.modal-backdrop.fade.in').length < 1) {
                $("#Form").after('<div class="modal-backdrop fade in"></div>');
            }
            $('#SwitherModal').slideDown(200).addClass('in');
        },
        init: function (settings) {

            $('#SwitherModal .modal-content .close').click(function (event) {
                event.preventDefault();
                $('#SwitherModal').slideUp(800).removeClass('in');
                $("div.modal-backdrop.in").remove();
                $("body#Body").removeClass('modal-open');
            });

            $("body").click(function (e) {
                var _this = $("#SwitherModal .wsc-switcher-wrapper");
                OutModal = false;
                OutButton = false;
                if ((e.pageY < _this.offset().top) || (e.pageY > _this.offset().top + _this.height()) ||
            (e.pageX < _this.offset().left) || (e.pageX > _this.offset().left + _this.width())) {
                    OutModal = true;
                }
                var _this = $(".btn-wsc-switcher-modal-toggle");
                if ((e.pageY < _this.offset().top) || (e.pageY > _this.offset().top + _this.height()) ||
            (e.pageX < _this.offset().left) || (e.pageX > _this.offset().left + _this.width())) {
                    OutButton = true;
                }
                if (OutModal && OutButton) {
                    $('#SwitherModal').slideUp(800).removeClass('in');
                    $("div.modal-backdrop.in").remove();
                    $("body#Body").removeClass('modal-open');
                }
            });

            if ($('#SwitherModal .pick-a-color-markup').length < 1) {
                if ($("#SwitherModal .pick-a-color").length) {
                    $("#SwitherModal .pick-a-color").pickAColor();
                }
            }

            $('[data-toggle="tab"]').click(saveActiveTab)
           
            wscInitSwitcher();
        }
    }

})()