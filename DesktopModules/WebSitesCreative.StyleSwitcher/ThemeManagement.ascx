﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ThemeManagement.ascx.cs"    
    Inherits="WebSitesCreative.Modules.StyleSwitcher.ThemeManagement" %>

<div id="wsc-switcher-wrapper" class="wsc-switcher-wrapper">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;</button>
                <h4 class="modal-title" id="myModalLabel">
                    <%=GetLocalized("AdminTitle")%>
                    -
                    <%=CurrentSkinName%></h4>
            </div>
            <div class="modal-body">
                <%if (IsLoaded) {%>
                <div class="row">
                    <div class="col-md-3">
                        <!-- Nav tabs -->
                        <ul class="nav nav-pills nav-stacked">
                            <asp:Repeater runat="server" ID="rptTabMenu">
                                <ItemTemplate>
                                    <li class='<%# Eval("Name").ToString() == ActiveTab ? "active" : ""%>'>
                                        <a href='<%#"#"+Eval("Name") %>' data-toggle="tab">
                                            <%#Eval("Text") %></a></li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </div>
                    <div class="col-md-9">
                        <!-- Top buttons -->
                        <div class="wsc-clone-buttons modal-footer" style="border-top-width: 0px; margin-top: 0px; padding-top: 0px;">
                            <asp:LinkButton CssClass="btn btn-default" ID="cmdExport2" runat="server" OnClick="cmdExport_Click"
                                CausesValidation="false"><span><%= GetLocalized("cmdExport")%></span></asp:LinkButton>
                            <asp:LinkButton CssClass="btn btn-default" ID="cmdClone2" runat="server" OnClick="cmdClone_Click"
                                CausesValidation="false"><span><%= GetLocalized("cmdClone")%></span></asp:LinkButton>
                            <asp:LinkButton CssClass="btn btn-default" ID="cmdClear2" runat="server" OnClick="cmdClear_Click"
                                CausesValidation="false"><span><%= GetLocalized("cmdClear")%></span></asp:LinkButton>
                            <asp:LinkButton CssClass="btn btn-default" ID="cmdCancel2" runat="server" OnClick="cmdCancel_Click"
                                CausesValidation="false"><span><%= GetLocalized("cmdCancel")%></span></asp:LinkButton>
                            <asp:LinkButton CssClass="btn btn-primary" ID="cmdApply2" runat="server" OnClick="cmdApply_Click"><span><%= GetLocalized("cmdApply")%></span></asp:LinkButton>
                        </div>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <asp:Repeater ID="rptTabContent" runat="server" OnItemDataBound="OnItemDataBound">
                                <ItemTemplate>
                                    <div id='<%#Eval("Name") %>' class='tab-pane <%# Eval("Name").ToString() == ActiveTab ? "active" : ""%>'>
                                        <asp:PlaceHolder ID="phXmlSettings" runat="server"></asp:PlaceHolder>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
                <%} else { %>
                <div style="min-height: 300px">
                    <div class="spinner"></div>
                </div>                
                <%} %>
            </div>
        </div>
    </div>
    <%if (IsLoaded) {%>
    <div class="modal-footer sw-float-footer">
        <asp:HiddenField ID="hfCloneValue" runat="server"></asp:HiddenField>

        <a href="#wsc-switcher-wrapper"><i class="sw-go-top hidden-xs hidden-sm fa fa-angle-double-up"></i></a>
        <asp:LinkButton CssClass="btn btn-default" ID="cmdExport" runat="server" OnClick="cmdExport_Click"
            CausesValidation="false"><span><%= GetLocalized("cmdExport")%></span></asp:LinkButton>
        <asp:LinkButton CssClass="btn btn-default" ID="cmdClone" runat="server" OnClick="cmdClone_Click"
            CausesValidation="false"><span><%= GetLocalized("cmdClone")%></span></asp:LinkButton>
        <asp:LinkButton CssClass="btn btn-default" ID="cmdClear" runat="server" OnClick="cmdClear_Click"
            CausesValidation="false"><span><%= GetLocalized("cmdClear")%></span></asp:LinkButton>
        <asp:LinkButton CssClass="btn btn-default" ID="cmdCancel" runat="server" OnClick="cmdCancel_Click"
            CausesValidation="false"><span><%= GetLocalized("cmdCancel")%></span></asp:LinkButton>
        <asp:LinkButton CssClass="btn btn-primary" ID="cmdApply" runat="server" OnClick="cmdApply_Click"><span><%= GetLocalized("cmdApply")%></span></asp:LinkButton>
    </div>
    <%} %>
</div>

<script>
    function wsc_cloneTheme(newThemeName, hfID, btnID) {
        var val = prompt('Please enter new skin name.', newThemeName);
        if (val) {
            $.ajax({
                type: 'POST',
                url: '<%= ResolveUrl("~/DesktopModules/WebSitesCreative.StyleSwitcher/ThemeManagementHelper.asmx/CheckThemeName")%>',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: "{ newName: '" + val + "' }",
                success: function (msg) {
                    if (!msg.d || confirm('Theme with name ' + val + ' already exists. Do you want to replace it?')) {
                        document.getElementById(hfID).value = val; __doPostBack(btnID, '');
                    }
                }
            });
        }
    }
</script>