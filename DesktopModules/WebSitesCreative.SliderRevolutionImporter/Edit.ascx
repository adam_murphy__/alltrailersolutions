﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Edit.ascx.cs" Inherits="WebSitesCreative.Modules.SliderRevolutionImporter.Edit" %>

<div class="wsc_importer_choose_ctrls">
    <br />
    <div class="dnnForm">
        <div class="dnnFormItem wsc_importer_choose_info"><span id="">Use the Choose File button to browse your local file system to find the Revolution Slider Template zip package to import and then click Import Slider.</span></div>    
        <asp:FileUpload runat="server" ID="fuArchive" CssClass="wsc_file_upload_slider"/>
        <br/>
    </div>
    <br />
    <div class="dnnFormMessage dnnFormInfo">
        <span>Your site is configured with a maximum file uload size of 28 MB.</span>
    </div>

    <ul class="dnnActions dnnClear">
        <li><asp:LinkButton ID="btnUpload" CssClass="dnnPrimaryAction" runat="server" OnClick="btnUpload_Click" Text="Import Slider"/></li>
        <li><asp:LinkButton CssClass="dnnSecondaryAction" runat="server" causesvalidation="False" OnClick="btnCancel_Click">Cancel</asp:LinkButton></li>
    </ul>

    <% if (!string.IsNullOrEmpty(lblMessage.Text))
       { %>
    <div class="dnnFormMessage dnnFormValidationSummary">
        <asp:Label runat="server" ID="lblMessage"></asp:Label>
    </div>
    <% } %>
</div>

<script>
    function wscConfirmUpload() {
        if (!$('.wsc_file_upload_slider').val()) return true
        return confirm('Slider has been modified and your changes may be lost. Do you really want to import new slider?');
    }
</script>