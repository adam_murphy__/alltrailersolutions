﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Activate.ascx.cs" Inherits="WebSitesCreative.Modules.SliderRevolutionImporter.Activate" %>

<% if (!IsActivated) { %>
<div class="wsc_importer_choose_ctrls">
    <br />
    <div class="dnnFormMessage dnnFormInfo">
        <span>Slider Revolution Importer will be activated for <strong><%= Request.Url.Host %></strong> domain. You should be able to find your Purchase ID from DNN Store's purchase confirmation email.</span>
    </div>

     <div class="dnnFormItem">
            <asp:Label CssClass="wsc_label" ID="lblPurchaseID" runat="server" resourcekey="lblPurchaseID"></asp:Label>:&nbsp;
         
<%--            <span class="required">*</span>
            <span class="wsc_tooltip_label">
                <%=GetLocalized("lblPurchaseID.Description")%></span>--%>

            <asp:TextBox CssClass="wsc_textbox text" runat="server" ID="tbPurchaseID" AUTOCOMPLETE="Off"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="rfvPurchaseId" Display="Dynamic" CssClass="required"
                resourcekey="rfvPurchaseId.ErrorMessage" ControlToValidate="tbPurchaseID"></asp:RequiredFieldValidator>
     </div>

    <ul class="dnnActions dnnClear">
        <li><asp:LinkButton ID="btnActivate" CssClass="dnnPrimaryAction" runat="server" OnClick="btnActivate_Click" resourcekey="btnActivate"/></li>
        <li><a class="dnnSecondaryAction" href="/" id="">Cancel</a></li>
    </ul>
</div>
<% } else { %>
<div class="wsc_importer_choose_ctrls">
    <br />
    <div class="dnnFormMessage dnnFormInfo">
        <span>Slider Revolution Importer is already <strong>activated</strong> for <strong><%= Request.Url.Host %></strong> domain.</span>
    </div>

    <ul class="dnnActions dnnClear">
<li><asp:LinkButton ID="btnDeactivate"  CssClass="dnnPrimaryAction" runat="server" OnClick="btnDeactivate_Click" resourcekey="btnDeactivate"/></li>
        <li><a class="dnnSecondaryAction" href="/" id="">Cancel</a></li>
    </ul>
</div>
<% } %>

<% if (!string.IsNullOrEmpty(lblMessage.Text))
    { %>
<div class="dnnFormMessage dnnFormValidationSummary">
    <asp:Label runat="server" ID="lblMessage"></asp:Label>
</div>
<% } %>