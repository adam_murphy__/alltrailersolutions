﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditSlides.ascx.cs"
    Inherits="WebSitesCreative.Modules.SliderRevolutionImporter.EditSlides" %>

<%@ Import Namespace="WebSitesCreative.Modules.SliderRevolutionImporter.Components" %>
<%@ Import Namespace="WebSitesCreative.Modules.SliderRevolutionImporter.Data" %>
<%@ Register TagPrefix="wsc" TagName="FileChooser" Src="Controls/FileChooser.ascx" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.UI.WebControls" Assembly="DotNetNuke" %>

<div class="wsc_admin_content">
    <div class="wsc_admin_box">
        <div class="wsc_admin_box_head">
            <div class="wsc_admin_title">
                Slides Management
            </div>
        </div>
        <div class="wsc_admin_box_body">
            <div class="wsc_admin_fields">
                <asp:MultiView ID="BannersMultiView" runat="server" ActiveViewIndex="0">
                    <asp:View ID="ViewBanners" runat="server">
                        <div class="wsc_slide_container">
                            <asp:Repeater ID="rptImages" runat="server" OnItemDataBound="rptImages_ItemDataBound">
                                <ItemTemplate>
                                    <div class="wsc_slide_box">
                                        <asp:HiddenField runat="server" Value='<%# Eval("ItemId") %>' />
                                        <div class="wsc_slide_head">
                                            <div class="wsc_slide_title">
                                                <asp:Label runat="server" resourcekey="lblSlideTitle">
                                                </asp:Label>&nbsp;
                          <%# Eval("Title") %>
                                            </div>
                                            <div class="wsc_controls_box">
                                                <a title="Drag-n-Drop" class="wsc_drag_icon"><%= GetLocalized("btnDrag") %></a>
                                                <%--<asp:LinkButton CssClass="wsc_copy_icon" ID="btnMakeCopy" runat="server" CommandArgument='<%# Eval("ItemId") %>'
                                                    OnCommand="btnMakeCopy_Command" resourcekey="btnMakeCopy"></asp:LinkButton>--%>
                                                <asp:LinkButton CssClass="wsc_delete_icon" ID="btnDelete" runat="server" CommandArgument='<%# Eval("ItemId") %>'
                                                    OnCommand="btnDelete_Command" resourcekey="btnDelete"></asp:LinkButton>
                                                <asp:LinkButton CssClass="wsc_edit_icon" ID="btnEdit" runat="server" CommandArgument='<%# Eval("ItemId") %>'
                                                    OnCommand="btnEdit_Command" resourcekey="btnEdit"></asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="wsc_controls">
                                            <div class="wsc_image_wrapper">
                                                <asp:MultiView runat="server" ActiveViewIndex='<%#(int)((Preview)Eval("Preview")).Type%>'>
                                                    <asp:View runat="server">
                                                        <asp:Image ID="BannerImage" Style="height: 150px" AlternateText='<%# Eval("Title") %>'
                                                            runat="server" ImageUrl='<%# !string.IsNullOrWhiteSpace(((Preview)Eval("Preview")).Value) && ((Preview)Eval("Preview")).Type == PreviewType.Image ? (VirtualPathUtility.IsAppRelative(((Preview)Eval("Preview")).Value) ? UrlHelper.AddQuery(VirtualPathUtility.ToAppRelative(((Preview)Eval("Preview")).Value)) : ((Preview)Eval("Preview")).Value) : "assets/images/no-preview.jpg" %>' />
                                                    </asp:View>
                                                    <asp:View runat="server">
                                                        <div style='height: 150px; width: 150px; <%#((Preview)Eval("Preview")).Value%>' alt='<%# Eval("Title") %>'></div>
                                                    </asp:View>
                                                </asp:MultiView>
                                            </div>
                                            <div class="clear_float">
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                        <script type="text/javascript">
                            jQuery1124(document).ready(
                                function () {
                                    $('#savingSpinner').hide();
                                    $('.wsc_slide_container').sortable();
                                }
                            );                
    
                            function saveSlidesOrder(redirectToOnSuccess) {
                                var elements = $('.wsc_slide_box > input[type=hidden]');
                                var ids = $.map(elements, function (item) { return item.value; });                    
                                var baseUrl = $.ServicesFramework(<%=ModuleContext.ModuleId %>).getServiceRoot('<%=Consts.MODULE_FOLDER_NAME%>');

                    $('#savingSpinner').show();
                    $.post(baseUrl + 'SliderAPI/SaveSlidesOrder', {moduleId: <%=ModuleContext.ModuleId %>, ids: ids})
                        .fail(function (data) {
                            if (data.status === 400) {
                                alert('ERROR: ' + JSON.parse(data.responseText).error);
                            } else {
                                alert(data.responseText);
                            }
                            $('#savingSpinner').hide();
                        })
                        .done(function( data ) {
                            $('#savingSpinner').hide();
                            window.location = redirectToOnSuccess;
                        });
                }

                        </script>
                        <div class="wsc_controls_group">
                            <a class="wsc_admin_button wsc_small wsc_green" onclick="saveSlidesOrder('<%=DotNetNuke.Common.Globals.NavigateURL()%>')">
                                <span><%= GetLocalized("btnSaveSlidesOrder")%></span>
                            </a>
                            <span id="savingSpinner" class="spinner wsc_small">
                                <img src="/images/icon_wait.gif" /></span>
                            <asp:LinkButton CssClass="wsc_admin_button wsc_small wsc_red" ID="btnReset" runat="server"
                                OnClick="btnReset_Click" CausesValidation="false"><span><%= GetLocalized("btnReset")%></span></asp:LinkButton>
                        </div>
                    </asp:View>
                    <asp:View ID="ViewBanner" runat="server">
                        <asp:HiddenField ID="hfEditSlideId" runat="server" Value='<%#Eval("ElementId")%>'></asp:HiddenField>

                        <asp:Repeater ID="rptLayers" runat="server">
                            <ItemTemplate>
                                <asp:HiddenField ID="hfElementId" runat="server" Value='<%#Eval("ElementId")%>' />
                                <asp:HiddenField ID="hfType" runat="server" Value='<%#Eval("Type")%>' />
                                <asp:MultiView runat="server" ActiveViewIndex='<%#(int)Eval("Type")%>'>
                                    <asp:View runat="server">
                                        <div class="wsc_label">
                                            <asp:Label CssClass="SubHead" runat="server" resourcekey="lblImage"></asp:Label>
                                            <%# FileHelper.GetName((string)Eval("Value")) %>
                                        </div>
                                        <div class="wsc_controls wsc_file_chooser">
                                            <wsc:FileChooser runat="server" ID="fcImage" IsRequired="true" OldFileName='<%#Eval("Value")%>' Type='<%#LayerType.Image%>'></wsc:FileChooser>
                                        </div>
                                    </asp:View>
                                    <asp:View runat="server">
                                        <div class="wsc_label">
                                            <asp:Label CssClass="SubHead" runat="server" resourcekey="lblAudio"></asp:Label>
                                            <%# FileHelper.GetName((string)Eval("Value")) %>
                                        </div>
                                        <div class="wsc_controls wsc_file_chooser">
                                            <wsc:FileChooser runat="server" ID="fcAudio" IsRequired="true" OldFileName='<%#Eval("Value")%>' Type='<%#LayerType.Audio%>'></wsc:FileChooser>
                                        </div>
                                    </asp:View>
                                    <asp:View runat="server">
                                        <div class="wsc_label">
                                            <asp:Label CssClass="SubHead" runat="server" resourcekey="lblVideo"></asp:Label>
                                            <%# FileHelper.GetName((string)Eval("Value")) %>
                                        </div>
                                        <div class="wsc_controls wsc_file_chooser">
                                            <wsc:FileChooser runat="server" ID="fcVideo" IsRequired="true" OldFileName='<%#Eval("Value")%>' Type='<%#LayerType.Video%>'></wsc:FileChooser>
                                        </div>
                                    </asp:View>
                                    <asp:View runat="server">
                                        <div class="wsc_label">
                                            <asp:Label CssClass="SubHead" runat="server" resourcekey="lblTitle"></asp:Label>
                                        </div>
                                        <div class="wsc_controls">
                                            <asp:TextBox ID="tbTitle" runat="server" CssClass="wsc_text_field" Text='<%#Eval("Value")%>'></asp:TextBox>
                                        </div>
                                    </asp:View>
                                    <asp:View runat="server">
                                        <div class="wsc_label">
                                            <asp:Label CssClass="SubHead" runat="server" resourcekey="lblText"></asp:Label>
                                        </div>
                                        <div class="wsc_controls">
                                            <asp:TextBox CssClass="wsc_text_field" ID="tbText" TextMode="MultiLine"
                                                Width="600px" Height="100px" runat="server" Text='<%# Eval("Value") %>'></asp:TextBox>
                                        </div>
                                    </asp:View>
                                    <asp:View runat="server">
                                        <div class="wsc_label">
                                            <asp:Label CssClass="SubHead" runat="server" resourcekey="lblYouTubeID"></asp:Label>
                                        </div>
                                        <div class="wsc_controls">
                                            <asp:TextBox ID="tbYouTubeID" runat="server" CssClass="wsc_text_field" Text='<%#Eval("Value")%>'></asp:TextBox>
                                        </div>
                                    </asp:View>
                                    <asp:View runat="server">
                                        <div class="wsc_label">
                                            <asp:Label CssClass="SubHead" runat="server" resourcekey="lblVimeoID"></asp:Label>
                                        </div>
                                        <div class="wsc_controls">
                                            <asp:TextBox ID="tbVimeoID" runat="server" CssClass="wsc_text_field" Text='<%#Eval("Value")%>'></asp:TextBox>
                                        </div>
                                    </asp:View>
                                    <asp:View runat="server">
                                        <div class="wsc_label">
                                            <asp:Label CssClass="SubHead" runat="server" resourcekey="lblLink"></asp:Label>
                                        </div>
                                        <div class="wsc_controls">
                                            <asp:TextBox ID="tbLink" runat="server" CssClass="wsc_text_field" Text='<%#Eval("Value")%>'></asp:TextBox>
                                            <asp:TextBox ID="tbLinkText" runat="server" CssClass="wsc_text_field" Text='<%#DataBinder.Eval(Container,"DataItem") is LinkLayer ? Eval("Text") : ""%>'></asp:TextBox>
                                        </div>
                                    </asp:View>
                                    <asp:View runat="server">
                                        <div class="wsc_label">
                                            <asp:Label CssClass="SubHead" runat="server" resourcekey="lblUnknown"></asp:Label>
                                            <%# FileHelper.GetName((string)Eval("Value")) %>
                                        </div>
                                        <div class="wsc_controls wsc_file_chooser">
                                            <wsc:FileChooser runat="server" ID="icUnknown" IsRequired="true" OldFileName='<%#Eval("Value")%>' Type='<%#LayerType.Unknown%>'></wsc:FileChooser>
                                        </div>
                                    </asp:View>
                                </asp:MultiView>
                            </ItemTemplate>
                        </asp:Repeater>

                        <div class="wsc_controls_group">
                            <asp:LinkButton CssClass="wsc_admin_button wsc_small" ID="btnSave" runat="server"
                                CausesValidation="true" OnClick="btnSave_Click"><span><%= GetLocalized("btnSave")%></span></asp:LinkButton>
                            <asp:LinkButton CssClass="wsc_admin_button wsc_small wsc_red" ID="btnCancel"
                                CausesValidation="false" runat="server" OnClick="btnCancel_Click"><span><%= GetLocalized("btnCancel")%></span></asp:LinkButton>
                        </div>
                    </asp:View>
                </asp:MultiView>
            </div>
        </div>
    </div>
    <asp:LinkButton CssClass="CommandButton wsc_admin_button wsc_gray" ID="cmdGoToView"
        runat="server" CausesValidation="False" OnClick="cmdGoToView_Click"><span class="wsc_door_icon"><%= GetLocalized("cmdGoToView")%></span></asp:LinkButton>
</div>
