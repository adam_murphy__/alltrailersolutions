﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="View.ascx.cs" Inherits="WebSitesCreative.Modules.SliderRevolutionImporter.View" %>

<%@ Import Namespace="WebSitesCreative.Modules.SliderRevolutionImporter.Data" %>

<% if (IsGenericSkinRequired && !IsValidSkinApplied)
   { %>
       <div class="dnnFormMessage dnnFormValidationSummary">
          Invalid skin applied!
       </div>
<% } else {%>


        <% if (IsActivated) {%>

            <% if (!string.IsNullOrEmpty(this.Content))
               { %>
                <%= Server.HtmlDecode(this.Content) %>
            <% }
                else
                { %>
            Click <a href="<%= EditUrl() %>">Import Slider</a> or select 'Import Slider' from the Edit Action Menu or navigate to <a href="javascript:window.open('<%= VisualEditorUrl %>');void(0);">Visual Editor</a>
            <% } %>

        <% } else { %>

            Please <a href="<%= PurchaseUrl %>">enter your Purchase ID</a> to activate your slider first. <a href="javascript:window.open('<%= NeedHelpUrl %>');void(0);">Need help?</a>

        <% } %>
    <% } %>
