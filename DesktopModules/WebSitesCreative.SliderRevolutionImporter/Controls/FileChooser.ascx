﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FileChooser.ascx.cs" EnableViewState="true"
    Inherits="WebSitesCreative.Modules.SliderRevolutionImporter.Controls.FileChooser" %>
<asp:FileUpload CssClass="wsc_fileuploader" runat="server" ID="uplFile" />
<img runat="server" id="imgOldFile" title="image" alt="image" style="max-height: 150px;
    max-width: 150px;" />
<asp:RequiredFieldValidator runat="server" ID="fileRequiredValidator" ControlToValidate="uplFile"></asp:RequiredFieldValidator>
<asp:CustomValidator runat="server" ID="fileTypeValidator" Display="Dynamic" ErrorMessage="Wrong file format"
    OnServerValidate="fileTypeValidator_Validate"></asp:CustomValidator>