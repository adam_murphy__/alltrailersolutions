﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RssFeed.aspx.cs" 
    ContentType="text/xml" Inherits="WebSitesCreative.Modules.PostIt.Rss" %>
<%@ Import Namespace="WebSitesCreative.Modules.PostIt.Components" %>
<%@ Import Namespace="WebSitesCreative.Modules.PostIt.Components.Helpers" %><?xml version="1.0" ?>
<rss version="2.0">
  <channel>
    <title><%= FormatToXml(Title) %></title>
    <link><%= UrlsHelper.FormatUrlToGlobal(Link)%></link>
    <description><%= FormatToXml(Description) %></description>
    <asp:Repeater id="rptRSS" runat="server">  
        <ItemTemplate>
    <item>
        <title><%# HttpUtility.HtmlEncode(FormatToXml(DataBinder.Eval(Container.DataItem, "Title").ToString())) %></title>
        <description>
            <asp:PlaceHolder runat="server" Visible='<%# Eval("Image") != null %>'>
                <![CDATA[ <img src="<%# FilesHelper.GetThumbnailUrl((int) Eval("ModuleId"), Eval("Image") != null ? Eval("Image").ToString(): "")%>" /><br />]]>
            </asp:PlaceHolder>
            <%# FormatToXml(Eval("Summary").ToString()) %>
        </description>
        <link>
            <%# UrlsHelper.FormatUrlToGlobal(UrlsHelper.GetPostUrl(Eval("Url").ToString(), Tab.TabID)) %>
        </link>          
        <pubDate>
            <%# DateTimeHelper.FormatDateForRss((DateTime)Eval("Date")) %>
        </pubDate>
        <guid>
            <%# UrlsHelper.FormatUrlToGlobal(UrlsHelper.GetPostUrl(Eval("Url").ToString(), Tab.TabID))%>
        </guid>
    </item>
          </ItemTemplate>
        </asp:Repeater>
    </channel>
</rss>  