<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditPostIt.ascx.cs"
    Inherits="WebSitesCreative.Modules.PostIt.EditPostIt" %>
<%@ Import Namespace="DotNetNuke.Common.Utilities" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="WebSitesCreative.Modules.PostIt.Components.Helpers" %>
<%@ Register TagPrefix="ctrls" TagName="TextEditor" Src="../../controls/TextEditor.ascx" %>
<%@ Register TagPrefix="cust" TagName="ImageChooser" Src="Controls/ImageChooser.ascx" %>
<%@ Register TagPrefix="cust" TagName="EditAudioFile" Src="Controls/EditAudioFile.ascx" %>

<%@ Register Assembly="DotNetNuke.Web" Namespace="DotNetNuke.Web.UI.WebControls" TagPrefix="dnn" %>
<%@ Register TagPrefix="dnnui" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<script type="text/javascript" language="javascript">
    jQueryWSC171(document).ready(function () {
        <% if(Null.IsNull(ItemId))
           {%>
        //script for create url property by title
        jQueryWSC171('#<%=txtTitle.ClientID%>').keyup(function () {
            var titleText = jQueryWSC171('#<%=txtTitle.ClientID%>').val();
            if (titleText) {
                var urlText = titleText.replace(/[^a-zA-Z0-9\-]+/g, '-').replace(/^[-]*/, "").replace(/[-]*$/, "").toLowerCase();
                jQueryWSC171('#<%=txtUrl.ClientID%>').val(urlText);
            }
        });
        <%
           } %>

        jQueryWSC171(".wsc_admin_box.wsc_collapsible.wsc_collapsed_by_default").collapsiblePanel();

        jQueryWSC171(".wsc_admin_box.wsc_collapsible.wsc_expanded_by_default").collapsiblePanel({isOpen : true });

        // make nice form elements
        jQueryWSC171("input, select, button, textarea", jQueryWSC171(".wsc_admin_fields")).uniform();

        jQueryWSC171("#tags").tagit({
            tagSource: <%=AllTags %>,
            initialTags : <%=InitialTags %>,
            triggerKeys: ['enter', 'comma', 'tab'],
            select: true
        });
    });
</script>
<div class="wsc_admin_content">
    <div class="wsc_admin_box">
        <div class="wsc_admin_box_head">
            <div class="wsc_admin_title">
                General Post Settings
            </div>
        </div>
        <div class="wsc_admin_box_body">
            <ul class="wsc_admin_fields">
                <li>
                    <asp:Label CssClass="wsc_label" ID="lblTitle" runat="server" resourcekey="lblTitle"></asp:Label><span>*</span>
                    <span class="wsc_tooltip_label">
                        <%=GetLocalized("lblTitle.Description")%></span>
                    <asp:TextBox CssClass="wsc_textbox text" runat="server" ID="txtTitle" AUTOCOMPLETE="Off"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ID="titleRequiredValidator" Display="Dynamic"
                        resourcekey="titleRequiredValidator.ErrorMessage" ControlToValidate="txtTitle"></asp:RequiredFieldValidator>
                </li>
                <li>
                    <asp:Label CssClass="wsc_label" ID="lblUrl" runat="server" resourcekey="lblUrl"></asp:Label><span>*</span>
                    <span class="wsc_tooltip_label">
                        <%=GetLocalized("lblUrl.Description")%></span>
                    <asp:TextBox CssClass="wsc_textbox text" runat="server" ID="txtUrl"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ID="urlRequiredValidator" Display="Dynamic"
                        resourcekey="urlRequiredValidator.ErrorMessage" ControlToValidate="txtUrl" ></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="urlRegexValidator" Display="Dynamic"
                        resourcekey="urlRegexValidator.ErrorMessage" ControlToValidate="txtUrl" ValidationExpression="^[a-zA-Z0-9][a-zA-Z0-9-_]*$"></asp:RegularExpressionValidator>
                    <asp:CustomValidator runat="server" ID="urlCustomValidator" Display="Dynamic" resourcekey="urlCustomValidator.ErrorMessage"
                        OnServerValidate="urlCustomValidator_Validate"></asp:CustomValidator>
                </li>
                <li>
                    <asp:Label CssClass="wsc_label" ID="lblDate" runat="server" resourcekey="lblDate"></asp:Label><span>*</span>
                    <span class="wsc_tooltip_label">
                        <%=GetLocalized("lblDate.Description")%></span>
                     <dnnui:RadDateTimePicker ID="dtPostDateTime" Width="835px" runat="server" RenderMode="Lightweight" style="display: block" DateInput-CssClass="wsc_datepicker_input">
                     </dnnui:RadDateTimePicker>
                    
                     <asp:RequiredFieldValidator runat="server" ID="dateTimeRequiredValidator" Display="Dynamic"
                        resourcekey="dateTimeRequiredValidator.ErrorMessage" ControlToValidate="dtPostDateTime" ErrorMessage="Please choose date and time"></asp:RequiredFieldValidator>
                </li>
                <li class="wsc_uploader">
                    <asp:Label CssClass="wsc_label" ID="lblPostImage" runat="server" resourcekey="lblPostImage"></asp:Label>
                    <span class="wsc_tooltip_label">
                        <%=GetLocalized("lblPostImage.Description")%></span><br />
                    <cust:ImageChooser runat="server" ID="postImage" IsRequired="false">
                    </cust:ImageChooser>
                    <asp:CheckBox CssClass="wsc_inpost" runat="server" ID="cbIncludeInPostBody" resourcekey="cbIncludeInPostBody" />
                </li>
                <li>
                    <asp:Label CssClass="wsc_label" ID="lblSummary" runat="server" resourcekey="lblSummary"></asp:Label><span>*</span>
                    <span class="wsc_tooltip_label">
                        <%=GetLocalized("lblSummary.Description")%></span>
                    <asp:TextBox CssClass="wsc_textbox uniform" runat="server" ID="txtSummary" TextMode="MultiLine"
                        Rows="6"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ID="summaryRequiredValidator" Display="Dynamic"
                        resourcekey="summaryRequiredValidator.ErrorMessage" ControlToValidate="txtSummary"></asp:RequiredFieldValidator>
                </li>
                <li>
                    <ctrls:texteditor id="txtContent" runat="server" height="500" width="100%" />
                    <asp:RequiredFieldValidator ID="valContent" resourcekey="valContent.ErrorMessage"
                        ControlToValidate="txtContent" CssClass="NormalRed" Display="Dynamic" runat="server" />
                </li>
                <li>
                    <asp:Label CssClass="wsc_label" ID="lblTags" runat="server" resourcekey="lblTags"></asp:Label>
                    <span class="wsc_tooltip_label">
                        <%=GetLocalized("lblTags.Description")%></span>
                    <ul id="tags" name="txtTags">
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="wsc_admin_box wsc_collapsible wsc_collapsed_by_default" title="Meta information">
        <ul class="wsc_admin_fields">
            <li>
                <div class="wsc_controls_group">
                    <div class="wsc_label">
                        <asp:Label CssClass="wsc_label SubHead" ID="lblKeywords" runat="server" resourcekey="lblKeywords"></asp:Label>
                        <span class="wsc_tooltip_label">
                            <%=GetLocalized("lblKeywords.Description")%></span>
                    </div>
                    <div class="wsc_controls">
                        <asp:TextBox CssClass="wsc_textbox uniform" runat="server" ID="txtKeywords" TextMode="MultiLine"
                            Rows="6"></asp:TextBox>
                    </div>
                    <div class="wsc_label">
                        <asp:Label CssClass="wsc_label SubHead" ID="lblDescription" runat="server" resourcekey="lblDescription"></asp:Label>
                        <span class="wsc_tooltip_label">
                            <%=GetLocalized("lblDescription.Description")%></span>
                    </div>
                    <div class="wsc_controls">
                        <asp:TextBox CssClass="wsc_textbox uniform" runat="server" ID="txtDescription" TextMode="MultiLine"
                            Rows="6"></asp:TextBox>
                    </div>
                    <div class="wsc_label">
                        <asp:Label CssClass="wsc_label SubHead" ID="lblPageHeaderTags" runat="server" resourcekey="lblPageHeaderTags"></asp:Label>
                        <span class="wsc_tooltip_label">
                            <%=GetLocalized("lblPageHeaderTags.Description")%></span>
                    </div>
                    <div class="wsc_controls">
                        <asp:TextBox CssClass="wsc_textbox uniform" runat="server" ID="txtPageHeaderTags"
                            TextMode="MultiLine" Rows="6"></asp:TextBox>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <div title="Audio file" runat="server" id="audioFilePanel">
        <cust:EditAudioFile runat="server" id="editAudioFile"></cust:EditAudioFile>
    </div>
    <div class="wsc_admin_box">
        <ul class="wsc_admin_fields">
            <li>
                <asp:Button CssClass="CommandButton" ID="cmdUpdate" resourcekey="cmdUpdate" runat="server"
                    CausesValidation="true" OnClick="cmdUpdate_Click"></asp:Button>&nbsp;
                <asp:Button CssClass="CommandButton" ID="cmdChangePublish" runat="server" CausesValidation="true"
                    OnClick="cmdChangePublish_Click"></asp:Button>&nbsp;
                <asp:Button CssClass="CommandButton" ID="cmdCancel" resourcekey="cmdCancel" runat="server"
                    CausesValidation="False" OnClick="cmdCancel_Click"></asp:Button>&nbsp;
                <asp:Button CssClass="CommandButton" ID="cmdDelete" resourcekey="cmdDelete" runat="server"
                    CausesValidation="False" OnClick="cmdDelete_Click"></asp:Button>&nbsp; </li>
        </ul>
    </div>
</div>
