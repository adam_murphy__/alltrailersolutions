<%@ Control Language="C#" AutoEventWireup="false" Inherits="WebSitesCreative.Modules.PostIt.Settings"
    CodeBehind="Settings.ascx.cs" %>
<%@ Register TagPrefix="dnn" TagName="Label" Src="~/controls/LabelControl.ascx" %>
<table cellspacing="0" cellpadding="2" border="0" summary="PostIt Settings Design Table">
    <tr>
        <td class="SubHead" width="200">
            <dnn:Label id="lblThumbSize" runat="server" suffix=":">
            </dnn:Label>
        </td>
        <td valign="bottom">
            <asp:TextBox ID="txtThumbSize" CssClass="NormalTextBox" Width="150" runat="server" />
            <asp:RequiredFieldValidator runat="server" ID="thumbSizeRequiredValidator" Display="Dynamic"
                resourcekey="thumbSizeRequiredValidator.ErrorMessage" ControlToValidate="txtThumbSize"></asp:RequiredFieldValidator>
            <asp:RangeValidator runat="server" ID="thumbSizeRangeValidator" Display="Dynamic"
                ControlToValidate="txtThumbSize" resourcekey="thumbSizeRangeValidator.ErrorMessage"
                MinimumValue="1" Type="Integer" MaximumValue="15000"></asp:RangeValidator>
        </td>
    </tr>
    <tr>
        <td class="SubHead" width="200">
            <dnn:Label id="lblTitleFormat" runat="server" suffix=":">
            </dnn:Label>
        </td>
        <td valign="bottom">
            <asp:TextBox ID="txtTitleFormat" CssClass="NormalTextBox" Width="150" runat="server" />
            <asp:RequiredFieldValidator runat="server" ID="titleFormatRequiredValidator" Display="Dynamic"
                resourcekey="TitleFormatRequiredValidator.ErrorMessage" ControlToValidate="txtTitleFormat"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="SubHead" width="200">
            <dnn:Label id="lblPageLimit" runat="server" suffix=":">
            </dnn:Label>
        </td>
        <td valign="bottom">
            <asp:TextBox ID="txtPageLimit" CssClass="NormalTextBox" Width="150" runat="server" />
            <asp:RequiredFieldValidator runat="server" ID="pageLimitRequiredValidator" Display="Dynamic"
                resourcekey="pageLimitRequiredValidator.ErrorMessage" ControlToValidate="txtPageLimit"></asp:RequiredFieldValidator>
            <asp:RangeValidator runat="server" ID="pageLimitRangeValidator" Display="Dynamic"
                ControlToValidate="txtPageLimit" resourcekey="pageLimitRangeValidator.ErrorMessage"
                Type="Integer" MinimumValue="1" MaximumValue="15000"></asp:RangeValidator>
        </td>
    </tr>
    <tr>
        <td class="SubHead" width="200">
            <dnn:Label id="lblPaging" runat="server" suffix=":">
            </dnn:Label>
        </td>
    </tr>
    <tr>
        <td class="SubHead" width="200">
            <asp:Label ID="lblPrevNext" runat="server" resourcekey="lblPrevNext"></asp:Label>
        </td>
        <td valign="bottom">
            <asp:CheckBox ID="chbxPrevNext" runat="server" />
        </td>
    </tr>
    <tr>
        <td class="SubHead" width="200">
            <asp:Label ID="lblPageNumbers" runat="server" resourcekey="lblPageNumbers"></asp:Label>
        </td>
        <td valign="bottom">
            <asp:CheckBox ID="chbxPageNumbers" runat="server" />
        </td>
    </tr>
    <tr>
        <td class="SubHead" width="200">
            <dnn:Label id="lblImageAlign" runat="server" suffix=":">
            </dnn:Label>
        </td>
        <td valign="bottom">
            <asp:RadioButton ID="rbImageLeft" runat="server" GroupName="ImageAlign" resourcekey="rbImageLeft" />
            <asp:RadioButton ID="rbImageCenter" runat="server" GroupName="ImageAlign" resourcekey="rbImageCenter" />
            <asp:RadioButton ID="rbImageRight" runat="server" GroupName="ImageAlign" resourcekey="rbImageRight" />
        </td>
    </tr>
</table>
