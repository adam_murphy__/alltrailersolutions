﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="PageChooser.ascx.cs" Inherits="WebSitesCreative.Modules.PostIt.Controls.PageChooser" %>

<asp:DropDownList ID="ddlPages" DataTextField="Path" DataValueField="ModuleId" runat="server">
</asp:DropDownList>
