﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImageChooser.ascx.cs"
    Inherits="WebSitesCreative.Modules.PostIt.Controls.ImageChooser" %>

<asp:FileUpload CssClass="wsc_fileuploader" runat="server" ID="uplImage" />
<img runat="server" id="imgOldImage" title="image" alt="image" style="max-height: 150px;
    max-width: 150px;" />
<asp:RequiredFieldValidator runat="server" ID="imageRequiredValidator" ControlToValidate="uplImage"></asp:RequiredFieldValidator>
<asp:CustomValidator runat="server" ID="fileTypeValidator" Display="Dynamic" ErrorMessage="Wrong file format"
    OnServerValidate="fileTypeValidator_Validate"></asp:CustomValidator>