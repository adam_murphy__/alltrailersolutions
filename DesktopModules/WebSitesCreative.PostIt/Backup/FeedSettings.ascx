<%@ Control Language="C#" AutoEventWireup="false" Inherits="WebSitesCreative.Modules.PostIt.FeedSettings"
    CodeBehind="FeedSettings.ascx.cs" %>
<%@ Register TagPrefix="dnn" TagName="Label" Src="~/controls/LabelControl.ascx" %>
<%@ Register TagPrefix="wsc" TagName="PageChooser" Src="Controls/PageChooser.ascx" %>
<table cellspacing="0" cellpadding="2" border="0" summary="PostIt Settings Design Table">
    <tr>
        <td class="SubHead" width="150">
            <dnn:Label id="lblFeedModuleId" runat="server" suffix=":">
            </dnn:Label>
        </td>
        <td valign="bottom">
            <wsc:PageChooser ID="cPageChooser" runat="server">
            </wsc:PageChooser>
        </td>
    </tr>
    <tr>
        <td class="SubHead" width="150">
            <dnn:Label id="lblFeedSize" runat="server" suffix=":">
            </dnn:Label>
        </td>
        <td valign="bottom">
            <asp:TextBox ID="txtFeedSize" CssClass="NormalTextBox" Width="390" runat="server" />
            <asp:RequiredFieldValidator runat="server" ID="feedSizeRequiredValidator" Display="Dynamic"
                resourcekey="feedSizeRequiredValidator.ErrorMessage" ControlToValidate="txtFeedSize"></asp:RequiredFieldValidator>
            <asp:RangeValidator runat="server" ID="feedSizeRangeValidator" Display="Dynamic"
                ControlToValidate="txtFeedSize" resourcekey="feedSizeRangeValidator.ErrorMessage"
                MinimumValue="1" Type="Integer" MaximumValue="1000"></asp:RangeValidator>
        </td>
    </tr>
     <tr>
        <td class="SubHead" width="150">
            <dnn:Label id="lblFeedTag" runat="server" suffix=":">
            </dnn:Label>
        </td>
        <td valign="bottom">
            <asp:DropDownList ID="ddlFeedTag" CssClass="NormalTextBox" runat="server" DataValueField="Name" DataTextField="Name" />
        </td>
    </tr>
</table>
