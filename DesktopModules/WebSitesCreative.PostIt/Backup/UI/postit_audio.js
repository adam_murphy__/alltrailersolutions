﻿(function ($) {
    $.fn.postit_audio = function (options) {
        var opts = $.extend({}, $.fn.postit_audio.defaults, options);
        return this.each(function () {
            var playerElement = $(this);

            var suppliedFormats = "";
            if (opts.mp3File) {
                suppliedFormats += "mp3";
            }
            if (opts.oggFile && opts.oggFile.length > 0) {
                if (suppliedFormats.length > 0) {
                    suppliedFormats += ", ";
                }
                suppliedFormats += "oga";
            }
            playerElement.jPlayer({
                ready: function () {
                    playerElement.jPlayer("setMedia", { mp3: opts.mp3File, oga: opts.oggFile });
                },
                swfPath: opts.swfPath,
                cssSelectorAncestor: opts.cssSelectorAncestor,
                errorAlerts: true,
                supplied: suppliedFormats,
                noConflict: 'jQueryWSC171'
            });


            var isPlayed = false;

            playerElement.bind($.jPlayer.event.play, function (e) {
                if (!isPlayed) {
                    $('.wsc_postit_audioplayer .audio_player_context').show('slow');
                    isPlayed = true;
                }
            });
        });
    };
})(jQueryWSC171);