﻿(function ($) {
    $.fn.extend({
        collapsiblePanel: function (options) {
            var defaults = { isOpen: false };

            var opts = $.extend(defaults, options);
            // Call the ConfigureCollapsiblePanel function for the selected element
            return $(this).each(function () {
                $(this).addClass("ui-widget");

                // Check if there are any child elements, if not then wrap the inner text within a new div.
                if ($(this).children().length == 0) {
                    $(this).wrapInner("<div></div>");
                }

                // Wrap the contents of the container within a new div.
                $(this).children().wrapAll("<div class='wsc_admin_box_body' style='display: none;'></div>");

                // Create a new div as the first item within the container.  Put the title of the panel in here.
                $("<div class='wsc_admin_box_head wsc_collapsed'><div class='wsc_admin_title'>" + $(this).attr("title") + "</div></div>").prependTo(jQueryWSC171(this));

                // Assign a call to CollapsibleContainerTitleOnClick for the click event of the new title div.
                $(".wsc_admin_box_head", this).click(CollapsibleContainerTitleOnClick);

                if (opts.isOpen) {
                    var title = $(".wsc_admin_box_head", this);
                    title.addClass("wsc_expanded");
                    title.removeClass("wsc_collapsed");

                    $('.wsc_admin_box_body', this).css('display', '');
                }
            });
        }
    });


    function CollapsibleContainerTitleOnClick() {
        var title = this;
        body = ".wsc_admin_box_body";
        // The item clicked is the title div... get this parent (the overall container) and toggle the content within it.
        $(body, $(this).parent()).slideToggle();
        if ($(title).hasClass("wsc_expanded")) {
            $(title).removeClass("wsc_expanded");
            $(title).addClass("wsc_collapsed");
        }
        else {
            $(title).addClass("wsc_expanded");
            $(title).removeClass("wsc_collapsed");
        }

    }

})(jQueryWSC171);


