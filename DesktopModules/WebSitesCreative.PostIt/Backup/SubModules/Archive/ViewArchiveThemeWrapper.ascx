﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="~/Components/Theme/ThemePortalModuleWrapper.cs"
    Inherits="WebSitesCreative.Modules.PostIt.Components.Theme.ThemePortalModuleWrapper" %>
     <script language="c#" runat="server">
         protected override void OnInit(EventArgs e)
         {
             ThemedControlName = "ViewArchive.ascx";
             base.OnInit(e);
         }
     </script>
<asp:PlaceHolder ID="phControlContainer" runat="server">
</asp:PlaceHolder>
