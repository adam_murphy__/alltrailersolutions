﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ViewArchive.ascx.cs" Inherits="WebSitesCreative.Modules.PostIt.SubModules.Archive.ViewArchive" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="WebSitesCreative.Modules.PostIt.Components" %>
<%@ Import Namespace="WebSitesCreative.Modules.PostIt.Components.Helpers" %>
<div class="wsc_archive_view">
    <ul class="wsc_archive_cloud wsc_custom_list">
        <asp:Repeater runat="server" ID="rptArchiveItems">
            <ItemTemplate>
            <li>
               <a class="wsc_date" href='<%# UrlsHelper.GetArchiveUrl(Eval("Month").ToString(),Eval("Year").ToString(), PostItTabId) %>'>
               <%# Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase((new DateTime(Convert.ToInt32(Eval("Year")), Convert.ToInt32(Eval("Month")), 1)).ToString("MMMM yyyy")) %></a>
               <span class="wsc_posts_count">(<%# Eval("Count") %>)</span>
            </li>
            </ItemTemplate>
        </asp:Repeater>
    </ul>
   
</div>