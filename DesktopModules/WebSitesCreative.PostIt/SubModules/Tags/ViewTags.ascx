﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ViewTags.ascx.cs" Inherits="WebSitesCreative.Modules.PostIt.SubModules.Tags.ViewTags" %>
<%@ Import Namespace="WebSitesCreative.Modules.PostIt.Components.Helpers" %>
<div class="wsc_tags_view">
    <ul class="wsc_tags_cloud wsc_custom_list">
        <asp:Repeater runat="server" ID="rptTags">
            <ItemTemplate>
            <li>
                <a class="wsc_tag" href="<%# UrlsHelper.GetTagUrl(Eval("Name").ToString(), PostItTabId) %>"><%# Eval("Name")%></a>
                <span class="wsc_posts_count">(<%# Eval("Count") %>)</span>
            </li>
            </ItemTemplate>
        </asp:Repeater>
    </ul>
</div>
