﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Settings.ascx.cs" Inherits="WebSitesCreative.Modules.PostIt.SubModules.Tags.Settings" %>
<%@ Register TagPrefix="wsc" TagName="PageChooser" Src="../../Controls/PageChooser.ascx"  %>
<%@ Register TagPrefix="dnn" TagName="Label" Src="~/controls/LabelControl.ascx" %>

<table cellspacing="0" cellpadding="2" border="0" summary="PostIt Tags Settings Design Table">
    <tr>
        <td class="SubHead" width="150">
            <dnn:Label id="lblModuleId" runat="server" suffix=":"></dnn:Label></td>
        <td valign="bottom">
            <wsc:PageChooser ID="cPageChooser" runat="server"></wsc:PageChooser>
        </td>
    </tr>    
</table>
