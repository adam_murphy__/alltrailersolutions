<%@ Control Language="C#" Inherits="WebSitesCreative.Modules.PostIt.ViewPostItFeed"
    AutoEventWireup="true" CodeBehind="ViewPostItFeed.ascx.cs" %>
    <%@ Import Namespace="WebSitesCreative.Modules.PostIt.Components" %>
<%@ Import Namespace="WebSitesCreative.Modules.PostIt.Components.Helpers" %>
<div class="wsc_posts_feed">
    <asp:Repeater runat="server" ID="rptPosts" OnItemDataBound="rptPosts_ItemDataBound">
        <ItemTemplate>
            <div class="news_box wsc_pi_feed_item">
                <div class="wsc_image_frame imgLeft wsc_pi_img_frame">
                    <div class="wsc_frame_tl">
                        <div class="wsc_frame_tr">
                            <div class="wsc_frame_tc">
                            </div>
                        </div>
                    </div>
                    <div class="wsc_frame_cl">
                        <div class="wsc_frame_cr">
                            <div class="wsc_frame_cc">
                            <a href='<%# UrlsHelper.GetPostUrl(Eval("Url") as string, FeedTabId) %>'>
                                <img runat="server" id="img" src='<%# FilesHelper.GetThumbnailUrl(PostItFeedSettings.FeedModuleId, Eval("Image") as string) %>'
                                    alt='<%# Eval("Title") %>' title='<%# Eval("Title") %>' /></a>
                                <div class="clear">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wsc_frame_bl">
                        <div class="wsc_frame_br">
                            <div class="wsc_frame_bc">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wsc_pi_feed_content">
                    <div class="wsc_title wsc_pi_feed_title">
                        <a href='<%# UrlsHelper.GetPostUrl(Eval("Url") as string, FeedTabId) %>'>
                            <%# Eval("Title") %></a>
                    </div>
                    <span class="wsc_pi_feed_info">
                        <span class="wsc_text_posted"><%=GetLocalized("Posted")%></span>
                        <span class="wsc_info_date"><%# ((DateTime)Eval("Date")).ToShortDateString() %></span>
                        <span class="wsc_text_by"><%=GetLocalized("ByUser")%></span>
                        <span class="wsc_info_author"><%# Eval("CreatedByUserName") %></span>
                        <span class="wsc_tags_cloug">
                        <asp:PlaceHolder ID="PlaceHolder1" runat="server" Visible='<%# !String.IsNullOrEmpty((String)Eval("Tags")) %>'>
                                                                <%=GetLocalized("InTags")%>
                                    <asp:Repeater ID="Repeater1" DataSource='<%# Eval("SplittedTags") %>' runat="server">
                                        <ItemTemplate>
                                            <a class="wsc_tag" href='<%# UrlsHelper.GetTagUrl((String)Container.DataItem, TabId) %>'>
                                                <%# Container.DataItem%></a>
                                        </ItemTemplate>
                                    </asp:Repeater>
                        </asp:PlaceHolder>
                        </span>
                    </span>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
</div>
