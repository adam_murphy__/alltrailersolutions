﻿DROP TABLE {databaseOwner}[{objectQualifier}wsc_PostIt_AudioFile]
GO

CREATE TABLE {databaseOwner}[{objectQualifier}wsc_PostIt_AudioFile](
	[AudioFileId] [int] IDENTITY(1,1) NOT NULL,
	[Mp3FileName] [nvarchar](256) NULL,
	[Mp3UploadedFileName] [nvarchar](256) NULL,
	[OggFileName] [nvarchar](256) NULL,
	[OggUploadedFileName] [nvarchar](256) NULL,
	[Title] [nvarchar](256) NULL,
 CONSTRAINT [PK_{objectQualifier}wsc_PostIt_AudioFile] PRIMARY KEY CLUSTERED 
(
	[AudioFileId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE {databaseOwner}[{objectQualifier}wsc_PostIt_Post]
ADD CONSTRAINT [FK_{objectQualifier}wsc_PostIt_Post_AudioFile] 
FOREIGN KEY	(AudioFileId) 
REFERENCES {databaseOwner}[{objectQualifier}wsc_PostIt_AudioFile] (AudioFileId) 
	ON UPDATE  NO ACTION 
	ON DELETE  NO ACTION 
GO

ALTER PROCEDURE {databaseOwner}[{objectQualifier}wsc_PostIt_CreatePost]
    @ModuleId       int,
    @ItemId         int,
	@Content        nvarchar(max),
	@CreatedByUser  int,
    @Date           datetime,
	@Title          nvarchar(max),
    @Url            nvarchar(max),
    @Summary        nvarchar(max),
    @Image          nvarchar(max),
    @IncludeImageInPost bit,
	@Published		bit,
	@ModifiedOn		datetime,
	@MetaInformationId int,
	@MetaKeywords   nvarchar(max),
	@MetaDescription   nvarchar(max),
	@MetaUrl		nvarchar(max),
	@PageHeaderTags	nvarchar(max),
	@Tags			nvarchar(max),
	@AudioFile_Mp3FileName  nvarchar(max),	
	@AudioFile_Mp3UploadedFileName nvarchar(max),	
	@AudioFile_OggFileName  nvarchar(max),
	@AudioFile_OggUploadedFileName nvarchar(max),
	@AudioFile_Title nvarchar(max)
AS
BEGIN
    --meta
	DECLARE @metaID int;
	SELECT @metaID = ID FROM {databaseOwner}[{objectQualifier}wsc_PostIt_MetaInformation]
		  		  WHERE  Url = @MetaUrl	
	UPDATE {databaseOwner}[{objectQualifier}wsc_PostIt_MetaInformation]
		set    Keywords		= @MetaKeywords,
			   Description	= @MetaDescription,
			   PageHeaderTags = @PageHeaderTags
		WHERE  Url = @MetaUrl	
	IF @@ROWCOUNT=0
	BEGIN
		INSERT INTO {databaseOwner}[{objectQualifier}wsc_PostIt_MetaInformation] (
			Url,
			Keywords,
			Description,
			PageHeaderTags
		) 
		values (
			@MetaUrl,
			@MetaKeywords,
			@MetaDescription,
			@PageHeaderTags
		)
		set @metaID = scope_identity()
	END;
	
	--Audio File
	DECLARE @audioFileId int;
	INSERT INTO {databaseOwner}[{objectQualifier}wsc_PostIt_AudioFile] (
			[Mp3FileName],
			[Mp3UploadedFileName],
			[OggFileName],
			[OggUploadedFileName],
			[Title]
		) 
		values (
			@AudioFile_Mp3FileName,
			@AudioFile_Mp3UploadedFileName,
			@AudioFile_OggFileName,
			@AudioFile_OggUploadedFileName,
			@AudioFile_Title
		)
		set @audioFileID = scope_identity()

	--Post
	INSERT INTO {databaseOwner}[{objectQualifier}wsc_PostIt_Post] (
		ModuleId,
		Content,
		CreatedByUser,
		Date,
		Title,
		Url,
		Summary,
		[Image],
		IncludeImageInPost,
		Published,
		ModifiedOn,
		MetaInformationId,
		AudioFileId
	) 
	values (
		@ModuleId,
		@Content,
		@CreatedByUser,
		@Date,
		@Title,
		@Url,
		@Summary,
		@Image,
		@IncludeImageInPost,
		@Published,
		@ModifiedOn,
		@metaID,
		@AudioFileId
	)

	--Tags
	DECLARE @postID int
	set @postID = scope_identity()

	Declare @RowData nvarchar(max)
	Set @RowData = @Tags
	Declare @SplitOn nvarchar(2)
	Set @SplitOn = ', '
	Declare @TmpTags table 
	(
		Name nvarchar(256)
	) 
	While (Charindex(@SplitOn,@RowData)>0)
	Begin
		Insert Into @TmpTags (Name)
		Select Name = ltrim(rtrim(Substring(@RowData,1,Charindex(@SplitOn,@RowData)-1)))

		Set @RowData = Substring(@RowData,Charindex(@SplitOn,@RowData)+1,len(@RowData))
	End
	Insert Into @TmpTags (name)
	Select Name = ltrim(rtrim(@RowData))

	INSERT INTO {databaseOwner}[{objectQualifier}wsc_PostIt_Tags] ([Name], [ModuleID])
    SELECT [Name], @ModuleId 
    FROM @TmpTags 
    WHERE [Name] <> '' AND [Name] NOT IN (SELECT [Name] 
                        FROM {databaseOwner}[{objectQualifier}wsc_PostIt_Tags]
						WHERE [ModuleID] = @ModuleId)

	INSERT INTO {databaseOwner}[{objectQualifier}wsc_PostIt_PostTag] ([TagID],[PostID])
    SELECT t.ID, @postID
    FROM @TmpTags tmpt
	INNER JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_Tags] t ON (t.Name = tmpt.Name AND t.ModuleID=@ModuleId)
END
GO

ALTER PROCEDURE {databaseOwner}[{objectQualifier}wsc_PostIt_DeletePost]
	@ModuleId       int,
    @ItemId         int
AS
BEGIN
	DECLARE @metaID int;
	DECLARE @audioFileId int;
	
	SELECT @metaID = MetaInformationId, @audioFileId = AudioFileId 
	FROM {databaseOwner}[{objectQualifier}wsc_PostIt_Post]
    WHERE  ModuleId = @ModuleId AND ItemId = @ItemId
    
    --post
	DELETE
	FROM   {databaseOwner}[{objectQualifier}wsc_PostIt_Post]
	WHERE  ModuleId = @ModuleId AND ItemId = @ItemId

	--Meta
	DELETE
	FROM   {databaseOwner}[{objectQualifier}wsc_PostIt_MetaInformation]
	WHERE  ID = @metaID
	
	--Audio file
	DELETE
	FROM   {databaseOwner}[{objectQualifier}wsc_PostIt_AudioFile]
	WHERE AudioFileId = @audioFileId

	--Tags
	DELETE FROM {databaseOwner}[{objectQualifier}wsc_PostIt_Tags]
    WHERE ID IN (SELECT t.ID
	             FROM {databaseOwner}[{objectQualifier}wsc_PostIt_Tags] t
			     LEFT OUTER JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_PostTag] pt ON pt.TagID = t.ID
			     WHERE pt.ID IS NULL)
END

GO

ALTER PROCEDURE {databaseOwner}[{objectQualifier}wsc_PostIt_GetAllPosts]

	@moduleId int,
	@includeUnpublished bit
AS
BEGIN
	SELECT p.*, 
			audio.Mp3FileName AudioFile_Mp3FileName,
			audio.Mp3UploadedFileName AudioFile_Mp3UploadedFileName,
			audio.OggFileName AudioFile_OggFileName,
			audio.OggUploadedFileName AudioFile_OggUploadedFileName,
			audio.Title AudioFile_Title,			
			 u.FirstName + ' ' + u.LastName AS CreatedByUserName,
			 stuff((select ', ' + t.Name 
               FROM {databaseOwner}[{objectQualifier}wsc_PostIt_PostTag] pt
			   INNER JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_Tags] t ON pt.TagID = t.ID			   
               WHERE pt.PostID = p.ItemID
               FOR XML PATH ('')
             ), 1,2,'') AS Tags
	  FROM {databaseOwner}[{objectQualifier}wsc_PostIt_Post] p 
	  LEFT JOIN {databaseOwner}[{objectQualifier}Users] u ON p.CreatedByUser = u.UserId
	  LEFT JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_AudioFile]	audio ON p.AudioFileId = audio.AudioFileId
	  WHERE p.ModuleId = @moduleId AND (@includeUnpublished = 1 OR p.Published = 1)
END

GO

ALTER PROCEDURE {databaseOwner}[{objectQualifier}wsc_PostIt_GetPaggedPosts]
	@moduleId int,
	@includeUnpublished bit,
	@start int,
	@limit int
AS
BEGIN
	declare @end int;
	set @end = @start + @limit - 1;

	WITH ordered_posts AS (
	  SELECT p.*, 
			 audio.Mp3FileName AudioFile_Mp3FileName,
			 audio.Mp3UploadedFileName AudioFile_Mp3UploadedFileName,
			 audio.OggFileName AudioFile_OggFileName,
			 audio.OggUploadedFileName AudioFile_OggUploadedFileName,
			 audio.Title AudioFile_Title,
			 u.FirstName + ' ' + u.LastName AS CreatedByUserName,
			 stuff((select ', ' + t.Name 
               FROM {databaseOwner}[{objectQualifier}wsc_PostIt_PostTag] pt
			   INNER JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_Tags] t ON pt.TagID = t.ID
               WHERE pt.PostID = p.ItemID
               FOR XML PATH ('')
             ), 1,2,'') AS Tags,
			 row_number() OVER(
               ORDER BY p.Date DESC, p.ItemId DESC
			 ) AS row_num
	  FROM {databaseOwner}[{objectQualifier}wsc_PostIt_Post] p 
	  LEFT JOIN {databaseOwner}[{objectQualifier}Users] u ON p.CreatedByUser = u.UserId
	  LEFT JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_AudioFile]	audio ON p.AudioFileId = audio.AudioFileId
	  WHERE p.ModuleId = @moduleId AND (@includeUnpublished = 1 OR p.Published = 1)
	)

	SELECT * 
	FROM ordered_posts
	WHERE row_num BETWEEN @start AND @end
END

GO

ALTER PROCEDURE {databaseOwner}[{objectQualifier}wsc_PostIt_GetPaggedPostsByArchiveItem]
	@moduleId int,
	@includeUnpublished bit,	
	@start int,
	@limit int,
	@month int,
	@year int
AS
BEGIN
	DECLARE @end int;
	SET @end = @start + @limit - 1;

	WITH ordered_posts AS (
	  SELECT p.*, 
			 audio.Mp3FileName AudioFile_Mp3FileName,
			 audio.Mp3UploadedFileName AudioFile_Mp3UploadedFileName,
			 audio.OggFileName AudioFile_OggFileName,
			 audio.OggUploadedFileName AudioFile_OggUploadedFileName,
			 audio.Title AudioFile_Title,
			 u.FirstName + ' ' + u.LastName AS CreatedByUserName,
			 stuff((select ', ' + t.Name 
               FROM {databaseOwner}[{objectQualifier}wsc_PostIt_PostTag] pt
			   INNER JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_Tags] t ON pt.TagID = t.ID
               WHERE pt.PostID = p.ItemID
               FOR XML PATH ('')
             ), 1,2,'') AS Tags,
			 row_number() OVER(
               ORDER BY p.Date DESC, p.ItemId DESC
			 ) AS row_num
	  FROM {databaseOwner}[{objectQualifier}wsc_PostIt_Post] p 
	  LEFT JOIN {databaseOwner}[{objectQualifier}Users] u ON p.CreatedByUser = u.UserId
	  LEFT JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_AudioFile]	audio ON p.AudioFileId = audio.AudioFileId
	  WHERE p.ModuleId = @moduleId AND (@includeUnpublished = 1 OR p.Published = 1) AND (MONTH(Date) = @month) AND (YEAR(DATE) = @year)
	)

	SELECT * 
	FROM ordered_posts
	WHERE row_num BETWEEN @start AND @end
END

GO

ALTER PROCEDURE {databaseOwner}[{objectQualifier}wsc_PostIt_GetPaggedPostsByTag] 
	@moduleId int,
	@includeUnpublished bit,	
	@start int,
	@limit int,
	@tag nvarchar(max)
AS
BEGIN
	declare @end int;
	set @end = @start + @limit - 1;

	WITH ordered_posts AS (
	  SELECT p.*, 
			 audio.Mp3FileName AudioFile_Mp3FileName,
			 audio.Mp3UploadedFileName AudioFile_Mp3UploadedFileName,
			 audio.OggFileName AudioFile_OggFileName,
			 audio.OggUploadedFileName AudioFile_OggUploadedFileName,
			 audio.Title AudioFile_Title,
			 u.FirstName + ' ' + u.LastName AS CreatedByUserName,
			 stuff((select ', ' + t.Name 
               FROM {databaseOwner}[{objectQualifier}wsc_PostIt_PostTag] pt
			   INNER JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_Tags] t ON pt.TagID = t.ID
               WHERE pt.PostID = p.ItemID
               FOR XML PATH ('')
             ), 1,2,'') AS Tags,
			 row_number() OVER(
               ORDER BY p.Date DESC, p.ItemId DESC
			 ) AS row_num
	  FROM {databaseOwner}[{objectQualifier}wsc_PostIt_Post] p 
	  LEFT JOIN {databaseOwner}[{objectQualifier}Users] u ON p.CreatedByUser = u.UserId
	  LEFT JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_AudioFile]	audio ON p.AudioFileId = audio.AudioFileId
	  INNER JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_PostTag] postTag ON postTag.[PostID] = p.[ItemID] 
      INNER JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_Tags] tag ON postTag.[TagID]=tag.[ID]
	  WHERE p.ModuleId = @moduleId AND (@includeUnpublished = 1 OR p.Published = 1) AND tag.[Name]=@tag
	)

	SELECT * 
	FROM ordered_posts
	WHERE row_num BETWEEN @start AND @end
END

GO

ALTER PROCEDURE {databaseOwner}[{objectQualifier}wsc_PostIt_GetPostById]
	@ModuleId int,
    @ItemId int,
	@includeUnpublished bit
AS
BEGIN
	SELECT p.*, 
		    audio.Mp3FileName AudioFile_Mp3FileName,
			audio.Mp3UploadedFileName AudioFile_Mp3UploadedFileName,
			audio.OggFileName AudioFile_OggFileName,
			audio.OggUploadedFileName AudioFile_OggUploadedFileName,
			audio.Title AudioFile_Title,
		   u.FirstName + ' ' + u.LastName AS CreatedByUserName,
		   stuff((select ', ' + t.Name 
               FROM {databaseOwner}[{objectQualifier}wsc_PostIt_PostTag] pt
			   INNER JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_Tags] t ON pt.TagID = t.ID
               WHERE pt.PostID = p.ItemID
               FOR XML PATH ('')
             ), 1,2,'') AS Tags,
		   m.Keywords AS MetaKeywords,
		   m.Description AS MetaDescription,
		   m.PageHeaderTags AS PageHeaderTags
	FROM {databaseOwner}[{objectQualifier}wsc_PostIt_Post] p
	LEFT JOIN {databaseOwner}[{objectQualifier}Users] u ON p.CreatedByUser = u.UserId
	LEFT JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_MetaInformation] m ON p.MetaInformationId = m.ID
	LEFT JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_AudioFile]	audio ON p.AudioFileId = audio.AudioFileId
	WHERE  p.ModuleId = @ModuleId AND p.ItemId = @ItemId AND
           (@includeUnpublished = 1 OR p.Published = 1)
END


GO

ALTER PROCEDURE {databaseOwner}[{objectQualifier}wsc_PostIt_GetPostByUrl] 
	@ModuleId int,
    @Url nvarchar(max),
	@includeUnpublished bit
AS
BEGIN
	SELECT p.*, 
		    audio.Mp3FileName AudioFile_Mp3FileName,
			audio.Mp3UploadedFileName AudioFile_Mp3UploadedFileName,
			audio.OggFileName AudioFile_OggFileName,
			audio.OggUploadedFileName AudioFile_OggUploadedFileName,
			audio.Title AudioFile_Title,
           u.FirstName + ' ' + u.LastName AS CreatedByUserName,
		   stuff((select ', ' + t.Name 
               FROM {databaseOwner}[{objectQualifier}wsc_PostIt_PostTag] pt
			   INNER JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_Tags] t ON pt.TagID = t.ID
               WHERE pt.PostID = p.ItemID
               FOR XML PATH ('')
             ), 1,2,'') AS Tags,
		   m.Keywords AS MetaKeywords,
		   m.Description AS MetaDescription,
		   m.PageHeaderTags AS PageHeaderTags
	FROM {databaseOwner}[{objectQualifier}wsc_PostIt_Post] p
	LEFT JOIN {databaseOwner}[{objectQualifier}Users] u ON p.CreatedByUser = u.UserId
	LEFT JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_MetaInformation] m ON p.MetaInformationId = m.ID
	LEFT JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_AudioFile]	audio ON p.AudioFileId = audio.AudioFileId
	WHERE p.ModuleId = @ModuleId AND p.Url = @Url AND
	      (@includeUnpublished = 1 OR p.Published = 1)
END

GO

ALTER PROCEDURE {databaseOwner}[{objectQualifier}wsc_PostIt_UpdatePost]
	@ModuleId       int,
    @ItemId         int,
	@Content        nvarchar(max),
	@CreatedByUser  int,
    @Date           datetime,
    @Title          nvarchar(max),
    @Url            nvarchar(max),
    @Summary        nvarchar(max),
    @Image          nvarchar(max),
    @IncludeImageInPost bit,
	@Published		bit,
	@ModifiedOn		datetime,
	@MetaInformationId int,
	@MetaKeywords   nvarchar(max),
	@MetaDescription   nvarchar(max),
	@MetaUrl		nvarchar(max),
	@PageHeaderTags nvarchar(max),
	@Tags			nvarchar(max),
	@AudioFile_Mp3FileName  nvarchar(max),
	@AudioFile_Mp3UploadedFileName nvarchar(max),
	@AudioFile_OggFileName  nvarchar(max),
	@AudioFile_OggUploadedFileName nvarchar(max),
	@AudioFile_Title  nvarchar(max)
AS
BEGIN
    --meta
    DECLARE @metaID int;
	set @metaID = @MetaInformationId	
	UPDATE {databaseOwner}[{objectQualifier}wsc_PostIt_MetaInformation]
	set    Url			= @MetaUrl,
		   Keywords		= @MetaKeywords,
		   Description	= @MetaDescription,
		   PageHeaderTags = @PageHeaderTags
	WHERE  ID = @MetaInformationId;
	IF @@ROWCOUNT=0
	BEGIN
		INSERT INTO {databaseOwner}[{objectQualifier}wsc_PostIt_MetaInformation] (
			Url,
			Keywords,
			Description,
			PageHeaderTags
		) 
		values (
			@MetaUrl,
			@MetaKeywords,
			@MetaDescription,
			@PageHeaderTags
		)
		set @metaID = scope_identity()
	END
	
	--audio file
	DECLARE @audioFileId int;
	SET @audioFileId =
		(SELECT AudioFileId 
		FROM {databaseOwner}[{objectQualifier}wsc_PostIt_Post] p
		WHERE p.ItemID = @ItemId)

	IF @audioFileId IS NULL 
	BEGIN
		INSERT INTO {databaseOwner}{objectQualifier}wsc_PostIt_AudioFile (
			Mp3FileName,
			Mp3UploadedFileName,
			OggFileName,
			OggUploadedFileName,
			Title
		)
		values (
			@AudioFile_Mp3FileName,
			@AudioFile_Mp3UploadedFileName,
			@AudioFile_OggFileName,
			@AudioFile_OggUploadedFileName,
			@AudioFile_Title
		)
		SET @audioFileId = SCOPE_IDENTITY()
	END
	ELSE
	BEGIN
		UPDATE {databaseOwner}[{objectQualifier}wsc_PostIt_AudioFile]
			SET Mp3FileName = @AudioFile_Mp3FileName,
				Mp3UploadedFileName = @AudioFile_Mp3UploadedFileName,
				OggFileName = @AudioFile_OggFileName,
				OggUploadedFileName = @AudioFile_OggUploadedFileName,
				Title = @AudioFile_Title				
			WHERE AudioFileId = @audioFileId
	END
		
		
	--post
	UPDATE {databaseOwner}[{objectQualifier}wsc_PostIt_Post]
	set    Content       = @Content,
		   CreatedByUser = @CreatedByUser,
		   Date	         = @Date,
		   Title         = @Title,
		   Url           = @Url,
		   Summary       = @Summary,
		   [Image]       = @Image,
		   IncludeImageInPost = @IncludeImageInPost,
		   Published     = @Published,
		   ModifiedOn    = @ModifiedOn,
		   MetaInformationId= @metaID,
		   AudioFileId = @audioFileId
	WHERE  ModuleId = @ModuleId AND ItemId = @ItemId

	--tags
	Declare @RowData nvarchar(max)
	Set @RowData = @Tags
	Declare @SplitOn nvarchar(2)
	Set @SplitOn = ', '
	Declare @TmpTags table 
	(
		Name nvarchar(256)
	) 
	While (Charindex(@SplitOn,@RowData)>0)
	Begin
		Insert Into @TmpTags (Name)
		Select Name = ltrim(rtrim(Substring(@RowData,1,Charindex(@SplitOn,@RowData)-1)))
		Set @RowData = Substring(@RowData,Charindex(@SplitOn,@RowData)+1,len(@RowData))
	End
	Insert Into @TmpTags (name)
	Select Name = ltrim(rtrim(@RowData))

	--insert new tags
	INSERT INTO {databaseOwner}[{objectQualifier}wsc_PostIt_Tags] ([Name], [ModuleID])
    SELECT [Name], @ModuleId 
    FROM @TmpTags 
    WHERE [Name] <> '' AND [Name] NOT IN (SELECT [Name] 
                        FROM {databaseOwner}[{objectQualifier}wsc_PostIt_Tags]
						WHERE [ModuleID] = @ModuleId)

    --insert new posttags
    INSERT INTO {databaseOwner}[{objectQualifier}wsc_PostIt_PostTag] ([TagID],[PostID])
    SELECT t.ID, @itemID
    FROM @TmpTags tmpt
	INNER JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_Tags] t ON (t.[Name] = tmpt.[Name] AND t.[ModuleID]=@ModuleId)
    WHERE t.[ID] NOT IN (SELECT [TagID] 
                         FROM {databaseOwner}[{objectQualifier}wsc_PostIt_PostTag] 
                         WHERE [PostID] = @itemID)
		
    --delete unused posttags
	DELETE FROM {databaseOwner}[{objectQualifier}wsc_PostIt_PostTag]
    WHERE ID IN (SELECT pt.ID
	             FROM {databaseOwner}[{objectQualifier}wsc_PostIt_PostTag] pt
			     INNER JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_Tags] t ON t.ID = pt.TagID
			     WHERE  pt.PostID = @itemID AND t.[Name] NOT IN (SELECT [Name] 
                                                               FROM @TmpTags))

	--delete unused tags
	DELETE FROM {databaseOwner}[{objectQualifier}wsc_PostIt_Tags]
    WHERE ID IN (SELECT t.ID
	             FROM {databaseOwner}[{objectQualifier}wsc_PostIt_Tags] t
			     LEFT OUTER JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_PostTag] pt ON pt.TagID = t.ID
			     WHERE pt.ID IS NULL)
END
GO