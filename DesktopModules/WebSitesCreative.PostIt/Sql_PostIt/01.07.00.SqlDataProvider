﻿/*********************************************************/
/*****                                               *****/
/*****                 RENAME  TABLES                *****/
/*****                                               *****/
/*********************************************************/

EXEC sp_rename '{databaseOwner}[{objectQualifier}WebSitesCreative_Post]', '{objectQualifier}wsc_PostIt_Post'
GO

EXEC sp_rename '{databaseOwner}[{objectQualifier}WebSitesCreative_PostTag]', '{objectQualifier}wsc_PostIt_PostTag'
GO

EXEC sp_rename '{databaseOwner}[{objectQualifier}WebSitesCreative_Tags]', '{objectQualifier}wsc_PostIt_Tags'
GO

EXEC sp_rename '{databaseOwner}[{objectQualifier}WebSitesCreative_MetaInformation]', '{objectQualifier}wsc_PostIt_MetaInformation'
GO


/*********************************************************/
/*****                                               *****/
/*****               RENAME PROCEDURES               *****/
/*****                                               *****/
/*********************************************************/

EXEC sp_rename '{databaseOwner}[{objectQualifier}WebSitesCreative_CreatePost]', '{objectQualifier}wsc_PostIt_CreatePost'
GO

EXEC sp_rename '{databaseOwner}[{objectQualifier}WebSitesCreative_DeletePost]', '{objectQualifier}wsc_PostIt_DeletePost'
GO

EXEC sp_rename '{databaseOwner}[{objectQualifier}WebSitesCreative_GetAllArchiveItems]', '{objectQualifier}wsc_PostIt_GetAllArchiveItems'
GO

EXEC sp_rename '{databaseOwner}[{objectQualifier}WebSitesCreative_GetAllPosts]', '{objectQualifier}wsc_PostIt_GetAllPosts'
GO

EXEC sp_rename '{databaseOwner}[{objectQualifier}WebSitesCreative_GetAllPostsCount]', '{objectQualifier}wsc_PostIt_GetAllPostsCount'
GO

EXEC sp_rename '{databaseOwner}[{objectQualifier}WebSitesCreative_GetAllTags]', '{objectQualifier}wsc_PostIt_GetAllTags'
GO

EXEC sp_rename '{databaseOwner}[{objectQualifier}WebSitesCreative_GetCountPostsByArchiveItem]', '{objectQualifier}wsc_PostIt_GetCountPostsByArchiveItem'
GO

EXEC sp_rename '{databaseOwner}[{objectQualifier}WebSitesCreative_GetCountPostsByTag]', '{objectQualifier}wsc_PostIt_GetCountPostsByTag'
GO

EXEC sp_rename '{databaseOwner}[{objectQualifier}WebSitesCreative_GetPaggedPosts]', '{objectQualifier}wsc_PostIt_GetPaggedPosts'
GO

EXEC sp_rename '{databaseOwner}[{objectQualifier}WebSitesCreative_GetPaggedPostsByArchiveItem]', '{objectQualifier}wsc_PostIt_GetPaggedPostsByArchiveItem'
GO

EXEC sp_rename '{databaseOwner}[{objectQualifier}WebSitesCreative_GetPaggedPostsByTag]', '{objectQualifier}wsc_PostIt_GetPaggedPostsByTag'
GO

EXEC sp_rename '{databaseOwner}[{objectQualifier}WebSitesCreative_GetPostById]', '{objectQualifier}wsc_PostIt_GetPostById'
GO

EXEC sp_rename '{databaseOwner}[{objectQualifier}WebSitesCreative_GetPostByUrl]', '{objectQualifier}wsc_PostIt_GetPostByUrl'
GO

EXEC sp_rename '{databaseOwner}[{objectQualifier}WebSitesCreative_UpdatePost]', '{objectQualifier}wsc_PostIt_UpdatePost'
GO

/*********************************************************/
/*****                                               *****/
/*****                  CREATE POST                  *****/
/*****                                               *****/
/*********************************************************/
ALTER PROCEDURE {databaseOwner}[{objectQualifier}wsc_PostIt_CreatePost]
    @ModuleId       int,
    @ItemId         int,
	@Content        nvarchar(max),
	@CreatedByUser  int,
    @Date           datetime,
	@Title          nvarchar(max),
    @Url            nvarchar(max),
    @Summary        nvarchar(max),
    @Image          nvarchar(max),
    @IncludeImageInPost bit,
	@Published		bit,
	@ModifiedOn		datetime,
	@MetaInformationId int,
	@MetaKeywords   nvarchar(max),
	@MetaDescription   nvarchar(max),
	@MetaUrl		nvarchar(max),
	@PageHeaderTags	nvarchar(max),
	@Tags			nvarchar(max)
AS
BEGIN
    --meta
	DECLARE @metaID int;
	SELECT @metaID = ID FROM {databaseOwner}[{objectQualifier}wsc_PostIt_MetaInformation]
		  		  WHERE  Url = @MetaUrl	
	UPDATE {databaseOwner}[{objectQualifier}wsc_PostIt_MetaInformation]
		set    Keywords		= @MetaKeywords,
			   Description	= @MetaDescription,
			   PageHeaderTags = @PageHeaderTags
		WHERE  Url = @MetaUrl	
	IF @@ROWCOUNT=0
	BEGIN
		INSERT INTO {databaseOwner}[{objectQualifier}wsc_PostIt_MetaInformation] (
			Url,
			Keywords,
			Description,
			PageHeaderTags
		) 
		values (
			@MetaUrl,
			@MetaKeywords,
			@MetaDescription,
			@PageHeaderTags
		)
		set @metaID = scope_identity()
	END;

	--Post
	INSERT INTO {databaseOwner}[{objectQualifier}wsc_PostIt_Post] (
		ModuleId,
		Content,
		CreatedByUser,
		Date,
		Title,
		Url,
		Summary,
		[Image],
		IncludeImageInPost,
		Published,
		ModifiedOn,
		MetaInformationId
	) 
	values (
		@ModuleId,
		@Content,
		@CreatedByUser,
		@Date,
		@Title,
		@Url,
		@Summary,
		@Image,
		@IncludeImageInPost,
		@Published,
		@ModifiedOn,
		@metaID
	)

	--Tags
	DECLARE @postID int
	set @postID = scope_identity()

	Declare @RowData nvarchar(max)
	Set @RowData = @Tags
	Declare @SplitOn nvarchar(2)
	Set @SplitOn = ', '
	Declare @TmpTags table 
	(
		Name nvarchar(256)
	) 
	While (Charindex(@SplitOn,@RowData)>0)
	Begin
		Insert Into @TmpTags (Name)
		Select Name = ltrim(rtrim(Substring(@RowData,1,Charindex(@SplitOn,@RowData)-1)))

		Set @RowData = Substring(@RowData,Charindex(@SplitOn,@RowData)+1,len(@RowData))
	End
	Insert Into @TmpTags (name)
	Select Name = ltrim(rtrim(@RowData))

	INSERT INTO {databaseOwner}[{objectQualifier}wsc_PostIt_Tags] ([Name], [ModuleID])
    SELECT [Name], @ModuleId 
    FROM @TmpTags 
    WHERE [Name] <> '' AND [Name] NOT IN (SELECT [Name] 
                        FROM {databaseOwner}[{objectQualifier}wsc_PostIt_Tags]
						WHERE [ModuleID] = @ModuleId)

	INSERT INTO {databaseOwner}[{objectQualifier}wsc_PostIt_PostTag] ([TagID],[PostID])
    SELECT t.ID, @postID
    FROM @TmpTags tmpt
	INNER JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_Tags] t ON (t.Name = tmpt.Name AND t.ModuleID=@ModuleId)
END

GO
/*********************************************************/
/*****                                               *****/
/*****                  UPDATE POST                  *****/
/*****                                               *****/
/*********************************************************/
ALTER PROCEDURE {databaseOwner}[{objectQualifier}wsc_PostIt_UpdatePost]
	@ModuleId       int,
    @ItemId         int,
	@Content        nvarchar(max),
	@CreatedByUser  int,
    @Date           datetime,
    @Title          nvarchar(max),
    @Url            nvarchar(max),
    @Summary        nvarchar(max),
    @Image          nvarchar(max),
    @IncludeImageInPost bit,
	@Published		bit,
	@ModifiedOn		datetime,
	@MetaInformationId int,
	@MetaKeywords   nvarchar(max),
	@MetaDescription   nvarchar(max),
	@MetaUrl		nvarchar(max),
	@PageHeaderTags nvarchar(max),
	@Tags			nvarchar(max)
AS
BEGIN
    --meta
    DECLARE @metaID int;
	set @metaID = @MetaInformationId	
	UPDATE {databaseOwner}[{objectQualifier}wsc_PostIt_MetaInformation]
	set    Url			= @MetaUrl,
		   Keywords		= @MetaKeywords,
		   Description	= @MetaDescription,
		   PageHeaderTags = @PageHeaderTags
	WHERE  ID = @MetaInformationId;
	IF @@ROWCOUNT=0
	BEGIN
		INSERT INTO {databaseOwner}[{objectQualifier}wsc_PostIt_MetaInformation] (
			Url,
			Keywords,
			Description,
			PageHeaderTags
		) 
		values (
			@MetaUrl,
			@MetaKeywords,
			@MetaDescription,
			@PageHeaderTags
		)
		set @metaID = scope_identity()
	END

	--post
	UPDATE {databaseOwner}[{objectQualifier}wsc_PostIt_Post]
	set    Content       = @Content,
		   CreatedByUser = @CreatedByUser,
		   Date	         = @Date,
		   Title         = @Title,
		   Url           = @Url,
		   Summary       = @Summary,
		   [Image]       = @Image,
		   IncludeImageInPost = @IncludeImageInPost,
		   Published     = @Published,
		   ModifiedOn    = @ModifiedOn,
		   MetaInformationId= @metaID
	WHERE  ModuleId = @ModuleId AND ItemId = @ItemId

	--tags
	Declare @RowData nvarchar(max)
	Set @RowData = @Tags
	Declare @SplitOn nvarchar(2)
	Set @SplitOn = ', '
	Declare @TmpTags table 
	(
		Name nvarchar(256)
	) 
	While (Charindex(@SplitOn,@RowData)>0)
	Begin
		Insert Into @TmpTags (Name)
		Select Name = ltrim(rtrim(Substring(@RowData,1,Charindex(@SplitOn,@RowData)-1)))
		Set @RowData = Substring(@RowData,Charindex(@SplitOn,@RowData)+1,len(@RowData))
	End
	Insert Into @TmpTags (name)
	Select Name = ltrim(rtrim(@RowData))

	--insert new tags
	INSERT INTO {databaseOwner}[{objectQualifier}wsc_PostIt_Tags] ([Name], [ModuleID])
    SELECT [Name], @ModuleId 
    FROM @TmpTags 
    WHERE [Name] <> '' AND [Name] NOT IN (SELECT [Name] 
                        FROM {databaseOwner}[{objectQualifier}wsc_PostIt_Tags]
						WHERE [ModuleID] = @ModuleId)

    --insert new posttags
    INSERT INTO {databaseOwner}[{objectQualifier}wsc_PostIt_PostTag] ([TagID],[PostID])
    SELECT t.ID, @itemID
    FROM @TmpTags tmpt
	INNER JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_Tags] t ON (t.[Name] = tmpt.[Name] AND t.[ModuleID]=@ModuleId)
    WHERE t.[ID] NOT IN (SELECT [TagID] 
                         FROM {databaseOwner}[{objectQualifier}wsc_PostIt_PostTag] 
                         WHERE [PostID] = @itemID)
		
    --delete unused posttags
	DELETE FROM {databaseOwner}[{objectQualifier}wsc_PostIt_PostTag]
    WHERE ID IN (SELECT pt.ID
	             FROM {databaseOwner}[{objectQualifier}wsc_PostIt_PostTag] pt
			     INNER JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_Tags] t ON t.ID = pt.TagID
			     WHERE  pt.PostID = @itemID AND t.[Name] NOT IN (SELECT [Name] 
                                                               FROM @TmpTags))

	--delete unused tags
	DELETE FROM {databaseOwner}[{objectQualifier}wsc_PostIt_Tags]
    WHERE ID IN (SELECT t.ID
	             FROM {databaseOwner}[{objectQualifier}wsc_PostIt_Tags] t
			     LEFT OUTER JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_PostTag] pt ON pt.TagID = t.ID
			     WHERE pt.ID IS NULL)
END
GO

/*********************************************************/
/*****                                               *****/
/*****            GET COUNT POSTS BY TAG             *****/
/*****                                               *****/
/*********************************************************/
ALTER PROCEDURE {databaseOwner}[{objectQualifier}wsc_PostIt_GetCountPostsByTag] 
	@moduleId int,
	@includeUnpublished bit,
	@tag nvarchar(max)
AS
BEGIN

	SELECT Count(p.ItemId)
	FROM {databaseOwner}[{objectQualifier}wsc_PostIt_Post] p 
	INNER JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_PostTag] postTag ON postTag.[PostID] = p.[ItemID] 
    INNER JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_Tags] tag ON postTag.[TagID]=tag.[ID]
	WHERE p.ModuleId = @moduleId AND (@includeUnpublished = 1 OR p.Published = 1) AND tag.[Name]=@tag
	
END
GO

/*********************************************************/
/*****                                               *****/
/*****        GET COUNT POSTS BY ARCHIVE ITEM        *****/
/*****                                               *****/
/*********************************************************/
ALTER PROCEDURE {databaseOwner}[{objectQualifier}wsc_PostIt_GetCountPostsByArchiveItem] 
	@moduleId int,
	@includeUnpublished bit,
	@month int,
	@year int
AS
BEGIN
	SELECT Count(p.ItemId)
	FROM {databaseOwner}[{objectQualifier}wsc_PostIt_Post] p 
	WHERE p.ModuleId = @moduleId AND (@includeUnpublished = 1 OR p.Published = 1) AND (MONTH(Date) = @month) AND (YEAR(DATE) = @year)
END
GO

/*********************************************************/
/*****                                               *****/
/*****                 GET ALL TAGS                  *****/
/*****                                               *****/
/*********************************************************/
ALTER PROCEDURE {databaseOwner}[{objectQualifier}wsc_PostIt_GetAllTags]
	@moduleId int,
	@includeUnpublished bit
AS
BEGIN	
	SELECT t.ID, t.[Name], COUNT(t.ID) AS [COUNT]
	FROM {databaseOwner}[{objectQualifier}wsc_PostIt_PostTag] pt
	INNER JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_Tags] t on t.ID = pt.[TagID]
	INNER JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_Post] p on p.ItemID = pt.[PostID]
	WHERE t.ModuleId = @moduleId AND (@includeUnpublished = 1 OR p.Published = 1)
	GROUP BY t.ID, t.[Name]
	ORDER BY t.[Name] asc
END
GO

/*********************************************************/
/*****                                               *****/
/*****            GET ALL ARCHIVE ITEMS              *****/
/*****                                               *****/
/*********************************************************/
ALTER PROCEDURE {databaseOwner}[{objectQualifier}wsc_PostIt_GetAllArchiveItems]
	@moduleId int,
	@includeUnpublished bit
AS
BEGIN	
SELECT MONTH(Date) AS [Month], 
 YEAR(DATE) AS [Year], 
 COUNT(p.ItemId) AS [Count]
 FROM {databaseOwner}[{objectQualifier}wsc_PostIt_Post] p
 WHERE p.ModuleId = @moduleId AND (@includeUnpublished = 1 OR p.Published = 1)
 GROUP BY YEAR(DATE), MONTH(Date)
 ORDER BY [Year] DESC, [Month] DESC
END
GO

/*********************************************************/
/*****                                               *****/
/*****                DELETE POST                    *****/
/*****                                               *****/
/*********************************************************/
ALTER PROCEDURE {databaseOwner}[{objectQualifier}wsc_PostIt_DeletePost]
	@ModuleId       int,
    @ItemId         int
AS
BEGIN
	DECLARE @metaID int;
	SELECT @metaID = MetaInformationId FROM {databaseOwner}[{objectQualifier}wsc_PostIt_Post]
		  		  WHERE  ModuleId = @ModuleId AND ItemId = @ItemId
    --post
	DELETE
	FROM   {databaseOwner}[{objectQualifier}wsc_PostIt_Post]
	WHERE  ModuleId = @ModuleId AND ItemId = @ItemId

	--Meta
	DELETE
	FROM   {databaseOwner}[{objectQualifier}wsc_PostIt_MetaInformation]
	WHERE  ID = @metaID

	--Tags
	DELETE FROM {databaseOwner}[{objectQualifier}wsc_PostIt_Tags]
    WHERE ID IN (SELECT t.ID
	             FROM {databaseOwner}[{objectQualifier}wsc_PostIt_Tags] t
			     LEFT OUTER JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_PostTag] pt ON pt.TagID = t.ID
			     WHERE pt.ID IS NULL)
END
GO

/*********************************************************/
/*****                                               *****/
/*****               GET POST BY URL                 *****/
/*****                                               *****/
/*********************************************************/
ALTER PROCEDURE {databaseOwner}[{objectQualifier}wsc_PostIt_GetPostByUrl] 
	@ModuleId int,
    @Url nvarchar(max),
	@includeUnpublished bit
AS
BEGIN
	SELECT p.*,
           u.FirstName + ' ' + u.LastName AS CreatedByUserName,
		   stuff((select ', ' + t.Name 
               FROM {databaseOwner}[{objectQualifier}wsc_PostIt_PostTag] pt
			   INNER JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_Tags] t ON pt.TagID = t.ID
               WHERE pt.PostID = p.ItemID
               FOR XML PATH ('')
             ), 1,2,'') AS Tags,
		   m.Keywords AS MetaKeywords,
		   m.Description AS MetaDescription,
		   m.PageHeaderTags AS PageHeaderTags
	FROM {databaseOwner}[{objectQualifier}wsc_PostIt_Post] p
	LEFT JOIN {databaseOwner}[{objectQualifier}Users] u ON p.CreatedByUser = u.UserId
	LEFT JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_MetaInformation] m ON p.MetaInformationId = m.ID
	WHERE p.ModuleId = @ModuleId AND p.Url = @Url AND
	      (@includeUnpublished = 1 OR p.Published = 1)
END
GO

/*********************************************************/
/*****                                               *****/
/*****               GET POST BY ID                  *****/
/*****                                               *****/
/*********************************************************/
ALTER PROCEDURE {databaseOwner}[{objectQualifier}wsc_PostIt_GetPostById]
	@ModuleId int,
    @ItemId int,
	@includeUnpublished bit
AS
BEGIN
	SELECT p.*,
		   u.FirstName + ' ' + u.LastName AS CreatedByUserName,
		   stuff((select ', ' + t.Name 
               FROM {databaseOwner}[{objectQualifier}wsc_PostIt_PostTag] pt
			   INNER JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_Tags] t ON pt.TagID = t.ID
               WHERE pt.PostID = p.ItemID
               FOR XML PATH ('')
             ), 1,2,'') AS Tags,
		   m.Keywords AS MetaKeywords,
		   m.Description AS MetaDescription,
		   m.PageHeaderTags AS PageHeaderTags
	FROM {databaseOwner}[{objectQualifier}wsc_PostIt_Post] p
	LEFT JOIN {databaseOwner}[{objectQualifier}Users] u ON p.CreatedByUser = u.UserId
	LEFT JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_MetaInformation] m ON p.MetaInformationId = m.ID
	WHERE  p.ModuleId = @ModuleId AND p.ItemId = @ItemId AND
           (@includeUnpublished = 1 OR p.Published = 1)
END
GO

/*********************************************************/
/*****                                               *****/
/*****          GET PAGGED POSTS BY TAG              *****/
/*****                                               *****/
/*********************************************************/
ALTER PROCEDURE {databaseOwner}[{objectQualifier}wsc_PostIt_GetPaggedPostsByTag] 
	@moduleId int,
	@includeUnpublished bit,	
	@start int,
	@limit int,
	@tag nvarchar(max)
AS
BEGIN
	declare @end int;
	set @end = @start + @limit - 1;

	WITH ordered_posts AS (
	  SELECT p.*, 
			 u.FirstName + ' ' + u.LastName AS CreatedByUserName,
			 stuff((select ', ' + t.Name 
               FROM {databaseOwner}[{objectQualifier}wsc_PostIt_PostTag] pt
			   INNER JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_Tags] t ON pt.TagID = t.ID
               WHERE pt.PostID = p.ItemID
               FOR XML PATH ('')
             ), 1,2,'') AS Tags,
			 row_number() OVER(
               ORDER BY p.Date DESC, p.ItemId DESC
			 ) AS row_num
	  FROM {databaseOwner}[{objectQualifier}wsc_PostIt_Post] p 
	  LEFT JOIN {databaseOwner}[{objectQualifier}Users] u ON p.CreatedByUser = u.UserId
	  INNER JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_PostTag] postTag ON postTag.[PostID] = p.[ItemID] 
      INNER JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_Tags] tag ON postTag.[TagID]=tag.[ID]
	  WHERE p.ModuleId = @moduleId AND (@includeUnpublished = 1 OR p.Published = 1) AND tag.[Name]=@tag
	)

	SELECT * 
	FROM ordered_posts
	WHERE row_num BETWEEN @start AND @end
END
GO

/*********************************************************/
/*****                                               *****/
/*****       GET PAGGED POSTS BY ARCHIVE ITEM        *****/
/*****                                               *****/
/*********************************************************/
ALTER PROCEDURE {databaseOwner}[{objectQualifier}wsc_PostIt_GetPaggedPostsByArchiveItem]
	@moduleId int,
	@includeUnpublished bit,	
	@start int,
	@limit int,
	@month int,
	@year int
AS
BEGIN
	DECLARE @end int;
	SET @end = @start + @limit - 1;

	WITH ordered_posts AS (
	  SELECT p.*, 
			 u.FirstName + ' ' + u.LastName AS CreatedByUserName,
			 stuff((select ', ' + t.Name 
               FROM {databaseOwner}[{objectQualifier}wsc_PostIt_PostTag] pt
			   INNER JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_Tags] t ON pt.TagID = t.ID
               WHERE pt.PostID = p.ItemID
               FOR XML PATH ('')
             ), 1,2,'') AS Tags,
			 row_number() OVER(
               ORDER BY p.Date DESC, p.ItemId DESC
			 ) AS row_num
	  FROM {databaseOwner}[{objectQualifier}wsc_PostIt_Post] p 
	  LEFT JOIN {databaseOwner}[{objectQualifier}Users] u ON p.CreatedByUser = u.UserId
	  WHERE p.ModuleId = @moduleId AND (@includeUnpublished = 1 OR p.Published = 1) AND (MONTH(Date) = @month) AND (YEAR(DATE) = @year)
	)

	SELECT * 
	FROM ordered_posts
	WHERE row_num BETWEEN @start AND @end
END
GO

/*********************************************************/
/*****                                               *****/
/*****               GET PAGGED POSTS                *****/
/*****                                               *****/
/*********************************************************/
ALTER PROCEDURE {databaseOwner}[{objectQualifier}wsc_PostIt_GetPaggedPosts]
	@moduleId int,
	@includeUnpublished bit,
	@start int,
	@limit int
AS
BEGIN
	declare @end int;
	set @end = @start + @limit - 1;

	WITH ordered_posts AS (
	  SELECT p.*, 
			 u.FirstName + ' ' + u.LastName AS CreatedByUserName,
			 stuff((select ', ' + t.Name 
               FROM {databaseOwner}[{objectQualifier}wsc_PostIt_PostTag] pt
			   INNER JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_Tags] t ON pt.TagID = t.ID
               WHERE pt.PostID = p.ItemID
               FOR XML PATH ('')
             ), 1,2,'') AS Tags,
			 row_number() OVER(
               ORDER BY p.Date DESC, p.ItemId DESC
			 ) AS row_num
	  FROM {databaseOwner}[{objectQualifier}wsc_PostIt_Post] p 
	  LEFT JOIN {databaseOwner}[{objectQualifier}Users] u ON p.CreatedByUser = u.UserId
	  WHERE p.ModuleId = @moduleId AND (@includeUnpublished = 1 OR p.Published = 1)
	)

	SELECT * 
	FROM ordered_posts
	WHERE row_num BETWEEN @start AND @end
END
GO

/*********************************************************/
/*****                                               *****/
/*****                GET ALL POSTS COUNT            *****/
/*****                                               *****/
/*********************************************************/
ALTER PROCEDURE {databaseOwner}[{objectQualifier}wsc_PostIt_GetAllPostsCount]
	@moduleId int,
	@includeUnpublished bit
AS
BEGIN
	SELECT Count(p.ItemId)
	FROM {databaseOwner}[{objectQualifier}wsc_PostIt_Post] p 
	LEFT JOIN {databaseOwner}[{objectQualifier}Users] u ON p.CreatedByUser = u.UserId
	WHERE p.ModuleId = @moduleId AND (@includeUnpublished = 1 OR p.Published = 1)
END
GO

/*********************************************************/
/*****                                               *****/
/*****                 GET ALL POSTS                 *****/
/*****                                               *****/
/*********************************************************/
ALTER PROCEDURE {databaseOwner}[{objectQualifier}wsc_PostIt_GetAllPosts]

	@moduleId int,
	@includeUnpublished bit
AS
BEGIN
	SELECT p.*, 
			 u.FirstName + ' ' + u.LastName AS CreatedByUserName,
			 stuff((select ', ' + t.Name 
               FROM {databaseOwner}[{objectQualifier}wsc_PostIt_PostTag] pt
			   INNER JOIN {databaseOwner}[{objectQualifier}wsc_PostIt_Tags] t ON pt.TagID = t.ID
               WHERE pt.PostID = p.ItemID
               FOR XML PATH ('')
             ), 1,2,'') AS Tags
	  FROM {databaseOwner}[{objectQualifier}wsc_PostIt_Post] p 
	  LEFT JOIN {databaseOwner}[{objectQualifier}Users] u ON p.CreatedByUser = u.UserId
	  WHERE p.ModuleId = @moduleId AND (@includeUnpublished = 1 OR p.Published = 1)
END
GO