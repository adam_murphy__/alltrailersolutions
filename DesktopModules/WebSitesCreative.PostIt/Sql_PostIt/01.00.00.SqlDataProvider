﻿/*********************************************************/
/*****             SqlDataProvider                   *****/
/*****                                               *****/
/*****   To manually execute this script you must    *****/
/*****   perform a search and replace operation      *****/
/*****   for {databaseOwner} and {objectQualifier}   *****/
/*****                                               *****/
/*********************************************************/


/*********************************************************/
/*****                                               *****/
/*****           CREATE POST IT TABLE                *****/
/*****                                               *****/
/*********************************************************/


CREATE TABLE {databaseOwner}[{objectQualifier}WebSitesCreative_Post](
	[ModuleID] [int] NOT NULL,
	[ItemID] [int] IDENTITY(1,1) NOT NULL,
	[Content] [nvarchar](max) NOT NULL,
	[CreatedByUser] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Url] [nvarchar](250) NOT NULL CONSTRAINT [DF_{objectQualifier}WebSitesCreative_Post_Url]  DEFAULT (''),
	[Summary] [nvarchar](max) NULL,
	[Image] [nvarchar](max) NULL,
	[IncludeImageInPost] [bit] NULL,
	[Published] [bit] NOT NULL CONSTRAINT [DF_{objectQualifier}WebSitesCreative_Post_Published]  DEFAULT ((0)),
 CONSTRAINT [PK_{objectQualifier}WebSitesCreative_Post] PRIMARY KEY CLUSTERED 
(
	[ItemID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [IX_{objectQualifier}WebSitesCreative_Post_Url] UNIQUE NONCLUSTERED 
(
	[Url] ASC,
	[ModuleID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE {databaseOwner}[{objectQualifier}WebSitesCreative_Post]  WITH NOCHECK ADD  CONSTRAINT [FK_{objectQualifier}WebSitesCreative_Post_Modules] FOREIGN KEY([ModuleID])
REFERENCES {databaseOwner}[{objectQualifier}Modules] ([ModuleID])
ON DELETE CASCADE
GO
ALTER TABLE {databaseOwner}[{objectQualifier}WebSitesCreative_Post] CHECK CONSTRAINT [FK_{objectQualifier}WebSitesCreative_Post_Modules]
GO

/*********************************************************/
/*****                                               *****/
/*****                CREATE POST                    *****/
/*****                                               *****/
/*********************************************************/

CREATE PROCEDURE {databaseOwner}[{objectQualifier}WebSitesCreative_CreatePost]
    @ModuleId       int,
    @ItemId         int,
	@Content        ntext,
	@CreatedByUser  int,
    @date           datetime,
	@title          nvarchar(max),
    @url            nvarchar(max),
    @summary        nvarchar(max),
    @image          nvarchar(max),
    @includeImageInPost bit,
	@published bit
AS
BEGIN
	INSERT INTO {databaseOwner}[{objectQualifier}WebSitesCreative_Post] (
		ModuleId,
		Content,
		CreatedByUser,
		Date,
		Title,
		Url,
		Summary,
		[Image],
		IncludeImageInPost,
		Published
	) 
	values (
		@ModuleId,
		@Content,
		@CreatedByUser,
		@date,
		@title,
		@url,
		@summary,
		@image,
		@includeImageInPost,
		@published
	)
END
GO

/*********************************************************/
/*****                                               *****/
/*****                DELETE POST                    *****/
/*****                                               *****/
/*********************************************************/

CREATE PROCEDURE {databaseOwner}[{objectQualifier}WebSitesCreative_DeletePost]
	@ModuleId       int,
    @ItemId         int
AS
BEGIN
	DELETE
	FROM   {databaseOwner}[{objectQualifier}WebSitesCreative_Post]
	WHERE  ModuleId = @ModuleId AND ItemId = @ItemId
END
GO

/*********************************************************/
/*****                                               *****/
/*****                GET ALL POSTS                  *****/
/*****                                               *****/
/*********************************************************/

CREATE PROCEDURE {databaseOwner}[{objectQualifier}WebSitesCreative_GetAllPosts]

	@moduleId int,
	@includeUnpublished bit
AS
BEGIN
	SELECT p.*, 
		   u.FirstName + ' ' + u.LastName AS CreatedByUserName
	FROM {databaseOwner}[{objectQualifier}WebSitesCreative_Post] p 
	LEFT JOIN Users u ON p.CreatedByUser = u.UserId
	WHERE ModuleId = @moduleId AND (@includeUnpublished = 1 OR Published = 1)
END
GO

/*********************************************************/
/*****                                               *****/
/*****                GET ALL POSTS COUNT            *****/
/*****                                               *****/
/*********************************************************/

CREATE PROCEDURE {databaseOwner}[{objectQualifier}WebSitesCreative_GetAllPostsCount]

	@moduleId int,
	@includeUnpublished bit
AS
BEGIN
	SELECT Count(p.ItemId)
	FROM {databaseOwner}[{objectQualifier}WebSitesCreative_Post] p 
	LEFT JOIN Users u ON p.CreatedByUser = u.UserId
	WHERE ModuleId = @moduleId AND (@includeUnpublished = 1 OR Published = 1)
END
GO

/*********************************************************/
/*****                                               *****/
/*****               GET PAGGED POSTS                *****/
/*****                                               *****/
/*********************************************************/

CREATE PROCEDURE {databaseOwner}[{objectQualifier}WebSitesCreative_GetPaggedPosts]

	@moduleId int,
	@includeUnpublished bit,
	@start int,
	@limit int
AS
BEGIN
	declare @end int;
	set @end = @start + @limit - 1;

	WITH ordered_posts AS (
	  SELECT p.*, 
			 u.FirstName + ' ' + u.LastName AS CreatedByUserName,
			 row_number() OVER(
			   ORDER BY p.Date DESC, p.ItemId DESC
			 ) AS row_num
	  FROM {databaseOwner}[{objectQualifier}WebSitesCreative_Post] p 
	  LEFT JOIN Users u ON p.CreatedByUser = u.UserId
	  WHERE ModuleId = @moduleId AND (@includeUnpublished = 1 OR Published = 1)
	)

	SELECT * 
	FROM ordered_posts
	WHERE row_num BETWEEN @start AND @end
END
GO

/*********************************************************/
/*****                                               *****/
/*****                GET POST BY ID                 *****/
/*****                                               *****/
/*********************************************************/

CREATE PROCEDURE {databaseOwner}[{objectQualifier}WebSitesCreative_GetPostById]
	@ModuleId int,
    @ItemId int,
	@includeUnpublished bit
AS
BEGIN
	SELECT p.*,
		   u.FirstName + ' ' + u.LastName AS CreatedByUserName
	FROM {databaseOwner}[{objectQualifier}WebSitesCreative_Post] p
	LEFT JOIN Users u ON p.CreatedByUser = u.UserId
	WHERE  ModuleId = @ModuleId AND ItemId = @ItemId AND
	       (@includeUnpublished = 1 OR Published = 1)
END
GO

/*********************************************************/
/*****                                               *****/
/*****                GET POST BY URL                *****/
/*****                                               *****/
/*********************************************************/

CREATE PROCEDURE {databaseOwner}[{objectQualifier}WebSitesCreative_GetPostByUrl] 
	-- Add the parameters for the stored procedure here
	@ModuleId int,
    @Url nvarchar(max),
	@includeUnpublished bit
AS
BEGIN
	SELECT p.*,
           u.FirstName + ' ' + u.LastName AS CreatedByUserName
	FROM {databaseOwner}[{objectQualifier}WebSitesCreative_Post] p
	LEFT JOIN Users u ON p.CreatedByUser = u.UserId
	WHERE ModuleId = @ModuleId AND Url = @Url AND
	      (@includeUnpublished = 1 OR Published = 1)
END

GO

/*********************************************************/
/*****                                               *****/
/*****                UPDATE POST                    *****/
/*****                                               *****/
/*********************************************************/

CREATE PROCEDURE {databaseOwner}[{objectQualifier}WebSitesCreative_UpdatePost]
	@ModuleId       int,
    @ItemId         int,
	@Content        ntext,
	@CreatedByUser  int,
    @Date           datetime,
    @Title          nvarchar(max),
    @Url            nvarchar(max),
    @Summary        nvarchar(max),
    @Image          nvarchar(max),
    @IncludeImageInPost bit,
	@Published bit
AS
BEGIN
	UPDATE {databaseOwner}[{objectQualifier}WebSitesCreative_Post]
	set    Content       = @Content,
		   CreatedByUser = @CreatedByUser,
		   Date	         = @Date,
		   Title         = @Title,
		   Url           = @Url,
		   Summary       = @Summary,
		   [Image]       = @Image,
		   IncludeImageInPost = @IncludeImageInPost,
		   Published     = @Published
	WHERE  ModuleId = @ModuleId AND ItemId = @ItemId
END
GO