﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditAudioFile.ascx.cs" Inherits="WebSitesCreative.Modules.PostIt.Controls.EditAudioFile" %>
<%@ Register TagPrefix="cust" TagName="FileChooser" Src="FileChooser.ascx" %>

<%-- ToDo: uncomment and implement --%>

<%--<script type="text/javascript">
    (function ($) {
        $(document).ready(function () {
            $('.edit-audio-file .wsc_fileuploader').change(function (event) {
                var file = $(this).val();
                var titleText = $('#<%= txtTitle.ClientID %>').val();
                if (!titleText || titleText.length == 0) {
                    $('#<%= txtTitle.ClientID %>').val(file);
                }
            });
        });
    })(jQueryWSC171);
</script>--%>

<ul class="wsc_admin_fields edit-audio-file">
    <li class="wsc_uploader">
        <asp:Label CssClass="wsc_label" ID="lblMp3AudioFile" runat="server" resourcekey="lblMp3AudioFile"></asp:Label>
        <span class="wsc_tooltip_label">
            <%=GetLocalized("lblMp3AudioFile.Description")%></span><br />
        <cust:FileChooser runat="server" ID="mp3FileChooser_AudioFile" FileExtensions="mp3" IsRequired="false">
        </cust:FileChooser>
    </li>

    <%--<li class="wsc_uploader">
        <asp:Label CssClass="wsc_label" ID="lblOggAudioFile" runat="server" resourcekey="lblOggAudioFile"></asp:Label>
        <span class="wsc_tooltip_label">
            <%=GetLocalized("lblOggAudioFile.Description")%></span><br />
        <cust:FileChooser runat="server" ID="oggFileChooser_AudioFile" FileExtensions="ogg" IsRequired="false">
        </cust:FileChooser>
    </li>
    <li>
        <asp:Label CssClass="wsc_label" ID="lblUrl" runat="server" resourcekey="lblTitle"></asp:Label>
        <span class="wsc_tooltip_label">
            <%=GetLocalized("lblTitle.Description")%></span>
        <asp:TextBox CssClass="wsc_textbox text" runat="server" ID="txtTitle"></asp:TextBox>
    </li>--%>
</ul>