﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FileChooser.ascx.cs" Inherits="WebSitesCreative.Modules.PostIt.Controls.FileChooser" %>
<%@ Import Namespace="WebSitesCreative.Modules.PostIt.Components.Helpers" %>

<% if (!string.IsNullOrEmpty(OldFileName))
   {%>
<br/>
<a href="<%= FilesHelper.GetFileUrl(ModuleId, OldFileName) %>" target="_blank">
    <asp:Label runat="server" ID="lblPreviousUploadedFile"></asp:Label>
</a>
<% } %>
<br/>
<asp:FileUpload CssClass="wsc_fileuploader" runat="server" ID="uplFile" />

<asp:RequiredFieldValidator runat="server" ID="fileRequiredValidator" ControlToValidate="uplFile">
</asp:RequiredFieldValidator>

<asp:CustomValidator runat="server" ID="fileTypeValidator" Display="Dynamic" ErrorMessage="Wrong file format"
    OnServerValidate="fileTypeValidator_Validate"></asp:CustomValidator>