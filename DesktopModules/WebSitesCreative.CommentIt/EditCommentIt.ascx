﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditCommentIt.ascx.cs"
    Inherits="WebSitesCreative.Modules.CommentIt.EditCommentIt" %>
<%@ Register TagPrefix="wsc" TagName="Label" Src="Controls/WSCLabel.ascx" %>
<div class="wsc_admin_content">
    <div class="wsc_admin_box">
        <div class="wsc_admin_box_head">
            <div class="wsc_admin_title">
                <span>Step 1: </span>
                <%= GetLocalized("Step1Title.Text")%>
            </div>
        </div>
        <div class="wsc_admin_box_body">
            <div class="wsc_info_box">
                <div class="wsc_info_head">
                    <div class="wsc_info_title">
                        <%= GetLocalized("WhatIsDisqus.Title")%>
                    </div>
                </div>
                <div class="wsc_info_body">
                    <%= GetLocalized("WhatIsDisqus.Text")%>
                </div>
            </div>
            <div class="wsc_controls_group">
            <div class="wsc_label">
                <wsc:Label runat="server" id="lblDisqusShortname">
                </wsc:Label>
            </div>
            <div class="wsc_controls">
                <asp:TextBox ID="txtDisqusShortName" CssClass="NormalTextBox" Width="150" runat="server" />
                <a class="wsc_admin_button wsc_small" href="http://disqus.com/admin/register/" target="_blank">
                    <span>
                        <%= GetLocalized("lnkGetShortname.Text")%></span></a>
                <asp:RequiredFieldValidator CssClass="wsc_error" runat="server" ID="disqusShortNameRequiredValidator"
                    Display="Dynamic" resourcekey="DisqusShortNameRequiredValidator.ErrorMessage"
                    ControlToValidate="txtDisqusShortName"></asp:RequiredFieldValidator>
            </div>
            </div>
            <div class="wsc_info">
                <%= GetLocalized("Step1Tip.Text")%></div>
        </div>
    </div>
    <div class="wsc_admin_box">
        <div class="wsc_admin_box_head">
            <div class="wsc_admin_title">
                <span>Step 2: </span>
                <%= GetLocalized("Step2Title.Text") %>
            </div>
        </div>
        <div class="wsc_admin_box_body">
            <div class="wsc_controls_group">
                <div class="wsc_label">
                    <wsc:Label runat="server" id="lblDisqusDeveloperMode">
                    </wsc:Label>
                </div>
                <div class="wsc_controls">
                    <asp:RadioButtonList runat="server" ID="rblDeveloperMode" RepeatDirection="Horizontal">
                    </asp:RadioButtonList>
                    <asp:RequiredFieldValidator runat="server" ID="DevelperModeRequiredValidator" Display="Dynamic"
                        resourcekey="DevelperModeRequiredValidator.ErrorMessage" ControlToValidate="rblDeveloperMode"></asp:RequiredFieldValidator>
                </div>
                <div class="wsc_label">
                    <wsc:Label id="lblDisqusIdentifierMode" runat="server">
                    </wsc:Label>
                </div>
                <div class="wsc_controls">
                    <asp:RadioButtonList runat="server" ID="rblIdentifierMode" RepeatDirection="Horizontal">
                    </asp:RadioButtonList>
                    <asp:RequiredFieldValidator runat="server" ID="IdentifierModeRequiredValidator" Display="Dynamic"
                        resourcekey="IdentifierModeRequiredValidator.ErrorMessage" ControlToValidate="rblIdentifierMode"></asp:RequiredFieldValidator>
                </div>
            </div>
        </div>
    </div>
    <asp:LinkButton runat="server" ID="lnkUpdateSettings" OnClick="lnkUpdateSettings_Click"
        class="wsc_admin_button wsc_medium" CausesValidation="true"><span>Done!</span></asp:LinkButton>
</div>
