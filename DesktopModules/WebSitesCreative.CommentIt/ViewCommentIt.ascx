<%@ Control Language="C#" Inherits="WebSitesCreative.Modules.CommentIt.ViewCommentIt"
    AutoEventWireup="true" CodeBehind="ViewCommentIt.ascx.cs" %>
<%@ Import Namespace="WebSitesCreative.Modules.CommentIt.Components.Settings" %>


<%if (String.IsNullOrEmpty(DisqusSettings.Shortname) && IsEditable)
  {%>
  <a href='<%= EditUrl() %>' title="configure comments module">Configure comments module</a>
<%}
  else
  {
      if (DisqusSettings.IdentifierMode != IdentifierMode.PostIT || HttpContext.Current.Items["WebSitesCreative.PostID"] != null)
      {%>

<div id="disqus_thread"></div>
<script type="text/javascript">
    /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
    var disqus_shortname = '<%=DisqusSettings.Shortname%>'; // required: replace example with your forum shortname

    // The following are highly recommended additional parameters. Remove the slashes in front to use.
    var disqus_identifier = '<%=DisqusSettings.IdentifierMode == IdentifierMode.TabId
                                ? String.Format("thread_{0}", TabId)
                                : String.Format("thread_{0}_{1}", TabId, HttpContext.Current.Items["WebSitesCreative.PostID"])%>';
    var disqus_url = '<%=Request.Url.GetComponents(UriComponents.SchemeAndServer, UriFormat.UriEscaped) +
                            Request.RawUrl%>';
    var disqus_developer = <%=(int) DisqusSettings.DeveloperMode%>; // 1 == developer mode is on

    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function () {
        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
        dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>

<%
      }%>

<%
  }%>