﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WSCLabel.ascx.cs" Inherits="WebSitesCreative.Modules.CommentIt.Controls.WSCLabel" %>

<label class="SubHead"><%= GetLocalized(ID) %>: <a href="javascript:void(0);" title='<%= GetLocalized(ID + ".Help") %>' class="vtip"><%= GetLocalized("WhatThis.Text")%></a></label>