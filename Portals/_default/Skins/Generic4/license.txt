﻿1) REDISTRIBUTION NOT PERMITTED
<p>The resale and/or redistribution of the Software Product is strictly prohibited.</p>
2) STANDARD LICENSE
<p>If you have purchased an Standard License, you may use this Software Product on a single website(installation) only, belonging to either you or your client. You need to purchase the same Software Product again if you wish to use it on another website(installation).</p>
3) ENTERPRISE LICENSE
<p>If you have purchased an Enterprise License, you may use this Software Product on an unlimited number of websites(installations), belonging to either you or your client.</p>
4) MODIFICATIONS
<p>You are authorized to make any necessary modification(s) to our Software Product to fit your purposes.</p>
5) UNAUTHORIZED USE
<p>You may not place any of our products, modified or unmodified, on a diskette, CD, website or any other medium and offer them for redistribution or resale of any kind without prior written consent from WebSitesCreative.</p>