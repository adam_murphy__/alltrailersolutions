﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="../../Controls/BannerThemeControl/BannerThemeControl.ascx.cs"
    Inherits="WebSitesCreative.Modules.Banner.Controls.BannerThemeControl" %>
<%@ Import Namespace="WebSitesCreative.Modules.Banner.Components.Helpers" %>
<%@ Register TagPrefix="cs" Namespace="WebSitesCreative.Modules.Banner.Controls"
    Assembly="WebSitesCreative.Modules.Banner" %>

<script runat="server" language="C#">
    protected void Page_Load(object sender, EventArgs e)
    {
        RegisterJS("../../js/jquery.nerveSlider.min.js");
    }
</script>

<cs:InlineScript ID="InlineScript" runat="server">
    <script type="text/javascript" language="javascript">
        $(".myslider.wsc_call_module<%=ModuleId%>").show();
        $(".myslider.wsc_call_module<%=ModuleId%>").startslider({
            sliderWidth: <%=Width%>,
            sliderHeight: <%=Height%>,
            slideTransitionSpeed: <%=GetSetting("slideTransitionSpeed") %>,
            slideTransitionEasing: "<%=GetSetting("slideTransitionEasing") %>",
            slidesDraggable: <%=GetSetting("slidesDraggable") %>,
            slideTransitionDelay: <%=GetSetting("slideTransitionDelay") %>,
            slideTransition: "<%=GetSetting("slideTransition") %>",
            showDots: <%=GetSetting("showDots") %>,
            sliderResizable: true,
            showTimer: <%=GetSetting("showTimer") %>
        });
    </script>
</cs:InlineScript>

<div class="myslider wsc_call_module<%=ModuleId%>">
    <asp:Repeater ID="rptrImages" runat="server">
        <ItemTemplate>
            <asp:PlaceHolder ID="pnlDinamic" runat="server">
                <a href="<%# Eval("Url")%>">
                    <img alt="<%# Eval("Title")%>" src="<%# FilesHelper.GetFileUrl(ModuleId, Eval("ResizedImageName").ToString()) %>" />
                </a>
            </asp:PlaceHolder>
        </ItemTemplate>
    </asp:Repeater>
</div>
