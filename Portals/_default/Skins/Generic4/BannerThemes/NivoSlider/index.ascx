﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="../../Controls/BannerThemeControl/BannerThemeControl.ascx.cs"
    Inherits="WebSitesCreative.Modules.Banner.Controls.BannerThemeControl" %>
<%@ Import Namespace="WebSitesCreative.Modules.Banner.Components.Helpers" %>
<%@ Register TagPrefix="cs" Namespace="WebSitesCreative.Modules.Banner.Controls"
    Assembly="WebSitesCreative.Modules.Banner" %>

<div class="nivo nivo_height">
    <!-- nivo slider starts -->
    <div class="slider-wrapper theme-default">
        <div class="nivoSlider wsc_call_module<%=ModuleId%>">
            <!-- add your images here -->
            <asp:Repeater ID="rptrImages" runat="server">
                <ItemTemplate>
                    <asp:PlaceHolder ID="pnlDinamic" runat="server" Visible='<%# ((bool)Eval("IsImageSlide")) %>'>
                    <a href="<%# (Eval("Url") == "") ? "#_" : Eval("Url") %>" target="<%# (Eval("Url") == "") ? "_self" : "_blank" %>">
                        <img src="<%# FilesHelper.GetFileUrl(ModuleId, Eval("ResizedImageName").ToString()) %>"
                            alt="<%# Eval("Title")%>" title="#nivocaption<%# (Container.ItemIndex + 1)%>" />
                    </a>
                    </asp:PlaceHolder>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
    <!-- add your captions here -->
    <asp:Repeater ID="rptrImages2" runat="server">
        <ItemTemplate>
            <asp:PlaceHolder ID="pnlDinamic2" runat="server" Visible='<%# ((bool)Eval("IsImageSlide")) %>'>
                <div id="nivocaption<%# (Container.ItemIndex + 1)%>" class="nivo-html-caption">
                    <%# (Eval("Description") == "") ? Eval("Title") : Eval("Description") %></div>
            </asp:PlaceHolder>
        </ItemTemplate>
    </asp:Repeater>
</div>

<!--#include file="../../CommonParts/Scripts/jquery.nivo.slider.pack.js.ascx"-->

<cs:InlineScript ID="InlineScript" runat="server">
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $('.nivoSlider.wsc_call_module<%=ModuleId%>').nivoSlider({
                effect: '<%=GetSetting("effect") %>',
                animSpeed: <%=GetSetting("animSpeed") %>,
                pauseTime: <%=GetSetting("pauseTime") %>,
                directionNav: <%=GetSetting("Navigation") %>,
                pauseOnHover: <%=GetSetting("pauseOnHover") %>
            });
        });
    </script>
</cs:InlineScript>