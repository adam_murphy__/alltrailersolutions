﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="../../Controls/BannerThemeControl/BannerThemeControl.ascx.cs"
    Inherits="WebSitesCreative.Modules.Banner.Controls.BannerThemeControl" %>
<%@ Import Namespace="WebSitesCreative.Modules.Banner.Components.Helpers" %>
<%@ Register TagPrefix="cs" Namespace="WebSitesCreative.Modules.Banner.Controls"
    Assembly="WebSitesCreative.Modules.Banner" %>

<cs:InlineScript ID="InlineScript1" runat="server">
<%-- Load scripts and styles at the bottom of the page--%>
<link id="wsc-metroslider" href="<%= TemplateSourceDirectory %>/../../css/conditional/metroslider.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="<%= TemplateSourceDirectory %>/../../js/conditional/touchSlider.js"></script>
    
    <script type="text/javascript" language="javascript">
$(window).load(function () {
         if ( <%=GetSetting("VertSup")%> === true) {
            var slides = $(".metro-container.wsc_call_module<%=ModuleId%> .metro-wrapper > .metro-slide");
                for (i = 0; i < slides.length; i++) {
                    if (((i + 1) % 3) == 0) {
                        var $startNestedSlide = slides.eq(i - 1).addClass('startVertical');
                        var $endNestedSlide = slides.eq(i).addClass('endVertical');
                    }
                }
                $('.metro-container.wsc_call_module<%=ModuleId%> .metro-wrapper > .metro-slide.startVertical').each(function () {
                    $(this).next('.endVertical').andSelf().wrapAll('<div class="metro-slide"><div class="metro-container metro-nested"><div class="metro-wrapper"/>');
                });
        }
                var metroColH =  <%=GetSetting("ActiveNumber")%>, 
            metroCol = 6;

            var mtResizeTimeout;
            $(window).on("resize", function () {
                clearTimeout(mtResizeTimeout);
                mtResizeTimeout = setTimeout(function () {
                    $(window).trigger("metroresize");
                }, 200);
            });

            HeightCalc();

            var metroN1 = $('.metro-container.wsc_call_module<%=ModuleId%>').first().metro({
                autoResize: true,
                autoplay: 5000,
                // slidesPerView:4,
                slidesPerSlide: metroColH
            });

            //Navigation arrows
            if ($('.metro-container.wsc_call_module<%=ModuleId%> > .metro-wrapper > .metro-slide').length <= metroColH) {
                $('.metro-container.wsc_call_module<%=ModuleId%> .arrow-left').hide();
                $('.metro-container.wsc_call_module<%=ModuleId%> .arrow-right').hide();
            }
            $('.metro-container.wsc_call_module<%=ModuleId%> .arrow-left').on('click', function (e) {
                e.preventDefault();
                metroN1.swipePrev();
            });
            $('.metro-container.wsc_call_module<%=ModuleId%> .arrow-right').on('click', function (e) {
                e.preventDefault();
                metroN1.swipeNext();
            });

            //Vertical
            var metroVerticalSlides = [];

            $('.metro-container.metro-container-horizontal').each(function () {
                var $submetroContent = $(this),
					$submetroContentLength = $submetroContent.find('.metro-slide').length,
					$subUpArrow = $submetroContent.find('.arrow-top'),
					$subDownArrow = $submetroContent.find('.arrow-bottom');
                if ($submetroContentLength <= metroCol) {
                    $($subUpArrow).hide();
                    $($subDownArrow).hide();
                }
                var metroN2 = $submetroContent.first().metro({
                    slidesPerSlide: metroCol,
                    mode: 'vertical'
                });

                metroVerticalSlides.push(metroN2);

                var metroN2Length = metroN2.slides.length;
                // $subUpArrow.addClass('disable');
                $subUpArrow.click(function (e) {
                    e.preventDefault();
                    metroN2.swipePrev();
                });

                $subDownArrow.click(function (e) {
                    e.preventDefault();
                    metroN2.swipeNext();
                });
            });

            $(window).on("metroresize", function () {
                HeightCalc();
                metroN1.reInit();
                if (metroVerticalSlides.length > 0) {
                    var arrLingth = metroVerticalSlides.length;
                    for (var i = 0; i < arrLingth; i++) {
                        metroVerticalSlides[i].reInit();
                    }
                }
            }).trigger('metroresize');

            $('#loading-labe').css('display', 'none');

        });

    </script>
</cs:InlineScript>

<cs:InlineStyle ID="InlineStyle1" runat="server"> 
    <style type="text/css">
        .metro-container.wsc_call_module<%=ModuleId%>
        {
          /*   height:auto; */
        }
        
        .metro-container.wsc_call_module<%=ModuleId%> .metro-wrapper
        {
            /* height:<%= Height %>px; */
             opacity: 1; 
             transition-duration: 0s;
        }
        
         .metro-container.wsc_call_module<%=ModuleId%> .metro-slide
         {
             width: <%= Width %>px; 
         }
         
         .metro-container.wsc_call_module<%=ModuleId%> .metro-nested .metro-slide
         {
             width:100%;
         }
        
    </style>
</cs:InlineStyle>


<div class="metro-container wsc_call_module<%=ModuleId%>" >
   <a href="#_"><em class="arrow-left fa fa-chevron-left"></em></a><a href="#_"><em class="arrow-right fa fa-chevron-right"></em></a>

    <div class="metro-wrapper">
        <asp:Repeater ID="rptrImages" runat="server">
            <ItemTemplate>
                <asp:PlaceHolder ID="pnlDinamic" runat="server" Visible='<%# ((bool)Eval("IsImageSlide")) %>'>
                <div class="metro-slide">
		            <img class="preload-me" src="<%# FilesHelper.GetFileUrl(ModuleId, Eval("ResizedImageName").ToString()) %>" alt="<%# Eval("Title")%>" >

                    <span class="link show-content"><i class="corner"></i><i class="fa fa-plus"></i></span>
		            <div class="metro-caption">
			            <h4><a href="<%# (Eval("Url") == "" ? "#_" : Eval("Url")) %>">
                            <%# Eval("Title")%>
                        </a></h4>
                        <div><%# Eval("Description")%></div>
                        <i class="fa fa-link"></i>
                        <a class="metro-link" href="<%# (Eval("Url") == "" ? "#_" : Eval("Url")) %>">Details</a>
			            <span class="close-link"> <i class="fa fa-times"></i></span>
		            </div>

	            </div>
                </asp:PlaceHolder>
            </ItemTemplate>
        </asp:Repeater>

    </div>


</div>

<div style="display: block;" id="loading-labe" class="loading-label">
<div class="metro-spinner"></div>
</div>
		

            

