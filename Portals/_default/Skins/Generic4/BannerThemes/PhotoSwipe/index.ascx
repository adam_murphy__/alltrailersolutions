﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="../../Controls/BannerThemeControl/BannerThemeControl.ascx.cs"
    Inherits="WebSitesCreative.Modules.Banner.Controls.BannerThemeControl" %>
<%@ Import Namespace="WebSitesCreative.Modules.Banner.Components.Helpers" %>
<%@ Register TagPrefix="cs" Namespace="WebSitesCreative.Modules.Banner.Controls"
    Assembly="WebSitesCreative.Modules.Banner" %>
<%@ Register TagPrefix="tags" TagName="Tags" Src="Tags.ascx" %>

<%if (GetSetting("pswpGalleryMode") == "tags")
{ %>
<div class="<%=GetSetting("pswpGalleryControlAlignment")%> <%=GetSetting("pswpGalleryControlStyle")%>">
    <tags:Tags ID="Tags1" runat="server" />
</div>

<div class="wsc_photoswipe_module<%=ModuleId%> row isotope <%=GetSetting("pswpGalleryPadding")%>">
    <asp:Repeater ID="rptrImages" runat="server">
        <ItemTemplate>
            <asp:PlaceHolder ID="pnlDinamic" runat="server" Visible='<%# ((bool)Eval("IsImageSlide")) %>'>
                <figure class="<%#(GetSetting("pswpGalleryLayout"))%> isotope-item <%#  String.Join(" ", ((string[])Eval("SplittedTags"))) %>" data-category="<%#  String.Join(" ", ((string[])Eval("SplittedTags"))) %>">
                    <a href="<%# FilesHelper.GetFileUrl(ModuleId, Eval("ImageName").ToString()) %>"
                        data-width="<%#((Size)Eval("OriginalImageSize")).Width%>" data-height="<%#((Size)Eval("OriginalImageSize")).Height%>" data-index="<%# Container.ItemIndex %>">
                        <%if (GetSetting("pswpGalleryThumb") == "thumb_resize")
                        { %>
                        <img src="<%# FilesHelper.GetFileUrl(ModuleId, Eval("ResizedImageName").ToString()) %>">
                        <%} else { %>
                        <img src="<%# FilesHelper.GetFileUrl(ModuleId, Eval("ImageName").ToString()) %>">
                        <%} %>
                    </a>
                    <figcaption itemprop="caption description"><%# Eval("Title")%></figcaption>
                </figure>
            </asp:PlaceHolder>
        </ItemTemplate>
    </asp:Repeater>
</div>

<cs:InlineScript ID="InlineScript" runat="server">

    <%-- Load scripts and styles at the bottom of the page--%>
    <link id="wsc-photoswipe" href="<%= TemplateSourceDirectory %>/../../css/conditional/photoswipe.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="<%= TemplateSourceDirectory %>/../../js/conditional/photoswipe.min.js"></script>
    <script type="text/javascript" src="<%= TemplateSourceDirectory %>/../../js/conditional/photoswipe-ui-default.min.js"></script>
    <script type="text/javascript" src="<%= TemplateSourceDirectory %>/../../js/isotope.pkgd.min.js"></script>
    
    <script type="text/javascript" language="javascript">
        $("<div class='pswp' tabindex='-1' role='dialog' aria-hidden='true'>\
            <div class='pswp__bg'></div>\
            <div class='pswp__scroll-wrap'>\
                <div class='pswp__container'>\
                    <div class='pswp__item'></div>\
                    <div class='pswp__item'></div>\
                    <div class='pswp__item'></div>\
                </div>\
                <div class='pswp__ui pswp__ui--hidden'>\
                    <div class='pswp__top-bar'>\
                        <div class='pswp__counter'></div>\
                        <button type='button' class='pswp__button pswp__button--close' title='Close (Esc)'></button>\
                        <button type='button' class='pswp__button pswp__button--share' title='Share'></button>\
                        <button type='button' class='pswp__button pswp__button--fs' title='Toggle fullscreen'></button>\
                        <button type='button' class='pswp__button pswp__button--zoom' title='Zoom in/out'></button>\
                        <div class='pswp__preloader'>\
                            <div class='pswp__preloader__icn'>\
                                <div class='pswp__preloader__cut'>\
                                    <div class='pswp__preloader__donut'></div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                    <div class='pswp__share-modal pswp__share-modal--hidden pswp__single-tap'>\
                        <div class='pswp__share-tooltip'></div>\
                    </div>\
                    <button type='button' class='pswp__button pswp__button--arrow--left' title='Previous (arrow left)'></button>\
                    <button type='button' class='pswp__button pswp__button--arrow--right' title='Next (arrow right)'></button>\
                    <div class='pswp__caption'>\
                        <div class='pswp__caption__center'></div>\
                    </div>\
                </div>\
            </div>\
        </div>").appendTo("body form");

        if (!($('#options ul#filters.module<%=ModuleId%> li:nth-child(2)').length)) {
        $('#options ul#filters.module<%=ModuleId%>').parents('#options').css("display", "none");
        }

        $('.wsc_photoswipe_module<%=ModuleId%>').each(function () {
            var $pic = $(this),
                getItems = function () {
                    var items = [];
                    $pic.find('a').each(function () {
                        var $href = $(this).attr('href'),
                            $width = $(this).data('width'),
                            $height = $(this).data('height'),
                            $thumb = $(this).children("img").attr("src"),
                            $description = $(this).siblings('figcaption').html();

                        var item = {
                            src: $href,
                            msrc: $thumb,
                            w: $width,
                            h: $height,
                            title: $description
                        }

                        items.push(item);
                    });
                    return items;
                }

            var items = getItems();

            var $pswp = $('.pswp')[0];
            $pic.on('click', 'figure', function (event) {
                event.preventDefault();

                var $index = $(this).index();
                var options = {
                    index: $index,
                    bgOpacity: 0.7,
                    showHideOpacity: true,
                    //animation
                    getThumbBoundsFn: function(index) {
                        var thumbnail = event.target;
                        var pageYScroll = window.pageYOffset || document.documentElement.scrollTop;
                        var rect = thumbnail.getBoundingClientRect();
                        return {x: rect.left, y: rect.top + pageYScroll, w: rect.width};
                    }

                }

                // Initialize PhotoSwipe
                var lightBox = new PhotoSwipe($pswp, PhotoSwipeUI_Default, items, options);
                lightBox.init();
            });
        });

        $(window).load(function () {
            $('#filters li a').each(function () {
                var _this = $(this);
                var tag_text = $(this).html();
                var new_tag_text = tag_text.replace(/_/g, ' ');
                new_tag_text = new_tag_text.replace(/aNd/, '&amp;');
                _this.html(new_tag_text);
            });
            var $container = $('.wsc_photoswipe_module<%=ModuleId%>').imagesLoaded(function () {
                // init Isotope after all images have loaded
                $container.isotope({
                    itemSelector: '.isotope-item',
                    transitionDuration: '0.6s',
                    percentPosition: true
                });
            });
            $('#filters.option-set.module<%=ModuleId%> li').on('click', 'a', function () {
                var filterValue = $(this).attr('data-filter');
                // use filterFn if matches value
                $container.isotope({ filter: filterValue });
            });
            $('#options #filters.module<%=ModuleId%> li').on('click', 'a', function () {
                $('#options #filters.module<%=ModuleId%> li a.selected').removeClass('selected');
                $(this).addClass('selected');
            });
        });
    </script>
</cs:InlineScript>
<%} %>

<%if (GetSetting("pswpGalleryMode") == "pagination")
{ %>
    <div class="<%=GetSetting("pswpGalleryControlAlignment")%>  <%=GetSetting("pswpGalleryControlStyle")%>">
        <div class="holder holder_module<%=ModuleId%> images">
        </div>
    </div>

    <div class="wsc_photoswipe_module<%=ModuleId%> row  <%=GetSetting("pswpGalleryPadding")%>">
    <ul id="itemContainer<%=ModuleId%>">
        <asp:Repeater ID="rptrImages2" runat="server">
            <ItemTemplate>
                <asp:PlaceHolder ID="pnlDinamic" runat="server" Visible='<%# ((bool)Eval("IsImageSlide")) %>'>
                    <li>
                        <figure class="<%#(GetSetting("pswpGalleryLayout"))%> isotope-item <%#  String.Join(" ", ((string[])Eval("SplittedTags"))) %>" data-category="<%#  String.Join(" ", ((string[])Eval("SplittedTags"))) %>">
                            <a href="<%# FilesHelper.GetFileUrl(ModuleId, Eval("ImageName").ToString()) %>"
                                data-width="<%#((Size)Eval("OriginalImageSize")).Width%>" data-height="<%#((Size)Eval("OriginalImageSize")).Height%>" data-index="<%# Container.ItemIndex %>">
                                <%if (GetSetting("pswpGalleryThumb") == "thumb_resize")
                                { %>
                                <img src="<%# FilesHelper.GetFileUrl(ModuleId, Eval("ResizedImageName").ToString()) %>">
                                <%} else { %>
                                <img src="<%# FilesHelper.GetFileUrl(ModuleId, Eval("ImageName").ToString()) %>">
                                <%} %>
                            </a>
                            <figcaption itemprop="caption description"><%# Eval("Description")%></figcaption>
                        </figure>
                    </li>
                </asp:PlaceHolder>
            </ItemTemplate>
        </asp:Repeater>
    </ul>
    </div>

    <div class="<%=GetSetting("pswpGalleryControlAlignment")%>  <%=GetSetting("pswpGalleryControlStyle")%>">
        <div class="holder holder_module<%=ModuleId%> images">
        </div>
    </div>

<cs:InlineScript ID="InlineScript2" runat="server">

    <link id="wsc-photoswipe" href="<%= TemplateSourceDirectory %>/../../css/conditional/photoswipe.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="<%= TemplateSourceDirectory %>/../../js/conditional/photoswipe.min.js"></script>
    <script type="text/javascript" src="<%= TemplateSourceDirectory %>/../../js/conditional/photoswipe-ui-default.min.js"></script>
    <script type="text/javascript" src="<%= TemplateSourceDirectory %>/../../js/jPages.js"></script>
    <script type="text/javascript" language="javascript">
        $("<div class='pswp' tabindex='-1' role='dialog' aria-hidden='true'>\
            <div class='pswp__bg'></div>\
            <div class='pswp__scroll-wrap'>\
                <div class='pswp__container'>\
                    <div class='pswp__item'></div>\
                    <div class='pswp__item'></div>\
                    <div class='pswp__item'></div>\
                </div>\
                <div class='pswp__ui pswp__ui--hidden'>\
                    <div class='pswp__top-bar'>\
                        <div class='pswp__counter'></div>\
                        <button type='button' class='pswp__button pswp__button--close' title='Close (Esc)'></button>\
                        <button type='button' class='pswp__button pswp__button--share' title='Share'></button>\
                        <button type='button' class='pswp__button pswp__button--fs' title='Toggle fullscreen'></button>\
                        <button type='button' class='pswp__button pswp__button--zoom' title='Zoom in/out'></button>\
                        <div class='pswp__preloader'>\
                            <div class='pswp__preloader__icn'>\
                                <div class='pswp__preloader__cut'>\
                                    <div class='pswp__preloader__donut'></div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                    <div class='pswp__share-modal pswp__share-modal--hidden pswp__single-tap'>\
                        <div class='pswp__share-tooltip'></div>\
                    </div>\
                    <button type='button' class='pswp__button pswp__button--arrow--left' title='Previous (arrow left)'></button>\
                    <button type='button' class='pswp__button pswp__button--arrow--right' title='Next (arrow right)'></button>\
                    <div class='pswp__caption'>\
                        <div class='pswp__caption__center'></div>\
                    </div>\
                </div>\
            </div>\
        </div>").appendTo("body form");

        $('.wsc_photoswipe_module<%=ModuleId%>').each(function () {
            var $pic = $(this),
                getItems = function () {
                    var items = [];
                    $pic.find('a').each(function () {
                        var $href = $(this).attr('href'),
                            $width = $(this).data('width'),
                            $height = $(this).data('height'),
                            $thumb = $(this).children("img").attr("src"),
                            $description = $(this).siblings('figcaption').html();

                        var item = {
                            src: $href,
                            msrc: $thumb,
                            w: $width,
                            h: $height,
                            title: $description
                        }

                        items.push(item);
                    });
                    return items;
                }

            var items = getItems();
            console.log(items);
            var $pswp = $('.pswp')[0];
            $pic.on('click', 'li', function (event) {
                event.preventDefault();

                var $index = $(this).index();
                var options = {
                    index: $index,
                    bgOpacity: 0.7,
                    showHideOpacity: true,
                    //animation
                    getThumbBoundsFn: function(index) {
                        var thumbnail = event.target;
                        var pageYScroll = window.pageYOffset || document.documentElement.scrollTop;
                        var rect = thumbnail.getBoundingClientRect();
                        return {x: rect.left, y: rect.top + pageYScroll, w: rect.width};
                    }

                }

                // Initialize PhotoSwipe
                var lightBox = new PhotoSwipe($pswp, PhotoSwipeUI_Default, items, options);
                lightBox.init();
            });
        });
        $(window).load(function () {
            $("div.holder_module<%=ModuleId%>.images").jPages({
                containerID  : "itemContainer<%=ModuleId%>",
                perPage      : <%=GetSetting("PerPage") %>, 
                keyBrowse   : true 
            });
            if (!($(".holder_module<%=ModuleId%> a:contains('2')").length)) {
                $('.holder_module<%=ModuleId%>').css("display", "none");
            };
        });
    </script>
</cs:InlineScript>

<%} %>