﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="../../Controls/BannerThemeControl/BannerThemeControl.ascx.cs"
    Inherits="WebSitesCreative.Modules.Banner.Controls.BannerThemeControl" %>
<%@ Import Namespace="WebSitesCreative.Modules.Banner.Components.Helpers" %>
<%@ Register TagPrefix="cs" Namespace="WebSitesCreative.Modules.Banner.Controls"
    Assembly="WebSitesCreative.Modules.Banner" %>

<div class="swiper-container wsc_call_module<%=ModuleId%> <%=GetSetting("sliderMode")%> " style="height: <%=Height%>px;">
    <div class="swiper-wrapper">
        <asp:Repeater ID="rptrImages" runat="server">
                <ItemTemplate>
                    <asp:PlaceHolder ID="pnlDinamic" runat="server" Visible='<%# ((bool)Eval("IsImageSlide")) %>'>
                        <%if (GetSetting("sliderMode") == "wsc-swiper-regular2")
                        { %>
                            <div class="swiper-slide">
                            <img alt="<%# Eval("Title")%>" src="<%# FilesHelper.GetFileUrl(ModuleId, Eval("ResizedImageName").ToString()) %>" />
                        <%} else { %>
                            <%-- Slider image --%>
                            <div class="swiper-slide <%# (GetSetting("swiperBWSwitch") == "bw-switch-no" ? GetSetting("swiperCaptionColor") : (((Container.ItemIndex + (GetSetting("swiperBWSwitch") == "bw-sw-white-first" ? 0 : 1)) % 2) == 0 ? "wsc-content-white" : "wsc-content-dark"))%>" style="background-image: url('<%# FilesHelper.GetFileUrl(ModuleId, Eval("ResizedImageName").ToString()) %>');">
                            
                            <%-- Slider's image dark\white overlay --%>
                            <%if ((GetSetting("swiperBWOverlay") != "overlay-no") && (GetSetting("swiperBWSwitch") == "bw-switch-no")) 
                               { 
                                  if (GetSetting("swiperCaptionColor") == "wsc-content-white") { %><div class="wsc-solid-overlay wsc-dark-<%=GetSetting("swiperBWOverlay")%>"></div><% } %>
                                <%if (GetSetting("swiperCaptionColor") == "wsc-content-dark") { %><div class="wsc-solid-overlay wsc-white-<%=GetSetting("swiperBWOverlay")%>"></div><% } %>
                            <% } %>

                            <%if ((GetSetting("swiperBWOverlay") != "overlay-no") && (GetSetting("swiperBWSwitch") != "bw-switch-no"))
                              { %>
                               
                               <%#(((Container.ItemIndex + (GetSetting("swiperBWSwitch") == "bw-sw-white-first" ? 0 : 1)) % 2) == 0 ? "<div class='wsc-solid-overlay wsc-dark-" + GetSetting("swiperBWOverlay") + "'></div>" : "<div class='wsc-solid-overlay wsc-white-" + GetSetting("swiperBWOverlay") + "'></div>")%>

                            <%} %>
                            
                            <%-- Caption --%>
                            <div class="wsc-swiper-slide-content <%=GetSetting("swiperCaptionVMode")%> wsc-pad-lg <%=GetSetting("swiperCaptionHMode")%> "><%# (Eval("Title") == "" ? "" : "<h1>" + Eval("Title") + "</h1>")%><%# Eval("Description")%><%# (Eval("Url") == "" ? "" : "<a class='wsc-button wsc-button-large wsc-button-border wsc-margin-top-sm' href='" + Eval("Url") + "'>Explore more</a>")%></div>
                        <%} %>
                        </div>
                    </asp:PlaceHolder>
                </ItemTemplate>
            </asp:Repeater>
    </div>
    <%if (GetSetting("pagination") == "show")
    { %>
        <div class="swiper-pagination"></div>
    <%} %>
    <%if (GetSetting("navigation") == "show")
    { %>
        <a href="#_" class="button-next fa fa-angle-right"></a>
        <a href="#_" class="button-prev fa fa-angle-left"></a>
    <%} %>
</div>


<cs:InlineScript ID="InlineScript" runat="server">

    <%-- Load scripts and styles at the bottom of the page--%>
    <link id="wsc-swiperslider" href="<%= TemplateSourceDirectory %>/../../css/conditional/swiper-slider.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="<%= TemplateSourceDirectory %>/../../js/swiper.jquery.min.js"></script>

    <script type="text/javascript" language="javascript">
        var sliderHeight = function() {
            var container = $('.swiper-container.wsc_call_module<%=ModuleId%>');
            var parent = $('.swiper-container.wsc_call_module<%=ModuleId%>').parents('.wsc_pane');
            var parentHeight = parent.outerHeight();
            container.css("height", parentHeight);
        };
        $(document).ready(function () {
            var mySwiper = new Swiper('.swiper-container.wsc_call_module<%=ModuleId%>', {
                autoplay: <%=GetSetting("autoplay") %>,
                speed: <%=GetSetting("animSpeed") %>,
                effect: '<%=GetSetting("effect") %>',
                loop: <%=GetSetting("loop") %>,
                <%if (GetSetting("navigation") == "show")
                { %>
                    nextButton: '.wsc_call_module<%=ModuleId%> .button-next',
                    prevButton: '.wsc_call_module<%=ModuleId%> .button-prev',
                <%} %>
                <%if (GetSetting("pagination") == "show")
                { %>
                    pagination: '.wsc_call_module<%=ModuleId%> .swiper-pagination',
                <%} %>
                paginationType: '<%=GetSetting("paginationType") %>',
                <%if (GetSetting("paginationType") == "bullets")
                { %>
                    paginationClickable: true,
                <%} %>
                slidesPerView: 1
            })
            <%if (GetSetting("sliderMode") == "fullHeight")
            { %>
                sliderHeight();
            <%} %>
        });
        $(window).resize(function() {
            <%if (GetSetting("sliderMode") == "fullHeight")
            { %>
                sliderHeight();
            <%} %>
        });
    </script>
</cs:InlineScript>