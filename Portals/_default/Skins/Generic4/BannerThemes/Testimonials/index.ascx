﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="../../Controls/BannerThemeControl/BannerThemeControl.ascx.cs"
    Inherits="WebSitesCreative.Modules.Banner.Controls.BannerThemeControl" %>
<%@ Import Namespace="WebSitesCreative.Modules.Banner.Components.Helpers" %>
<%@ Register TagPrefix="cs" Namespace="WebSitesCreative.Modules.Banner.Controls"
    Assembly="WebSitesCreative.Modules.Banner" %>

<cs:InlineScript runat="server">
    <script type="text/javascript" language="javascript">
        $(window).load(function () {
            $(".wsc-testimonial.wsc_call_module<%=ModuleId%> .flexslider").flexslider({
                selector: ".slider-wrap > .slide",
                animation: 'fade',
                easing: "swing",
                direction: 'horizontal',
                slideshow: <%=GetSetting("slideshow") %>,
                slideshowSpeed: <%=GetSetting("slideshowSpeed") %>,
                animationSpeed: <%=GetSetting("animationSpeed") %>,
                pauseOnHover: <%=GetSetting("pauseOnHover") %>,
                video: false,
                controlNav: <%=GetSetting("controlNav") %>,
                directionNav: false
            });
        });
    </script>
</cs:InlineScript>

<div class="wsc-testimonial wsc_call_module<%=ModuleId%> <%=GetSetting("sliderMode")%>">
    <div class="flexslider">
        <div class="slider-wrap">
            <asp:Repeater ID="rptrImages" runat="server">
                <ItemTemplate>
                    <asp:PlaceHolder ID="pnlDinamic" runat="server" Visible='<%# ((bool)Eval("IsImageSlide")) %>'>
                        <div class="slide">
                            <div class="wsc-testi-image">
                                <a href="#_">
                                    <img src='<%# FilesHelper.GetFileUrl(ModuleId, Eval("ResizedImageName").ToString()) %>' alt="">
                                </a>
                            </div>
                            <div class="wsc-testi-content">
                                <%# Eval("Description")%>
                            </div>
                        </div>
                    </asp:PlaceHolder>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</div>