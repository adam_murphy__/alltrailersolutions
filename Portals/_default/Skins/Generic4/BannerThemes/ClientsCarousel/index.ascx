﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="../../Controls/BannerThemeControl/BannerThemeControl.ascx.cs"
    Inherits="WebSitesCreative.Modules.Banner.Controls.BannerThemeControl" %>
<%@ Import Namespace="WebSitesCreative.Modules.Banner.Components.Helpers" %>
<%@ Register TagPrefix="cs" Namespace="WebSitesCreative.Modules.Banner.Controls"
    Assembly="WebSitesCreative.Modules.Banner" %>

<script runat="server" language="C#">
protected void Page_Load(object sender, EventArgs e)
{
    RegisterJS("../../js/jquery.carouFredSel-6.2.1-packed.js");
}
</script>

<cs:InlineScript runat="server">
    <script type="text/javascript" language="javascript">
        $(window).load(function () {
            $('.slider-related-wrapper .show').parents('.slider-related-wrapper').css('padding', '40px 0 0');
            $('.slider-related-wrapper #slider_related.wsc_call_module<%=ModuleId%>').parents('.slider-related-wrapper').css('display', 'block');
            var portfolioCarousel = $("#slider_related.wsc_call_module<%=ModuleId%>");
            portfolioCarousel.carouFredSel({
                width: '100%', height: "auto", responsive: true,
                circular: true, 
                infinite: true, 
                auto: {
                    play: <%=GetSetting("autoplay") %>, 
                    timeoutDuration: <%=GetSetting("timeout") %> 
                },
                items: {
                    width: <%=GetSetting("Width") %>, 
                    visible: { min: 1, max: <%=GetSetting("items") %>} 
                },
                mousewheel: <%=GetSetting("mousewheel") %>,
                scroll: {
                    easing: "linear",
                    pauseOnHover: <%=GetSetting("pause") %>,
                    duration: <%=GetSetting("duration") %>, 
                    items: <%=GetSetting("itemsScroll") %>
                },
                swipe: {
                    onMouse: true, 
                    onTouch: true,
                    duration: 500 
                },
                prev: { button: "#sl-prev<%=GetSetting("controlPosition") %>_related", key: "left" },
                next: { button: "#sl-next<%=GetSetting("controlPosition") %>_related", key: "right" },
                onCreate: function () {
                    $(window).on('resize', function () {
                        portfolioCarousel.parent().add(portfolioCarousel).css('height', portfolioCarousel.children().first().outerHeight() + 'px');
                    })
		        .trigger('resize');
                }
            });
        });
    </script>
</cs:InlineScript>


<div class="slider-related-wrapper">
<div id="slider_related" class="wsc_call_module<%=ModuleId%>">
  <asp:Repeater ID="rptrImages" runat="server">
        <ItemTemplate>
            <asp:PlaceHolder ID="pnlDinamic" runat="server" Visible='<%# ((bool)Eval("IsImageSlide")) %>'>
                <div class="slider-item">
				    <div class="slider-image">
                        <a href="<%# (Eval("Url") == "" ? "#_" : Eval("Url")) %>" title="" <%# Eval("Url") == "" ? "" : (GetSetting("linkTarget")) %>>
                            <img src='<%# FilesHelper.GetFileUrl(ModuleId, Eval("ResizedImageName").ToString()) %>' alt="">
                        </a>
                    </div>                    
                </div>
            </asp:PlaceHolder>
        </ItemTemplate>
    </asp:Repeater>
</div>
<div id="sl-prev<%=GetSetting("controlPosition") %>_related" class="<%=GetSetting("control") %> widget-scroll-prev<%=GetSetting("controlPosition") %>"><i class="fa fa-chevron-left white"></i></div>
<div id="sl-next<%=GetSetting("controlPosition") %>_related" class="<%=GetSetting("control") %> widget-scroll-next<%=GetSetting("controlPosition") %>"><i class=" fa fa-chevron-right white but_marg"></i></div>
</div>    