﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Tags.ascx.cs" Inherits="WebSitesCreative.Modules.Banner.Controls.Tags" %>
<div id="options">
    <ul id="filters" class="option-set">
    <li><a href="#filter" data-filter="*" class="selected">All</a></li>
        <asp:Repeater runat="server" ID="rptTags">
            <ItemTemplate>
                <li>
                    <a href="#filter" data-filter=".<%# Eval("Name")%>"><%# Eval("Name")%></a>   
                </li>
            </ItemTemplate>
        </asp:Repeater>
    </ul>
    <div class="clear"></div>
</div>
