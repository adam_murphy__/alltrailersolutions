﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="../../Controls/BannerThemeControl/BannerThemeControl.ascx.cs"
    Inherits="WebSitesCreative.Modules.Banner.Controls.BannerThemeControl" %>
<%@ Import Namespace="WebSitesCreative.Modules.Banner.Components.Helpers" %>
<%@ Register TagPrefix="cs" Namespace="WebSitesCreative.Modules.Banner.Controls"
    Assembly="WebSitesCreative.Modules.Banner" %>
<%@ Register TagPrefix="tags" TagName="Tags" Src="Tags.ascx" %>

<script runat="server" language="C#">
    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetSetting("prtfGalleryMode") == "tags") {
            RegisterJS("../../js/isotope.pkgd.min.js");
        } else {    
            RegisterJS("../../js/jPages.js");
        }            
    }
</script>

<%if (GetSetting("prtfGalleryMode") == "tags")
{ %>

    <%if (GetSetting("slideMargin") == " no_margin")
    { %>
        <div class="no_margin">
    <%} else { %>
        <div class="video_tags">
    <%} %>
            <tags:Tags ID="Tags1" runat="server" /> 
        </div>


            <div class="wsc_portfolio_gal wsc_portfolio_video portfolio_module<%=ModuleId%> row isotope">
                <asp:Repeater ID="rptrImages" runat="server">
                    <ItemTemplate>
                        <asp:PlaceHolder ID="pnlDinamic" runat="server" Visible='<%# ((bool)Eval("IsImageSlide")) %>'>
                            <div class="<%#(GetSetting("prtfGalleryLayout"))%> item_wrapper <%#(GetSetting("slideMargin"))%> isotope-item <%#  String.Join(" ", ((string[])Eval("SplittedTags"))) %>" data-category="<%#  String.Join(" ", ((string[])Eval("SplittedTags"))) %>">
                                <div class="vendor">
                                    <iframe  src="<%# Eval("Url")%>"></iframe>
                                </div>
                            </div>
                        </asp:PlaceHolder>
                    </ItemTemplate>
                </asp:Repeater>
            </div>

        <cs:InlineScript ID="InlineScript" runat="server">
            <script type="text/javascript" language="javascript">
                $(window).load(function () {
                    var $container = $('.wsc_portfolio_gal.portfolio_module<%=ModuleId%>').imagesLoaded(function () {
                        // init Isotope after all images have loaded
                        $container.isotope({
                            itemSelector: '.isotope-item',
                            transitionDuration: '0.6s',
                            percentPosition: true
                        });
                    });
                    $('#filters.option-set li').on('click', 'a', function () {
                        var filterValue = $(this).attr('data-filter');
                        // use filterFn if matches value
                        $container.isotope({ filter: filterValue });
                    });
                    $('#options #filters li').on('click', 'a', function () {
                        $('#options #filters li a.selected').removeClass('selected');
                        $(this).addClass('selected');
                    });
                });
            </script>
        </cs:InlineScript>

<%} %>

<%if (GetSetting("prtfGalleryMode") == "pagination")
{ %>

    <div class="holder holder_module<%=ModuleId%> video">
    </div>

    <%if (GetSetting("slideMargin") == " no_margin")
    { %>
        <div class="row no_margin">
    <%} else {%>
        <div class="row">
    <%} %>
        <ul id="itemContainer">
            <asp:Repeater ID="rptrImages2" runat="server">
                <ItemTemplate>
                    <asp:PlaceHolder ID="pnlDinamic" runat="server" Visible='<%# ((bool)Eval("IsImageSlide")) %>'>
                    <li>
                        <div class="<%#(GetSetting("prtfGalleryLayout"))%> item_wrapper <%#(GetSetting("slideMargin"))%>">
                            <div class="vendor">
                                <iframe src="<%# Eval("Url")%>"></iframe>
                            </div>
                        </div>
                    </li>
                    </asp:PlaceHolder>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
    </div>

    <div class="holder holder_module<%=ModuleId%> video">
    </div>

    <cs:InlineScript ID="InlineScript2" runat="server">
        <script type="text/javascript" language="javascript">
            $(window).load(function () {
                $(".holder_module<%=ModuleId%>.video + .row").css("display", "block");
                $("div.holder_module<%=ModuleId%>.video").jPages({
                    containerID  : "itemContainer",
                    perPage      : <%=GetSetting("PerPage") %>, 
		            keyBrowse   : true 
                });
            });
        </script>
    </cs:InlineScript>

<%} %>