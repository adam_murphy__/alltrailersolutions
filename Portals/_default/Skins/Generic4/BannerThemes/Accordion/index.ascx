﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="../../Controls/BannerThemeControl/BannerThemeControl.ascx.cs"
    Inherits="WebSitesCreative.Modules.Banner.Controls.BannerThemeControl" %>
<%@ Import Namespace="WebSitesCreative.Modules.Banner.Components.Helpers" %>
<%@ Register TagPrefix="cs" Namespace="WebSitesCreative.Modules.Banner.Controls"
    Assembly="WebSitesCreative.Modules.Banner" %>

<div id="accordion_<%=ModuleId%>" class="accordion panel-group">

    <asp:Repeater ID="rptrImages" runat="server">
        <ItemTemplate>
            <asp:PlaceHolder ID="PlaceHolder" runat="server" Visible='<%# !((bool)Eval("IsImageSlide")) %>'>
                <div class="panel">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="#accordion_<%=ModuleId%>_<%# (Container.ItemIndex + 1)%>" data-parent="#accordion_<%=ModuleId%>" data-toggle="collapse" class="accordion-toggle">
					        <%# Eval("Title")%></a>
                        </h4>
                    </div>
                    <div id="accordion_<%=ModuleId%>_<%# (Container.ItemIndex + 1)%>" class="panel-collapse collapse <%# (((Container.ItemIndex + 1) == (int.Parse(GetSetting("openslide")))) ? "in" : "") %>">
                        <div class="panel-body">
                            <%# Eval("Text")%>
                        </div>
                    </div>
                </div>
            </asp:PlaceHolder>
        </ItemTemplate>
    </asp:Repeater>

 </div>
