﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="../../Controls/BannerThemeControl/BannerThemeControl.ascx.cs"
    Inherits="WebSitesCreative.Modules.Banner.Controls.BannerThemeControl" %>
<%@ Import Namespace="WebSitesCreative.Modules.Banner.Components.Helpers" %>
<%@ Register TagPrefix="cs" Namespace="WebSitesCreative.Modules.Banner.Controls"
    Assembly="WebSitesCreative.Modules.Banner" %>

<script runat="server" language="C#">
protected void Page_Load(object sender, EventArgs e)
{
    RegisterJS("../../js/jquery.carouFredSel-6.2.1-packed.js");
}
</script>

<%if ((GetSetting("titleType") == " carousel_title_default") && (GetSetting("items") != "1"))
{ %>
<cs:InlineScript runat="server">
    <script type="text/javascript" language="javascript">
        var descriptionPos = function () {
                $('.portfolio-overlay').each(function () {
                    var defaultHeight = <%=GetSetting("Height") %>;
                    var itemHeight = $(this).parents('.carousel_title_default.slider-item').outerHeight();
                    if (defaultHeight > itemHeight) {
                        $(this).css('height', itemHeight);
                    } else {
                        $(this).css('height', defaultHeight);
                    };
                });
        }
        $(window).load(function () {
            $('.slider-related-wrapper .show').parents('.slider-related-wrapper').css('padding', '40px 0 0');
            $('.slider-related-wrapper #slider_related.wsc_call_module<%=ModuleId%>').parents('.slider-related-wrapper').css('display', 'block');
            var portfolioCarousel = $("#slider_related.wsc_call_module<%=ModuleId%>");
            portfolioCarousel.carouFredSel({
                width: "100%", height: "variable", responsive: true,
                            circular: <%=GetSetting("circular") %>, 
                            infinite: <%=GetSetting("infinite") %>, 
                            auto: {
                                play: <%=GetSetting("autoplay") %>, 
                                timeoutDuration: <%=GetSetting("timeout") %>, 
                                delay: <%=GetSetting("delay") %>
                            },
                            items: {
                                width: <%=GetSetting("Width") %>, 
                                visible: { min: 1, max: <%=GetSetting("items") %>} 
                            },
                            mousewheel: <%=GetSetting("mousewheel") %>,
                            scroll: {
                                easing: "swing",
                                pauseOnHover: <%=GetSetting("pause") %>, 
                                items: <%=GetSetting("itemsScroll") %>
                            },
                            swipe: {
                                onMouse: true, 
                                onTouch: true 
                            },
                            prev: { button: "#sl-prev<%=GetSetting("controlPosition") %>_related_module<%=ModuleId%>", key: "left" },
                            next: { button: "#sl-next<%=GetSetting("controlPosition") %>_related_module<%=ModuleId%>", key: "right" }
            });
            descriptionPos();
        });
        $(window).resize(function() {
            descriptionPos();
        });
    </script>
</cs:InlineScript>
<%} else if ((GetSetting("titleType") != " carousel_title_default") && (GetSetting("items") != "1")) { %>
<cs:InlineScript ID="InlineScript2" runat="server">
    <script type="text/javascript" language="javascript">
        if ("<%=GetSetting("titleType") %>" == " carousel_overlay") {
            var descriptionPos = function () {
                $('.item_description').each(function () {
                    var itemHeight = $(this).parents('.carousel_overlay.slider-item').outerHeight();
                    $(this).css('padding-top', (itemHeight/ 2 - 40));
                });
            }
        };
        $(window).load(function () {
            $('.slider-related-wrapper .show').parents('.slider-related-wrapper').css('padding', '40px 0 0');
            $('.slider-related-wrapper #slider_related.wsc_call_module<%=ModuleId%>').parents('.slider-related-wrapper').css('display', 'block');
            var portfolioCarousel = $("#slider_related.wsc_call_module<%=ModuleId%>");
            portfolioCarousel.carouFredSel({
                width: '100%', height: "auto", responsive: true,
                circular: <%=GetSetting("circular") %>, 
                infinite: <%=GetSetting("infinite") %>, 
                auto: {
                    play: <%=GetSetting("autoplay") %>, 
                    timeoutDuration: <%=GetSetting("timeout") %>, 
                    delay: <%=GetSetting("delay") %>
                    },
                items: { 
                    width: <%=GetSetting("Width") %>, 
                    visible: { min: 1, max: <%=GetSetting("items") %>} 
                    },
                mousewheel: <%=GetSetting("mousewheel") %>,
                scroll: {
                    easing: "swing",
                    fx: "scroll", 
                    pauseOnHover: <%=GetSetting("pause") %>,
                    items: <%=GetSetting("itemsScroll") %> 
                    },
                swipe: {
                    onMouse: true, 
                    onTouch: true 
                    },
                prev: { button: "#sl-prev<%=GetSetting("controlPosition") %>_related_module<%=ModuleId%>", key: "left" },
                next: { button: "#sl-next<%=GetSetting("controlPosition") %>_related_module<%=ModuleId%>", key: "right" },
                onCreate: function () {
                    $(window).on('resize', function () {
                        portfolioCarousel.parent().add(portfolioCarousel).css('height', portfolioCarousel.children().first().outerHeight() + 'px');
                    })
		        .trigger('resize');
                }
            });
            if ("<%=GetSetting("titleType") %>" == " carousel_overlay") {
                descriptionPos();
            };
        });
        if ("<%=GetSetting("titleType") %>" == " carousel_overlay") {
            $(window).resize(function() {
                descriptionPos();
            });
        };
    </script>
</cs:InlineScript>
<%} else { %>
<cs:InlineScript ID="InlineScript3" runat="server">
    <script type="text/javascript" language="javascript">
        $(window).load(function () {
            $('.slider-single-wrapper .show').parents('.slider-single-wrapper').css('padding', '24px 0 0');
            $('.slider-single-wrapper #slider_small.wsc_call_module<%=ModuleId%>').parents('.slider-single-wrapper').css('display', 'block');
            var portfolioCarousel = $("#slider_small.wsc_call_module<%=ModuleId%>");
            portfolioCarousel.carouFredSel({
                width: '100%', height: "auto", responsive: true,
                circular: <%=GetSetting("circular") %>, 
                infinite: <%=GetSetting("infinite") %>, 
                auto: {
                    play: <%=GetSetting("autoplay") %>, 
                    timeoutDuration: <%=GetSetting("timeout") %>, 
                    delay: <%=GetSetting("delay") %>
                    },
                items: { 
                    width: <%=GetSetting("Width") %>, 
                    visible: { min: 1, max: 1} 
                    },
                mousewheel: <%=GetSetting("mousewheel") %>, 
                scroll: {
                    easing: "swing",
                    fx: "<%=GetSetting("transition") %>", 
                    pauseOnHover: <%=GetSetting("pause") %>,
                    items: <%=GetSetting("itemsScroll") %> 
                    },
                swipe: {
                    onMouse: true, /* Determines whether the carousel should scroll via dragging (on non-touch-devices only). "true"/"false" */
                    onTouch: true /* Determines whether the carousel should scroll via swiping gestures (on touch-devices only). "true"/"false" */
                    },
                prev: { button: "#sl-prev<%=GetSetting("controlPosition") %>_single_module<%=ModuleId%>", key: "left" },
                next: { button: "#sl-next<%=GetSetting("controlPosition") %>_single_module<%=ModuleId%>", key: "right" },
                onCreate: function () {
                    $(window).on('resize', function () {
                        portfolioCarousel.parent().add(portfolioCarousel).css('height', portfolioCarousel.children().first().outerHeight() + 'px');
                    })
		        .trigger('resize');
                }
            });  
        });
    </script>
</cs:InlineScript>
<%} %>

<%if (GetSetting("items") != "1")
{ %>
<div class="slider-related-wrapper">
<div id="slider_related" class="wsc_call_module<%=ModuleId%>">
  <asp:Repeater ID="rptrImages" runat="server">
        <ItemTemplate>
            <asp:PlaceHolder ID="pnlDinamic" runat="server" Visible='<%# ((bool)Eval("IsImageSlide")) %>'>
                <div class="slider-item <%# (GetSetting("titleType")) %> <%# (GetSetting("prtfGalleryTitle")) %>">
				    <div class="slider-image">
                        <img src="<%# FilesHelper.GetFileUrl(ModuleId, Eval("ResizedImageName").ToString()) %>" alt="">
                        <div class="portfolio-overlay">
                            <%if (GetSetting("titleType") == " carousel_overlay")
                            { %>
                            <div class="item_description">
                                <a href="#_"><%# Eval("Title")%></a><br />
                            </div>
                            <%} %>
                            <a href="<%# FilesHelper.GetFileUrl(ModuleId, Eval("ImageName").ToString()) %>" class="<%# Eval("Url") == "" ? "center-icon" : "left-icon" %>" data-rel="prettyPhoto[portfolio<%=ModuleId%>]"><i class="fa fa-plus"></i></a>
                            <a href="<%# Eval("Url") %>" class="right-icon <%# Eval("Url") == "" ? "hidden" : "" %>" <%# (GetSetting("linkTarget")) %>"><i class="fa fa-link"></i></a>
                        </div>
                    </div>
                    <%if ((GetSetting("prtfGalleryTitle") == " show_title") && (GetSetting("titleType") != " carousel_overlay"))
                    { %>
                    <div class="slider-title">
                        <%if (GetSetting("prtfGalleryTitle") == " show_title")
                        { %>
                        <h3><a href="#"><%# Eval("Title")%></a></h3>
                        <%} %>
                        <%if (GetSetting("titleType") == " carousel_title_default")
                        { %>
                        <%# Eval("Description")%>
                        <%} %>
                    </div>
                    <%} %>
                </div>
            </asp:PlaceHolder>
        </ItemTemplate>
    </asp:Repeater>
</div>
<div id="sl-prev<%=GetSetting("controlPosition") %>_related_module<%=ModuleId%>" class="<%=GetSetting("control") %> widget-scroll-prev<%=GetSetting("controlPosition") %> module<%=ModuleId%>"><i class="fa fa-chevron-left white"></i></div>
<div id="sl-next<%=GetSetting("controlPosition") %>_related_module<%=ModuleId%>" class="<%=GetSetting("control") %> widget-scroll-next<%=GetSetting("controlPosition") %> module<%=ModuleId%>"><i class=" fa fa-chevron-right white but_marg"></i></div>
</div>    

<cs:InlineScript ID="InlineScript1" runat="server">
    <script type="text/javascript" language="javascript">
        $('.slider-related-wrapper .portfolio-overlay .item_description').css('padding-top', ($('.slider-related-wrapper .slider-item').height() / 2 - 40));
    </script>
</cs:InlineScript>
<%} else { %>
<div class="slider-single-wrapper">
<div class="slider-item-wrapper">
<div id="slider_small" class="wsc_call_module<%=ModuleId%>">
  <asp:Repeater ID="rptrImages2" runat="server">
        <ItemTemplate>
            <asp:PlaceHolder ID="pnlDinamic2" runat="server" Visible='<%# ((bool)Eval("IsImageSlide")) %>'>
                <div class="slider-item <%# (GetSetting("titleType")) %>">
				    <div class="slider-image">
                        <img src='<%# FilesHelper.GetFileUrl(ModuleId, Eval("ResizedImageName").ToString()) %>' alt="">
				        <div class="slider-single-overlay">
                            <%if (GetSetting("titleType") == " carousel_overlay")
                            { %>
                            <div class="item_description">
                                <a href="#_"><%# Eval("Title")%></a><br />
                            </div>
                            <%} %>
                            <a href="<%# FilesHelper.GetFileUrl(ModuleId, Eval("ImageName").ToString()) %>" class="<%# Eval("Url") == "" ? "center-icon" : "left-icon" %>" data-rel="prettyPhoto[portfolio<%=ModuleId%>]"><i class="fa fa-plus"></i></a>
                            <a href="<%# Eval("Url") %>" class="right-icon <%# Eval("Url") == "" ? "hidden" : "" %>" <%# (GetSetting("linkTarget")) %>"><i class="fa fa-link"></i></a>
                        </div>
                    </div>                    
                </div>
            </asp:PlaceHolder>
        </ItemTemplate>
    </asp:Repeater>
</div>
<div id="sl-prev<%=GetSetting("controlPosition") %>_single_module<%=ModuleId%>" class="<%=GetSetting("control") %> widget-scroll-prev<%=GetSetting("controlPosition") %> module<%=ModuleId%>"><i class="fa fa-chevron-left white"></i></div>
<div id="sl-next<%=GetSetting("controlPosition") %>_single_module<%=ModuleId%>" class="<%=GetSetting("control") %> widget-scroll-next<%=GetSetting("controlPosition") %> module<%=ModuleId%>"><i class=" fa fa-chevron-right white but_marg"></i></div>
</div> 
</div>
<%} %>