﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="../../Controls/BannerThemeControl/BannerThemeControl.ascx.cs"
    Inherits="WebSitesCreative.Modules.Banner.Controls.BannerThemeControl" %>
<%@ Import Namespace="WebSitesCreative.Modules.Banner.Components.Helpers" %>
<%@ Register TagPrefix="cs" Namespace="WebSitesCreative.Modules.Banner.Controls"
    Assembly="WebSitesCreative.Modules.Banner" %>
<%@ Register TagPrefix="tags" TagName="Tags" Src="Tags.ascx" %>

<%--<script runat="server" language="C#">
    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetSetting("prtfGalleryMode") == "tags") {
            RegisterJS("../../js/isotope.pkgd.min.js");
        } else {    
            RegisterJS("../../js/jPages.js");
        }            
    }
</script>--%>

<%if (GetSetting("prtfGalleryMode") == "tags")
{ %>

<%if (GetSetting("slideMargin") == " no_margin")
{ %>
<div class="no_margin">
<%} %>
    <div class="<%=GetSetting("prtfGalleryControlAlignment")%> <%=GetSetting("prtfGalleryControlStyle")%>">
        <tags:Tags ID="Tags1" runat="server" />
    </div>
<%if (GetSetting("slideMargin") == " no_margin")
{ %>
</div>
<%} %>

    <div class="wsc_portfolio_gal portfolio_module<%=ModuleId%> row isotope">
        <asp:Repeater ID="rptrImages" runat="server">
            <ItemTemplate>
                <asp:PlaceHolder ID="pnlDinamic" runat="server" Visible='<%# ((bool)Eval("IsImageSlide")) %>'>
                    <div class="isotope-item <%# (GetSetting("prtfGalleryLayout")) %> <%# (GetSetting("prtfGalleryType")) %>  <%# (GetSetting("slideMargin")) %> <%# (GetSetting("titleType")) %> <%# (GetSetting("prtfGalleryTitle")) %> item_wrapper <%#  String.Join(" ", ((string[])Eval("SplittedTags"))) %>" data-category="<%#  String.Join(" ", ((string[])Eval("SplittedTags"))) %>">
                        <div class="portfolio_image">
                            <%if (GetSetting("prtfGalleryType") == " type_default")
                            { %>
                            <img src="<%# FilesHelper.GetFileUrl(ModuleId, Eval("ResizedImageName").ToString()) %>" alt="">
                            <%} else { %>
                            <img src="<%# FilesHelper.GetFileUrl(ModuleId, Eval("ImageName").ToString()) %>" alt="">
                            <%} %>
                            <div class="portfolio-overlay">
                                <%if (GetSetting("titleType") == " overlay")
                                { %>
                                <div class="item_description">
                                    <a href="<%# Eval("Url") %>" <%# (GetSetting("linkTarget")) %>><%# Eval("Title")%></a><br />
                                </div>
                                <%} %>
                                <%if (GetSetting("linkOnly") == "false")
                                { %>
                                <a href="<%# FilesHelper.GetFileUrl(ModuleId, Eval("ImageName").ToString()) %>" class="<%# Eval("Url") == "" ? "center-icon" : "left-icon" %>" data-rel="prettyPhoto[portfolio<%=ModuleId%>]"><i class="fa fa-plus"></i></a>
                                <a href="<%# Eval("Url") %>" class="right-icon <%# Eval("Url") == "" ? "hidden" : "" %>" <%# (GetSetting("linkTarget")) %>><i class="fa fa-link"></i></a>
                                <%} else{ %>
                                <a href="<%# Eval("Url") %>" class="center-icon <%# Eval("Url") == "" ? "hidden" : "" %>" <%# (GetSetting("linkTarget")) %>><i class="fa fa-link"></i></a>
                                <%} %>
                            </div>
                        </div>
                        <%if ((GetSetting("prtfGalleryTitle") == " show_title") && (GetSetting("titleType") != " overlay"))
                          { %>
                        <div class="item_description">
                            <a href="<%# Eval("Url") %>" <%# (GetSetting("linkTarget")) %>>
                                <%# Eval("Title")%></a><br />
                        </div>
                        <%} %>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="pnlTextSlide" runat="server" Visible='false'>
                    <%# Eval("Text") %>
                </asp:PlaceHolder>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    
    <cs:InlineScript ID="InlineScript" runat="server">
        <script type="text/javascript" src="<%= TemplateSourceDirectory %>/../../js/isotope.pkgd.min.js"></script>
        <script type="text/javascript" language="javascript">
            if (!($('#options ul#filters.module<%=ModuleId%> li:nth-child(2)').length)) {
                $('#options ul#filters.module<%=ModuleId%>').parents('#options').css("display", "none");
            }
            var galleryMix = function () {
                var elemWidth = $(".wsc_portfolio_gal.portfolio_module<%=ModuleId%> .item_wrapper").outerWidth();
                var elemHeight = $(".wsc_portfolio_gal.portfolio_module<%=ModuleId%> .item_wrapper").outerHeight(true);
                if ("<%=GetSetting("slideMargin") %>" == " no_margin") {
                    if ("<%=GetSetting("prtfGalleryLayout") %>" == "col-md-6 col-sm-6 col-xs-12") {
                        $(".wsc_portfolio_gal.portfolio_module<%=ModuleId%> .item_wrapper:nth-child(3n+3)").css("width",elemWidth*2);
                        $(".wsc_portfolio_gal.portfolio_module<%=ModuleId%> .item_wrapper:nth-child(3n+3)").css("height",elemHeight*2);
                    } else if ("<%=GetSetting("prtfGalleryLayout") %>" == "col-md-4 col-sm-6 col-xs-12") {
                        $(".wsc_portfolio_gal.portfolio_module<%=ModuleId%> .item_wrapper:nth-child(5n+2)").css("width",(elemWidth*2));
                        $(".wsc_portfolio_gal.portfolio_module<%=ModuleId%> .item_wrapper:nth-child(5n+2)").css("height",(elemHeight*2));
                    } else {
                        $(".wsc_portfolio_gal.portfolio_module<%=ModuleId%> .item_wrapper:nth-child(9n+5)").css("width",elemWidth*2);
                        $(".wsc_portfolio_gal.portfolio_module<%=ModuleId%> .item_wrapper:nth-child(9n+5)").css("height",elemHeight*2);
                    };
                } else {
                    if ("<%=GetSetting("prtfGalleryLayout") %>" == "col-md-6 col-sm-6 col-xs-12") {
                        $(".wsc_portfolio_gal.portfolio_module<%=ModuleId%> .item_wrapper:nth-child(3n+3)").css("width",elemWidth*2);
                        $(".wsc_portfolio_gal.portfolio_module<%=ModuleId%> .item_wrapper:nth-child(3n+3)").css("height",elemHeight*2-20);
                    } else if ("<%=GetSetting("prtfGalleryLayout") %>" == "col-md-4 col-sm-6 col-xs-12") {
                        $(".wsc_portfolio_gal.portfolio_module<%=ModuleId%> .item_wrapper:nth-child(5n+2)").css("width",(elemWidth*2));
                        $(".wsc_portfolio_gal.portfolio_module<%=ModuleId%> .item_wrapper:nth-child(5n+2)").css("height",(elemHeight*2-20));
                    } else {
                        $(".wsc_portfolio_gal.portfolio_module<%=ModuleId%> .item_wrapper:nth-child(9n+5)").css("width",elemWidth*2);
                        $(".wsc_portfolio_gal.portfolio_module<%=ModuleId%> .item_wrapper:nth-child(9n+5)").css("height",elemHeight*2-40);
                    };
                };
            };
            var descriptionPos = function () {
                $('.no_title.margin .item_description').css('width', $('.no_title.margin').width());
                $('.item_description').each(function () {
                    var itemHeight = $(this).parents('.overlay.item_wrapper').outerHeight();
                    $(this).css('padding-top', (itemHeight/ 2 - 40));
                });
            };
            $(window).load(function () {
                $('#filters li a').each(function () {
                    var _this = $(this);
                    var tag_text = $(this).html();
                    var new_tag_text = tag_text.replace(/_/g, ' ');
                    new_tag_text = new_tag_text.replace(/aNd/, '&amp;');
                    _this.html(new_tag_text);
                });
                var $container = $('.wsc_portfolio_gal.portfolio_module<%=ModuleId%>').imagesLoaded(function () {
                    // init Isotope after all images have loaded
                    $container.isotope({
                        itemSelector: '.isotope-item',
                        transitionDuration: '0.6s',
                        percentPosition: true
                    });
                });
                $('#filters.option-set.module<%=ModuleId%> li').on('click', 'a', function() {
                    var filterValue = $(this).attr('data-filter');
                    // use filterFn if matches value
                    $container.isotope({ filter: filterValue });
                });
                $('#options #filters.module<%=ModuleId%> li').on('click', 'a', function() {
                    $('#options #filters.module<%=ModuleId%> li a.selected').removeClass('selected');
                    $(this).addClass('selected');
                });
                <%if ((GetSetting("prtfGalleryMixing") == "mixing_on") && (GetSetting("prtfGalleryType") == " type_default")  && (GetSetting("titleType") != " title_default"))
                { %>
                if ($(window).width() > 767) {galleryMix();}
                <%} %>
                descriptionPos();
            });
            $(window).resize(function() {
                <%if ((GetSetting("prtfGalleryMixing") == "mixing_on") && (GetSetting("prtfGalleryType") == " type_default") && (GetSetting("titleType") != " title_default"))
                { %>
                    if ($(window).width() > 767) {galleryMix();}
                <%} %>
                descriptionPos();
            });
        </script>
    </cs:InlineScript>   
<%} %>

<%if (GetSetting("prtfGalleryMode") == "pagination")
{ %>
    <div class="<%=GetSetting("prtfGalleryControlAlignment")%>  <%=GetSetting("prtfGalleryControlStyle")%>">
        <div class="holder holder_module<%=ModuleId%> images">
        </div>
    </div>

    <div class="row">
    <ul id="itemContainer">
        <asp:Repeater ID="rptrImages2" runat="server">
            <ItemTemplate>
                <asp:PlaceHolder ID="pnlDinamic" runat="server" Visible='<%# ((bool)Eval("IsImageSlide")) %>'>
                    <li>
                    <div class="<%#(GetSetting("prtfGalleryLayout"))%> item_wrapper <%#(GetSetting("slideMargin"))%> <%# (GetSetting("titleType")) %> <%# (GetSetting("prtfGalleryTitle")) %>">
                        <div class="portfolio_image">
                            <img src="<%# FilesHelper.GetFileUrl(ModuleId, Eval("ResizedImageName").ToString()) %>" alt="">
                            <div class="portfolio-overlay">
                                <%if (GetSetting("titleType") == " overlay")
                                { %>
                                <div class="item_description">
                                    <a href="<%# Eval("Url") %>" <%# (GetSetting("linkTarget")) %>><%# Eval("Title")%></a><br />
                                </div>
                                <%} %>
                                <a href="<%# FilesHelper.GetFileUrl(ModuleId, Eval("ImageName").ToString()) %>" class="<%# Eval("Url") == "" ? "center-icon" : "left-icon" %>" data-rel="prettyPhoto[portfolio<%=ModuleId%>]"><i class="fa fa-plus"></i></a>
                                <a href="<%# Eval("Url") %>" class="right-icon <%# Eval("Url") == "" ? "hidden" : "" %>" <%# (GetSetting("linkTarget")) %>><i class="fa fa-link"></i></a>
                            </div>
                        </div>
                        <%if ((GetSetting("prtfGalleryTitle") == " show_title") && (GetSetting("titleType") != " overlay"))
                          { %>
                        <div class="item_description">
                            <a href="<%# Eval("Url") %>" <%# (GetSetting("linkTarget")) %>>
                                <%# Eval("Title")%></a><br />
                        </div>
                        <%} %>
                    </div>
                    </li>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="pnlTextSlide" runat="server" Visible='false'>
                    <%# Eval("Text") %>
                </asp:PlaceHolder>
            </ItemTemplate>
        </asp:Repeater>
    </ul>
    </div>

    <div class="<%=GetSetting("prtfGalleryControlAlignment")%>  <%=GetSetting("prtfGalleryControlStyle")%>">
        <div class="holder holder_module<%=ModuleId%> images">
        </div>
    </div>

<cs:InlineScript ID="InlineScript2" runat="server">

    <script type="text/javascript" src="<%= TemplateSourceDirectory %>/../../js/jPages.js"></script>
    <script type="text/javascript" language="javascript">
        $(window).load(function () {
            $(".holder_module<%=ModuleId%>.images + .row").css("display", "block");
            $("div.holder_module<%=ModuleId%>.images").jPages({
                containerID  : "itemContainer",
                perPage      : <%=GetSetting("PerPage") %>, 
		        keyBrowse   : true 
            });
            if (!($(".holder_module<%=ModuleId%> a:contains('2')").length)) {
                $('.holder_module<%=ModuleId%>').css("display", "none");
            };
            $('.no_title.margin .item_description').css('width', $('.no_title.margin').width());
            $('.overlay .portfolio-overlay .item_description').css('padding-top', ($('.overlay.item_wrapper').height()/2 - 40));
        });
        $(window).resize(function() {
            $('.no_title.margin .item_description').css('width', $('.no_title.margin').width());
            $('.overlay .portfolio-overlay .item_description').css('padding-top', ($('.overlay.item_wrapper').height()/2 - 40));
        });
    </script>
</cs:InlineScript>

<%} %>