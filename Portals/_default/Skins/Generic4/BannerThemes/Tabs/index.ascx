﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="../../Controls/BannerThemeControl/BannerThemeControl.ascx.cs"
    Inherits="WebSitesCreative.Modules.Banner.Controls.BannerThemeControl" %>
<%@ Import Namespace="WebSitesCreative.Modules.Banner.Components.Helpers" %>
<%@ Register TagPrefix="cs" Namespace="WebSitesCreative.Modules.Banner.Controls"
    Assembly="WebSitesCreative.Modules.Banner" %>

<div class="tabbable tabs-<%=GetSetting("tabsPosition") %>">
<ul id="myTab_<%=ModuleId%>" class="nav nav-tabs">
    <asp:Repeater ID="rptrImages" runat="server">
        <ItemTemplate>
            <asp:PlaceHolder ID="PlaceHolder" runat="server" Visible='<%# !((bool)Eval("IsImageSlide")) %>'>
                <li class='<%# (GetSetting("Active") == (Container.ItemIndex + 1).ToString()) ? "active" : "" %>'><a href="#tab<%# (Container.ItemIndex + 1)%>_<%=ModuleId%>" data-toggle="tab">
                    <%# Eval("Title")%></a> </li>
            </asp:PlaceHolder>
        </ItemTemplate>
    </asp:Repeater>
</ul>
<div id="myTabContent_<%=ModuleId%>" class="tab-content">
    <asp:Repeater ID="rptrImages2" runat="server">
        <ItemTemplate>
            <asp:PlaceHolder ID="pnlTextSlide2" runat="server" Visible='<%# !((bool)Eval("IsImageSlide")) %>'>
                <div class="tab-pane <%# (GetSetting("Active") == (Container.ItemIndex + 1).ToString()) ? "active" : "" %>" id="tab<%# (Container.ItemIndex + 1)%>_<%=ModuleId%>">
                    <%# Eval("Text")%>
                </div>
            </asp:PlaceHolder>
        </ItemTemplate>
    </asp:Repeater>
</div>
</div>

 <cs:InlineScript ID="InlineScript1" runat="server">
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
        /*
            var openItem = <%=GetSetting("Active") %>;
            var $itemList = $('#myTab_<%=ModuleId%>.nav-tabs > li');
            var $contentList = $('#myTabContent-<%=ModuleId%>.tab-content div.tab-pane');
            
            if ((openItem > 0)&&(openItem <= $itemList.length)){
                $('#myTab-<%=ModuleId%>.nav-tabs').find($itemList[openItem-1]).addClass('active');
                $('#myTabContent-<%=ModuleId%>.tab-content').find($contentList[openItem-1]).addClass('active');
            }
            else{
                $('#myTab-<%=ModuleId%>.nav-tabs').find($itemList[0]).addClass('active');
                $('#myTabContent-<%=ModuleId%>.tab-content').find($contentList[0]).addClass('active');
            }
*/
        });
    </script>
</cs:InlineScript>
