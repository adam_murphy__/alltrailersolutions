<%@ Control Language="C#" Inherits="WebSitesCreative.Modules.PostIt.ViewPostItFeed"
    AutoEventWireup="true" CodeBehind="ViewPostItFeed.ascx.cs" %>
<%@ Import Namespace="WebSitesCreative.Modules.PostIt.Components" %>
<%@ Import Namespace="WebSitesCreative.Modules.PostIt.Components.Helpers" %>
<ul class="media-list">
    <asp:Repeater runat="server" ID="rptPosts" OnItemDataBound="rptPosts_ItemDataBound">
        <ItemTemplate>
        <li class="media">
            <a class="wsc_blog_feed_img" href='<%# UrlsHelper.GetPostUrl(Eval("Url") as string, FeedTabId) %>'>
                <img runat="server" id="img" class="img-rounded" src='<%# FilesHelper.GetThumbnailUrl(PostItFeedSettings.FeedModuleId, Eval("Image") as string) %>' alt='<%# Eval("Title") %>' title='<%# Eval("Title") %>' />
            </a>
            <div class="media-body">
                <small class="text-muted"><%# ((DateTime)Eval("Date")).ToShortDateString() %></small>
                <br />
                <a href='<%# UrlsHelper.GetPostUrl(Eval("Url") as string, FeedTabId) %>'>
                    <%# Eval("Title") %></a>
            </div>
            </li>
        </ItemTemplate>
    </asp:Repeater>
</ul>
