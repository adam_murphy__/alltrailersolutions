﻿<%@ Control Language="C#" Inherits="WebSitesCreative.Modules.PostIt.ViewPostIt" AutoEventWireup="true"
    CodeBehind="ViewPostIt.ascx.cs" %>
<%@ Import Namespace="WebSitesCreative.Modules.PostIt.Components.Helpers" %>
<%@ Register TagPrefix="wsc" TagName="ViewPost" Src="ViewPost.ascx" %>
<%@ Register TagPrefix="wsc" TagName="RssSubscribe" Src="Controls/RssSubscribe.ascx" %>
<%@ Register TagPrefix="wsc" TagName="Paging" Src="Controls/Paging.ascx" %>
<wsc:RssSubscribe runat="server"></wsc:RssSubscribe>
<asp:MultiView runat="server" ID="mvContent">
    <asp:View runat="server" ID="viewPostsFeed">
        <%if (IsEditable)
          { %>
        <script type="text/javascript" language="javascript">
          (function($) {
            $(document).ready(function() {

              $('p.wsc_post_summary').editableText({
                change: function(element, oldValue, newValue) {
                  var postId = $('input[type="hidden"]', $(element).parent())[0].value;
                  $.ajax({
                    type: "POST",
                    url: '<%= TemplateSourceDirectory %>/InPlaceEditingService.asmx/SaveSummary',
                    data: JSON.stringify({ summary: newValue, postId: postId, moduleId: <%=ModuleId %>, portalId: <%=PortalId %>, tabId: <%=TabId %> }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(data, status) {
                      if (data.d == false) {
                        element.html(oldValue);
                      }
                    },
                    error: function() { element.html(oldValue); }
                  });
                }
              });
            });
          })
        </script>
        <%} %>
        <div class="wsc_posts_list wsc_posts_common">
            <asp:Repeater runat="server" ID="rptPosts" OnItemDataBound="rptPosts_ItemDataBound">
                <ItemTemplate>
                    <div class="row wsc-margin-bottom-sm">
                        <!--date-->
                        <div class="col-blog col-md-1">
                            <div class="btn btn-medium btn-rounded btn-blog1">
                                <%# ((DateTime)Eval("Date")).Day %><br /><span class="wsc_date"><%# ((DateTime)Eval("Date")).ToString("MMM") %></span><br />
                                <i class="fa fa-comments fa-2x"></i><br /><a href="<%# UrlsHelper.GetPostUrl(Eval("Url") as string, TabId)%>#disqus_thread" class="com_no"></a>
                            </div>
                        </div>
                        <!--post-->
                        <div class="col-blog col-md-11">
                        <div class="wsc_pi_post_header">
                            <div class="wsc_pi_post_controls">
                                <asp:LinkButton CssClass="wsc_pi_button wsc_pi_control_button" runat="server" ID="lnkEditPost"
                                    OnCommand="lnkEdit_Command" CommandArgument='<%# Eval("ItemId") %>' Visible='<%# IsEditable %>'><%= GetLocalized("Edit") %></asp:LinkButton>
                                <asp:LinkButton CssClass="wsc_pi_button wsc_pi_control_button" runat="server" ID="lnkDelete"
                                    OnCommand="lnkDelete_Command" CommandArgument='<%# Eval("ItemId") %>' Visible='<%# IsEditable %>'><%= GetLocalized("Delete") %></asp:LinkButton>
                                <asp:LinkButton CssClass="wsc_pi_button wsc_pi_control_button" runat="server" ID="lnkChangePublish"
                                    OnCommand="lnkChangePublish_Command" CommandArgument='<%# Eval("ItemId") %>'
                                    Visible='<%# IsEditable %>'> <%# ((bool)Eval("Published")) ? GetLocalized("Unpublish") : GetLocalized("Publish") %></asp:LinkButton>
                                <asp:Label CssClass="NormalRed" runat="server" ID="lblUnpublish" resourcekey="UnpublishedStatus"
                                    Visible='<%# !(bool)Eval("Published") %>'></asp:Label>
                            </div>
                            <div class="wsc_image_wrapper">
                                <asp:PlaceHolder ID="PlaceHolder2" runat="server" Visible='<%# !String.IsNullOrEmpty(Eval("Image") as string) %>'>
                                    <img src='<%# FilesHelper.GetFileUrl(ModuleId, Eval("Image") as string) %>' runat="server"
                                            id="img" alt='<%# Eval("Title") %>' title='<%# Eval("Title") %>' />
                                    <div class="image_overlay">
                                        <a class="center-icon" href='<%# UrlsHelper.GetPostUrl(Eval("Url") as string, TabId) %>'><em class="fa fa-hand-pointer-o"></em></a>
                                    </div>
                                </asp:PlaceHolder>
                            </div>
                            <h1 class="post_link"><a href='<%# UrlsHelper.GetPostUrl(Eval("Url") as string, TabId) %>'><%# Eval("Title") %></a> <span runat='server' visible='<%# Eval("HasAudio") %>'><img src="<%= ResolveUrl(UIHelper.GetUIFileUrl("images/audio.png")) %>" style="width: 16px" alt='Audio' /></span></h1>
                            <div class="post-meta text-muted">
                                <ul>
                                    <li>
                                        <%=GetLocalized("ByUser")%>
                                        
                                            <%# Eval("CreatedByUserName") %> </li>
                                    <li>
                                        <asp:PlaceHolder runat="server" Visible='<%# !String.IsNullOrEmpty((String)Eval("Tags")) %>'>
                                            <%=GetLocalized("InTags")%>
                                            <asp:Repeater DataSource='<%# Eval("SplittedTags") %>' runat="server">
                                                <ItemTemplate>
                                                    <a class="wsc_tag" href='<%# UrlsHelper.GetTagUrl((String)Container.DataItem, TabId) %>'>
                                                        <%# Container.DataItem%></a>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </asp:PlaceHolder>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div>
                            <asp:HiddenField ID="hfPostId" runat="server" Value='<%#Eval("ItemId") %>' />
                            <p class="wsc_post_summary">
                                <%# Eval("Summary") %></p>
                        </div>
                        <div class="read_more">
                            <a class="" href='<%# UrlsHelper.GetPostUrl(Eval("Url") as string, TabId) %>'>
                                <%= GetLocalized("ViewMore") %></a>
                        </div>
                        <div class="pad30"></div>
                    </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
            <div class="row">
            <div class="col-md-1">&nbsp;</div>
            <div class="col-md-8">
            <div class="posts-paging">
                <wsc:Paging ID="Paging" runat="server"></wsc:Paging>
            </div>
            </div>
            </div>
        </div>
    </asp:View>
    <asp:View runat="server" ID="viewPost">
        <wsc:ViewPost runat="server" ID="viewPostControl"></wsc:ViewPost>
    </asp:View>
</asp:MultiView>
