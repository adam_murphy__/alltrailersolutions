﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Paging.ascx.cs" Inherits="WebSitesCreative.Modules.PostIt.Controls.Paging" %>
<%@ Import Namespace="WebSitesCreative.Modules.PostIt.Components.Helpers" %>
<div class="pagination">
    <ul>
        <li>
            <asp:PlaceHolder ID="phPrev" runat="server"><a class="wsc_tag" href='<%=GetPrevUrl() %>'>
                <%= GetLocalized("cmdPrev")%></a> </asp:PlaceHolder>
        </li>
        <asp:Repeater ID="rptPaging" runat="server">
            <ItemTemplate>
                <asp:PlaceHolder ID="pnlDinamic" runat="server" Visible='<%# Convert.ToBoolean(Eval("IsLink"))%>'>
                <li>
                    <a class='<%#Convert.ToBoolean(Eval("First")) ? " First ":""%> <%#Convert.ToBoolean(Eval("Last")) ? " Last ":""%>'
                        href='<%#GetPaggingUrl(Eval("PagingElementName").ToString()) %>'>
                        <%# Eval("PagingElementName")%></a> 
                </li>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="pnlStatic" runat="server" Visible='<%# !Convert.ToBoolean(Eval("IsLink")) %>'>
                <li class="<%#Convert.ToBoolean(Eval("Current")) ? " active ":""%>">
                    <span class='<%#Convert.ToBoolean(Eval("First")) ? " First ":""%> <%#Convert.ToBoolean(Eval("Last")) ? " Last ":""%> <%#Convert.ToBoolean(Eval("Ellipsis")) ? " Ellipsis ":""%>'>
                        <%# Eval("PagingElementName")%></span> 
                </li>
                </asp:PlaceHolder>
            </ItemTemplate>
        </asp:Repeater>
        <li>
            <asp:PlaceHolder ID="phNext" runat="server"><a class="wsc_tag" href='<%=GetNextUrl() %>'>
                <%= GetLocalized("cmdNext")%></a> </asp:PlaceHolder>
        </li>
    </ul>
</div>
