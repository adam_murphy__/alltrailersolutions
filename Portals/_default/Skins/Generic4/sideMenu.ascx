<%@ Control Language="vb" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="NAV" Src="~/Admin/Skins/Nav.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TEXT" Src="~/Admin/Skins/Text.ascx" %>
<%@ Register TagPrefix="dnn" TagName="BREADCRUMB" Src="~/Admin/Skins/BreadCrumb.ascx" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/User.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LINKS" Src="~/Admin/Skins/Links.ascx" %>
<%@ Register TagPrefix="dnn" TagName="PRIVACY" Src="~/Admin/Skins/Privacy.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TERMS" Src="~/Admin/Skins/Terms.ascx" %>
<%@ Register TagPrefix="dnn" TagName="COPYRIGHT" Src="~/Admin/Skins/Copyright.ascx" %>
<%@ Register TagPrefix="dnn" TagName="STYLES" Src="~/Admin/Skins/Styles.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="wsc" TagName="SWITCHER" src="~/DesktopModules/WebSitesCreative.StyleSwitcher/ViewSwitcher.ascx" %>

<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<%-- Common Styles --%>
<!--#include file="CommonParts/AddStyles.ascx"-->
    
<!--#include file="CommonParts/PreScripts.ascx"-->

<!-- Browser notification -->
<!--[if lte IE 8]>
<div class="browser-notification ie8">
	<p>Your browser (Internet Explorer 8 or lower) is <strong>out of date</strong>. It has known <strong>security flaws</strong> and may <strong>not display all features</strong> of this and other websites. <a href="//www.browser-update.org/update.html">Learn how to update your browser</a>.</p>
	<div class="close">X</div>
</div>
<![endif]-->
<!-- // Browser notification -->		
<div class="wsc_generic_skin wsc_sidebar_skin <%=GetSetting("FooterStyle")%> <%=GetSetting("Layout")%> <%=GetSetting("ChooseMenus")%> <%=GetSetting("HeaderAlignment")%>" data-loading="<%=GetSetting("Preloader")%>">
   
    <%-- Include Side Menu Left Aligned --%>
    <%If ((GetSetting("SideMenuAlignment") = "wsc-sidemenu-left") Or (GetSetting("SideMenuAlignment") = "")) Then%>
        <!--#include file="CommonParts/SideMenu.ascx"-->   
    <%End If%>

    <div class="wsc-content-wrap">
        <div class="wsc_generic_inside_wrap"><%--Inside Wrap--%>

        <!-- Top Pane -->
        <div class="wsc_pane TopPane" id="TopPane" runat="server"></div>
        <div class="clear"></div>

        <%If ((GetSetting("HeaderSwitcher") = "header_show") Or (GetSetting("HeaderSwitcher") = "")) Then%>
        <!--header-->
	    <section id="header" class="header <%=GetColorClass("HeaderBackgroundColor")%>">
            <%--Top Bar--%>
            <!--#include file="CommonParts/TopBarFluid.ascx"-->
            <%--End Top Bar--%>
		    <div class="container-fluid">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".main_menu.wsc_main">
			        <i class="fa fa-bars"></i>
			    </button>

                <!--logo-->
		        <div class="logo hidden-lg hidden-md">
                    <%-- Case of system logo --%>
                    <%If ((GetSetting("LogoCustomDefault") = "sm_logo_system") Or (GetSetting("LogoCustomDefault") = "")) Then%>
                        <dnn:LOGO runat="server" ID="dnnLOGOsideMenu" />
                    <%End If%>

                    <%-- Case of custom logo --%>
                    <%If (GetSetting("LogoCustomDefault") = "sm_logo_custom") Then%>
                        <a href="/"><img src='<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("CustomLogoFile")%>?v=<%=GetSetting("timestamp")%>' alt="" /></a>
                    <%End If%>
                    <%-- <a href="index.html"><img src="<%= TemplateSourceDirectory %>/img/logo.png" alt="" class="animated bounceInDown" /></a> --%> 
                </div>
                <!--//logo-->

                <div class="clearmob"></div>
			    <!--menu-->
			    <nav class="main_menu wsc_main collapse">
                    <div class="menu_wrap">
                        <div class="menu_inner">
                            <%If ((GetSetting("SideMenuDisplay") = "all_menu_items") Or (GetSetting("SideMenuDisplay") = "")) Then%>
                                <dnn:MENU ID="MENU10" MenuStyle="MainNav" runat="server"></dnn:MENU>
                            <%End If%>
                            <%If (GetSetting("SideMenuDisplay") = "only_children_items") Then%>
                                <dnn:MENU ID="MENU11" MenuStyle="MainNav" NodeSelector="RootChildren" runat="server"></dnn:MENU>
                            <%End If%>
                        </div>
				    </div>
                </nav>
                <!--//menu-->
    <%--        <div class="wf-td mini-search wf-mobile-hidden">
                    <div class="searchform">
                        <input style="display: none; visibility: visible;" class="field searchform-s" name="s" placeholder="Type and hit enter �" type="text">
                        <input class="assistive-text searchsubmit" value="Go!" type="submit">
                        <a href="#go" class="submit"></a>
                    </div>		
                </div>--%>
                <span class="clearer"></span>
            </div>
        </section>
	    <!--//header-->
	    <%End If%>

        <%--Banner Pane--%>
        <section id="banner" class="<%=GetColorClass("ContentBackgroundColor")%>">
            <div class="wsc_pane BannerPane" id="BannerPane" runat="server">
	        </div>
            <div class="container-fluid">
	            <div class="row">
	                <div class="wsc_pane col-md-12 BannerBondPane" id="BannerBondPane" runat="server">
                    </div>
		        </div>
	            <div class="row">
	                <div class="wsc_pane col-md-6 BannerLeftPane" id="BannerLeftPane" runat="server">
                    </div>
	                <div class="wsc_pane col-md-6 BannerRightPane" id="BannerRightPane" runat="server">
                    </div>
		        </div>
	        </div>
        </section>

        <%--Intro Pane--%>
	    <section id="intro" class="<%=GetColorClass("ColoredSectionsBackgroundColor")%>">
	        <div class="container-fluid intro_wrapper">
	            <div class="row">
	                <div class="wsc_pane col-md-12 IntroPane" id="IntroPane" runat="server">
                    </div>
		        </div>
	        </div>
	    </section>

        <!--main content panes-->		
	    <section id="main_content">
            <div class="inner_content <%=GetColorClass("ContentBackgroundColor")%>">
                <div class="container-fluid">
                    <!--#include file="CommonParts/PanesPlain.ascx"-->
                </div>    
            </div>

            <!--outro pane-->	
            <section id="outro">
                <div class="strip <%=GetColorClass("ColoredSectionsBackgroundColor")%>">
	                <div class="wsc_pane OutroPane" id="OutroPane" runat="server"></div>
                </div>
            </section>
            <!--//outro pane-->	

        </section>
        <!--//main content panes-->	


        <%--Footer--%>
        <%If ((GetSetting("FooterSwitcher") = "footer_show") Or (GetSetting("FooterSwitcher") = "")) Then%>
            <div class="footer_wrapper">
	            <!--footer -->
	            <section id="footer">
                    <div class="wsc_pane SocialFooter" id="SocialFooter" runat="server"></div>
	            </section>
	            <!--//footer -->
	            <!--footer 2 -->
	            <section id="footer2" class="<%=GetColorClass("FooterBackgroundColor")%>">
		            <div class="container-fluid">
			            <div class="row">
				            <div class="col-md-12">
				            <div class="copyright">
                                <dnn:COPYRIGHT ID="FooterCopyright" runat="server" /> :
                                <dnn:TERMS ID="dnnTerms" runat="server" /> :
                                <dnn:PRIVACY ID="dnnPrivacy" runat="server" />
				            </div>
				            </div>
			            </div>
		            </div>
	            </section>
                <!--alternative footer panes-->
	            <section id="footer_alt" class="<%=GetColorClass("FooterBackgroundColor")%>">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="wsc_pane col-md-3" id="FooterPane0" runat="server">
                            </div>
                            <div class="wsc_pane col-md-3" id="FooterPane25" runat="server">
                            </div>
                            <div class="wsc_pane col-md-3" id="FooterPane50" runat="server">
                            </div>
                            <div class="wsc_pane col-md-3" id="FooterPane75" runat="server">
                            </div>
                        </div>
                        <div class="row">
                            <div class="wsc_pane col-md-4" id="FooterThirdLeftPane" runat="server">
                            </div>
                            <div class="wsc_pane col-md-4" id="FooterThirdMiddlePane" runat="server">
                            </div>
                            <div class="wsc_pane col-md-4" id="FooterThirdRightPane" runat="server">
                            </div>
                        </div>
                        <div class="row">
                            <div class="wsc_pane col-md-6" id="FooterHalfLeftPane" runat="server">
                            </div>
                            <div class="wsc_pane col-md-6" id="FooterHalfRightPane" runat="server">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <!--#include file="CommonParts/FooterSocial.ascx"-->
                            </div>
                            <div class="col-md-6">
			                    <div class="copyright">
                                    <dnn:COPYRIGHT ID="COPYRIGHT1" runat="server" /> :
                                    <dnn:TERMS ID="TERMS1" runat="server" /> :
                                    <dnn:PRIVACY ID="PRIVACY1" runat="server" />
			                    </div>
                            </div>
                        </div>
                    </div>
	            </section>
            </div>
        <%End If%>
    					
        <!--up to top button-->
        <!--#include file="CommonParts/ToTopButton.ascx"-->
        <!--//up to top-->

    </div><%--Inside Wrap--%> 
</div>

    <%-- Include Side Menu Right Aligned --%>
    <%If (GetSetting("SideMenuAlignment") = "wsc-sidemenu-right") Then%>
        <!--#include file="CommonParts/SideMenuRight.ascx"-->   
    <%End If%>

</div>
<div class="common_background"></div>

<!-- Switcher module script -->
<script type="text/VB" runat="server" language="vb">
    Private Function GetSetting(key As String) As String
        Dim httpContext = System.Web.HttpContext.Current
        If httpContext.Items("WCS_StyleSwitcherData") Is Nothing Then
            Dim activeTab = TabController.CurrentPage
            Dim tc As New TabController()
            Dim frontTab = tc.GetTab(activeTab.TabID, activeTab.PortalID, True)
            Dim skinSrc As String = frontTab.SkinSrc
            If skinSrc = "" Then
                skinSrc = PortalController.GetCurrentPortalSettings().DefaultPortalSkin
            End If
            skinSrc = DotNetNuke.UI.Skins.SkinController.FormatSkinSrc(skinSrc, PortalController.GetCurrentPortalSettings())
            Dim filePath As String = httpContext.Server.MapPath(DotNetNuke.UI.Skins.SkinController.FormatSkinPath(skinSrc) + "StyleSwitcher\SwitcherDataSideMenu.xml")
            If IO.File.Exists(filePath) Then
                Dim doc = New System.Xml.XmlDocument()
                doc.Load(filePath)
                Dim listNode = doc.SelectSingleNode("/ArrayOfThemeSetting")
                Dim themeSettingNodeList = listNode.SelectNodes("ThemeSetting")
                Dim data = New Generic.Dictionary(Of String, String)()
                For Each node As System.Xml.XmlNode In themeSettingNodeList
                    data.Add(node.Attributes.GetNamedItem("Name").Value, node.Attributes.GetNamedItem("Value").Value)
                Next
                httpContext.Items("WCS_StyleSwitcherData") = data
            Else
                Return String.Empty
            End If
        End If
        Dim items = DirectCast(httpContext.Items("WCS_StyleSwitcherData"), Generic.Dictionary(Of String, String))
        Return If(items.ContainsKey(key), items(key), String.Empty)
    End Function
</script>

<%-- Imported Switcher module functions --%>
<!--#include file="CommonParts/SwitcherFunctions.ascx"-->

<%--Switcher module control--%>
<%If DotNetNuke.Security.PortalSecurity.IsInRole("Host") Then%>
<div class="wsc_switcher_control">
    <%--Button trigger modal--%>
    <a id="SwitcherBtn" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-cog"></span></a>
    <wsc:SWITCHER runat="server" ID="Switcher" SwitcherDataFileName="SwitcherDataSideMenu.xml" SwitcherSettingsFileName="SwitcherSettingsWrap.xml" />
</div>
<% End If%>

<%--Back to home button--%>
<%If (GetSetting("ToHomeBtn") <> "") Then%>
    <div class="wsc_back_home">
    <a class="btn btn-primary btn-lg" href="<%=GetSetting("ToHomeBtn")%>"><span class="glyphicon glyphicon-home"></span></a>
    </div>
<%End If%>

<%----------------------------------- STYLES ----%>

<style>html, body, form#Form, .wsc_sidebar_skin, .wsc-sidebar-wrap {height: 100%;} .wsc_generic_skin{position:static;}</style>

<%----------------------------------- JAVA SCRIPT ----%>
<script type="text/javascript">
    var SkinPath = "<%= TemplateSourceDirectory %>";
</script>

<%-- Common Scripts --%>
<!--#include file="CommonParts/AddScripts.ascx"-->

<dnn:STYLES runat="server" ID="StylesIE7" Name="IE7Minus" StyleSheet="css/ie8skin.css"
    Condition="IE 7" UseSkinPath="true" />
<dnn:STYLES runat="server" ID="StylesIE8" Name="IE8Minus" StyleSheet="css/ie8skin.css"
    Condition="IE 8" UseSkinPath="true" />