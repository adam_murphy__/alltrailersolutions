<%@ Control Language="vb" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="NAV" Src="~/Admin/Skins/Nav.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TEXT" Src="~/Admin/Skins/Text.ascx" %>
<%@ Register TagPrefix="dnn" TagName="BREADCRUMB" Src="~/Admin/Skins/BreadCrumb.ascx" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/User.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LINKS" Src="~/Admin/Skins/Links.ascx" %>
<%@ Register TagPrefix="dnn" TagName="PRIVACY" Src="~/Admin/Skins/Privacy.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TERMS" Src="~/Admin/Skins/Terms.ascx" %>
<%@ Register TagPrefix="dnn" TagName="COPYRIGHT" Src="~/Admin/Skins/Copyright.ascx" %>
<%@ Register TagPrefix="dnn" TagName="STYLES" Src="~/Admin/Skins/Styles.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="wsc" TagName="SWITCHER" src="~/DesktopModules/WebSitesCreative.StyleSwitcher/ViewSwitcher.ascx" %>

<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<%-- Common Styles --%>
<!--#include file="CommonParts/AddStyles.ascx"-->

<!--#include file="CommonParts/PreScripts.ascx"-->

<!-- Browser notification -->
<!--[if lte IE 8]>
<div class="browser-notification ie8">
	<p>Your browser (Internet Explorer 8 or lower) is <strong>out of date</strong>. It has known <strong>security flaws</strong> and may <strong>not display all features</strong> of this and other websites. <a href="//www.browser-update.org/update.html">Learn how to update your browser</a>.</p>
	<div class="close">X</div>
</div>
<![endif]-->
<!-- // Browser notification -->		
<div class="wsc_generic_skin wsc_onepage  <%=GetSetting("FooterStyle")%> <%=GetSetting("Layout")%> <%=GetSetting("ChooseMenus")%> <%=GetSetting("HeaderAlignment")%>" data-loading="<%=GetSetting("Preloader")%>" id="page-item-1">
    <!-- Top Pane -->
    <div class="wsc_pane TopPane" id="TopPane" runat="server"></div>
    <div class="clear"></div>
    
    <%If ((GetSetting("HeaderSwitcher") = "header_show") Or (GetSetting("HeaderSwitcher") = "")) Then%>
    <!--header-->
	<section id="header" class="header <%=GetColorClass("HeaderBackgroundColor")%>">
        <div class="header_inner">
            <!--top bar-->
            <!--#include file="CommonParts/TopBar.ascx"-->
            <!--//top bar-->
		    <div class="container wsc-header-container">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".main_menu.wsc_main">
			        <i class="fa fa-bars"></i>
			    </button>
		        <!--logo-->
		        <div class="logo">
                    <%-- Case of system logo --%>
                    <%If ((GetSetting("LogoCustomDefault") = "sm_logo_system") Or (GetSetting("LogoCustomDefault") = "")) Then%>
                        <dnn:LOGO runat="server" ID="dnnLOGO" />
                    <%End If%>

                    <%-- Case of custom logo --%>
                    <%If (GetSetting("LogoCustomDefault") = "sm_logo_custom") Then%>
                        <a href="/"><img src='<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("CustomLogoFile")%>?v=<%=GetSetting("timestamp")%>' alt="" /></a>
                    <%End If%>
                    <%-- <a href="index.html"><img src="<%= TemplateSourceDirectory %>/img/logo.png" alt="" class="animated bounceInDown" /></a> --%> 
                </div>
                <!--//logo-->
                <div class="clearmob"></div>
			    <!--menu-->
			    <nav class="main_menu wsc_main collapse">
                    <div class="menu_wrap">
                        <div class="menu_inner">
                            <dnn:MENU ID="MENU" MenuStyle="MainNav" runat="server"></dnn:MENU>
                        </div>
                        <%--<a class="existing-content-menu" href="#_"><i class="glyphicon glyphicon-reorder"></i></a>--%>
                        <%--<cs:MENUBOX runat="server" ID="menuBox1" SkinId="MenuSkin" SkinLocation="Skin" />--%>
				    </div>
                </nav>
                <!--//menu-->
<%--            <div class="wf-td mini-search wf-mobile-hidden">
                    <div class="searchform">
                        <input style="display: none; visibility: visible;" class="field searchform-s" name="s" placeholder="Type and hit enter …" type="text">
                        <input class="assistive-text searchsubmit" value="Go!" type="submit">
                        <a href="#go" class="submit"></a>
                    </div>		
                </div>--%>
                <span class="clearer"></span>
            </div>
        </div>
    </section>
	<!--//header-->
	<%End If%>

    <section id="main_content">
        <!--#include file="CommonParts/Panes.ascx"-->
    </section>

    <%--Menu Item Last (Footer)--%>
    <%If ((GetSetting("FooterSwitcher") = "footer_show") Or (GetSetting("FooterSwitcher") = "")) Then%>
    <section class="footer_onepage">
        <div id="page-item-last" class="page-section footer">
            <%--Page Title--%>
	        <p class="page-title btn"><span class="section_name"><span class="section_tooltip">last section</span></span></p>
            <div class="clear"></div>
            <!--#include file="CommonParts/Footer.ascx"-->
        </div>
    </section>
    <%End If%>

    <%-- To top button --%>
    <!--#include file="CommonParts/ToTopButton.ascx"-->

</div>
<div class="common_background"></div>

<!-- Switcher module script -->
<script type="text/VB" runat="server" language="vb">
    Private Function GetSetting(key As String) As String
        Dim httpContext = System.Web.HttpContext.Current
        If httpContext.Items("WCS_StyleSwitcherData") Is Nothing Then
            Dim activeTab = TabController.CurrentPage
            Dim tc As New TabController()
            Dim frontTab = tc.GetTab(activeTab.TabID, activeTab.PortalID, True)
            Dim skinSrc As String = frontTab.SkinSrc
            If skinSrc = "" Then
                skinSrc = PortalController.GetCurrentPortalSettings().DefaultPortalSkin
            End If
            skinSrc = DotNetNuke.UI.Skins.SkinController.FormatSkinSrc(skinSrc, PortalController.GetCurrentPortalSettings())
            Dim filePath As String = httpContext.Server.MapPath(DotNetNuke.UI.Skins.SkinController.FormatSkinPath(skinSrc) + "StyleSwitcher\SwitcherDataOnePage.xml")
            If IO.File.Exists(filePath) Then
                Dim doc = New System.Xml.XmlDocument()
                doc.Load(filePath)
                Dim listNode = doc.SelectSingleNode("/ArrayOfThemeSetting")
                Dim themeSettingNodeList = listNode.SelectNodes("ThemeSetting")
                Dim data = New Generic.Dictionary(Of String, String)()
                For Each node As System.Xml.XmlNode In themeSettingNodeList
                    data.Add(node.Attributes.GetNamedItem("Name").Value, node.Attributes.GetNamedItem("Value").Value)
                Next
                httpContext.Items("WCS_StyleSwitcherData") = data
            Else
                Return String.Empty
            End If
        End If
        Dim items = DirectCast(httpContext.Items("WCS_StyleSwitcherData"), Generic.Dictionary(Of String, String))
        Return If(items.ContainsKey(key), items(key), String.Empty)
    End Function
</script>

<%-- Imported Switcher module functions --%>
<!--#include file="CommonParts/SwitcherFunctions.ascx"-->

<!-- Switcher module control -->
<%If DotNetNuke.Security.PortalSecurity.IsInRole("Host") Then%>
<div class="wsc_switcher_control">
    <!-- Button trigger modal -->
    <a id="SwitcherBtn" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-cog"></span></a>
    <wsc:SWITCHER runat="server" ID="Switcher1" SwitcherDataFileName="SwitcherDataOnePage.xml" SwitcherSettingsFileName="SwitcherSettingsOnePage.xml" />
</div>
<% End If%>

<%--Back to home button--%>
<%If (GetSetting("ToHomeBtn") <> "") Then%>
    <div class="wsc_back_home">
    <a class="btn btn-primary btn-lg" href="<%=GetSetting("ToHomeBtn")%>"><span class="glyphicon glyphicon-home"></span></a>
    </div>
<%End If%>
<%----------------------------------- STYLES ----%>

<%-- Parallax Styles --%>
<!--#include file="CommonParts/OnePage/Sections/ParallaxPanes/ParallaxSwitcher.ascx"-->


<%----------------------------------- JAVA SCRIPT ----%>
<script type="text/javascript">
    var SkinPath = "<%= TemplateSourceDirectory %>";
</script>

<!--#include file="CommonParts/OnePage/MenuAnchors.ascx"-->
<script type="text/javascript" src="<%= TemplateSourceDirectory %>/js/jquery.onepage.js"></script> 

<%-- Common Scripts --%>
<!--#include file="CommonParts/AddScripts.ascx"-->

<dnn:STYLES runat="server" ID="StylesIE7" Name="IE7Minus" StyleSheet="css/ie8skin.css"
    Condition="IE 7" UseSkinPath="true" />
<dnn:STYLES runat="server" ID="StylesIE8" Name="IE8Minus" StyleSheet="css/ie8skin.css"
    Condition="IE 8" UseSkinPath="true" />