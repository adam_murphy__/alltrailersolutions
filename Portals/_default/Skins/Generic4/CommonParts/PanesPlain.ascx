﻿<%-- Using for sideMenu.ascx --%>  

    <%------------------- CONTENT SECTION 1 -------------------%>  

    <div class="row">
        <div class="col-md-12 wsc_pane ContentPane" id="ContentPane" runat="server">
        </div>
    </div>
    <div class="row">
        <div class="wsc_pane col-md-6" id="LeftHalfPane" runat="server">
        </div>
        <div class="wsc_pane col-md-6" id="RightHalfPane" runat="server">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 wsc_pane WidePane" id="WidePane" runat="server">
        </div>
    </div>


    <%------------------- CONTENT SECTION 2 -------------------%>  

    <div class="row">
    <div class="wsc_pane FullWidePane2" id="FullWidePane2" runat="server"></div>
    </div>

    <div class="row">
        <div class="wsc_pane col-md-4" id="ThirdLeftPane" runat="server">
        </div>
        <div class="wsc_pane col-md-4" id="ThirdMiddlePane" runat="server">
        </div>
        <div class="wsc_pane col-md-4" id="ThirdRightPane" runat="server">
        </div>
    </div>
    <div class="row">
        <div class="wsc_pane col-md-3" id="NarrowPane0" runat="server">
        </div>
        <div class="wsc_pane col-md-3" id="NarrowPane25" runat="server">
        </div>
        <div class="wsc_pane col-md-3" id="NarrowPane50" runat="server">
        </div>
        <div class="wsc_pane col-md-3" id="NarrowPane75" runat="server">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 wsc_pane WidePane2" id="WidePane2" runat="server">
        </div>
    </div>



    <%------------------- CONTENT SECTION 3 -------------------%>
    <div class="row">
    <div class="wsc_pane FullWidePane3" id="FullWidePane3" runat="server"></div>
    </div>

    <div class="row">
        <div class="wsc_pane col-md-4" id="LeftNarrowPane" runat="server">
        </div>
        <div class="wsc_pane col-md-8" id="RightWidePane" runat="server">
        </div>
    </div>
    <div class="row">
        <div class="wsc_pane col-md-8" id="LeftWidePane" runat="server">
        </div>
        <div class="wsc_pane col-md-4" id="RightNarrowPane" runat="server">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 wsc_pane WidePane3" id="WidePane3" runat="server">
        </div>
    </div>



    <%------------------- CONTENT SECTION 4 -------------------%> 
    <div class="row">
    <div class="wsc_pane FullWidePane4" id="FullWidePane4" runat="server"></div>
    </div>

    <div class="row">
        <div class="wsc_pane col-md-3" id="LeftQuarterPane" runat="server">
        </div>
        <div class="wsc_pane col-md-9" id="RightThreeQuartersPane" runat="server">
        </div>
    </div>
    <div class="row">
        <div class="wsc_pane col-md-9" id="LeftThreeQuartersPane" runat="server">
        </div>
        <div class="wsc_pane col-md-3" id="RightQuarterPane" runat="server">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 wsc_pane WidePane4" id="WidePane4" runat="server">
        </div>
    </div>


    <%------------------- CONTENT SECTION 5 -------------------%>
    <div class="row">
    <div class="wsc_pane FullWidePane5" id="FullWidePane5" runat="server"></div>
    </div>

    <div class="row">
        <div class="wsc_pane col-md-6" id="BottomHalfLeftPane" runat="server">
        </div>
        <div class="wsc_pane col-md-6" id="BottomHalfRightPane" runat="server">
        </div>
    </div>
    <div class="row">
        <div class="wsc_pane col-md-4" id="BottomThirdLeftPane" runat="server">
        </div>
        <div class="wsc_pane col-md-4" id="BottomThirdMiddlePane" runat="server">
        </div>
        <div class="wsc_pane col-md-4" id="BottomThirdRightPane" runat="server">
        </div>
    </div>
    <div class="row">
        <div class="wsc_pane col-md-3" id="BottomNarrowPane0" runat="server">
        </div>
        <div class="wsc_pane col-md-3" id="BottomNarrowPane25" runat="server">
        </div>
        <div class="wsc_pane col-md-3" id="BottomNarrowPane50" runat="server">
        </div>
        <div class="wsc_pane col-md-3" id="BottomNarrowPane75" runat="server">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 wsc_pane WidePane5" id="WidePane5" runat="server">
        </div>
    </div>

    <div class="row">
    <div class="wsc_pane FullWidePane6" id="FullWidePane6" runat="server"></div>
    </div>