﻿<div class="container">
    <div class="row">
        <div class="wsc_pane col-md-12" id="FooterWideTop" runat="server">
        </div>
    </div>
    <div class="row">
        <div class="wsc_pane col-md-3" id="FooterPane0" runat="server">
        </div>
        <div class="wsc_pane col-md-3" id="FooterPane25" runat="server">
        </div>
        <div class="wsc_pane col-md-3" id="FooterPane50" runat="server">
        </div>
        <div class="wsc_pane col-md-3" id="FooterPane75" runat="server">
        </div>
    </div>
    <div class="row">
        <div class="wsc_pane col-md-4" id="FooterThirdLeftPane" runat="server">
        </div>
        <div class="wsc_pane col-md-4" id="FooterThirdMiddlePane" runat="server">
        </div>
        <div class="wsc_pane col-md-4" id="FooterThirdRightPane" runat="server">
        </div>
    </div>
    <div class="row">
        <div class="wsc_pane col-md-6" id="FooterHalfLeftPane" runat="server">
        </div>
        <div class="wsc_pane col-md-6" id="FooterHalfRightPane" runat="server">
        </div>
    </div>
    <div class="row">
        <div class="wsc_pane col-md-12" id="FooterWideBottom" runat="server">
        </div>
    </div>
    <div class="row">

    <%If ((GetSetting("SocialIconsPlacement") = "wsc_social_footer") Or (GetSetting("SocialIconsPlacement") = "")) Then%>
        <div class="col-md-6">
            <!--#include file="FooterSocial.ascx"-->
        </div>
        <div class="col-md-6">
			<div class="copyright">
                <dnn:COPYRIGHT ID="COPYRIGHT1" runat="server" /> :
                <dnn:TERMS ID="TERMS1" runat="server" /> :
                <dnn:PRIVACY ID="PRIVACY1" runat="server" />
			</div>
        </div>
    <%End If%>

    <%If (GetSetting("SocialIconsPlacement") = "wsc_social_header") Then%>
        <div class="col-md-12">
			<div class="wsc-text-center wsc-marg-top-sm">
                <dnn:COPYRIGHT ID="COPYRIGHT2" runat="server" /> :
                <dnn:TERMS ID="TERMS2" runat="server" /> :
                <dnn:PRIVACY ID="PRIVACY2" runat="server" />
			</div>
        </div>
    <%End If%>

    </div>
</div>
