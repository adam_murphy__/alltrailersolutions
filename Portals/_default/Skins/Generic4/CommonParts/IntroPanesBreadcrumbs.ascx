﻿<section id="intro" class="<%=GetColorClass("ColoredSectionsBackgroundColor")%>">
	<div class="intro_wrapper">
        <div class="container">
            <%If (GetSetting("BreadcrumbsOnOff") = "show") Then%>
            <div class="row">
	            <div class="col-md-12 wsc-breadcrumbs">
                    You are here: <dnn:BREADCRUMB ID="dnnBreadcrumb" runat="server" CssClass="breadcrumbLink" RootLevel="0" Separator=" &frasl; " />    	             
                </div>            
            </div>
            <%End If%>
	        <div class="row">
	            <div class="wsc_pane col-md-12 IntroPane" id="IntroPane" runat="server">
                </div>
		    </div>
	    </div>
        <div class="wsc_pane IntroPane" id="IntroWidePane" runat="server">
        </div>
    </div>
</section>