﻿<section id="header" class="header <%=GetColorClass("HeaderBackgroundColor")%>">
    <div class="header_inner">
        <!--top bar-->
        <!--#include file="TopBar.ascx"-->
        <!--//top bar-->
		<div class="container <%=GetSetting("HeaderLayoutWidth")%> wsc-header-container">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".main_menu.wsc_main">
			    <i class="fa fa-bars"></i>
			</button>
            <button type="button" class="navbar-toggle wsc-offscreen-toggle">
			    <i class="fa fa-bars"></i>
			</button>
		    <!--logo-->
		    <div class="logo">
                <%-- Case of system logo --%>
                <%If ((GetSetting("LogoCustomDefault") = "sm_logo_system") Or (GetSetting("LogoCustomDefault") = "")) Then%>
                    <dnn:LOGO runat="server" ID="dnnLOGO" />
                <%End If%>

                <%-- Case of custom logo --%>
                <%If (GetSetting("LogoCustomDefault") = "sm_logo_custom") Then%>
                    <a href="/"><img src='<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("CustomLogoFile")%>?v=<%=GetSetting("timestamp")%>' alt="" /></a>
                <%End If%>
                <%-- <a href="index.html"><img src="<%= TemplateSourceDirectory %>/img/logo.png" alt="" class="animated bounceInDown" /></a> --%> 
            </div>
            <!--//logo-->
            <div class="clearmob visible-xs visible-sm"></div>
			<!--menu-->
			<nav class="main_menu wsc_main collapse">
                <div class="menu_wrap">
                    <div class="menu_inner">
                        <dnn:MENU ID="MENU" MenuStyle="MainNav" runat="server"></dnn:MENU>
                    </div>
				</div>
            </nav>
            <!--//menu-->
            <%--div class="wf-td mini-search wf-mobile-hidden">
                <div class="searchform">
                    <input style="display: none; visibility: visible;" class="field searchform-s" name="s" placeholder="Type and hit enter …" type="text">
                    <input class="assistive-text searchsubmit" value="Go!" type="submit">
                    <a href="#go" class="submit"></a>
                </div>		
            </div>--%>
            <span class="clearer"></span>
        </div>
    </div>
</section>