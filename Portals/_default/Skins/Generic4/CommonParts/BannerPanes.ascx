﻿<section id="banner" class="<%=GetColorClass("BannerBackgroundColor")%>">
    <div class="wsc_pane BannerPane" id="BannerPane" runat="server">
	</div>
    <div class="container">
	    <div class="row">
	        <div class="wsc_pane col-md-12 BannerBondPane" id="BannerBondPane" runat="server">
            </div>
		</div>
	    <div class="row">
	        <div class="wsc_pane col-md-6 BannerLeftPane" id="BannerLeftPane" runat="server">
            </div>
	        <div class="wsc_pane col-md-6 BannerRightPane" id="BannerRightPane" runat="server">
            </div>
		</div>
	</div>
</section>