﻿<div class="inner_content">
    
    <%-- OnePage wrap for 1st section --%>
    <div id="page-item-top" class="page-section <%=GetColorClass("ContentBackgroundColor")%>">

        <div class="container">
            <div class="row">
                <div class="col-md-12 wsc_pane ContentPane" id="ContentPane" runat="server">
                </div>
            </div>
            <div class="row">
                <div class="wsc_pane col-md-6" id="LeftHalfPane" runat="server">
                </div>
                <div class="wsc_pane col-md-6" id="RightHalfPane" runat="server">
                </div>
            </div>
            <div class="row">
                <div class="wsc_pane col-md-4" id="ThirdLeftPane" runat="server">
                </div>
                <div class="wsc_pane col-md-4" id="ThirdMiddlePane" runat="server">
                </div>
                <div class="wsc_pane col-md-4" id="ThirdRightPane" runat="server">
                </div>
            </div>
            <div class="row">
                <div class="wsc_pane col-md-3" id="NarrowPane0" runat="server">
                </div>
                <div class="wsc_pane col-md-3" id="NarrowPane25" runat="server">
                </div>
                <div class="wsc_pane col-md-3" id="NarrowPane50" runat="server">
                </div>
                <div class="wsc_pane col-md-3" id="NarrowPane75" runat="server">
                </div>
            </div>
            <div class="row">
                <div class="wsc_pane col-md-8" id="LeftWidePane" runat="server">
                </div>
                <div class="wsc_pane col-md-4" id="RightNarrowPane" runat="server">
                </div>
            </div>
            <div class="row">
                <div class="wsc_pane col-md-4" id="LeftNarrowPane" runat="server">
                </div>
                <div class="wsc_pane col-md-8" id="RightWidePane" runat="server">
                </div>
            </div>
            <div class="row">
                <div class="wsc_pane col-md-9" id="LeftThreeQuartersPane" runat="server">
                </div>
                <div class="wsc_pane col-md-3" id="RightQuarterPane" runat="server">
                </div>
            </div>
            <div class="row">
                <div class="wsc_pane col-md-3" id="LeftQuarterPane" runat="server">
                </div>
                <div class="wsc_pane col-md-9" id="RightThreeQuartersPane" runat="server">
                </div>
            </div>
            <!--#include file="AdditionalPanes.ascx"-->
            <div class="row">
                <div class="wsc_pane col-md-6" id="BottomHalfLeftPane" runat="server">
                </div>
                <div class="wsc_pane col-md-6" id="BottomHalfRightPane" runat="server">
                </div>
            </div>
            <div class="row">
                <div class="wsc_pane col-md-4" id="BottomThirdLeftPane" runat="server">
                </div>
                <div class="wsc_pane col-md-4" id="BottomThirdMiddlePane" runat="server">
                </div>
                <div class="wsc_pane col-md-4" id="BottomThirdRightPane" runat="server">
                </div>
            </div>
            <div class="row">
                <div class="wsc_pane col-md-3" id="BottomNarrowPane0" runat="server">
                </div>
                <div class="wsc_pane col-md-3" id="BottomNarrowPane25" runat="server">
                </div>
                <div class="wsc_pane col-md-3" id="BottomNarrowPane50" runat="server">
                </div>
                <div class="wsc_pane col-md-3" id="BottomNarrowPane75" runat="server">
                </div>
            </div>
            <div class="row">
                <div class="wsc_pane col-md-offset-4 col-md-4" id="CenterNarrowPane" runat="server">
                </div>
            </div>
            <div class="row">
                <div class="wsc_pane col-md-offset-3 col-md-6" id="CenterPane" runat="server">
                </div>
            </div>
            <div class="row">
                <div class="wsc_pane col-md-offset-2 col-md-8" id="CenterWidePane" runat="server">
                </div>
            </div>
            <div class="row">
                <div class="wsc_pane col-md-offset-1 col-md-10" id="CenterExtraWidePane" runat="server">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 wsc_pane WidePane" id="WidePane" runat="server">
                </div>
            </div>
            <!--#include file="AdditionalPanesBottom.ascx"-->
        </div>

        <%-- Screen-wide Pane --%>
        <div class="wsc_pane FullWidePane" id="FullWidePane" runat="server"></div>

        <%------------------- ADDITIONAL PANES (+ COLOR + PARALLAX) for SECTION 1 -------------------%>

        <%-- Adding additional section of panes via switcher module --%>
        <%If (GetSetting("AdditonalPanesSections") <> "0") Then%>

            <!--#include file="ParallaxPanes/PanesSection1.ascx"-->
        
        <%End If%>

    </div>

    <%-- OnePage wrap for 2st section --%>
    <div id="page-item-2" class="page-section">

        <%------------------- ADDITIONAL PANES (+ COLOR + PARALLAX) for SECTION 2 -------------------%>  

        <%-- Adding additional section of panes via switcher module --%>
        <%If ((GetSetting("AdditonalPanesSections") = "2") Or (GetSetting("AdditonalPanesSections") = "3") Or (GetSetting("AdditonalPanesSections") = "4") Or (GetSetting("AdditonalPanesSections") = "5")) Then%>

            <!--#include file="ParallaxPanes/PanesSection2.ascx"-->
        
        <%End If%>

    </div>

    <%-- OnePage wrap for 3rd section --%>
    <div id="page-item-3" class="page-section">

        <%------------------- ADDITIONAL PANES (+ COLOR + PARALLAX) for SECTION 3 -------------------%>  
    
        <%-- Adding additional section of panes via switcher module --%>
        <%If ((GetSetting("AdditonalPanesSections") = "3") Or (GetSetting("AdditonalPanesSections") = "4") Or (GetSetting("AdditonalPanesSections") = "5")) Then%>

        <!--#include file="ParallaxPanes/PanesSection3.ascx"-->

        <%End If%>

    </div>

    <%-- OnePage wrap for 4th section --%>
    <div id="page-item-4" class="page-section">

        <%------------------- ADDITIONAL PANES (+ COLOR + PARALLAX) for SECTION 4 -------------------%>  
    
        <%-- Adding additional section of panes via switcher module --%>
        <%If ((GetSetting("AdditonalPanesSections") = "4") Or (GetSetting("AdditonalPanesSections") = "5")) Then%>

        <!--#include file="ParallaxPanes/PanesSection4.ascx"-->
    
        <%End If%>

    </div>

    <%-- OnePage wrap for 5th section --%>
    <div id="page-item-5" class="page-section">

        <%------------------- ADDITIONAL PANES (+ COLOR + PARALLAX) for SECTION 5 -------------------%>  

        <%-- Adding additional section of panes via switcher module --%>
        <%If (GetSetting("AdditonalPanesSections") = "5") Then%>

        <!--#include file="ParallaxPanes/PanesSection5.ascx"-->
    
        <%End If%>

    </div>

</div>

<%--Outro pane--%>
<section id="outro">
    <div class="strip <%=GetColorClass("ColoredSectionsBackgroundColor")%>">
	    <div class="wsc_pane OutroPane" id="OutroPane" runat="server"></div>
    </div>
</section>