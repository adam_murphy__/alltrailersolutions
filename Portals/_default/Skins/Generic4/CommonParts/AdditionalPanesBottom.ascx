     
<%--
<!-- 50% Pane + 50% Pane -->   
            <div class="row">

		        <div class="wsc_pane col-md-6" id="ABLeftHalfPane" runat="server">
		        </div> 
				
		        <div class="wsc_pane col-md-6" id="ABRightHalfPane" runat="server">
		        </div> 
					
            </div>
 <!-- End 50% Pane + 50% Pane -->
 --%>


      
<%--
<!-- 33% Pane + 33% Pane + 33% Pane -->   
            <div class="row">

		        <div class="wsc_pane col-md-4" id="ABThirdLeftPane" runat="server">
		        </div> 
				
		        <div class="wsc_pane col-md-4" id="ABThirdMiddlePane" runat="server">
		        </div> 
					
		        <div class="wsc_pane col-md-4" id="ABThirdRightPane" runat="server">
		        </div> 
				
            </div>
<!-- End 33% Pane + 33% Pane + 33% Pane --> 
 --%>


       
<%--
<!-- 25% Pane + 25% Pane + 25% Pane + 25% Pane -->  
	        <div class="row">
		        
                <div class="wsc_pane col-md-3" id="ABNarrowPane0" runat="server">
				</div> 
				
			    <div class="wsc_pane col-md-3" id="ABNarrowPane25" runat="server">
				</div> 
					
			    <div class="wsc_pane col-md-3" id="ABNarrowPane50" runat="server">
				</div> 
				
			    <div class="wsc_pane col-md-3" id="ABNarrowPane75" runat="server">
				</div>
                 
			</div> 
<!-- End 25% Pane + 25% Pane + 25% Pane + 25% Pane -->
 --%>



<%--
<!-- 33% Pane + 66% Pane -->         				
			<div class="row">
			
                <div class="wsc_pane col-md-4" id="ABLeftNarrowPane" runat="server">
                </div>

                <div class="wsc_pane col-md-8" id="ABRightWidePane" runat="server">
                </div>

            </div>
<!-- End 33% Pane + 66% Pane -->
 --%>


       
<%--
<!-- 66% Pane + 33% Pane -->  
            <div class="row">
                
                <div class="wsc_pane col-md-8" id="ABLeftWidePane" runat="server">
			    </div>
			    
                <div class="wsc_pane col-md-4" id="ABRightNarrowPane" runat="server">
			    </div>

            </div>
<!-- End 66% Pane + 33% Pane -->
 --%>


       
<%--
<!-- 25% Pane + 75% Pane -->  
            <div class="row">
			
                <div class="wsc_pane col-md-3" id="ABLeftQuarterPane" runat="server">
			    </div>

			    <div class="wsc_pane col-md-9" id="ABRightThreeQuartersPane" runat="server">
			    </div>

            </div>
<!-- End 25% Pane + 75% Pane -->
--%>


       
<%--
<!-- 75% Pane + 25% Pane -->  
            <div class="row">

                <div class="wsc_pane col-md-9" id="ABLeftThreeQuartersPane" runat="server">
			    </div>
			    
                <div class="wsc_pane col-md-3" id="ABRightQuarterPane" runat="server">
			    </div>

            </div>
<!-- End 75% Pane + 25% Pane -->
--%>
