﻿<script type="text/javascript">

    var MenuToSection = [[1, '<%=GetSetting("OnePageSection1")%>'], [2, '<%=GetSetting("OnePageSection2")%>'], [3, '<%=GetSetting("OnePageSection3")%>'], [4, '<%=GetSetting("OnePageSection4")%>'], [5, '<%=GetSetting("OnePageSection5")%>'], [6, '<%=GetSetting("OnePageSection6")%>'], [7, '<%=GetSetting("OnePageSection7")%>']];

    jQuery(document).ready(function ($) {
        function SetMenuAnchors(initLink, menulist) {
            for (var i = 0; i < menulist.length; i++) {
                if (MenuToSection[i][1] != "none") {
                    //Set section name
                    var $FindMenuItem = $('.main_menu').find(menulist[i]).children('a');
                    $FindMenuItem.addClass('target-anch');
                    var ItemName = $('.main_menu .menu_inner .nav').find($FindMenuItem).html();
                    var $find_div = 'div[id =page-item-' + MenuToSection[i][1] + ']';
                    $('.wsc_generic_skin').find($find_div).children('.page-title').children('span.section_name').prepend(ItemName + " - ");

                    //Add a menu item suffix as anchor to a section
                    $FindMenuItem.attr('href', initialLink + "#page-item-" + MenuToSection[i][1]);

                    // Display section
                    $('wsc_generic_skin').find('div[id =page-item-' + MenuToSection[i][1] + ']').addClass('active_section').css('display', 'block');
                }
            }
        }
        $('.main_menu .menu_inner .nav').addClass('onePageNav');
        var menulist = $('.nav.onePageNav li.root');
        var initialLink = $('.main_menu').find(menulist[0]).children('a').attr('href');
        SetMenuAnchors(initialLink, menulist);
    });

</script>