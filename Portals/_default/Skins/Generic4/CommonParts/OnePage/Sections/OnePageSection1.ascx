<div id="page-item-top" class="page-section">

    <!-- Page Title -->
	<p class="page-title btn"><span class="section_name"><span class="section_tooltip">section 1</span></span></p>
    <div class="clear"></div>

    <!-- Banner Pane -->
    <div class="wsc_pane BannerPane" id="BannerPane" runat="server"></div>
    <div class="clear"></div>

    <!-- Intro-->
	<div id="intro">
	    <div class="container intro_wrapper">
	        <div class="row">
	            <div class="wsc_pane col-md-12 IntroPane" id="IntroPane" runat="server">
                </div>
		    </div>
	    </div>
	</div>

    <div class="p-parallax p-parallax-one-1 <%=GetSetting("ParallaxFontColor")%>">
        <!--#include file="ParallaxPanes/ParallaxPanes1.ascx"-->
    </div>

    <div class="wsc_pane FullWidePane" id="FullWidePane_1" runat="server"></div>

    <div class="container">
        
        <div class="row">
            <div class="col-md-12 wsc_pane ContentPane" id="ContentPane" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-6" id="LeftHalfPane_1" runat="server">
            </div>
            <div class="wsc_pane col-md-6" id="RightHalfPane_1" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 wsc_pane WidePane" id="WidePane_1" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-4" id="ThirdLeftPane_1" runat="server">
            </div>
            <div class="wsc_pane col-md-4" id="ThirdMiddlePane_1" runat="server">
            </div>
            <div class="wsc_pane col-md-4" id="ThirdRightPane_1" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-3" id="NarrowPane0_1" runat="server">
            </div>
            <div class="wsc_pane col-md-3" id="NarrowPane25_1" runat="server">
            </div>
            <div class="wsc_pane col-md-3" id="NarrowPane50_1" runat="server">
            </div>
            <div class="wsc_pane col-md-3" id="NarrowPane75_1" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-4" id="LeftNarrowPane_1" runat="server">
            </div>
            <div class="wsc_pane col-md-8" id="RightWidePane_1" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-8" id="LeftWidePane_1" runat="server">
            </div>
            <div class="wsc_pane col-md-4" id="RightNarrowPane_1" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-3" id="LeftQuarterPane_1" runat="server">
            </div>
            <div class="wsc_pane col-md-9" id="RightThreeQuartersPane_1" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-9" id="LeftThreeQuartersPane_1" runat="server">
            </div>
            <div class="wsc_pane col-md-3" id="RightQuarterPane_1" runat="server">
            </div>
        </div>
    </div>

</div>