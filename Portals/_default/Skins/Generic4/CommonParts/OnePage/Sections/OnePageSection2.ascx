<div id="page-item-2" class="page-section">

    <!-- Page Title -->
	<p class="page-title btn"><span class="section_name"><span class="section_tooltip">section 2</span></span></p>
    <div class="clear"></div>

    <div class="p-parallax p-parallax-one-2 <%=GetSetting("ParallaxFontColor")%>">
        <!--#include file="ParallaxPanes/ParallaxPanes2.ascx"-->
    </div>

    <div class="wsc_pane FullWidePane" id="FullWidePane_2" runat="server"></div>

    <div class="container">
        <div class="row">
            <div class="wsc_pane col-md-6" id="LeftHalfPane_2" runat="server">
            </div>
            <div class="wsc_pane col-md-6" id="RightHalfPane_2" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 wsc_pane WidePane" id="WidePane_2" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-4" id="ThirdLeftPane_2" runat="server">
            </div>
            <div class="wsc_pane col-md-4" id="ThirdMiddlePane_2" runat="server">
            </div>
            <div class="wsc_pane col-md-4" id="ThirdRightPane_2" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-3" id="NarrowPane0_2" runat="server">
            </div>
            <div class="wsc_pane col-md-3" id="NarrowPane25_2" runat="server">
            </div>
            <div class="wsc_pane col-md-3" id="NarrowPane50_2" runat="server">
            </div>
            <div class="wsc_pane col-md-3" id="NarrowPane75_2" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-4" id="LeftNarrowPane_2" runat="server">
            </div>
            <div class="wsc_pane col-md-8" id="RightWidePane_2" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-8" id="LeftWidePane_2" runat="server">
            </div>
            <div class="wsc_pane col-md-4" id="RightNarrowPane_2" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-3" id="LeftQuarterPane_2" runat="server">
            </div>
            <div class="wsc_pane col-md-9" id="RightThreeQuartersPane_2" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-9" id="LeftThreeQuartersPane_2" runat="server">
            </div>
            <div class="wsc_pane col-md-3" id="RightQuarterPane_2" runat="server">
            </div>
        </div>
    </div>

</div>