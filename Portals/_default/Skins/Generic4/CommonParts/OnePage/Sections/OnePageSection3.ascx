<div id="page-item-3" class="page-section">

    <!-- Page Title -->
	<p class="page-title btn"><span class="section_name"><span class="section_tooltip">section 3</span></span></p>
    <div class="clear"></div>

    <div class="p-parallax p-parallax-one-3 <%=GetSetting("ParallaxFontColor")%>">
        <!--#include file="ParallaxPanes/ParallaxPanes3.ascx"-->
    </div>

    <div class="wsc_pane FullWidePane" id="FullWidePane_3" runat="server"></div>

    <div class="container">
        <div class="row">
            <div class="wsc_pane col-md-6" id="LeftHalfPane_3" runat="server">
            </div>
            <div class="wsc_pane col-md-6" id="RightHalfPane_3" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 wsc_pane WidePane" id="WidePane_3" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-4" id="ThirdLeftPane_3" runat="server">
            </div>
            <div class="wsc_pane col-md-4" id="ThirdMiddlePane_3" runat="server">
            </div>
            <div class="wsc_pane col-md-4" id="ThirdRightPane_3" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-3" id="NarrowPane0_3" runat="server">
            </div>
            <div class="wsc_pane col-md-3" id="NarrowPane25_3" runat="server">
            </div>
            <div class="wsc_pane col-md-3" id="NarrowPane50_3" runat="server">
            </div>
            <div class="wsc_pane col-md-3" id="NarrowPane75_3" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-4" id="LeftNarrowPane_3" runat="server">
            </div>
            <div class="wsc_pane col-md-8" id="RightWidePane_3" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-8" id="LeftWidePane_3" runat="server">
            </div>
            <div class="wsc_pane col-md-4" id="RightNarrowPane_3" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-3" id="LeftQuarterPane_3" runat="server">
            </div>
            <div class="wsc_pane col-md-9" id="RightThreeQuartersPane_3" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-9" id="LeftThreeQuartersPane_3" runat="server">
            </div>
            <div class="wsc_pane col-md-3" id="RightQuarterPane_3" runat="server">
            </div>
        </div>
    </div>

</div>