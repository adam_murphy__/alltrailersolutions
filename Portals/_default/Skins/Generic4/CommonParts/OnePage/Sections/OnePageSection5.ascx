<div id="page-item-5" class="page-section">

    <!-- Page Title -->
	<p class="page-title btn"><span class="section_name"><span class="section_tooltip">section 5</span></span></p>
    <div class="clear"></div>

    <div class="p-parallax p-parallax-one-5 <%=GetSetting("ParallaxFontColor")%>">
        <!--#include file="ParallaxPanes/ParallaxPanes5.ascx"-->
    </div>

    <div class="wsc_pane FullWidePane" id="FullWidePane_5" runat="server"></div>

    <div class="container">
        <div class="row">
            <div class="wsc_pane col-md-6" id="LeftHalfPane_5" runat="server">
            </div>
            <div class="wsc_pane col-md-6" id="RightHalfPane_5" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 wsc_pane WidePane" id="WidePane_5" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-4" id="ThirdLeftPane_5" runat="server">
            </div>
            <div class="wsc_pane col-md-4" id="ThirdMiddlePane_5" runat="server">
            </div>
            <div class="wsc_pane col-md-4" id="ThirdRightPane_5" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-3" id="NarrowPane0_5" runat="server">
            </div>
            <div class="wsc_pane col-md-3" id="NarrowPane25_5" runat="server">
            </div>
            <div class="wsc_pane col-md-3" id="NarrowPane50_5" runat="server">
            </div>
            <div class="wsc_pane col-md-3" id="NarrowPane75_5" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-4" id="LeftNarrowPane_5" runat="server">
            </div>
            <div class="wsc_pane col-md-8" id="RightWidePane_5" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-8" id="LeftWidePane_5" runat="server">
            </div>
            <div class="wsc_pane col-md-4" id="RightNarrowPane_5" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-3" id="LeftQuarterPane_5" runat="server">
            </div>
            <div class="wsc_pane col-md-9" id="RightThreeQuartersPane_5" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-9" id="LeftThreeQuartersPane_5" runat="server">
            </div>
            <div class="wsc_pane col-md-3" id="RightQuarterPane_5" runat="server">
            </div>
        </div>
    </div>

</div>