<div class="parallax ParallaxOnePanes6">
<div class="parallax_overlay <%=GetSetting("ParallaxOverlayStyle")%>">
    
	<div class="container">

        <!-- 100% Pane -->
        <div class="row">
		    <div class="wsc_pane col-md-12" id="WideParallax_6" runat="server"></div> 
        </div>

        <!-- 50% Pane --> 
        <div class="row">
            <div class="wsc_pane col-md-6" id="LeftHalfParallax_6" runat="server"></div>
            <div class="wsc_pane col-md-6" id="RightHalfParallax_6" runat="server"></div>
        </div>

        <!-- 30% and 70% Pane --> 
        <div class="row">
            <div class="wsc_pane col-md-4" id="LeftNarrowParallax_6" runat="server"></div>
            <div class="wsc_pane col-md-8" id="RightWideParallax_6" runat="server"></div>
        </div>

        <!-- 70% and 30% Pane --> 
        <div class="row">
            <div class="wsc_pane col-md-8" id="LeftWideParallax_6" runat="server"></div>
            <div class="wsc_pane col-md-4" id="RightNarrowParallax_6" runat="server"></div>
        </div>

        <!-- 33% Pane --> 
        <div class="row">
            <div class="wsc_pane col-md-4" id="ThirdLeftParallax_6" runat="server"></div>
            <div class="wsc_pane col-md-4" id="ThirdMiddleParallax_6" runat="server"></div>
            <div class="wsc_pane col-md-4" id="ThirdRightParallax_6" runat="server"></div>
        </div>

        <!-- 25% Pane --> 
        <div class="row">
            <div class="wsc_pane col-md-3" id="Narrow25Parallax_6" runat="server"></div>
            <div class="wsc_pane col-md-3" id="Narrow50Parallax_6" runat="server"></div>
            <div class="wsc_pane col-md-3" id="Narrow75Parallax_6" runat="server"></div>
            <div class="wsc_pane col-md-3" id="Narrow100Parallax_6" runat="server"></div>
        </div>

        <!-- Bottom 100% Pane -->
        <div class="row">
            <div class="wsc_pane col-md-12" id="BottomParallax_6" runat="server"></div>
        </div>

	</div>

</div>
</div>