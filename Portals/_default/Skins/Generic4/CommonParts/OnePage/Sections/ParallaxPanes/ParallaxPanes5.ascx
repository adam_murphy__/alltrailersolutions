<div class="parallax ParallaxOnePanes5">
<div class="parallax_overlay <%=GetSetting("ParallaxOverlayStyle")%>">
    
	<div class="container">

        <!-- 100% Pane -->
        <div class="row">
		    <div class="wsc_pane col-md-12" id="WideParallax_5" runat="server"></div> 
        </div>

        <!-- 50% Pane --> 
        <div class="row">
            <div class="wsc_pane col-md-6" id="LeftHalfParallax_5" runat="server"></div>
            <div class="wsc_pane col-md-6" id="RightHalfParallax_5" runat="server"></div>
        </div>

        <!-- 30% and 70% Pane --> 
        <div class="row">
            <div class="wsc_pane col-md-4" id="LeftNarrowParallax_5" runat="server"></div>
            <div class="wsc_pane col-md-8" id="RightWideParallax_5" runat="server"></div>
        </div>

        <!-- 70% and 30% Pane --> 
        <div class="row">
            <div class="wsc_pane col-md-8" id="LeftWideParallax_5" runat="server"></div>
            <div class="wsc_pane col-md-4" id="RightNarrowParallax_5" runat="server"></div>
        </div>

        <!-- 33% Pane --> 
        <div class="row">
            <div class="wsc_pane col-md-4" id="ThirdLeftParallax_5" runat="server"></div>
            <div class="wsc_pane col-md-4" id="ThirdMiddleParallax_5" runat="server"></div>
            <div class="wsc_pane col-md-4" id="ThirdRightParallax_5" runat="server"></div>
        </div>

        <!-- 25% Pane --> 
        <div class="row">
            <div class="wsc_pane col-md-3" id="Narrow25Parallax_5" runat="server"></div>
            <div class="wsc_pane col-md-3" id="Narrow50Parallax_5" runat="server"></div>
            <div class="wsc_pane col-md-3" id="Narrow75Parallax_5" runat="server"></div>
            <div class="wsc_pane col-md-3" id="Narrow100Parallax_5" runat="server"></div>
        </div>

        <!-- Bottom 100% Pane -->
        <div class="row">
            <div class="wsc_pane col-md-12" id="BottomParallax_5" runat="server"></div>
        </div>

	</div>

</div>
</div>