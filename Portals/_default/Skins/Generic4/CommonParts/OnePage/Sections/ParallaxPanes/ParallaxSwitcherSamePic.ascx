﻿<%-- PARALLAX IMAGES 
Paralax sections 1,2,3,4,5 have the same images at the skins other than parallax--%>
<style type="text/css">
    .ParallaxPanes1,
    .ParallaxPanes2,
    .ParallaxPanes3,
    .ParallaxPanes4,
    .ParallaxPanes5 {background-image: url('<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("ParallaxBackground1")%>?v=<%=GetSetting("timestamp")%>');}
</style>