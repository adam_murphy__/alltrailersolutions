﻿<style type="text/css">
    /*-----------------------------------------------------------------------------------*/
    /*	PARALLAX IMAGES
    /*-----------------------------------------------------------------------------------*/

    /* Paralax Panes 1
    ---------------------------------------------------------- */
    .ParallaxOnePanes1 {background-image: url('<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("ParallaxOneBackground1")%>?v=<%=GetSetting("timestamp")%>');}

    /* Paralax Panes 2
    ---------------------------------------------------------- */
    .ParallaxOnePanes2 {background-image: url('<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("ParallaxOneBackground2")%>?v=<%=GetSetting("timestamp")%>');}

    /* Paralax Panes 3
    ---------------------------------------------------------- */
    .ParallaxOnePanes3 {background-image: url('<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("ParallaxOneBackground3")%>?v=<%=GetSetting("timestamp")%>');}

    /* Paralax Panes 4
    ---------------------------------------------------------- */
    .ParallaxOnePanes4 {background-image: url('<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("ParallaxOneBackground4")%>?v=<%=GetSetting("timestamp")%>');}
    
    /* Paralax Panes 5
    ---------------------------------------------------------- */
    .ParallaxOnePanes5 {background-image: url('<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("ParallaxOneBackground5")%>?v=<%=GetSetting("timestamp")%>');}

    /* Paralax Panes 6
    ---------------------------------------------------------- */
    .ParallaxOnePanes6 {background-image: url('<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("ParallaxOneBackground6")%>?v=<%=GetSetting("timestamp")%>');}
    
</style>