<div id="page-item-4" class="page-section">

    <!-- Page Title -->
	<p class="page-title btn"><span class="section_name"><span class="section_tooltip">section 4</span></span></p>
    <div class="clear"></div>

    <div class="p-parallax p-parallax-one-4 <%=GetSetting("ParallaxFontColor")%>">
        <!--#include file="ParallaxPanes/ParallaxPanes4.ascx"-->
    </div>

    <div class="wsc_pane FullWidePane" id="FullWidePane_4" runat="server"></div>

    <div class="container">
        <div class="row">
            <div class="wsc_pane col-md-6" id="LeftHalfPane_4" runat="server">
            </div>
            <div class="wsc_pane col-md-6" id="RightHalfPane_4" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 wsc_pane WidePane" id="WidePane_4" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-4" id="ThirdLeftPane_4" runat="server">
            </div>
            <div class="wsc_pane col-md-4" id="ThirdMiddlePane_4" runat="server">
            </div>
            <div class="wsc_pane col-md-4" id="ThirdRightPane_4" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-3" id="NarrowPane0_4" runat="server">
            </div>
            <div class="wsc_pane col-md-3" id="NarrowPane25_4" runat="server">
            </div>
            <div class="wsc_pane col-md-3" id="NarrowPane50_4" runat="server">
            </div>
            <div class="wsc_pane col-md-3" id="NarrowPane75_4" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-4" id="LeftNarrowPane_4" runat="server">
            </div>
            <div class="wsc_pane col-md-8" id="RightWidePane_4" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-8" id="LeftWidePane_4" runat="server">
            </div>
            <div class="wsc_pane col-md-4" id="RightNarrowPane_4" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-3" id="LeftQuarterPane_4" runat="server">
            </div>
            <div class="wsc_pane col-md-9" id="RightThreeQuartersPane_4" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-9" id="LeftThreeQuartersPane_4" runat="server">
            </div>
            <div class="wsc_pane col-md-3" id="RightQuarterPane_4" runat="server">
            </div>
        </div>
    </div>

</div>