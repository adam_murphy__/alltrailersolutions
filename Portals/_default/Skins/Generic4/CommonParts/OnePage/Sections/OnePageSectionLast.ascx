<div id="page-item-last" class="page-section footer">

    <!-- Page Title -->
	<p class="page-title btn"><span class="section_name"><span class="section_tooltip">last section</span></span></p>
    <div class="clear"></div>

    <div class="footer_wrapper">
	    <!--footer -->
	    <section id="footer">
            <div class="wsc_pane SocialFooter" id="SocialFooter" runat="server"></div>
	    </section>
	    <!--//footer -->

	    <!--footer 2 -->
	    <section id="footer2">
		    <div class="container">
			    <div class="row">
				    <div class="col-md-12">
				    <div class="copyright">
                        <dnn:COPYRIGHT ID="FooterCopyright" runat="server" /> :
                        <dnn:TERMS ID="dnnTerms" runat="server" /> :
                        <dnn:PRIVACY ID="dnnPrivacy" runat="server" />
				    </div>
				    </div>
			    </div>
		    </div>
	    </section>
        <!--//footer 2 -->

        <!--alternative footer panes-->
	    <section id="footer_alt">
            <!--#include file="../../FooterPanes.ascx"-->
	    </section>
        <!--//alternative footer panes-->

	</div>
    
</div>