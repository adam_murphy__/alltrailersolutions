<div id="page-item-6" class="page-section">

    <!-- Page Title -->
	<p class="page-title btn"><span class="section_name"><span class="section_tooltip">section 6</span></span></p>
    <div class="clear"></div>

    <div class="p-parallax p-parallax-one-6 <%=GetSetting("ParallaxFontColor")%>">
        <!--#include file="ParallaxPanes/ParallaxPanes6.ascx"-->
    </div>

    <div class="wsc_pane FullWidePane" id="FullWidePane_6" runat="server"></div>

    <div class="container">
        <div class="row">
            <div class="wsc_pane col-md-6" id="LeftHalfPane_6" runat="server">
            </div>
            <div class="wsc_pane col-md-6" id="RightHalfPane_6" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 wsc_pane WidePane" id="WidePane_6" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-4" id="ThirdLeftPane_6" runat="server">
            </div>
            <div class="wsc_pane col-md-4" id="ThirdMiddlePane_6" runat="server">
            </div>
            <div class="wsc_pane col-md-4" id="ThirdRightPane_6" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-3" id="NarrowPane0_6" runat="server">
            </div>
            <div class="wsc_pane col-md-3" id="NarrowPane25_6" runat="server">
            </div>
            <div class="wsc_pane col-md-3" id="NarrowPane50_6" runat="server">
            </div>
            <div class="wsc_pane col-md-3" id="NarrowPane75_6" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-4" id="LeftNarrowPane_6" runat="server">
            </div>
            <div class="wsc_pane col-md-8" id="RightWidePane_6" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-8" id="LeftWidePane_6" runat="server">
            </div>
            <div class="wsc_pane col-md-4" id="RightNarrowPane_6" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-3" id="LeftQuarterPane_6" runat="server">
            </div>
            <div class="wsc_pane col-md-9" id="RightThreeQuartersPane_6" runat="server">
            </div>
        </div>
        <div class="row">
            <div class="wsc_pane col-md-9" id="LeftThreeQuartersPane_6" runat="server">
            </div>
            <div class="wsc_pane col-md-3" id="RightQuarterPane_6" runat="server">
            </div>
        </div>
    </div>

    <div class="strip">
	    <div class="wsc_pane OutroPane" id="OutroPane" runat="server"></div>
    </div>

</div>