﻿<section id="intro" class="<%=GetColorClass("ColoredSectionsBackgroundColor")%>">
	<div class="intro_wrapper">
	    <div class="container">
	        <div class="row">
	            <div class="wsc_pane col-md-12 IntroPane" id="IntroPane" runat="server">
                </div>
		    </div>
	    </div>
        <div class="wsc_pane IntroPane" id="IntroWidePane" runat="server">
        </div>
	</div>
</section>