﻿<%If (GetSetting("TobBarOnOff") <> "wsc_tb_hidden") Then%>
    <div id="top-bar" class="<%=GetSetting("TobBarOnOffDevice")%>">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-sm-6 top-bar-left">
                    <%If ((GetSetting("TopBarDisplay") = "tb_contacts") Or (GetSetting("TopBarDisplay") = "")) Then%>
                        <!--#include file="TopContacts.ascx"-->
                    <%End If%>
                    <%If (GetSetting("TopBarDisplay") = "tb_pane") Then%>
                        <div class="wsc-td">
                            <div class="wsc_pane TopBarPane" id="TopBarLeftPane" runat="server"></div>
                        </div>
                    <%End If%>
                    <%If (GetSetting("TopBarDisplay") = "tb_pane_pagename") Then%>
                        <div class="wsc-td">
                            <span class="sb-content-title"><%= PortalSettings.ActiveTab.Title %></span>
                        </div>
                        <div class="wsc-td">
                            <div class="wsc_pane TopBarPane" id="TopBarLeft" runat="server"></div>
                        </div>
                    <%End If%>
                </div>
                <div class="col-md-6 col-sm-6 top-bar-right">
                    <div class="wsc_top_bar_logins">
                        <!--search-->
                        <div class="clearer search_mobile_clearer"></div>
                        <div class="wsc_search_wrap wsc_use_forms">
                            <a href="#go" class="wsc_search_trigger"><i class="fa fa-search">&nbsp;</i><i class="fa fa-times">&nbsp;</i></a>
                            <dnn:SEARCH runat="server" ID="SEARCH1" CssClass="ServerSkinWidget" UseDropDownList="false" Submit="" />   
                        </div>
                        <div class="clearer search_mobile_clearer"></div>
                        <!--//search-->
                        <!--languages-->
                        <dnn:LANGUAGE runat="server" ID="dnnLANGUAGE" ShowLinks="True" ShowMenu="False" UseDropDownList="true"/>
                        <!--//languages-->
                    </div>
                    <div class="wsc_pane TopBarPane" id="TopBarPane" runat="server"></div>
                    <div class="wsc_top_bar_logins">
                        <!--logins-->
                        <i class="fa fa-lock">&nbsp;</i>
                        <dnn:USER ID="USER" runat="server" LegacyMode="false" />
                        <i class="fa fa-key">&nbsp;</i>
                        <dnn:LOGIN ID="LOGIN" CssClass="LoginLink" runat="server" LegacyMode="false" />
                    </div>
                    <div class="dnnClear"></div>
                </div> 
            </div>
        </div>
    </div>
<%End If%>