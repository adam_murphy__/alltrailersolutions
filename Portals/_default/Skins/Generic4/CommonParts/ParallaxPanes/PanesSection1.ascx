<%-- Switch this section background style --%>
<%-- Transparent or image or solid color background --%>
<%If ((GetSetting("AddSectionBackgroundStyle1") = "") Or (GetSetting("AddSectionBackgroundStyle1") = "transparent") Or (GetSetting("AddSectionBackgroundStyle1") = "image") Or (GetSetting("AddSectionBackgroundStyle1") = "color")) Then%>
    <div class="ContentPanes1 ColorPanes ColorPanes1">
    <div class="wsc-add-section-inner <%=GetColorClass("AddSectionBackgroundColor1")%>">
    
    <%-- Load background image in case of image background --%>
    <%If (GetSetting("AddSectionBackgroundStyle1") = "image") Then%>
        <style>.ContentPanes1 {background-image: url('<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("CustomParallaxBackground1")%>?v=<%=GetSetting("timestamp")%>'); background-position: center <%=GetSetting("AddSectionImageVPosition1")%>;}</style>
    <%End If%>

    <%-- Case of transparent background --%>     
    <%If (GetSetting("AddSectionBackgroundStyle1") = "transparent") Then%>
        <style>.ContentPanes1 {background-color: transparent !important;}</style>
    <%End If%>   
            
<%End If%>
<%-- Parallax image background --%>
<%If (GetSetting("AddSectionBackgroundStyle1") = "parallax") Then%>
    <div class="parallax ParallaxPanes1">
    <div class="parallax_overlay <%=GetColorClass("AddSectionBackgroundColor1")%>">
    <style>.ParallaxPanes1 {background-image: url('<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("CustomParallaxBackground1")%>?v=<%=GetSetting("timestamp")%>'); background-position-y: top !important; }</style>
<%End If%>

	<div class="container">

        <%-- 100% Pane --%>
        <div class="row">
		    <div class="wsc_pane col-md-12" id="WideSection1" runat="server"></div> 
        </div>

        <%-- 50% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-6" id="LeftHalfSection1" runat="server"></div>
            <div class="wsc_pane col-md-6" id="RightHalfSection1" runat="server"></div>
        </div>

        <%-- 30% and 70% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-4" id="LeftNarrowSection1" runat="server"></div>
            <div class="wsc_pane col-md-8" id="RightWideSection1" runat="server"></div>
        </div>

        <%-- 70% and 30% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-8" id="LeftWideSection1" runat="server"></div>
            <div class="wsc_pane col-md-4" id="RightNarrowSection1" runat="server"></div>
        </div>

        <%-- 75% and 25% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-9" id="ThreeQuarterLeftSection1" runat="server"></div>
            <div class="wsc_pane col-md-3" id="OneQuarterRightSection1" runat="server"></div>
        </div>

        <%-- 25% and 75% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-3" id="OneQuarterLeftSection1" runat="server"></div>
            <div class="wsc_pane col-md-9" id="ThreeQuarterRightSection1" runat="server"></div>
        </div>

        <%-- 33% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-4" id="ThirdLeftSection1" runat="server"></div>
            <div class="wsc_pane col-md-4" id="ThirdMiddleSection1" runat="server"></div>
            <div class="wsc_pane col-md-4" id="ThirdRightSection1" runat="server"></div>
        </div>

        <%-- 25% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-3" id="Narrow25Section1" runat="server"></div>
            <div class="wsc_pane col-md-3" id="Narrow50Section1" runat="server"></div>
            <div class="wsc_pane col-md-3" id="Narrow75Section1" runat="server"></div>
            <div class="wsc_pane col-md-3" id="Narrow100Section1" runat="server"></div>
        </div>

        <%-- 20% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-15 col-sm-3" id="Fifth1Section1" runat="server"></div>
            <div class="wsc_pane col-md-15 col-sm-3" id="Fifth2Section1" runat="server"></div>
            <div class="wsc_pane col-md-15 col-sm-3" id="Fifth3Section1" runat="server"></div>
            <div class="wsc_pane col-md-15 col-sm-3" id="Fifth4Section1" runat="server"></div>
            <div class="wsc_pane col-md-15 col-sm-3" id="Fifth5Section1" runat="server"></div>
        </div>

        <%-- Bottom 100% Pane --%>
        <div class="row">
            <div class="wsc_pane col-md-12" id="BottomSection1" runat="server"></div>
        </div>

	</div>

    <%-- Screen-wide Pane --%>
    <div class="wsc_pane" id="FullWideSection1" runat="server"></div>

</div>
</div>  