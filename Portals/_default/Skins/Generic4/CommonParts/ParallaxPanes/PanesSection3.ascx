<%-- Switch this section background style --%>
<%-- Transparent or image background --%>
<%If ((GetSetting("AddSectionBackgroundStyle3") = "") Or (GetSetting("AddSectionBackgroundStyle3") = "transparent") Or (GetSetting("AddSectionBackgroundStyle3") = "image") Or (GetSetting("AddSectionBackgroundStyle3") = "color")) Then%>
    <div class="ContentPanes3 ColorPanes ColorPanes3">
    <div class="wsc-add-section-inner <%=GetColorClass("AddSectionBackgroundColor3")%>">

    <%-- Load background image in case of image background --%>
    <%If (GetSetting("AddSectionBackgroundStyle3") = "image") Then%>
        <style>.ContentPanes3 {background-image: url('<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("CustomParallaxBackground3")%>?v=<%=GetSetting("timestamp")%>'); background-position: center <%=GetSetting("AddSectionImageVPosition3")%>;}</style>
    <%End If%>
        
    <%-- Case of transparent background --%>     
    <%If (GetSetting("AddSectionBackgroundStyle3") = "transparent") Then%>
        <style>.ContentPanes3 {background-color: transparent !important;}</style>
    <%End If%>  
          
<%End If%>
<%-- Parallax image background --%>
<%If (GetSetting("AddSectionBackgroundStyle3") = "parallax") Then%>
    <div class="parallax ParallaxPanes3">
    <div class="parallax_overlay <%=GetColorClass("AddSectionBackgroundColor3")%>">
    <style>.ParallaxPanes3 {background-image: url('<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("CustomParallaxBackground3")%>?v=<%=GetSetting("timestamp")%>');}</style>
<%End If%>

	<div class="container">

        <%-- 100% Pane --%>
        <div class="row">
		    <div class="wsc_pane col-md-12" id="WideSection3" runat="server"></div> 
        </div>

        <%-- 50% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-6" id="LeftHalfSection3" runat="server"></div>
            <div class="wsc_pane col-md-6" id="RightHalfSection3" runat="server"></div>
        </div>

        <%-- 30% and 70% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-4" id="LeftNarrowSection3" runat="server"></div>
            <div class="wsc_pane col-md-8" id="RightWideSection3" runat="server"></div>
        </div>

        <%-- 70% and 30% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-8" id="LeftWideSection3" runat="server"></div>
            <div class="wsc_pane col-md-4" id="RightNarrowSection3" runat="server"></div>
        </div>

        <%-- 75% and 25% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-9" id="ThreeQuarterLeftSection3" runat="server"></div>
            <div class="wsc_pane col-md-3" id="OneQuarterRightSection3" runat="server"></div>
        </div>

        <%-- 25% and 75% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-3" id="OneQuarterLeftSection3" runat="server"></div>
            <div class="wsc_pane col-md-9" id="ThreeQuarterRightSection3" runat="server"></div>
        </div>

        <%-- 33% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-4" id="ThirdLeftSection3" runat="server"></div>
            <div class="wsc_pane col-md-4" id="ThirdMiddleSection3" runat="server"></div>
            <div class="wsc_pane col-md-4" id="ThirdRightSection3" runat="server"></div>
        </div>

        <%-- 25% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-3" id="Narrow25Section3" runat="server"></div>
            <div class="wsc_pane col-md-3" id="Narrow50Section3" runat="server"></div>
            <div class="wsc_pane col-md-3" id="Narrow75Section3" runat="server"></div>
            <div class="wsc_pane col-md-3" id="Narrow100Section3" runat="server"></div>
        </div>

        <%-- 20% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-15 col-sm-3" id="Fifth1Section3" runat="server"></div>
            <div class="wsc_pane col-md-15 col-sm-3" id="Fifth2Section3" runat="server"></div>
            <div class="wsc_pane col-md-15 col-sm-3" id="Fifth3Section3" runat="server"></div>
            <div class="wsc_pane col-md-15 col-sm-3" id="Fifth4Section3" runat="server"></div>
            <div class="wsc_pane col-md-15 col-sm-3" id="Fifth5Section3" runat="server"></div>
        </div>

        <%-- Bottom 100% Pane --%>
        <div class="row">
            <div class="wsc_pane col-md-12" id="BottomSection3" runat="server"></div>
        </div>

	</div>

    <%-- Screen-wide Pane --%>
    <div class="wsc_pane" id="FullWideSection3" runat="server"></div>

</div>
</div>