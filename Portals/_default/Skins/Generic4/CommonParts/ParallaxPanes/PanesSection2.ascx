<%-- Switch this section background style --%>
<%-- Transparent or image background --%>
<%If ((GetSetting("AddSectionBackgroundStyle2") = "") Or (GetSetting("AddSectionBackgroundStyle2") = "transparent") Or (GetSetting("AddSectionBackgroundStyle2") = "image") Or (GetSetting("AddSectionBackgroundStyle2") = "color")) Then%>
    <div class="ContentPanes2 ColorPanes ColorPanes2">
    <div class="wsc-add-section-inner <%=GetColorClass("AddSectionBackgroundColor2")%>">

    <%-- Load background image in case of image background --%>
    <%If (GetSetting("AddSectionBackgroundStyle2") = "image") Then%>
        <style>.ContentPanes2 {background-image: url('<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("CustomParallaxBackground2")%>?v=<%=GetSetting("timestamp")%>'); background-position: center <%=GetSetting("AddSectionImageVPosition2")%>;}</style>
    <%End If%>  

    <%-- Case of transparent background --%>     
    <%If (GetSetting("AddSectionBackgroundStyle2") = "transparent") Then%>
        <style>.ContentPanes2 {background-color: transparent !important;}</style>
    <%End If%>  

<%End If%>
<%-- Parallax image background --%>
<%If (GetSetting("AddSectionBackgroundStyle2") = "parallax") Then%>
    <div class="parallax ParallaxPanes2">
    <div class="parallax_overlay <%=GetColorClass("AddSectionBackgroundColor2")%>">
    <style>.ParallaxPanes2 {background-image: url('<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("CustomParallaxBackground2")%>?v=<%=GetSetting("timestamp")%>');}</style>
<%End If%>

	<div class="container">

        <%-- 100% Pane --%>
        <div class="row">
		    <div class="wsc_pane col-md-12" id="WideSection2" runat="server"></div> 
        </div>

        <%-- 50% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-6" id="LeftHalfSection2" runat="server"></div>
            <div class="wsc_pane col-md-6" id="RightHalfSection2" runat="server"></div>
        </div>

        <%-- 30% and 70% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-4" id="LeftNarrowSection2" runat="server"></div>
            <div class="wsc_pane col-md-8" id="RightWideSection2" runat="server"></div>
        </div>

        <%-- 70% and 30% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-8" id="LeftWideSection2" runat="server"></div>
            <div class="wsc_pane col-md-4" id="RightNarrowSection2" runat="server"></div>
        </div>

        <%-- 75% and 25% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-9" id="ThreeQuarterLeftSection2" runat="server"></div>
            <div class="wsc_pane col-md-3" id="OneQuarterRightSection2" runat="server"></div>
        </div>

        <%-- 25% and 75% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-3" id="OneQuarterLeftSection2" runat="server"></div>
            <div class="wsc_pane col-md-9" id="ThreeQuarterRightSection2" runat="server"></div>
        </div>

        <%-- 33% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-4" id="ThirdLeftSection2" runat="server"></div>
            <div class="wsc_pane col-md-4" id="ThirdMiddleSection2" runat="server"></div>
            <div class="wsc_pane col-md-4" id="ThirdRightSection2" runat="server"></div>
        </div>

        <%-- 25% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-3" id="Narrow25Section2" runat="server"></div>
            <div class="wsc_pane col-md-3" id="Narrow50Section2" runat="server"></div>
            <div class="wsc_pane col-md-3" id="Narrow75Section2" runat="server"></div>
            <div class="wsc_pane col-md-3" id="Narrow100Section2" runat="server"></div>
        </div>

        <%-- 20% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-15 col-sm-3" id="Fifth1Section2" runat="server"></div>
            <div class="wsc_pane col-md-15 col-sm-3" id="Fifth2Section2" runat="server"></div>
            <div class="wsc_pane col-md-15 col-sm-3" id="Fifth3Section2" runat="server"></div>
            <div class="wsc_pane col-md-15 col-sm-3" id="Fifth4Section2" runat="server"></div>
            <div class="wsc_pane col-md-15 col-sm-3" id="Fifth5Section2" runat="server"></div>
        </div>

        <%-- Bottom 100% Pane --%>
        <div class="row">
            <div class="wsc_pane col-md-12" id="BottomSection2" runat="server"></div>
        </div>

	</div>

    <%-- Screen-wide Pane --%>
    <div class="wsc_pane" id="FullWideSection2" runat="server"></div>

</div>
</div>