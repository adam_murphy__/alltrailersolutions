<%-- Switch this section background style --%>
<%-- Transparent or image background --%>
<%If ((GetSetting("AddSectionBackgroundStyle5") = "") Or (GetSetting("AddSectionBackgroundStyle5") = "transparent") Or (GetSetting("AddSectionBackgroundStyle5") = "image") Or (GetSetting("AddSectionBackgroundStyle5") = "color")) Then%>
    <div class="ContentPanes5 ColorPanes ColorPanes5">
    <div class="wsc-add-section-inner <%=GetColorClass("AddSectionBackgroundColor5")%>">

    <%-- Load background image in case of image background --%>
    <%If (GetSetting("AddSectionBackgroundStyle5") = "image") Then%>
        <style>.ContentPanes5 {background-image: url('<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("CustomParallaxBackground5")%>?v=<%=GetSetting("timestamp")%>'); background-position: center <%=GetSetting("AddSectionImageVPosition5")%>;}</style>
    <%End If%>  

    <%-- Case of transparent background --%>     
    <%If (GetSetting("AddSectionBackgroundStyle5") = "transparent") Then%>
        <style>.ContentPanes5 {background-color: transparent !important;}</style>
    <%End If%>  

<%End If%>
<%-- Parallax image background --%>
<%If (GetSetting("AddSectionBackgroundStyle5") = "parallax") Then%>
    <div class="parallax ParallaxPanes5">
    <div class="parallax_overlay <%=GetColorClass("AddSectionBackgroundColor5")%>">
    <style>.ParallaxPanes5 {background-image: url('<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("CustomParallaxBackground5")%>?v=<%=GetSetting("timestamp")%>');}</style>
<%End If%>

	<div class="container">

        <%-- 100% Pane --%>
        <div class="row">
		    <div class="wsc_pane col-md-12" id="WideSection5" runat="server"></div> 
        </div>

        <%-- 50% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-6" id="LeftHalfSection5" runat="server"></div>
            <div class="wsc_pane col-md-6" id="RightHalfSection5" runat="server"></div>
        </div>

        <%-- 30% and 70% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-4" id="LeftNarrowSection5" runat="server"></div>
            <div class="wsc_pane col-md-8" id="RightWideSection5" runat="server"></div>
        </div>

        <%-- 70% and 30% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-8" id="LeftWideSection5" runat="server"></div>
            <div class="wsc_pane col-md-4" id="RightNarrowSection5" runat="server"></div>
        </div>

        <%-- 75% and 25% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-9" id="ThreeQuarterLeftSection5" runat="server"></div>
            <div class="wsc_pane col-md-3" id="OneQuarterRightSection5" runat="server"></div>
        </div>

        <%-- 25% and 75% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-3" id="OneQuarterLeftSection5" runat="server"></div>
            <div class="wsc_pane col-md-9" id="ThreeQuarterRightSection5" runat="server"></div>
        </div>

        <%-- 33% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-4" id="ThirdLeftSection5" runat="server"></div>
            <div class="wsc_pane col-md-4" id="ThirdMiddleSection5" runat="server"></div>
            <div class="wsc_pane col-md-4" id="ThirdRightSection5" runat="server"></div>
        </div>

        <%-- 25% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-3" id="Narrow25Section5" runat="server"></div>
            <div class="wsc_pane col-md-3" id="Narrow50Section5" runat="server"></div>
            <div class="wsc_pane col-md-3" id="Narrow75Section5" runat="server"></div>
            <div class="wsc_pane col-md-3" id="Narrow100Section5" runat="server"></div>
        </div>

        <%-- 20% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-15 col-sm-3" id="Fifth1Section5" runat="server"></div>
            <div class="wsc_pane col-md-15 col-sm-3" id="Fifth2Section5" runat="server"></div>
            <div class="wsc_pane col-md-15 col-sm-3" id="Fifth3Section5" runat="server"></div>
            <div class="wsc_pane col-md-15 col-sm-3" id="Fifth4Section5" runat="server"></div>
            <div class="wsc_pane col-md-15 col-sm-3" id="Fifth5Section5" runat="server"></div>
        </div>

        <%-- Bottom 100% Pane --%>
        <div class="row">
            <div class="wsc_pane col-md-12" id="BottomSection5" runat="server"></div>
        </div>

	</div>

    <%-- Screen-wide Pane --%>
    <div class="wsc_pane" id="FullWideSection5" runat="server"></div>

</div>
</div>