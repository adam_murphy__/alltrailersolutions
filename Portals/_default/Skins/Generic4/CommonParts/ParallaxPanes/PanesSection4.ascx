<%-- Switch this section background style --%>
<%-- Transparent or image background --%>
<%If ((GetSetting("AddSectionBackgroundStyle4") = "") Or (GetSetting("AddSectionBackgroundStyle4") = "transparent") Or (GetSetting("AddSectionBackgroundStyle4") = "image") Or (GetSetting("AddSectionBackgroundStyle4") = "color")) Then%>
    <div class="ContentPanes4 ColorPanes ColorPanes4">
    <div class="wsc-add-section-inner <%=GetColorClass("AddSectionBackgroundColor4")%>">

    <%-- Load background image in case of image background --%>
    <%If (GetSetting("AddSectionBackgroundStyle4") = "image") Then%>
        <style>.ContentPanes4 {background-image: url('<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("CustomParallaxBackground4")%>?v=<%=GetSetting("timestamp")%>'); background-position: center <%=GetSetting("AddSectionImageVPosition4")%>;}</style>
    <%End If%>  

    <%-- Case of transparent background --%>     
    <%If (GetSetting("AddSectionBackgroundStyle4") = "transparent") Then%>
        <style>.ContentPanes4 {background-color: transparent !important;}</style>
    <%End If%>  

<%End If%>
<%-- Parallax image background --%>
<%If (GetSetting("AddSectionBackgroundStyle4") = "parallax") Then%>
    <div class="parallax ParallaxPanes4">
    <div class="parallax_overlay <%=GetColorClass("AddSectionBackgroundColor4")%>">
    <style>.ParallaxPanes4 {background-image: url('<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("CustomParallaxBackground4")%>?v=<%=GetSetting("timestamp")%>');}</style>
<%End If%>

	<div class="container">

        <%-- 100% Pane --%>
        <div class="row">
		    <div class="wsc_pane col-md-12" id="WideSection4" runat="server"></div> 
        </div>

        <%-- 50% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-6" id="LeftHalfSection4" runat="server"></div>
            <div class="wsc_pane col-md-6" id="RightHalfSection4" runat="server"></div>
        </div>

        <%-- 30% and 70% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-4" id="LeftNarrowSection4" runat="server"></div>
            <div class="wsc_pane col-md-8" id="RightWideSection4" runat="server"></div>
        </div>

        <%-- 70% and 30% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-8" id="LeftWideSection4" runat="server"></div>
            <div class="wsc_pane col-md-4" id="RightNarrowSection4" runat="server"></div>
        </div>

        <%-- 75% and 25% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-9" id="ThreeQuarterLeftSection4" runat="server"></div>
            <div class="wsc_pane col-md-3" id="OneQuarterRightSection4" runat="server"></div>
        </div>

        <%-- 25% and 75% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-3" id="OneQuarterLeftSection4" runat="server"></div>
            <div class="wsc_pane col-md-9" id="ThreeQuarterRightSection4" runat="server"></div>
        </div>

        <%-- 33% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-4" id="ThirdLeftSection4" runat="server"></div>
            <div class="wsc_pane col-md-4" id="ThirdMiddleSection4" runat="server"></div>
            <div class="wsc_pane col-md-4" id="ThirdRightSection4" runat="server"></div>
        </div>

        <%-- 25% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-3" id="Narrow25Section4" runat="server"></div>
            <div class="wsc_pane col-md-3" id="Narrow50Section4" runat="server"></div>
            <div class="wsc_pane col-md-3" id="Narrow75Section4" runat="server"></div>
            <div class="wsc_pane col-md-3" id="Narrow100Section4" runat="server"></div>
        </div>

        <%-- 20% Pane --%> 
        <div class="row">
            <div class="wsc_pane col-md-15 col-sm-3" id="Fifth1Section4" runat="server"></div>
            <div class="wsc_pane col-md-15 col-sm-3" id="Fifth2Section4" runat="server"></div>
            <div class="wsc_pane col-md-15 col-sm-3" id="Fifth3Section4" runat="server"></div>
            <div class="wsc_pane col-md-15 col-sm-3" id="Fifth4Section4" runat="server"></div>
            <div class="wsc_pane col-md-15 col-sm-3" id="Fifth5Section4" runat="server"></div>
        </div>

        <%-- Bottom 100% Pane --%>
        <div class="row">
            <div class="wsc_pane col-md-12" id="BottomSection4" runat="server"></div>
        </div>

	</div>

    <%-- Screen-wide Pane --%>
    <div class="wsc_pane" id="FullWideSection4" runat="server"></div>

</div>
</div>