<script type="text/javascript">

jQuery(function ($) {
    $.supersized({					

// Functionality
slide_interval          :   <%=GetSetting("BackgroundSlideshowPauseLength")%>,	  // Length between transitions
transition              :   <%=GetSetting("BackgroundSlideshowTransition")%>, // 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
transition_speed		:	<%=GetSetting("BackgroundSlideshowTransitionSpeed")%>,		// Speed of transition
															   
// Components							
slides 					:  	[			// Slideshow Images

<%If (GetSetting("BackgroundSlideshowAmount") = "2") Then%>

									{ image: '<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("CustomBackgroundSlide1")%>?v=<%=GetSetting("timestamp")%>'},
									{ image: '<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("CustomBackgroundSlide2")%>?v=<%=GetSetting("timestamp")%>'}

<%End If%>

<%If (GetSetting("BackgroundSlideshowAmount") = "3") Then%>

									{ image: '<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("CustomBackgroundSlide1")%>?v=<%=GetSetting("timestamp")%>'},
									{ image: '<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("CustomBackgroundSlide2")%>?v=<%=GetSetting("timestamp")%>'},
                                    { image: '<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("CustomBackgroundSlide3")%>?v=<%=GetSetting("timestamp")%>'}
<%End If%>

<%If (GetSetting("BackgroundSlideshowAmount") = "4") Then%>

									{ image: '<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("CustomBackgroundSlide1")%>?v=<%=GetSetting("timestamp")%>'},
									{ image: '<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("CustomBackgroundSlide2")%>?v=<%=GetSetting("timestamp")%>'},
                                    { image: '<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("CustomBackgroundSlide3")%>?v=<%=GetSetting("timestamp")%>'},
                                    { image: '<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("CustomBackgroundSlide4")%>?v=<%=GetSetting("timestamp")%>'}
<%End If%>

<%If (GetSetting("BackgroundSlideshowAmount") = "5") Then%>

									{ image: '<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("CustomBackgroundSlide1")%>?v=<%=GetSetting("timestamp")%>'},
									{ image: '<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("CustomBackgroundSlide2")%>?v=<%=GetSetting("timestamp")%>'},
                                    { image: '<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("CustomBackgroundSlide3")%>?v=<%=GetSetting("timestamp")%>'},
                                    { image: '<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("CustomBackgroundSlide4")%>?v=<%=GetSetting("timestamp")%>'},
                                    { image: '<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("CustomBackgroundSlide5")%>?v=<%=GetSetting("timestamp")%>'}
<%End If%>					
                            ]
					
    });
});

</script>