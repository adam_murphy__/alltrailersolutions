﻿<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%--<dnn:DnnJsInclude ID="bootstrapJS" runat="server" FilePath="js/bootstrap.min.js" PathNameAlias="SkinPath" priority="10" />--%>

<script type="text/javascript" src="<%= TemplateSourceDirectory %>/js/bootstrap.min.js"></script>

<%-- Switcher button script + its color picker load only for host --%>
<%If DotNetNuke.Security.PortalSecurity.IsInRole("Host") Then%>
<script type="text/javascript" src="<%= TemplateSourceDirectory %>/js/conditional/pick-a-color-1.2.3.min.js"></script>
<script type="text/javascript">

</script>
<% End If%>

<%-- Supersized Background Slideshow --%>
<%If (GetSetting("BackgroundSlideshowOnOff") = "1") Then%>
<script type="text/javascript" ><!--#include file="Scripts/supersized.3.2.7.min.js.ascx"--></script>
<!--#include file="BackgroundSlideshow.ascx"-->
<%End If%>

<script type="text/javascript" src="<%= TemplateSourceDirectory %>/js/plugins.js"></script>

<%-- Include MegaMenu scripts if used --%>
<%If ((GetSetting("MegaMenuTab1") <> "0") Or (GetSetting("MegaMenuTab2") <> "0")) Then%>
<!--#include file="MegaMenuSwitcher.ascx"-->
<%End If%>

<%-- Sticky Footer --%>
<%If (GetSetting("VerticalContentAlign") = "full_height") Then%>
<script type="text/javascript" >

// Set minimum height so footer will stay at the bottom of the window, even if there isn't enough content
function setMinHeight() {
    $('#main_content').css('min-height',
    $(window).outerHeight(true)
    - ($('body').outerHeight(true)
    - $('body').height())
    - $('#ControlBar').outerHeight()
    - $('.dnnControlPanel').outerHeight()
    - $('#header').outerHeight(true)
    - $('#banner').height()
    - $('#intro').height()
    - $('#map').height()
    - ($('#main_content').outerHeight(true) - $('#main_content').height())
    - $('.footer_wrapper').outerHeight(true)
    );

    $('.wsc-switcher-wrapper > .modal-dialog > .modal-content').css('min-height', $(window).outerHeight(true));
    
}

$(document).ready(function () {
    // Init
    setMinHeight();
});

// Window resize
$(window).on('resize', function () {
    var timer = window.setTimeout(function () {
        window.clearTimeout(timer);
        setMinHeight();
    }, 30);
});
</script>
<%End If%>

<script type="text/javascript" src="<%= TemplateSourceDirectory %>/js/scripts.js"></script>

<%-- Switcher module christmas snow --%>

<script type="text/javascript">
    $(document).ready(function () {
        /* OffScreen Side Navigation
        -------------------------------------------------------------------*/
        if($('.wsc-offscreen-container').length){
            $('body').addClass('wsc-has-offscreen-nav');
            $('.wsc_generic_inside_wrap').wrap('<div class="wsc-offscreen-wrap <%=GetSetting("SlideMenuType")%>"></div>');
            $(".wsc_generic_skin .wsc-offscreen-wrap").append('<div class="wsc-inside-overlay"></div>');
   
            $('.wsc-offscreen-toggle').click(function(){
                $('.wsc-offscreen-wrap').toggleClass('wsc-reveal-nav');
                $('.wsc-offscreen-container').toggleClass('wsc-reveal-nav');
            });

            $('.wsc-offscreen-container').click(function(){
                $('.wsc-offscreen-container').removeClass('wsc-reveal-nav');
                $('.wsc-offscreen-wrap').removeClass('wsc-reveal-nav');
            });

            $('#banner, #intro, #main_content, .footer_wrapper').click(function(){
                if($('.wsc-offscreen-wrap').hasClass('wsc-reveal-nav')){
                    $('.wsc-offscreen-wrap').removeClass('wsc-reveal-nav');
                    $('.wsc-offscreen-container').removeClass('wsc-reveal-nav');
                }
            });
        }
        else{
            $('body').removeClass('wsc-has-offscreen-nav');
        }
    });
</script>


<script type="text/javascript">

    $(window).load(function () {
            
        <%-- Switcher module gradient with 2 colors --%>
        <%If ((GetSetting("BackgroundImage") = "gradient2") And (GetSetting("ScreenGradientType") = "colorScroll")) Then%>            
            
        $('body, .tp-colorchange').colorScroll({
                colors: [{
                    color: '#<%=GetSetting("SolidBackgroundColor")%>',
                    position: '0%'
                }, {
                    color: '#<%=GetSetting("SolidBackgroundColor2")%>',
                    position: '100%'
                }]
            });
        $('.common_background').css('display', 'none');
        setTimeout(function(){$(window).trigger('resize');}, 1000);
        <%End If%>

        <%-- Switcher module gradient with 3 colors --%>
        <%If ((GetSetting("BackgroundImage") = "gradient3") And (GetSetting("ScreenGradientType") = "colorScroll")) Then%>

        $('body, .tp-colorchange').colorScroll({
            colors: [{  
                color: '#<%=GetSetting("SolidBackgroundColor")%>',
                    position: '0%'
                }, {
                    color: '#<%=GetSetting("SolidBackgroundColor2")%>',
                    position: '50%'
                }, {
                    color: '#<%=GetSetting("SolidBackgroundColor3")%>',
                    position: '100%'
                }]
                    });
        $('.common_background').css('display', 'none');
        setTimeout(function(){$(window).trigger('resize');}, 1000);
        <%End If%>

    });
</script>


<%-- Switcher module christmas snow --%>
<%If (GetSetting("ChristmasSnowOnOff") = "christmas_snow_on") Then%>
    <script type="text/javascript" ><!--#include file="Scripts/jquery.snow.js.ascx"--></script>
    <script type="text/javascript">
        $(document).ready(
        function () {
            $.fn.snow({ minSize: <%=GetSetting("MinFlakeSize")%>, maxSize: <%=GetSetting("MaxFlakeSize")%>, newOn: <%=GetSetting("NewFlakeOn")%>, flakeColor: '#<%=GetSetting("FlakeColor")%>' });
        }
        );
    </script>
<%End If%>

<%-- Specify the number of menu tab to hide its submenu --%>
<%If (GetSetting("HideSubMenu") <> "") Then%>
    <script type="text/javascript">
    $('.main_menu ul.sf-menu.nav > li.root > a').each(function(){
        _this=$(this);
        var str = $(this).clone().children().remove().end().text();
        if (str.indexOf('<%=GetSetting("HideSubMenu")%>') > -1) {
            _this.parents('li.root').addClass('wsc_hide_sub');
        }
     });
    </script>
<%End If%>

<%-- Custom Font Awesome bullet --%>
<%If (GetSetting("faBulletName") <> "") Then%>
    <script type="text/javascript">
    if ($('.wsc-custom-bullet').length > 0) {
        $('.wsc-custom-bullet').each(function () {
            <%If (GetSetting("faBulletColor") <> "wsc-bullet-color-default") Then%>    
                $(this).addClass('<%=GetSetting("faBulletColor")%>');
            <%End If%>
            $(this).find(">li").each(function () {
                $(this).prepend('<em class="fa-li fa fa-<%=GetSetting("faBulletName")%>"></em>');
            });
        });
    }
    </script>
<%End If%>

<%-- Switcher module text rotator --%>
<%If (GetSetting("TextRotatorPauseLength") <> "") Then%>
<script type="text/javascript">
    if ( $(".t-rotate-spin").length) {
            $(".t-rotate-spin").textrotator({
                animation: "spin",
                speed: <%=GetSetting("TextRotatorPauseLength")%>
            });
        }
        if ( $(".t-rotate-fade").length) {
            $(".t-rotate-fade").textrotator({
                animation: "fade",
                speed: <%=GetSetting("TextRotatorPauseLength")%>
            });
        }
        if ( $(".t-rotate-flip").length) {
            $(".t-rotate-flip").textrotator({
                animation: "flip",
                speed: <%=GetSetting("TextRotatorPauseLength")%>
            });
        }
        if ( $(".t-rotate-flipCube").length) {
            $(".t-rotate-flipCube").textrotator({
                animation: "flipCube",
                speed: <%=GetSetting("TextRotatorPauseLength")%>
            });
        }
        if ( $(".t-rotate-flipUp").length) {
            $(".t-rotate-flipUp").textrotator({
                animation: "flipUp",
                speed: <%=GetSetting("TextRotatorPauseLength")%>
            });
        }
</script>
<%End If%>

<%-- Switcher active tab saved and reproduced via cookies --%>
<%If DotNetNuke.Security.PortalSecurity.IsInRole("Host") Then%>
<script type="text/javascript">

</script>
<% End If%>