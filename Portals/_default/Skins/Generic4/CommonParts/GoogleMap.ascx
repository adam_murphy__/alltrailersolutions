﻿<script type="text/javascript">

/* Google map
-------------------------------------------------------------------*/

yepnope({
    test: $(".google-maps").length,
    yep: SkinPath + '/js/gmap3.min.js',
    callback: function () {
        var isDraggable = (jQuery.browser.mobile == false) ? true : false;
        /*function initialize() {*/
        $(".google-maps").gmap3({
            marker: {   /* You can check the full list of marker options here -->  https://developers.google.com/maps/documentation/javascript/reference?hl=fr#MarkerOptions */
                values: [
                { address: "<%=GetSetting("GoogleMapAdr1")%>", data: "<%=GetSetting("GoogleMapAdr1Text")%>" },
                { address: "<%=GetSetting("GoogleMapAdr2")%>", data: "<%=GetSetting("GoogleMapAdr2Text")%>" },
                { latLng: [<%=GetSetting("GoogleMapLat3")%>,<%=GetSetting("GoogleMapLong3")%>], data:"<%=GetSetting("GoogleMapCoord3Text")%>" }
                ],
                options: {
                    icon: SkinPath + "/img/marker.png", /* Icon for the foreground. */
                    animation: google.maps.Animation.DROP /* Two possible types of animation: "DROP" and "BOUNCE" */
                },
                events: {
                    mouseover: function (marker, event, context) {
                        var map = $(this).gmap3("get"),
                    infowindow = $(this).gmap3({ get: { name: "infowindow"} });
                        if (infowindow) {
                            infowindow.open(map, marker);
                            infowindow.setContent(context.data);
                        } else {
                            $(this).gmap3({
                                infowindow: {
                                    anchor: marker,
                                    options: { content: context.data }
                                }
                            });
                        }
                    },
                    mouseout: function () {
                        var infowindow = $(this).gmap3({ get: { name: "infowindow"} });
                        if (infowindow) {
                            infowindow.close();
                        }
                    }
                }
            },
            <%If (GetSetting("GoogleMapZoom") <> "") Then%>
            map: {   /* You can check the full list of map options here -->  https://developers.google.com/maps/documentation/javascript/reference#MapOptions */
                options: {
                    zoom: <%=GetSetting("GoogleMapZoom")%>, /* The initial Map zoom level. Required. */
                    scrollwheel: <%=GetSetting("GoogleMapWheel")%>, /* If false, disables scrollwheel zooming on the map. The scrollwheel is enabled by default. true/false */
                    mapTypeControl: <%=GetSetting("GoogleMapType")%>,  /* The initial enabled/disabled state of the Map type control. true/false */
                    streetViewControl: <%=GetSetting("GoogleMapStreet")%>, /* The initial enabled/disabled state of the Street View Pegman control. This control is part of the default UI, 
                                                and should be set to false when displaying a map type on which the Street View road overlay should not appear
                                                (e.g. a non-Earth map type). true/false */
                    scaleControl: false, /* The initial enabled/disabled state of the Scale control. true/false */
                    draggable: isDraggable,
                    mapTypeId: google.maps.MapTypeId.<%=GetSetting("GoogleMapViewType")%>
                }
            }
            <%End If%>
        });
    }
});


yepnope({
    test: $(".google-maps-hybrid").length,
    yep: SkinPath + '/js/gmap3.min.js',
    callback: function () {
        var isDraggable = (jQuery.browser.mobile == false) ? true : false;
        /*function initialize() {*/
        $(".google-maps-hybrid").gmap3({
            marker: {   /* You can check the full list of marker options here -->  https://developers.google.com/maps/documentation/javascript/reference?hl=fr#MarkerOptions */
                values: [
                { address: "<%=GetSetting("GoogleMapAdr1")%>", data: "<%=GetSetting("GoogleMapAdr1Text")%>" },
                { address: "<%=GetSetting("GoogleMapAdr2")%>", data: "<%=GetSetting("GoogleMapAdr2Text")%>" }
            ],
                options: {
                    icon: SkinPath + "/img/marker.png", /* Icon for the foreground. */
                    animation: google.maps.Animation.DROP /* Two possible types of animation: "DROP" and "BOUNCE" */
                },
                events: {
                    mouseover: function (marker, event, context) {
                        var map = $(this).gmap3("get"),
                    infowindow = $(this).gmap3({ get: { name: "infowindow"} });
                        if (infowindow) {
                            infowindow.open(map, marker);
                            infowindow.setContent(context.data);
                        } else {
                            $(this).gmap3({
                                infowindow: {
                                    anchor: marker,
                                    options: { content: context.data }
                                }
                            });
                        }
                    },
                    mouseout: function () {
                        var infowindow = $(this).gmap3({ get: { name: "infowindow"} });
                        if (infowindow) {
                            infowindow.close();
                        }
                    }
                }
            },
            <%If (GetSetting("GoogleMapZoom") <> "") Then%>
            map: {   /* You can check the full list of map options here -->  https://developers.google.com/maps/documentation/javascript/reference#MapOptions */
                options: {
                    zoom: <%=GetSetting("GoogleMapZoom")%>, /* The initial Map zoom level. Required. */
                    scrollwheel: <%=GetSetting("GoogleMapWheel")%>, /* If false, disables scrollwheel zooming on the map. The scrollwheel is enabled by default. true/false */
                    mapTypeControl: <%=GetSetting("GoogleMapType")%>,  /* The initial enabled/disabled state of the Map type control. true/false */
                    streetViewControl: <%=GetSetting("GoogleMapStreet")%>, /* The initial enabled/disabled state of the Street View Pegman control. This control is part of the default UI, 
                                                and should be set to false when displaying a map type on which the Street View road overlay should not appear
                                                (e.g. a non-Earth map type). true/false */
                    scaleControl: false, /* The initial enabled/disabled state of the Scale control. true/false */
                    draggable: isDraggable,
                    mapTypeId: google.maps.MapTypeId.HYBRID
                }
            }
            <%End If%>
        });
    }
});

</script>