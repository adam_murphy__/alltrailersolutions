﻿<div id="loading-spinner" class="static" data-loading="<%=GetSetting("Preloader")%>"><div class="spinner">Loading...</div></div>
<dnn:STYLES runat="server" id="wsc_bootstrap3" Name="wsc_bootstrap3" StyleSheet="css/bootstrap.min.css" UseSkinPath="true" />
<dnn:STYLES runat="server" id="wsc_stylecss" Name="wsc_stylecss" StyleSheet="css/style.css" UseSkinPath="true" />

<%--<link id="wsc-bootstrap3" href="<%= TemplateSourceDirectory %>/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
<link id="wsc-stylecss" href="<%= TemplateSourceDirectory %>/css/style.css" type="text/css" rel="stylesheet" />--%>

<%-- Add color picker styles for switcher in case of host user --%>
<%If DotNetNuke.Security.PortalSecurity.IsInRole("Host") Then%>
    <link id="wsc_pick_color" href="<%= TemplateSourceDirectory %>/css/conditional/pick-a-color-1.2.3.min.css" type="text/css" rel="stylesheet" />
<% End If%>

<%-- Google font --%>
<%If ((GetSetting("GoogleFontOnOff") = "google_font_on") And (GetSetting("GoogleFont") <> "")) Then%>
<link id="wsc_font_style3" href="//fonts.googleapis.com/css?family=<%=GetSetting("GoogleFont")%>" type="text/css" rel="stylesheet" />
<%End If%>

<!--[if lte IE 9]>
    <style>.wsc_generic_skin[data-loading="hidden"], .wsc_generic_skin[data-loading="hidden"] + .common_background {opacity:1;}</style>
<![endif]-->
<!-- Custom Font -->
<!--#include file="CustomFont.ascx"-->

<%-- Mobile side menu CSS file 
<dnn:STYLES runat="server" id="wsc_sidr" Name="wsc_sidr" StyleSheet="css/jquery.sidr.light.css" UseSkinPath="true" /> --%>

<%-- Switcher module delete in case of using MINI BOXED version 
<asp:PlaceHolder ID="PlaceHolder1" runat="server" Visible='<%#(GetSetting("Layout") = "skin_boxed_mini")%>' >
    <link id="wsc_widescreen" href="<%= TemplateSourceDirectory %>/css/widescreen.css" type="text/css" rel="stylesheet" />
</asp:PlaceHolder> --%>

<%-- Switcher module predefined color --%>
<%If (GetSetting("ColorScheme") <> "custom") Then%>
    <!--#include file="DefinedColor.ascx"-->
<%End If%>

<%-- Switcher module custom color --%>
<%If (GetSetting("ColorScheme") = "custom") Then%>
    <!--#include file="CustomColor.ascx"-->
<%End If%>

<%-- Background image defined --%>
<%If ((GetSetting("BackgroundImage") <> "custom.png") And (GetSetting("BackgroundImage") <> "none.png") And (GetSetting("BackgroundImage") <> "SolidColor") And (GetSetting("BackgroundImage") <> "gradient2") And (GetSetting("BackgroundImage") <> "gradient3")) Then%>
    <style>
        .common_background {background-image: url('<%= TemplateSourceDirectory %>/img/pat/<%=GetSetting("BackgroundImage")%>.jpg');}
    </style>
<%End If%>

<%-- Custom background image --%>
<%If (GetSetting("BackgroundImage") = "custom.png") Then%>
    <style>
         .common_background {background-image: url('<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("CustomBackground")%>?v=<%=GetSetting("timestamp")%>');}
    </style>
<%End If%>

<%-- NO background image --%>
<%If (GetSetting("BackgroundImage") = "none.png") Then%>
    <style>
         .common_background {display: none;}
    </style>
<%End If%>

<%-- Switcher module background pattern --%>
<%If ((GetSetting("BackgroundPattern") <> "custom.png") And (GetSetting("BackgroundPattern") <> "none.png")) Then%>
    <style>
         .common_background:after, .wsc-section-pattern  {background-image: url('<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("BackgroundPattern")%>');}
    </style>
<%End If%>

<%-- Switcher module custom background pattern --%>
<%If (GetSetting("BackgroundPattern") = "custom.png") Then%>
    <style>
        .common_background:after, .wsc-section-pattern {background-image: url('<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("CustomPattern")%>?v=<%=GetSetting("timestamp")%>');}
    </style>
<%End If%>

<%-- Solid background color --%>
<style>
    .common_background {background-color: #<%=GetSetting("SolidBackgroundColor")%>;}
</style>

<%-- Gradient 2 colors on background --%>
<%If ((GetSetting("BackgroundImage") = "gradient2") And (GetSetting("ScreenGradientType") = "regular")) Then%>
    <style>
        .common_background {
            background: #<%=GetSetting("SolidBackgroundColor")%>; /* Old browsers */
            background: -moz-linear-gradient(top, #<%=GetSetting("SolidBackgroundColor")%> 0%, #<%=GetSetting("SolidBackgroundColor2")%> 100%); /* FF3.6-15 */
            background: -webkit-linear-gradient(top, #<%=GetSetting("SolidBackgroundColor")%> 0%,#<%=GetSetting("SolidBackgroundColor2")%> 100%); /* Chrome10-25,Safari5.1-6 */
            background: linear-gradient(to bottom, #<%=GetSetting("SolidBackgroundColor")%> 0%,#<%=GetSetting("SolidBackgroundColor2")%> 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#<%=GetSetting("SolidBackgroundColor")%>', endColorstr='#<%=GetSetting("SolidBackgroundColor2")%>',GradientType=0 ); /* IE6-9 */
        }
    </style>
<%End If%>

<%-- Gradient 3 colors on background --%>
<%If ((GetSetting("BackgroundImage") = "gradient3") And (GetSetting("ScreenGradientType") = "regular")) Then%>
    <style>
        .common_background {
            background: #<%=GetSetting("SolidBackgroundColor")%>; /* Old browsers */
            background: -moz-linear-gradient(top, #<%=GetSetting("SolidBackgroundColor")%> 0%, #<%=GetSetting("SolidBackgroundColor2")%> 50%, #<%=GetSetting("SolidBackgroundColor3")%> 100%); /* FF3.6-15 */
            background: -webkit-linear-gradient(top, #<%=GetSetting("SolidBackgroundColor")%> 0%,#<%=GetSetting("SolidBackgroundColor2")%> 50%,#<%=GetSetting("SolidBackgroundColor3")%> 100%); /* Chrome10-25,Safari5.1-6 */
            background: linear-gradient(to bottom, #<%=GetSetting("SolidBackgroundColor")%> 0%,#<%=GetSetting("SolidBackgroundColor2")%> 50%,#<%=GetSetting("SolidBackgroundColor3")%> 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#<%=GetSetting("SolidBackgroundColor")%>', endColorstr='#<%=GetSetting("SolidBackgroundColor3")%>',GradientType=0 ); /* IE6-9 */        
        }
    </style>
<%End If%>

<%--Switcher module choose HEADER STYLE css file--%>
<%If ((GetSetting("ChooseHeaderStyle") = "theme_shadow_header_no") Or (GetSetting("ChooseHeaderStyle") = "theme_shadow_header_w") Or (GetSetting("ChooseHeaderStyle") = "theme_shadow_header") Or (GetSetting("ChooseHeaderStyle") = "theme_transparent_header")) Then%>
    <link id="ChooseHeaderStyle" href="<%= TemplateSourceDirectory %>/css/themes/<%=GetSetting("ChooseHeaderStyle")%>.css" type="text/css" rel="stylesheet" />
<%End If%> 

<%-- Down inner skin page title in case of transparent header themes --%>
<%If ((GetSetting("ChooseHeaderStyle") = "theme_shadow_header_no") Or (GetSetting("ChooseHeaderStyle") = "theme_shadow_header_w") Or (GetSetting("ChooseHeaderStyle") = "theme_shadow_header") Or (GetSetting("ChooseHeaderStyle") = "theme_transparent_header")) Then%>
    <style>
        .pagetitle_large .wsc_pane.IntroPane {padding-top: 170px;}
        .pagetitle_default #pagetitle {padding-top: 100px;}
        .pagetitle_mini #pagetitle {padding-top: 100px;}
        @media (max-width: 767px) {
            .pagetitle_large #pagetitle .wsc_pane.IntroPane {padding-top: 110px;}
        }
    </style>
<%End If%> 

<%-- Header background color --%>
<%If (GetSetting("HeaderBackgroundColor") <> "") Then%>
<style>
    .header, .sticky_header .header_inner {background-color: <%=HexToRGBA("#" + GetSetting("HeaderBackgroundColor"), GetSetting("ContentBackOpacity"))%>;}
    @media(max-width:979px){.main_menu /*Floating and Mobile menu bc*/ {background-color: <%=HexToRGBA("#" + GetSetting("HeaderBackgroundColor"), 1)%>;}}
    .menu_wrap .nav ul {background-color: <%=HexToRGBA("#" + GetSetting("HeaderBackgroundColor"), 1)%>;}
</style>
<%End If%>
   
<%-- Add bit of transparency to submenu and floating header when it is for body --%>
<%If (GetSetting("ContentBackOpacity") <> "1") Then%>
<style>
    .menu_wrap .nav ul, #floating-header.header {background-color: <%=HexToRGBA("#" + GetSetting("HeaderBackgroundColor"), 0.9)%>;}
</style>
<%End If%>

<%-- Header full width layout --%>
<%If (GetSetting("HeaderLayoutWidth") = "container-fluid") Then%>
<style>
/*From 767 to any*/
@media (min-width: 767px) {.container-fluid.wsc-header-container, #top-bar > .container-fluid {padding-left: 25px; padding-right: 25px;}}
</style>
<%End If%>



<%-- Content custom background color --%>
<%If (GetSetting("ContentBackgroundColor") <> "") Then%>
<style>
    #main_content,  .wsc_sidebar_skin {background-color: <%=HexToRGBA("#" + GetSetting("ContentBackgroundColor"), GetSetting("ContentBackOpacity"))%>;}
    .wsc-sidebar-inner li.root.active.with-arrow > a:after, .wsc-sidebar-inner li.root.with-arrow:not(.sub):hover > a:after {border-right-color:#<%=GetSetting("ContentBackgroundColor")%>;}
    .wsc-sidemenu-right .wsc-sidebar-inner li.root.active.with-arrow > a:after, .wsc-sidemenu-right .wsc-sidebar-inner li.root.with-arrow:not(.sub):hover > a:after {border-left-color:#<%=GetSetting("ContentBackgroundColor")%>;border-right-color:transparent;}
</style>
<%End If%>

<%-- Banner panes custom background color --%>
<%If (GetSetting("BannerBackgroundColor") <> "") Then%>
<style>
    #banner {background-color: <%=HexToRGBA("#" + GetSetting("BannerBackgroundColor"), GetSetting("ContentBackOpacity"))%>;}
</style>
<%End If%>

<%-- Footer background color --%>
<%If (GetSetting("FooterBackgroundColor") <> "") Then%>
<style>
    #footer_alt, #footer2 {background-color: <%=HexToRGBA("#" + GetSetting("FooterBackgroundColor"), GetSetting("ContentBackOpacity"))%>;}
</style>
<%End If%>

<%-- Intro and outro sections background color --%>
<%If (GetSetting("ColoredSectionsBackgroundColor") <> "") Then%><style>#intro, #outro .strip, .strip, .wsc-section-switcher {background-color: <%=HexToRGBA("#" + GetSetting("ColoredSectionsBackgroundColor"), GetSetting("ContentBackOpacity"))%>;}</style><%End If%>

<%-- Colored panes sections background color --%>
<%If (GetSetting("AddSectionBackgroundColor1") <> "") Then%><style>.ColorPanes1 {background-color: <%=HexToRGBA("#" + GetSetting("AddSectionBackgroundColor1"), GetSetting("ContentBackOpacity"))%>;}</style><%End If%>
<%If (GetSetting("AddSectionBackgroundColor2") <> "") Then%><style>.ColorPanes2 {background-color: <%=HexToRGBA("#" + GetSetting("AddSectionBackgroundColor2"), GetSetting("ContentBackOpacity"))%>;}</style><%End If%>
<%If (GetSetting("AddSectionBackgroundColor3") <> "") Then%><style>.ColorPanes3 {background-color: <%=HexToRGBA("#" + GetSetting("AddSectionBackgroundColor3"), GetSetting("ContentBackOpacity"))%>;}</style><%End If%>
<%If (GetSetting("AddSectionBackgroundColor4") <> "") Then%><style>.ColorPanes4 {background-color: <%=HexToRGBA("#" + GetSetting("AddSectionBackgroundColor4"), GetSetting("ContentBackOpacity"))%>;}</style><%End If%>
<%If (GetSetting("AddSectionBackgroundColor5") <> "") Then%><style>.ColorPanes5 {background-color: <%=HexToRGBA("#" + GetSetting("AddSectionBackgroundColor5"), GetSetting("ContentBackOpacity"))%>;}</style><%End If%>

<%-- The case of 0% background opacity (transparent background) --%>
<%If (GetSetting("ContentBackOpacity") = "0") Then%>
<style>
    .wsc_generic_skin {box-shadow: none !important;}
    #top-bar {background-color: transparent;}
    #top-bar:after{display: none;}
    #footer2 {border-top: none !important;}
</style>
<%End If%>


<%-- Page Title background color --%>
<%If (GetSetting("PageTitleBackgroundColor") <> "") Then%>
<style>
     #pagetitle {background-color: <%=HexToRGBA("#" + GetSetting("PageTitleBackgroundColor"), GetSetting("ContentBackOpacity"))%>;}
</style>
<%End If%>

<%--Supersized Background Slideshow--%>
<%If (GetSetting("BackgroundSlideshowOnOff") = "1") Then%>
    <style><!--#include file="Styles/supersized.css.ascx"--></style>
    <style>.common_background {display:none;}</style>
<%End If%>

<%--Square Corners--%>
<%If (GetSetting("SquareCorners") = "1") Then%>
    <style>
        .btn, .btn-rounded, .alert, .zocial, .fontawesome-icon-list .fa-hover a,
        .btn-lg, .btn-group-lg > .btn, .panel-group .panel,
        .pagination ul > li > a, .pagination ul > li > span,
        .menu_wrap .nav > li.active > a, .menu_wrap .nav > li.active,
        .menu_classic .menu_wrap .nav > li.active > a, .menu_classic .menu_wrap .nav > li.active,
        .go-top, .nav-pills > li > a, .th, .cbp_tmtimeline > li .cbp_tmlabel,
        .testimonial1, .testimonial2, .testimonial3,
        .big_button, a.big_button, .quote_sections, .quote_sections_hue, .quote_sections_dark, .quote_sections_light,
        .menu_wrap .nav ul, .menu_wrap .nav ul ul, .tile, .progress,
        .pricing-header-row-1, .pricing-footer, .slider-title,
        .widget-scroll-prev, .widget-scroll-next, .widget-scroll-prev2, 
        .widget-scroll-next2, .widget-scroll-prev3, .widget-scroll-next3,
        .panel-heading, .panel, #filters li a, #filters2 li a, .holder a,
        input[type="text"], input[type="search"],
        .wsc_use_forms input[type="text"], .wsc_use_forms input[type="search"],
        pre, .bs-docs-example::after, .bs-docs-example,
        .dnnFormItem input[type="text"], .dnnFormItem select, .dnnFormItem textarea, 
        .dnnFormItem input[type="email"], .dnnFormItem input[type="search"], .dnnFormItem input[type="password"],
        .menu_wrap .nav ul ul a, .wsc-promo.wsc-promo-border, .wsc-promo.wsc-promo-light,
        .rvdsfContentContainer ul.dnnAdminTabNav li a, .wsc_switcher_control > a#SwitcherBtn, .wsc_back_home a.btn,
        #filters, .modal-content, .sw-go-top
        {border-radius: 0px !important;}
    </style>
<%End If%>

<%--Custom radius round Corners--%>
<%If (GetSetting("SquareCorners") = "0") Then%>
    <style>
        .btn,.btn-rounded,.alert,.zocial,.fontawesome-icon-list .fa-hover a,.btn-lg,.btn-group-lg > .btn,.panel-group .panel,.pagination ul > li > a,.pagination ul > li > span,.menu_wrap .nav > li.active > a,.menu_wrap .nav > li.active,.menu_classic .menu_wrap .nav > li.active > a,.menu_classic .menu_wrap .nav > li.active,.go-top,.nav-pills > li > a,.th,.cbp_tmtimeline > li .cbp_tmlabel,.testimonial1,.testimonial2,.testimonial3,.big_button,a.big_button,.quote_sections,.quote_sections_hue,.quote_sections_dark,.quote_sections_light,.menu_wrap .nav ul,.menu_wrap .nav ul ul,.tile,.progress,.pricing-header-row-1,.pricing-footer,.slider-title,.widget-scroll-prev,.widget-scroll-next,.widget-scroll-prev2,.widget-scroll-next2,.widget-scroll-prev3,.widget-scroll-next3,.panel-heading,.panel,#filters li a,#filters2 li a,.holder a,input[type="text"],input[type="search"],.wsc_use_forms input[type="text"],.wsc_use_forms input[type="search"],pre,.bs-docs-example::after,.bs-docs-example,.dnnFormItem input[type="text"],.dnnFormItem select,.dnnFormItem textarea,.dnnFormItem input[type="email"],.dnnFormItem input[type="search"],.dnnFormItem input[type="password"],.menu_wrap .nav ul li.wsc_first a, .menu_wrap .nav ul li.wsc_last a, .wsc-promo.wsc-promo-border,.wsc-promo.wsc-promo-light,.wsc-button,.rvdsfContentContainer ul.dnnAdminTabNav li a, .wsc-testi-image img,
        .wsc-round-corners, .main_menu, .menu_wrap .nav .submenu_wrap li.wsc_first.wsc_last a, .modal-content, .sw-go-top
        {border-radius: <%=GetSetting("CornersRadius")%>px;}
        .menu_wrap .nav>li.active, .menu_wrap .nav>li.active>a, .menu_wrap .nav .submenu_wrap li.wsc_first a, .panel-default > .panel-heading, .rvdsfContentContainer ul.dnnAdminTabNav li a{-webkit-border-bottom-right-radius:0;-webkit-border-bottom-left-radius:0;-moz-border-radius-bottomright:0;-moz-border-radius-bottomleft:0;border-bottom-right-radius:0;border-bottom-left-radius:0}
        pre.prettyprint,.bs-docs-example,.bs-docs-example::after, .wsc_search_wrap.full_size input[type="text"] {-webkit-border-radius: 0px; -moz-border-radius: 0px; border-radius: 0px;}
        .menu_wrap .nav .submenu_wrap li.wsc_last a {-webkit-border-top-right-radius:0;-webkit-border-top-left-radius:0;-moz-border-radius-topright:0;-moz-border-radius-topleft:0;border-top-right-radius:0;border-top-left-radius:0}
        .wsc_switcher_control > a#SwitcherBtn, .wsc_back_home a.btn {border-radius: <%=GetSetting("CornersRadius")%>px 0 0 <%=GetSetting("CornersRadius")%>px;}
    </style>
<%End If%>

<%--MegaMenu1 custom content width--%>
<%If (GetSetting("MegaMenuContentAlign1") <> "mega_cc_no") Then%>
    <style type="text/css">
        .main_menu li.root .mega_cc_left_pad1 {padding-left: <%=GetSetting("MegaMenuCContentWidth1")%> !important;} 
        .main_menu li.root .mega_cc_right_pad1 {padding-right: <%=GetSetting("MegaMenuCContentWidth1")%> !important;}
    </style>
<%End If%>

<%--MegaMenu1 custom background--%>
<%If (GetSetting("MegaMenuContentAlign1") = "mega_cc_bg") Then%>
    <style type="text/css">
        .menu_wrap .nav li.wsc_mega ul.submenu_wrap.mega_cc_bg1 {overflow: hidden;}
        .menu_wrap .nav li.wsc_mega ul.submenu_wrap.mega_cc_bg1:before {background-image: url('<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("CustomMegaMenuBgPic")%>?v=<%=GetSetting("timestamp")%>');}
    </style>
<%End If%>

<%-- MegaMenu2 custom content width --%>
<%If (GetSetting("MegaMenuContentAlign2") <> "mega_cc_no") Then%>
    <style type="text/css">
        .main_menu li.root .mega_cc_left_pad2 {padding-left: <%=GetSetting("MegaMenuCContentWidth2")%> !important;} 
        .main_menu li.root .mega_cc_right_pad2 {padding-right: <%=GetSetting("MegaMenuCContentWidth2")%> !important;}
    </style>
<%End If%>

<%--MegaMenu2 custom background--%>
<%If (GetSetting("MegaMenuContentAlign2") = "mega_cc_bg") Then%>
    <style type="text/css">
        .menu_wrap .nav li.wsc_mega ul.submenu_wrap.mega_cc_bg2 {overflow: hidden;},
        .menu_wrap .nav li.wsc_mega ul.submenu_wrap.mega_cc_bg2:before {background-image: url('<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("CustomMegaMenuBgPic2")%>?v=<%=GetSetting("timestamp")%>');}
    </style>
<%End If%>

<%--MegaMenu3 custom content width--%>
<%If (GetSetting("MegaMenuContentAlign3") <> "mega_cc_no") Then%>
    <style type="text/css">
        .main_menu li.root .mega_cc_left_pad3 {padding-left: <%=GetSetting("MegaMenuCContentWidth3")%> !important;} 
        .main_menu li.root .mega_cc_right_pad3 {padding-right: <%=GetSetting("MegaMenuCContentWidth3")%> !important;}
    </style>
<%End If%>

<%--MegaMenu2 custom background--%>
<%If (GetSetting("MegaMenuContentAlign3") = "mega_cc_bg") Then%>
    <style type="text/css">
        .menu_wrap .nav li.wsc_mega ul.submenu_wrap.mega_cc_bg3 {overflow: hidden;}
        .menu_wrap .nav li.wsc_mega ul.submenu_wrap.mega_cc_bg3:before {background-image: url('<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("CustomMegaMenuBgPic3")%>?v=<%=GetSetting("timestamp")%>');}
    </style>
<%End If%>

<%-- Sidebar custom background color --%>
<%If ((GetSetting("SideMenuBackColor") <> "") And (GetSetting("SideMenuBackColorTransp") <> "image")) Then%>
    <style>   
        .wsc-sidebar, .wsc-sidebar-wrap, .wsc-content-dark.wsc-sidebar {background-color: <%=HexToRGBA("#" + GetSetting("SideMenuBackColor"), GetSetting("SideMenuBackColorTransp"))%>;}
    </style>
<%End If%>

<%-- Sidebar IMAGE --%>
<%If (GetSetting("SideMenuBackColorTransp") = "image") Then%>
<style>
        .wsc-sidebar {background-repeat: repeat; background-image: url(<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("CustomSideMenuBackground")%>?v=<%=GetSetting("timestamp")%>) !important;}
        .wsc-sidebar-wrap {background-color: transparent;}
</style>
<%End If%>

<%-- Sidebar WIDTH --%>
<%If (GetSetting("SideMenuWidth") <> "") Then%>
<style>
    /*.wsc-sidebar .submenu_wrap  {left: <%=GetSetting("SideMenuWidth")%>px !important;}
    .wsc-sidebar.wsc-sidemenu-right .submenu_wrap  {right: <%=GetSetting("SideMenuWidth")%>px !important; left: auto !important;}*/
    .wsc-sidebar, .wsc-sidebar-wrap, .wsc-sidebar-inner, .wsc-sidebar-inner > ul {min-width: <%=GetSetting("SideMenuWidth")%>px;}
</style>
<%End If%>

<%-- Sidebar skin submenu POSITION --%>
<%If ((GetSetting("SideMenuWidth") <> "") And (GetSetting("SideMenuScrollable") = "wsc-sidebar-not-scrollable") And (GetSetting("SideMenuAlignment") = "wsc-sidemenu-left")) Then%>
<style>
    .wsc-sidebar .submenu_wrap  {left: <%=GetSetting("SideMenuWidth")%>px;}
</style>
<%End If%>

<%If ((GetSetting("SideMenuWidth") <> "") And (GetSetting("SideMenuScrollable") = "wsc-sidebar-not-scrollable") And (GetSetting("SideMenuAlignment") = "wsc-sidemenu-right")) Then%>
<style>
    .wsc-sidebar .submenu_wrap  {right: <%=GetSetting("SideMenuWidth")%>px;}
</style>
<%End If%>

<%-- Custom Font Awesome bullet --%>
<%If (GetSetting("faBulletName") <> "") Then%>
<style>
    .wsc-custom-bullet {list-style-type: none; margin-left: 2.14286em; padding-left: 0;}
    .wsc-custom-bullet > li {position: relative;}
    .wsc-custom-bullet.wsc-bullet-dark > li > em {color: #111;}
    .wsc-custom-bullet.wsc-bullet-white > li > em {color: #eee;}
    .wsc-custom-bullet.wsc-bullet-gray > li > em {color: #888;}   
</style>
<%End If%>

<%-- Sticky header --%>
<%If ((GetSetting("MiniHeaderType") = "sticky_header") And (GetSetting("HeaderSwitcher") = "header_show")) Then%>
    <%-- Shadow header themes --%>
    <%If ((GetSetting("ChooseHeaderStyle") = "theme_shadow_header_no") Or (GetSetting("ChooseHeaderStyle") = "theme_shadow_header_w") Or (GetSetting("ChooseHeaderStyle") = "theme_shadow_header") Or (GetSetting("ChooseHeaderStyle") = "theme_transparent_header")) Then%>
    <style>
        @media (min-width: 980px) {
            .wsc_generic_skin #header, .wsc_generic_skin #header .header_inner {position: absolute;}
        }
        /*Top pane + header styles*/
        .wsc_generic_skin #header.wscTopFull {position: relative; margin-top: -88px;}
        .wsc_generic_skin.header_centered #header.wscTopFull,.wsc_generic_skin.menu_bottom #header.wscTopFull {margin-top: -152px;}
        .wsc_generic_skin.menu_bottom.menu_tabbed #header.wscTopFull {margin-top: -142px;}
        @media (max-width: 979px) {
            .wsc_generic_skin #header.wscTopFull nav.main_menu {position: absolute; padding-right: 30px; left: 0; background: rgba(11, 16, 22, 0.55) none repeat scroll center center; border-top: none;}
        }
        /*End Top pane + header styles*/
    </style>
    <%End If%>
    <!--#include file="Styles/sticky_header.ascx"-->
<%End If%>

<%-- Vertical Content Align Center --%>
<%If (GetSetting("VerticalContentAlign") = "align_center") Then%>
<style>
.go-top {display: none !important;} html, body, #Form {height: 100% !important;}
.wsc_generic_skin {position: relative; top: 50%; -webkit-transform: translateY(-50%); -ms-transform: translateY(-50%); transform: translateY(-50%);}
</style>
<%End If%>

<%-- Vertical Content Align Bottom --%>
<%If (GetSetting("VerticalContentAlign") = "align_bottom") Then%>
<style>
.go-top {display: none !important;} html, body, #Form {height: 100% !important;}
.wsc_generic_skin {position: absolute; left: 50%; margin-left: -240px; bottom: 0px;}
</style>
<%End If%>

<%--Custom styles--%>
<link id="wsc_custom" href="<%= TemplateSourceDirectory %>/css/custom.css" type="text/css" rel="stylesheet" />
   
<!-- Custom styles from StyleSwitcher -->
<style><%=GetSetting("SwitcherCustomCSS")%></style>

<!--[if lt IE 9]>
<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<script src="<%= TemplateSourceDirectory %>/js/respond.min.js"></script>
<![endif]-->