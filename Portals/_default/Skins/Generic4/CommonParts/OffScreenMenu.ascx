﻿<div class="wsc-sidebar wsc-offscreen-container main_menu <%=GetSetting("SideMenuTextColor")%>  <%=GetSetting("SideMenuScrollable")%> wsc-full-bar <%=GetSetting("SideMenuAlignment")%>">
    <div class="wsc-sidebar-wrap">

        <!--LOGO DESKTOP VISIBLE-->
		<div class="logo">
            <%-- Case of system logo --%>
            <%If ((GetSetting("SideMenuLogo") = "sm_logo_system") Or (GetSetting("SideMenuLogo") = "")) Then%>
                <dnn:LOGO runat="server" ID="dnnLOGO2" />
            <%End If%>

            <%-- Case of custom logo --%>
            <%If (GetSetting("SideMenuLogo") = "sm_logo_custom") Then%>
                <a href="/"><img src='<%= TemplateSourceDirectory %>/StyleSwitcher/<%=GetSetting("CustomLogoSideMenuFile")%>?v=<%=GetSetting("timestamp")%>' alt="" /></a>
            <%End If%>
        </div>
        <!--//LOGO DESKTOP VISIBLE-->

        <div class="wsc-sidebar-inner">
            <%If ((GetSetting("SideMenuDisplay") = "all_menu_items") Or (GetSetting("SideMenuDisplay") = "")) Then%>
                <dnn:MENU ID="MENU3" MenuStyle="SideNavInline" runat="server"></dnn:MENU>
            <%End If%>
            <%If (GetSetting("SideMenuDisplay") = "only_children_items") Then%>
                <dnn:MENU ID="MENU4" MenuStyle="SideNavInline" NodeSelector="RootChildren" runat="server"></dnn:MENU>
            <%End If%>
        </div>

    </div>
</div>
