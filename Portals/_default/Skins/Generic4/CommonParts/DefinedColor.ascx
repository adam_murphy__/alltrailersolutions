﻿<style type="text/css">
    /**** DEFINED COLOR ****/

    /* Header, Menu Drop-Down, Caption Color */

    .header,
    .sticky_header .header_inner,
    .menu_wrap .nav ul,
    .tp-caption.bg,
    .wsc-sidebar, .wsc-sidebar-wrap
    { 
	    background-color: #333333;
    }
    
    .navbar-toggle {
        color: #<%=GetSetting("ColorScheme")%>;
    }

    /* Supportive background colors 
    (Carousel description, testimonials, accordions, tooltips etc. backgrounds) */

    .asphalt {color:#333333; }

    .slider-title, .accordion-group,
    .nav-tabs > li > a,
    .tabbable.tabs-left .nav-tabs a,
    .tooltip-inner,
    #filters li a.selected, #filters2 li  a:hover,
    .holder a:hover, .holder_module a:hover,
    a.jp-disabled:hover, .holder a.jp-next:hover
    {
        background-color:#333333;
    }

    .nav-tabs > li > a,
    .nav-tabs > li > a:hover, .nav-tabs > li.active > a:hover,.nav-tabs > .active > a:focus, 
    .nav-tabs > li.ui-tabs-active > a:hover, .nav-tabs > .ui-tabs-active > a:focus,
    /*#filters li a:hover, #filters2 li  a:hover,*/
    .holder a:hover, .holder_module a:hover,
    a.jp-disabled:hover, .holder a.jp-next:hover
    {
        border-color:#333333;    
    }

    .slider-title:after,
    .tooltip.top .tooltip-arrow {border-top-color:#333333;}
    .tooltip.right .tooltip-arrow {border-right-color: #333333;}
    .tooltip.bottom .tooltip-arrow {border-bottom-color: #333333;}
    .tooltip.left .tooltip-arrow {border-right-color: #333333;}
    
    .wsc-testimonial .flex-control-nav li a {background-color: #<%=GetSetting("ColorScheme")%>}
    
    /* Portfolio colors */
    

    #filters li a:hover, .portfolio-overlay a:hover,
    .item_description a:hover {color: #<%=GetSetting("ColorScheme")%>; }

    /* Darker Base Color 
    (Menu active tab, post info background, text highlighting, timeline etc.)   */

    .menu_wrap .nav > li > a:hover, 
    .menu_wrap .nav > li > a:focus,
    .intro-icon-large:before,
    a.wsc_icon_disc .intro-icon-disc:hover,
    a.wsc_icon_disc:hover .intro-icon-small:before,
    .wsc-featured-box.wsc-fdbox-plain .wsc-fdbox-icon em, .wsc-featured-box.wsc-fdbox-plain .wsc-fdbox-icon img,
    a.list-group-item.active > .badge, .nav-pills > .active > a > .badge,
    .main_menu ul.nav > li > em.label, .main_menu ul.nav > li > a > em.label,
    .wsc-team-title span, .wsc-color-custom,
    .wsc-custom-bullet.wsc-bullet-hue > li > em, #filters li a:hover
    { 
        color: #<%=GetSetting("ColorScheme")%>;
    }

    .menu_wrap .nav > li.active > a, 
    .menu_wrap .nav > li.active,
    .menu_wrap .nav > li.active > a:hover,
    .btn-blog, .btn-blog1, .btn-blog:hover, .btn-blog1:hover,
    #footer, em.label.label-default,
    .wsc-section-colored, .wsc-background-custom,
    .wsc-promo.wsc-promo-flat
    {
        background-color: #<%=GetSetting("ColorScheme")%>;
    }

    <%If (GetSetting("ContentBackOpacity") <> "") Then%>
        #footer {background-color: <%=HexToRGBA("#" + GetSetting("ColorScheme"), GetSetting("ContentBackOpacity"))%>;}
    <%End If%>

    .tabbable.tabs-left .nav-tabs a,
    .tabbable.tabs-left .nav-tabs .active a, 
    .tabbable.tabs-left .nav-tabs a:hover
    {
        border-left-color: #<%=GetSetting("ColorScheme")%>;
    }
    
    .title-vert-border-right{
        border-right-color:#<%=GetSetting("CustomColor")%>;
    }

    /* Lighter Base Color 
    (Links, Buttons, Image hover, Icons etc.) */
    /*a:visited */
    .hue, .colour, a, a:visited,
    .dropcap2,
    .com_no:hover, a.com2_no,
    .wsc_required,
    .pricing-table h3,
    .wsc-featured-box.wsc-fdbox-border .wsc-fdbox-icon em,.wsc-featured-box.wsc-fdbox-border .wsc-fdbox-icon img
    {
        color: #<%=GetSetting("ColorScheme")%>;
    }

    .wsc-button.wsc-button-border.hue{color: #<%=GetSetting("CustomColor")%> !important;}
    .wsc-button.wsc-button-border.hue:hover{color: #fff !important;}

    .zocial, a.zocial,
    .zocial:hover, .zocial:focus,
    .hue_block, .hue_block:hover,
    .hover_img:hover, .hover_colour,
    .hover_img.zoom:hover, .hover_colour.zoom,
    .btn-primary, .th, .th:hover, .progress .bar,
    .testimonial3, .quote_sections_hue,
    .testimonial-icon-disc, .testimonial-icon-disc2, .testimonial-icon-disc3,
    .pager li > a, .pager li > span, 
    .pagination ul > li > a:hover, .pagination ul > li > a:focus,
    .pagination ul > .active > a, .pagination ul > .active > span,
    .pagination ul > .active > a, .pagination ul > .active > span,
    #filters li a.selected, #filters2 li a.selected,
    .holder a.jp-current, .holder_module a.active,
    .widget-scroll-prev:hover, .widget-scroll-next:hover, 
    .widget-scroll-prev2:hover, .widget-scroll-next2:hover, 
    .widget-scroll-prev3:hover, .widget-scroll-next3:hover,
    .screen-bg, .pricing-header-row-1, .pricing-footer,
    .nav-tabs > li.active > a, .nav-tabs > li.ui-tabs-active > a,
    .nav-tabs > li > a:hover, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus, 
    .nav-tabs > li.ui-tabs-active > a:hover, .nav-tabs > .ui-tabs-active > a:focus,
    .tabbable.tabs-left .nav-tabs .active a, .tabbable.tabs-left .nav-tabs a:hover,
    .fontawesome-icon-list .fa-hover a:hover,
    .accordion .panel-heading, .accordion .panel-default > .panel-heading,
    .nav-pills > li.active > a, .nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus,
    .wsc-button, .wsc-button.wsc-button-dark:hover,.i-circled:hover,.i-rounded:hover,
    .wsc-featured-box .wsc-fdbox-icon em,.wsc-featured-box .wsc-fdbox-icon img,
    .wsc-fdbox-effect.wsc-fdbox-dark .wsc-fdbox-icon em:hover,.wsc-fdbox-effect.wsc-fdbox-dark:hover .wsc-fdbox-icon em,
    .wsc-fdbox-border.wsc-fdbox-effect.wsc-fdbox-dark .wsc-fdbox-icon em:after,
    .wsc-button.wsc-button-border.hue:hover
    {
        background-color: #<%=GetSetting("ColorScheme")%>;
    }

    .pagination ul > li > a, .pagination ul > li > span,
    #filters li a.selected, #filters2 li a.selected,
    .holder a.jp-current, .holder_module a.active,
    .title-vert-border,
    .custom-title.title-border-color:before,
    .custom-title.title-bottom-border h1,
    .custom-title.title-bottom-border h2,
    .custom-title.title-bottom-border h3,
    .custom-title.title-bottom-border h4,
    .custom-title.title-bottom-border h5,
    .custom-title.title-bottom-border h6,
    .heading-block.border-color:after,
    .wsc-featured-box.wsc-fdbox-outline .wsc-fdbox-icon,
    .wsc-featured-box.wsc-fdbox-border .wsc-fdbox-icon,
    blockquote, .blockquote-reverse, .wsc-border-custom,
    .wsc-button.wsc-button-border.hue
    {
        border-color: #<%=GetSetting("ColorScheme")%>;
    }

    .testimonial3:after {border-top-color: #<%=GetSetting("ColorScheme")%>;}
    .wsc-fdbox-effect.wsc-fdbox-dark .wsc-fdbox-icon em:after{box-shadow:0 0 0 2px #<%=GetSetting("ColorScheme")%>;}
    .wsc-fdbox-border.wsc-fdbox-effect.wsc-fdbox-dark .wsc-fdbox-icon em:hover,.wsc-fdbox-border.wsc-fdbox-effect.wsc-fdbox-dark:hover .wsc-fdbox-icon em{box-shadow:0 0 0 1px #<%=GetSetting("ColorScheme")%>;}
    /* Metro slider */
    .metro-slide .link i.corner:before
    {
        border-top-color: #<%=GetSetting("ColorScheme")%> !important;
        border-top-color:<%=HexToRGBA("#" + GetSetting("ColorScheme"), "0.9")%> !important;
    }
    .metro-caption 
    {
        background-color:#<%=GetSetting("ColorScheme")%>;
        background: transparent;
        background-color:<%=HexToRGBA("#" + GetSetting("ColorScheme"), "0.9")%>;
        -ms-filter: "progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#<%=GetSetting("ColorScheme")%>,endColorstr=#<%=GetSetting("ColorScheme")%>)"; /* IE8 */
        filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#<%=GetSetting("ColorScheme")%>,endColorstr=#<%=GetSetting("ColorScheme")%>);   /* IE6 & 7 */
    }

    .spinner {color: #<%=GetSetting("ColorScheme")%>; }
    
    /* Bredcrumbs (RSTYLES)*/
    .breadcrumbsPast { background-color: #<%=GetSetting("ColorScheme")%>; }
    .taa_breadcrumbs .taa_breadcrumb.breadcrumbsPast:after { background-color: #<%=GetSetting("ColorScheme")%>; }

    /* Revindex categories

    .rvdsfCategoryContainer .rtLI .rtSelected,
    .rvdsfCategoryContainer .rtLI .rtHover {
        background-color: #<%=GetSetting("ColorScheme")%>;
    }  */
    
    /*Revindex cart summary color*/

    .TopBarPane .rvdsfCartSummaryDetailItem > span {
        background-color: #<%=GetSetting("ColorScheme")%>;    
    }
    
    .dnnFormItem input[type="text"]:active, 
    .dnnFormItem input[type="text"]:focus, 
    .dnnFormItem input[type="password"]:focus, 
    .dnnFormItem input[type="password"]:active, 
    .dnnFormItem input[type="email"]:active, 
    .dnnFormItem input[type="email"]:focus, 
    .dnnFormItem select:active, 
    .dnnFormItem select:focus, 
    .dnnFormItem textarea:active, 
    .dnnFormItem textarea:focus, 
    .dnnFormItem input[type="search"]:active, 
    .dnnFormItem input[type="search"]:focus 
    {      
        border:  1px solid <%=HexToRGBA("#" + GetSetting("ColorScheme"), "0.5")%>;
        box-shadow: 0 0 3px 0 <%=HexToRGBA("#" + GetSetting("ColorScheme"), "0.4")%>;
    }

    /*Revindex admin*/
    .rvdsfWelcomeSteps li a:hover {
        background-color: #<%=GetSetting("ColorScheme")%>;
    }
    
    .rvdsfWelcomeSteps li a:hover::before {
        border-color: #<%=GetSetting("ColorScheme")%>;
        border-left-color: transparent;
    }
    
    .rvdsfWelcomeSteps li a:hover::after {
        border-left-color: #<%=GetSetting("ColorScheme")%>;
    }
    
    /*Colored solid overlay*/
    .wsc-color-overlay {background-color: <%=HexToRGBA("#" + GetSetting("ColorScheme"), "0.5")%>}
    .wsc-color-overlay-more {background-color: <%=HexToRGBA("#" + GetSetting("ColorScheme"), "0.8")%>}
    .wsc-color-overlay-less {background-color: <%=HexToRGBA("#" + GetSetting("ColorScheme"), "0.3")%>}
    
</style>