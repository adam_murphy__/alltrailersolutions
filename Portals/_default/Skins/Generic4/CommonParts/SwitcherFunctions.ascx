﻿<script type="text/VB" runat="server" language="vb">
    Private Function GetColorClass(key As String) As String
        Dim hex As String = GetSetting(key)
        Dim hexClass As String = String.Empty
        Dim max As Double
        Dim min As Double
        Try
            Dim number As UInteger = Convert.ToUInt32(hex, 16)
            
            hex = "#" & hex
            Dim color As System.Drawing.Color = System.Drawing.ColorTranslator.FromHtml(hex)
            Dim r As Double = color.R / 255.0
            Dim g As Double = color.G / 255.0
            Dim b As Double = color.B / 255.0
            max = Math.Max(r, g)
            max = Math.Max(max, b)
            min = Math.Min(r, g)
            min = Math.Min(min, b)
            Dim L As Double = (max + min) / 2
            If L < 0.65 Then
                hexClass = "wsc-content-white"
            Else
                hexClass = "wsc-content-dark"
            End If
            Return hexClass
        Catch e As FormatException
            Return hexClass
        Catch e As OverflowException
            Return hexClass
        Catch e As ArgumentException
            Return hexClass
        End Try
    End Function
    Public Function HexToRGBA(hex As String, alpha As String) As String
        If hex = "#" Then
            hex = "#000"
        End If
        Dim color = System.Drawing.ColorTranslator.FromHtml(hex)
        Return String.Format("rgba({0},{1},{2},{3})", color.R, color.G, color.B, alpha)
    End Function
</script>
