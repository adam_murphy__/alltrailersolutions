﻿<style type="text/css">
    /**** CUSTOM FONT ****/

/* Unifont family */
body,
.welcome_index,
.price,
.tp-caption.large_bold_title,
.tp-caption.medium_text_shadow,
.slidercontainer .caption,
.slidercontainer .caption>div h1,
.menu_minimal .menu_wrap .nav > li > a,
.product .title a,
.scFeaturedProduct .title a,
.main_menu ul.nav > li > em.label,
.main_menu ul.nav > li > a > em.label,
.wsc-counter, .wsc-sidebar li.root > a,
.wsc-team-title h4, .wsc-team-title h3, .wsc-team-content p, .c_content .Normal
{
    /*font-family: 'Lato', Arial, sans-serif;*/
    font-family: <%=GetSetting("CustomFont")%>;
}

h1,h2,h3,h4,h5,h6 {
    font-family: <%=GetSetting("TitlesCustomFont")%>;
}

/* Font sizes */
<%If (GetSetting("ContentTypoIfCustom") = "custom") Then%>
    body, p, ul li {font-size: <%=GetSetting("ContentFontSize")%>px; font-weight: <%=GetSetting("ContentFontWeight")%>;}
<%End If%>

<%If (GetSetting("H1TitleIfCustom") = "custom") Then%>
    h1 { font-size: <%=GetSetting("H1FontSize")%>px; font-weight: <%=GetSetting("H1FontWeight")%>;}
<%End If%>
<%If (GetSetting("H2TitleIfCustom") = "custom") Then%>
    h2 { font-size: <%=GetSetting("H2FontSize")%>px; font-weight: <%=GetSetting("H2FontWeight")%>;}
<%End If%>
<%If (GetSetting("H3TitleIfCustom") = "custom") Then%>
    h3 { font-size: <%=GetSetting("H3FontSize")%>px; font-weight: <%=GetSetting("H3FontWeight")%>;}
<%End If%>
<%If (GetSetting("H4TitleIfCustom") = "custom") Then%>
    h4 { font-size: <%=GetSetting("H4FontSize")%>px; font-weight: <%=GetSetting("H4FontWeight")%>;}
<%End If%>
<%If (GetSetting("H5TitleIfCustom") = "custom") Then%>
    h5 { font-size: <%=GetSetting("H5FontSize")%>px; font-weight: <%=GetSetting("H5FontWeight")%>;}
<%End If%>
</style>