<%--
<!-- 50% Pane + 50% Pane -->         
<div class="row">

	<div class="wsc_pane col-md-6" id="AddLeftHalfPane" runat="server">
	</div> 
				
	<div class="wsc_pane col-md-6" id="AddRightHalfPane" runat="server">
	</div> 
					
</div>

<!-- End 50% Pane + 50% Pane -->
 --%>
       
<%--
<!-- 33% Pane + 33% Pane + 33% Pane --> 
 
<div class="row">

	<div class="wsc_pane col-md-4" id="AddThirdLeftPane" runat="server">
	</div> 
				
	<div class="wsc_pane col-md-4" id="AddThirdMiddlePane" runat="server">
	</div> 
					
	<div class="wsc_pane col-md-4" id="AddThirdRightPane" runat="server">
	</div> 
				
</div>

<!-- End 33% Pane + 33% Pane + 33% Pane --> 
 --%>

   
<%--
<!-- 25% Pane + 25% Pane + 25% Pane + 25% Pane --> 

<div class="row">
		        
    <div class="wsc_pane col-md-3" id="AddNarrowPane0" runat="server">
	</div> 
				
	<div class="wsc_pane col-md-3" id="AddNarrowPane25" runat="server">
	</div> 
					
	<div class="wsc_pane col-md-3" id="AddNarrowPane50" runat="server">
	</div> 
				
	<div class="wsc_pane col-md-3" id="AddNarrowPane75" runat="server">
	</div>
                 
</div> 

<!-- End 25% Pane + 25% Pane + 25% Pane + 25% Pane -->
 --%>
       
<%--	
<!-- 33% Pane + 66% Pane -->  
			
<div class="row">
			
    <div class="wsc_pane col-md-4" id="AddLeftNarrowPane" runat="server">
    </div>

    <div class="wsc_pane col-md-8" id="AddRightWidePane" runat="server">
    </div>

</div>

<!-- End 33% Pane + 66% Pane -->
 --%>
      
<%--

<!-- 66% Pane + 33% Pane -->  
 
<div class="row">
                
    <div class="wsc_pane col-md-8" id="AddLeftWidePane" runat="server">
	</div>
			    
    <div class="wsc_pane col-md-4" id="AddRightNarrowPane" runat="server">
	</div>

</div>

<!-- End 66% Pane + 33% Pane -->
 --%>
   
<%--

<!-- 25% Pane + 75% Pane -->   
   
<div class="row">
			
    <div class="wsc_pane col-md-3" id="AddLeftQuarterPane" runat="server">
	</div>

	<div class="wsc_pane col-md-9" id="AddRightThreeQuartersPane" runat="server">
	</div>

</div>

<!-- End 25% Pane + 75% Pane -->

 --%>
     
<%--

<!-- 75% Pane + 25% Pane -->  
  
<div class="row">

    <div class="wsc_pane col-md-9" id="AddLeftThreeQuartersPane" runat="server">
	</div>
			    
    <div class="wsc_pane col-md-3" id="AddRightQuarterPane" runat="server">
	</div>

</div>

<!-- End 75% Pane + 25% Pane -->

 --%>
