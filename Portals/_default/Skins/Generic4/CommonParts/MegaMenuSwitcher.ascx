﻿<script type="text/javascript" >
$(document).ready(function () {

<%-- CUSTOM MEGA MENU 1 SETTINGS --%>

<%If (GetSetting("MMTabName1") <> "") Then%>

    $('.main_menu ul.sf-menu.nav > li.root > a').each(function(){
        _this=$(this);
        var str = $(this).clone().children().remove().end().text();
        if (str.indexOf('<%=GetSetting("MMTabName1")%>') > -1) {
            var _thispar = _this.parents('li.root');
            _thispar.find('li').addClass('wsc_mega');
            _thispar.addClass('wsc_mega');
            _thispar.children('.submenu_wrap').css("width","<%=GetSetting("MegaMenuRealWidth1")%>");
            _thispar.find('li.category').addClass("col-md-<%=GetSetting("MegaMenuColumns1")%>");
            _thispar.addClass("mega_col<%=GetSetting("MegaMenuColumns1")%>").addClass("<%=GetSetting("MegaMenuWidth1")%>");
        
            <%-- Apply custom content if enabled --%>

            <%If ((GetSetting("MegaMenuContentAlign1") <> "mega_cc_no") And (GetSetting("MegaMenuContentAlign1") <> "mega_cc_bg")) Then%>
                  _thispar.children('.submenu_wrap').addClass("<%=GetSetting("MegaMenuContentAlign1")%>");
                  _thispar.children('.submenu_wrap').prepend('<div class="mega_c_content"><%=GetSetting("MegaMenuContent1")%></div>');
            
                  _thispar.find('.mega_c_content').css("width","<%=GetSetting("MegaMenuCContentWidth1")%>");
                  _thispar.find('.mega_cc_left').addClass('mega_cc_left_pad1');
                  _thispar.find('.mega_cc_right').addClass('mega_cc_right_pad1');
            <%End If%>

            <%If (GetSetting("MegaMenuContentAlign1") = "mega_cc_bg") Then%>
                  _thispar.children('.submenu_wrap').addClass('mega_cc_bg1');
            <%End If%>  
        
        }
    });

<%End If%>

<%-- CUSTOM MEGA MENU 2 SETTINGS --%>

<%If (GetSetting("MMTabName2") <> "") Then%>

    $('.main_menu ul.sf-menu.nav > li.root > a').each(function(){
        _this=$(this);
        var str = $(this).clone().children().remove().end().text();
        if (str.indexOf('<%=GetSetting("MMTabName2")%>') > -1) {
            var _thispar = _this.parents('li.root');
            _this.parents('li.root').find('li').addClass('wsc_mega');
            _this.parents('li.root').addClass('wsc_mega');
            _this.parents('li.root').children('.submenu_wrap').css("width","<%=GetSetting("MegaMenuRealWidth2")%>");
            _this.parents('li.root').find('li.category').addClass("col-md-<%=GetSetting("MegaMenuColumns2")%>");
            _this.parents('li.root').addClass("mega_col<%=GetSetting("MegaMenuColumns2")%>").addClass("<%=GetSetting("MegaMenuWidth2")%>");
        
            <%-- Apply custom content if enabled --%>

            <%If ((GetSetting("MegaMenuContentAlign2") <> "mega_cc_no") And (GetSetting("MegaMenuContentAlign2") <> "mega_cc_bg")) Then%>
                  _this.parents('li.root').children('.submenu_wrap').addClass("<%=GetSetting("MegaMenuContentAlign2")%>");
                  _this.parents('li.root').children('.submenu_wrap').prepend('<div class="mega_c_content"><%=GetSetting("MegaMenuContent2")%></div>');
                            
                  _thispar.find('.mega_c_content').css("width","<%=GetSetting("MegaMenuCContentWidth2")%>");
                  _thispar.find('.mega_cc_left').addClass('mega_cc_left_pad2');
                  _thispar.find('.mega_cc_right').addClass('mega_cc_right_pad2');
            <%End If%>   
            
            <%If (GetSetting("MegaMenuContentAlign2") = "mega_cc_bg") Then%>
                  _thispar.children('.submenu_wrap').addClass('mega_cc_bg2');
            <%End If%>   
        
        }
    });

<%End If%>

<%-- CUSTOM MEGA MENU 3 SETTINGS --%>

<%If (GetSetting("MMTabName3") <> "") Then%>

    $('.main_menu ul.sf-menu.nav > li.root > a').each(function(){
        _this=$(this);
        var str = $(this).clone().children().remove().end().text();
        if (str.indexOf('<%=GetSetting("MMTabName3")%>') > -1) {
            var _thispar = _this.parents('li.root');
            _this.parents('li.root').find('li').addClass('wsc_mega');
            _this.parents('li.root').addClass('wsc_mega');
            _this.parents('li.root').children('.submenu_wrap').css("width","<%=GetSetting("MegaMenuRealWidth3")%>");
            _this.parents('li.root').find('li.category').addClass("col-md-<%=GetSetting("MegaMenuColumns3")%>");
            _this.parents('li.root').addClass("mega_col<%=GetSetting("MegaMenuColumns3")%>").addClass("<%=GetSetting("MegaMenuWidth3")%>");
             
            <%-- Apply custom content if enabled --%>

            <%If ((GetSetting("MegaMenuContentAlign3") <> "mega_cc_no") And (GetSetting("MegaMenuContentAlign3") <> "mega_cc_bg")) Then%>
                  _this.parents('li.root').children('.submenu_wrap').addClass("<%=GetSetting("MegaMenuContentAlign3")%>");
                  _this.parents('li.root').children('.submenu_wrap').prepend('<div class="mega_c_content"><%=GetSetting("MegaMenuContent3")%></div>');
                      
                  _thispar.find('.mega_c_content').css("width","<%=GetSetting("MegaMenuCContentWidth3")%>");
                  _thispar.find('.mega_cc_left').addClass('mega_cc_left_pad3');
                  _thispar.find('.mega_cc_right').addClass('mega_cc_right_pad3');
            <%End If%>    

            <%If (GetSetting("MegaMenuContentAlign3") = "mega_cc_bg") Then%>
                  _thispar.children('.submenu_wrap').addClass('mega_cc_bg3');
            <%End If%>  
        
        }
    });
    
<%End If%>

});
</script>