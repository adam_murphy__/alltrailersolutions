﻿<div class="wsc_mini_contacts">
	<ul>
        <%If ((GetSetting("SocialIconsPlacement") = "wsc_social_footer") Or (GetSetting("SocialIconsPlacement") = "")) Then%>
            <%If (GetSetting("TopBarAddress") <> "") Then%>
                <li class="wsc-top-address"><i class="fa fa-map-marker">&nbsp;</i> <a href="#"><%=GetSetting("TopBarAddress")%></a></li>
            <%End If%>
            <%If (GetSetting("TopBarPhone") <> "") Then%>
		        <li class="wsc-top-phone"><i class="fa fa-phone">&nbsp;</i> <a href="#"><%=GetSetting("TopBarPhone")%></a> </li>
            <%End If%>
            <%If (GetSetting("TopBarEmail") <> "") Then%>
		        <li class="wsc-top-email"><i class="fa fa-envelope-o">&nbsp;</i><a href='mailto:<%=GetSetting("TopBarEmail")%>'><%=GetSetting("TopBarEmail")%></a></li>
            <%End If%>
	    <%End If%>
        <%If (GetSetting("SocialIconsPlacement") = "wsc_social_header") Then%>
            <li><!--#include file="FooterSocial.ascx"--></li>
        <%End If%>
    </ul>
</div>
<span class="clearer"></span>