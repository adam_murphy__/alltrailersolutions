﻿<div class="footer_wrapper">
	<!--footer -->
	<section id="footer" class="<%=GetColorClass("CustomColor")%>">
        <div class="wsc_pane SocialFooter" id="SocialFooter" runat="server"></div>
	</section>
	<!--//footer -->
     
	<!--footer 2 -->
	<section id="footer2" class="<%=GetColorClass("FooterBackgroundColor")%>">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
				<div class="copyright">
                    <dnn:COPYRIGHT ID="FooterCopyright" runat="server" /> :
                    <dnn:TERMS ID="dnnTerms" runat="server" /> :
                    <dnn:PRIVACY ID="dnnPrivacy" runat="server" />
				</div>
				</div>
			</div>
		</div>
	</section>
    <!--//footer 2 -->

    <!--alternative footer panes-->
	<section id="footer_alt" class="<%=GetColorClass("FooterBackgroundColor")%>">
        <!--#include file="FooterPanes.ascx"-->
	</section>
    <!--//alternative footer panes-->

</div>