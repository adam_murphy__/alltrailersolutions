﻿<script type="text/javascript">
    (isTouchDevice()) ? ($('body').addClass('touch')) : ($('body').addClass('no-touch'));
    function isTouchDevice() { return 'ontouchstart' in window || !!(navigator.msMaxTouchPoints); }
</script>

<%-- Nice Scroll Style Switcher control --%>
<%If (GetSetting("NiceScroll") = "nice_scroll_on") Then%>
<script type="text/javascript" ><!--#include file="Scripts/jquery.nicescroll.min.js.ascx"--></script>
<script type="text/javascript" >
    $(document).ready(function () {
        if ($.fn.niceScroll && $('body#Body').hasClass('no-touch')){

            $.browser.mozilla = /firefox/.test(navigator.userAgent.toLowerCase());
            if ($.browser.mozilla) {
                var nice = $("body#Body").niceScroll();
                nice.onscrollstart = function () {
                    $("body#Body").find("iframe").css({ "pointer-events": "none" });
                };
                nice.onscrollend = function () {
                    $("body#Body").find("iframe").css({ "pointer-events": "auto" });
                };
            }else{$("body#Body").niceScroll();}

            $("html").scroll(function(){
              $("html").getNiceScroll().resize();
            });
        }
    });
</script>
<%End If%>

<%-- Floating Header Style Switcher control --%>
<%If ((GetSetting("MiniHeaderType") = "floating_header") And (GetSetting("HeaderSwitcher") = "header_show")) Then%>
<script type="text/javascript">
    floating_header = true;
</script>
<%End If%>

<%-- Sticky Header Style Switcher control --%>
<%If ((GetSetting("MiniHeaderType") = "sticky_header") And (GetSetting("HeaderSwitcher") = "header_show") And Not((GetSetting("Layout") <> "skin_wide") And (GetSetting("ChooseMenus") = "wsc-slidemenu"))) Then%>
<script type="text/javascript">
    sticky_header = true;
</script>
<%End If%>

<%-- Top padding in case of transparent header themes --%>
<%If ((GetSetting("ChooseHeaderStyle") = "theme_transparent_header") Or (GetSetting("ChooseHeaderStyle") = "theme_shadow_header") Or (GetSetting("ChooseHeaderStyle") = "theme_shadow_header_w") Or (GetSetting("ChooseHeaderStyle") = "theme_shadow_header_no")) Then%>
    <script type="text/javascript">
        $(document).ready(function () {
            if ($('#banner .wsc_pane:not(.DNNEmptyPane)').length) {
                // If banner not empty do nothing
                // If page title is visible
                $('#pagetitle .pagetitle_overlay').addClass('wsc_transpheader_pad');
            } else if ($('#intro .wsc_pane:not(.DNNEmptyPane)').length) {
                // If intro not empty add padding class
                $('#intro').addClass('wsc_transpheader_pad');
            } else {
                // If banner and intro empty add main content padding
                $('.inner_content').addClass('wsc_transpheader_pad');
            }
        });
    </script>
    <style type="text/css">
        #intro.wsc_transpheader_pad, .pagetitle_overlay.wsc_transpheader_pad, .inner_content.wsc_transpheader_pad {padding-top: 85px;}
    </style>
<%End If%>

