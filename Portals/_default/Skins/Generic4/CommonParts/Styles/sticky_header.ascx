﻿<style>
    
    /*Header*/
    @media (min-width: 980px)
    {
        .wsc_generic_skin #header, .wsc_generic_skin #header .header_inner
        {
            height: 85px;
            min-height: 60px;
            position: relative;
            width: 100%;
        }
        .wsc_generic_skin.menu_tabbed #header, .wsc_generic_skin.menu_tabbed #header .header_inner
        {
            height: 88px;
        }
        .wsc_generic_skin #header.sticky_header, .wsc_generic_skin.menu_tabbed #header.sticky_header
        {
            height: 60px;
        }
        .wsc_generic_skin #header.sticky_header .header_inner, .wsc_generic_skin #header.sticky_header:not(.wscTopFull) .header_inner
        {
            line-height: 25px;
            height: 60px;
            min-height: 40px;
            position: fixed;
            -webkit-transform: translateZ(0);
            top: 0;
            width: 100%;
            z-index: 1000;
        }
        .wsc-sidemenu-right #header.sticky_header .header_inner {right:0;}
        .wsc-sidemenu-left #header.sticky_header .header_inner {left:0;}
        .wsc_generic_skin #header, .wsc_generic_skin .header_inner, .wsc_generic_skin #header.sticky_header
        {
            transition: height 0.4s ease, opacity 0.3s ease, right 0.4s ease, left 0.4s ease;
            -webkit-transition: height 0.4s ease, opacity 0.3s ease, right 0.4s ease, left 0.4s ease;
            -o-transition: height 0.4s ease, opacity 0.3s ease, right 0.4s ease, left 0.4s ease;
        }
        .wsc_generic_skin.wsc-sidemenu-right .wsc-offscreen-wrap.wsc-reveal-nav #header.sticky_header .header_inner {right:25%;}
        .wsc_generic_skin.wsc-sidemenu-left .wsc-offscreen-wrap.wsc-reveal-nav #header.sticky_header .header_inner {left:25%;}
        /*Header centered*/
        .wsc_generic_skin.header_centered #header.sticky_header .header_inner, .wsc_generic_skin.header_centered #header.sticky_header, .wsc_generic_skin.menu_tabbed.header_centered #header.sticky_header
        {
            height: 107px;
        }
        .wsc_generic_skin.header_centered #header, .wsc_generic_skin.header_centered #header .header_inner
        {
            height: 152px;
        }
        /*Header classic*/
        .wsc_generic_skin.menu_bottom #header.sticky_header .header_inner, .wsc_generic_skin.menu_bottom #header.sticky_header, .wsc_generic_skin.menu_tabbed.menu_bottom #header.sticky_header,
        .wsc_generic_skin.menu_tabbed.menu_bottom #header.sticky_header .header_inner
        {
            height: 107px;
        }
        .wsc_generic_skin.menu_bottom #header, .wsc_generic_skin.menu_bottom #header .header_inner
        {
            height: 152px;
        }
        .wsc_generic_skin.menu_bottom.menu_tabbed #header, .wsc_generic_skin.menu_bottom.menu_tabbed #header .header_inner
        {
            height: 142px;
        }
        /*Skin boxed*/
        .skin_boxed.wsc_generic_skin #header.sticky_header .header_inner
        {
            width: 1230px;
        }
    }
    @media (max-width: 1199px)
    {
        .skin_boxed.wsc_generic_skin #header.sticky_header .header_inner
        {
            width: 970px;
        }
    }
    @media (max-width: 979px)
    {
        .skin_boxed.wsc_generic_skin #header.sticky_header .header_inner
        {
            width: 754px;
        }
    }
    @media (max-width: 767px)
    {
        .skin_boxed.wsc_generic_skin #header.sticky_header .header_inner
        {
            width: 100%;
        }
    }
    /*Skin boxed mini*/
    .skin_boxed_mini.wsc_generic_skin #header.sticky_header .header_inner
    {
        width: 1000px;
    }
    @media (max-width: 979px)
    {
        .skin_boxed_mini.wsc_generic_skin #header.sticky_header .header_inner
        {
            width: 754px;
        }
    }
    @media (max-width: 767px)
    {
        .skin_boxed_mini.wsc_generic_skin #header.sticky_header .header_inner
        {
            width: 100%;
        }
    }
    /*Logo*/
    @media (min-width: 980px)
    {
        .wsc_generic_skin #header.sticky_header .logo a
        {
            margin-top: 16px;
        }
        .wsc_generic_skin #header.sticky_header .logo img
        {
            height: 30px;
            width: auto;
        }
        .wsc_generic_skin #header .logo img
        {
            height: 43px;
            transition: height 0.4s ease 0s;
            -webkit-transition: height 0.4s ease 0s;
            -o-transition: height 0.4s ease 0s;
        }
    }
    /*Menu*/
    @media (min-width: 980px) and (max-width: 1199px)
    {
        .wsc_generic_skin.menu_tabbed:not(.skin_boxed_mini) #header:not(.sticky_header) .menu_wrap
        {
            margin-top: 28px;
        }
        .wsc_generic_skin.menu_tabbed:not(.skin_boxed_mini) #header:not(.sticky_header) .menu_wrap .nav > li
        {
            min-width: 70px;
        }
        .wsc_generic_skin.menu_tabbed:not(.skin_boxed_mini) #header:not(.sticky_header) .menu_wrap .nav > li > a
        {
            padding: 20px 18px 18px;
        }
    }
    @media (min-width: 980px)
    {
        .wsc_generic_skin #header .menu_wrap
        {
            transition: margin 0.4s ease;
            -webkit-transition: margin 0.4s ease;
            -o-transition: margin 0.4s ease;
        }
        .wsc_generic_skin .sticky_header .menu_wrap
        {
            margin: 12px 0 14px;
        }
        .wsc_generic_skin.menu_tabbed .sticky_header .menu_wrap .nav
        {
            margin-top: 3px;
        }
        .wsc_generic_skin.menu_tabbed .sticky_header .menu_wrap .nav > li
        {
            /*min-width: 90px;*/
            min-width: auto;
        }
        .wsc_generic_skin #header.sticky_header .menu_wrap .nav li.root > a
        {
            font-size: 12px;
            letter-spacing: 1px;
            padding: 3px 13px;
        }
        .wsc_generic_skin #header.sticky_header .menu_wrap .nav li.root ul.submenu_wrap
        {
            top: 55px;
        }
        .wsc_generic_skin #header.sticky_header .menu_wrap .nav li.root.mega_wide ul.submenu_wrap
        {
            top: 70px;
        }
        .wsc_generic_skin:not(.menu_minimal) #header .menu_wrap .nav > li.active > a
        {
            color: #fff;
        }
        .wsc_generic_skin #header .menu_wrap .nav li.root > a
        {
            transition: padding 0.4s ease, font-size 0.4s ease;
            -webkit-transition: padding 0.4s ease, font-size 0.4s ease;
            -o-transition: padding 0.4s ease, font-size 0.4s ease;
        }
        .skin_boxed_mini.wsc_generic_skin.menu_tabbed #header:not(.sticky_header) .menu_wrap
        {
            margin-top: 28px;
        }
        .skin_boxed_mini.wsc_generic_skin.menu_tabbed #header:not(.sticky_header) .menu_wrap .nav > li
        {
            min-width: 70px;
        }
        .skin_boxed_mini.wsc_generic_skin.menu_tabbed #header:not(.sticky_header) .menu_wrap .nav > li > a
        {
            padding: 20px 18px 18px;
        }
        .wsc_generic_skin .wsc-offscreen-toggle
        {
            transition: top 0.4s ease;
            -webkit-transition: top 0.4s ease;
            -o-transition: top 0.4s ease;
        }
        .wsc_generic_skin #header.sticky_header .wsc-offscreen-toggle
        {
            top: 1px;
        }
    }
    /*Content*/
    .wsc_generic_skin .inner_content.wsc_transpheader_pad
    {
        padding-top: 0px;
    }
    
</style>

<%If (GetSetting("TobBarOnOff") <> "wsc_tb_hidden") Then%>
<style>
    /*Header*/
    @media (min-width: 980px)
    {
        .wsc_generic_skin #header, .wsc_generic_skin #header .header_inner
        {
            height: 119px;
        }
        .wsc_generic_skin.menu_tabbed #header, .wsc_generic_skin.menu_tabbed #header .header_inner
        {
            height: 122px;
        }
        .wsc_generic_skin #header.sticky_header, .wsc_generic_skin.menu_tabbed #header.sticky_header
        {
            height: 87px;
        }
        
        .wsc_generic_skin #header.sticky_header .header_inner, .wsc_generic_skin #header.sticky_header:not(.wscTopFull) .header_inner
        {
            height: 87px;
        }
        /*Top bar*/
        .wsc_generic_skin #header #top-bar
        {
            height: 34px;
        }
        .wsc_generic_skin #header.sticky_header #top-bar
        {
            padding: 0px;
            height: 26px;
        }
        .wsc_generic_skin #header.sticky_header #top-bar,
        .wsc_generic_skin #header.sticky_header #top-bar a,
        .wsc_generic_skin #header.sticky_header #top-bar li, #top-bar li a 
        {
            font-size: 11px;    
        }
        .wsc_generic_skin #header #top-bar
        {
            transition: height 0.4s ease, padding 0.4s ease, font-size 0.3s ease;
            -webkit-transition: height 0.4s ease, padding 0.4s ease, font-size 0.3s ease;
            -o-transition: height 0.4s ease, padding 0.4s ease, font-size 0.3s ease;
        }
        
        /*Header centered*/
        .wsc_generic_skin.header_centered #header.sticky_header .header_inner, .wsc_generic_skin.header_centered #header.sticky_header, .wsc_generic_skin.menu_tabbed.header_centered #header.sticky_header 
        {
            height: 140px;
        }
        .wsc_generic_skin.header_centered #header, .wsc_generic_skin.header_centered #header .header_inner
        {
            height: 186px;
        }
        /*Header classic*/
        .wsc_generic_skin.menu_bottom #header.sticky_header .header_inner, .wsc_generic_skin.menu_bottom #header.sticky_header, .wsc_generic_skin.menu_tabbed.menu_bottom #header.sticky_header,
        .wsc_generic_skin.menu_tabbed.menu_bottom #header.sticky_header .header_inner
        {
            height: 140px;
        }
        .wsc_generic_skin.menu_bottom #header, .wsc_generic_skin.menu_bottom #header .header_inner
        {
            height: 185px;
        }
        .wsc_generic_skin.menu_bottom.menu_tabbed #header, .wsc_generic_skin.menu_bottom.menu_tabbed #header .header_inner
        {
            height: 176px;
        }
    }
</style>
<%End If%>

<%If ((GetSetting("TobBarOnOff") <> "wsc_tb_hidden") And ((GetSetting("ChooseHeaderStyle") = "theme_transparent_header") Or (GetSetting("ChooseHeaderStyle") = "theme_shadow_header_no") Or (GetSetting("ChooseHeaderStyle") = "theme_shadow_header") Or (GetSetting("ChooseHeaderStyle") = "theme_shadow_header_w"))) Then%>
<style>
    /*Header*/
    @media (min-width: 980px)
    {
        /*Top pane + header styles*/
        .wsc_generic_skin #header.wscTopFull {margin-top: -119px;}
        .wsc_generic_skin.menu_tabbed #header.wscTopFull {margin-top: -122px;}
        .wsc_generic_skin.menu_bottom.menu_tabbed #header {height: 176px;}
        .wsc_generic_skin.menu_bottom.menu_tabbed #header.wscTopFull {margin-top: -176px;}
        .wsc_generic_skin.header_centered #header.wscTopFull {margin-top: -186px;}
        .wsc_generic_skin.menu_bottom #header.wscTopFull {margin-top: -185px;}
        
        /*Header classic*/
        .wsc_generic_skin.menu_bottom #header.sticky_header .header_inner, .wsc_generic_skin.menu_bottom #header.sticky_header, .wsc_generic_skin.menu_tabbed.menu_bottom #header.sticky_header
        {
            height: 140px;
        }
    }
    @media (min-width: 768px) and (max-width: 979px) 
    {
        .wsc_generic_skin #header.wscTopFull {margin-top: -135px;}
    }
    @media (max-width: 767px) 
    {
        .wsc_generic_skin #header.wscTopFull {margin-top: -166px;}
        .wsc_generic_skin.header_centered #header.wscTopFull {margin-top: -155px;}
        .wsc_generic_skin.menu_bottom #header.wscTopFull, .wsc_generic_skin.menu_bottom.menu_tabbed #header.wscTopFull {margin-top: -166px;}
    }
    @media (max-width: 420px) 
    {
        .wsc_generic_skin #header.wscTopFull {margin-top: -188px;}
        .wsc_generic_skin.header_centered #header.wscTopFull {margin-top: -155px;}
        .wsc_generic_skin.menu_bottom #header.wscTopFull, .wsc_generic_skin.menu_bottom.menu_tabbed #header.wscTopFull {margin-top: -166px;}
    }
</style>
<%End If%>