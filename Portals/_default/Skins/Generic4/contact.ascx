<%@ Control Language="vb" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="NAV" Src="~/Admin/Skins/Nav.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TEXT" Src="~/Admin/Skins/Text.ascx" %>
<%@ Register TagPrefix="dnn" TagName="BREADCRUMB" Src="~/Admin/Skins/BreadCrumb.ascx" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/User.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LINKS" Src="~/Admin/Skins/Links.ascx" %>
<%@ Register TagPrefix="dnn" TagName="PRIVACY" Src="~/Admin/Skins/Privacy.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TERMS" Src="~/Admin/Skins/Terms.ascx" %>
<%@ Register TagPrefix="dnn" TagName="COPYRIGHT" Src="~/Admin/Skins/Copyright.ascx" %>
<%@ Register TagPrefix="dnn" TagName="STYLES" Src="~/Admin/Skins/Styles.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="wsc" TagName="SWITCHER" src="~/DesktopModules/WebSitesCreative.StyleSwitcher/ViewSwitcher.ascx" %>

<%-- Common Styles --%>
<!--#include file="CommonParts/AddStyles.ascx"-->

<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<!--#include file="CommonParts/PreScripts.ascx"-->
<%If (GetSetting("GoogleMapKey") <> "") Then%>
<script type="text/javascript" src="//maps.google.com/maps/api/js?sensor=false&amp;language=en&key=<%=GetSetting("GoogleMapKey")%>"></script>
<%End If%>
<%If (GetSetting("GoogleMapKey") = "") Then%>
<script type="text/javascript" src="//maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>
<%End If%>
<!-- Browser notification -->
<!--[if lte IE 8]>
<div class="browser-notification ie8">
	<p>Your browser (Internet Explorer 8 or lower) is <strong>out of date</strong>. It has known <strong>security flaws</strong> and may <strong>not display all features</strong> of this and other websites. <a href="//www.browser-update.org/update.html">Learn how to update your browser</a>.</p>
	<div class="close">X</div>
</div>
<![endif]-->
<!-- // Browser notification -->
<div class="wsc_generic_skin  <%=GetSetting("FooterStyle")%> <%=GetSetting("PageTitleFontStyle")%> <%=GetSetting("PageTitleBackgroundType")%> <%=GetSetting("PageTitleStyle")%> <%=GetSetting("Layout")%> <%=GetSetting("ChooseMenus")%> <%=GetSetting("SideMenuAlignment")%>  <%=GetSetting("HeaderAlignment")%>" data-loading="<%=GetSetting("Preloader")%>">
    
<%--OffScreen Slide Menu--%>
<%If (GetSetting("ChooseMenus") = "wsc-slidemenu") Then%>
<!--#include file="CommonParts/OffScreenMenu.ascx"-->   
<%End If%>

<div class="wsc_generic_inside_wrap"><%--Inside Wrap--%>

    <%--Header--%>
    <%If ((GetSetting("HeaderSwitcher") = "header_show") Or (GetSetting("HeaderSwitcher") = "")) Then%>
        <!--#include file="CommonParts/Header.ascx"-->
    <%End If%>

    <%--Banner Pane--%>
    <!--#include file="CommonParts/BannerPanes.ascx"-->

    <%If (GetSetting("PageTitleOnOff") = "show") Then%>
    <!--page title-->
	<section id="pagetitle">
    <div class="pagetitle_overlay <%=GetSetting("PageTitleOverlay")%>">
	    <div class="container intro_wrapper <%=GetSetting("PageTitleAlignment")%>">
        
	        <div class="row">
	            <div class="wsc_pane col-md-12 IntroPane">
                    <h1 class="title"><%= PortalSettings.ActiveTab.TabName %></h1>
                    <span class="title-sub"><%= PortalSettings.ActiveTab.Title %></span>
                </div>
		    </div>
	        <div class="wsc-breadcrumbs">
            <%If (GetSetting("PageTitleBreadcrumbsOnOff") = "show") Then%>
                <dnn:BREADCRUMB ID="BREADCRUMB1" runat="server" CssClass="breadcrumbLink"  RootLevel="0" Separator=" &nbsp;&frasl;&nbsp; " HideWithNoBreadCrumb="true" />    	             
            <%End If%>
            </div>            
	    </div>
    </div>
	</section>
    <!--//page title-->
    <%End If%>

    <%--Intro--%>
    <!--#include file="CommonParts/IntroPanesBreadcrumbs.ascx"-->
	
    <%--Map--%>
    <section id="map">
        <div class="wsc_pane" id="MapPane" runat="server">
        </div>   
    </section>

    <%--Main content panes--%>		
	<section id="main_content">
        <!--#include file="CommonParts/Panes.ascx"-->
    </section>
    
    <%--Footer--%>
    <%If ((GetSetting("FooterSwitcher") = "footer_show") Or (GetSetting("FooterSwitcher") = "")) Then%>
        <!--#include file="CommonParts/Footer.ascx"-->
    <%End If%>
    			
    <%--Up to top button--%>
    <!--#include file="CommonParts/ToTopButton.ascx"-->

</div><%--Inside Wrap--%>
</div>
<div class="common_background"></div>

<!-- Switcher module script -->
<script type="text/VB" runat="server" language="vb">
    Private Function GetSetting(key As String) As String
        Dim httpContext = System.Web.HttpContext.Current
        If httpContext.Items("WCS_StyleSwitcherData") Is Nothing Then
            Dim activeTab = TabController.CurrentPage
            Dim tc As New TabController()
            Dim frontTab = tc.GetTab(activeTab.TabID, activeTab.PortalID, True)
            Dim skinSrc As String = frontTab.SkinSrc
            If skinSrc = "" Then
                skinSrc = PortalController.GetCurrentPortalSettings().DefaultPortalSkin
            End If
            skinSrc = DotNetNuke.UI.Skins.SkinController.FormatSkinSrc(skinSrc, PortalController.GetCurrentPortalSettings())
            Dim filePath As String = httpContext.Server.MapPath(DotNetNuke.UI.Skins.SkinController.FormatSkinPath(skinSrc) + "StyleSwitcher\SwitcherDataContact.xml")
            If IO.File.Exists(filePath) Then
                Dim doc = New System.Xml.XmlDocument()
                doc.Load(filePath)
                Dim listNode = doc.SelectSingleNode("/ArrayOfThemeSetting")
                Dim themeSettingNodeList = listNode.SelectNodes("ThemeSetting")
                Dim data = New Generic.Dictionary(Of String, String)()
                For Each node As System.Xml.XmlNode In themeSettingNodeList
                    data.Add(node.Attributes.GetNamedItem("Name").Value, node.Attributes.GetNamedItem("Value").Value)
                Next
                httpContext.Items("WCS_StyleSwitcherData") = data
            Else
                Return String.Empty
            End If
        End If
        Dim items = DirectCast(httpContext.Items("WCS_StyleSwitcherData"), Generic.Dictionary(Of String, String))
        Return If(items.ContainsKey(key), items(key), String.Empty)
    End Function
</script>

<%-- Imported Switcher module functions --%>
<!--#include file="CommonParts/SwitcherFunctions.ascx"-->

<!-- Switcher module control -->
<%If DotNetNuke.Security.PortalSecurity.IsInRole("Host") Then%>
<div class="wsc_switcher_control">
    <!-- Button trigger modal -->
    <a id="SwitcherBtn" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-cog"></span></a>
    <wsc:SWITCHER runat="server" ID="Switcher" SwitcherDataFileName="SwitcherDataContact.xml" SwitcherSettingsFileName="SwitcherSettingsInner.xml" />
</div>
<% End If%>

<%--Back to home button--%>
<%If (GetSetting("ToHomeBtn") <> "") Then%>
    <div class="wsc_back_home">
    <a class="btn btn-primary btn-lg" href="<%=GetSetting("ToHomeBtn")%>"><span class="glyphicon glyphicon-home"></span></a>
    </div>
<%End If%>

<%----------------------------------- STYLES ----%>

<%-- Load css if page title parallax style choosed --%>
<%If ((GetSetting("PageTitleBackgroundType") = "pt_back_parallax")  Or (GetSetting("PageTitleBackgroundType") = "pt_back_image") Or (GetSetting("PageTitleBackgroundType") = "pt_fixed_image")) Then%>
<!--#include file="CommonParts/ParallaxPanes/ParallaxPageTitle.ascx"-->
<%End If%>

<%----------------------------------- JAVA SCRIPT ----%>
<script type="text/javascript">
    var SkinPath = "<%= TemplateSourceDirectory %>";
</script>

<%-- Load js libs if page title parallax style choosed --%>
<%If (GetSetting("PageTitleBackgroundType") = "pt_back_parallax") Then%>

<%-- Parallax Script --%>
<script type="text/javascript" src="<%= TemplateSourceDirectory %>/js/conditional/jquery.parallax-1.1.3.js"></script>

<%End If%>

<%-- Common Scripts --%>
<!--#include file="CommonParts/AddScripts.ascx"-->
<!--#include file="CommonParts/GoogleMap.ascx"-->

<dnn:STYLES runat="server" ID="StylesIE7" Name="IE7Minus" StyleSheet="css/ie8skin.css"
    Condition="IE 7" UseSkinPath="true" />
<dnn:STYLES runat="server" ID="StylesIE8" Name="IE8Minus" StyleSheet="css/ie8skin.css"
    Condition="IE 8" UseSkinPath="true" />