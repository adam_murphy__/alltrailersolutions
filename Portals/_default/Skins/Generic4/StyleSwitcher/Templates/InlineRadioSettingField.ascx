﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RadioSettingField.ascx.cs"
    Inherits="WebSitesCreative.Modules.StyleSwitcher.Controls.SwitcherSettingsControls.RadioSettingField" %>
<%@ Import Namespace="WebSitesCreative.Modules.StyleSwitcher.Components.Helpers" %>
<div class="panel panel-default">
    <div class="panel-heading">
        <asp:Label ID="lblText" runat="server"></asp:Label>
        <asp:Label ID="lblHelp" runat="server" Visible="false"></asp:Label>
    </div>
    <div class="panel-body">
      <div class="form-inline">
        <asp:Repeater runat="server" ID="rpItems">
            <ItemTemplate>
                <div class="radio">
                    <label>
                        <asp:PlaceHolder runat="server" ID="phChecked" Visible='<%#DataBinder.Eval(Container,"DataItem.Value").ToString()==SelectedValue%>'>
                            <input type="radio" name="<%#SettingInfo.Name%>" checked="checked" value='<%#DataBinder.Eval(Container,"DataItem.Value") %>' />
                        </asp:PlaceHolder>
                        <asp:PlaceHolder runat="server" Visible='<%#DataBinder.Eval(Container,"DataItem.Value").ToString()!=SelectedValue%>'>
                            <input type="radio" name="<%#SettingInfo.Name%>" value='<%#DataBinder.Eval(Container,"DataItem.Value") %>' />
                        </asp:PlaceHolder>
			<asp:PlaceHolder runat="server" Visible='<%#DataBinder.Eval(Container,"DataItem.Image") == null%>'>
                            <%#DataBinder.Eval(Container,"DataItem.Text") %>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder runat="server" Visible='<%#DataBinder.Eval(Container,"DataItem.Image") != null%>'>
                            <img class="wsc-tooltip" data-toggle="tooltip" data-placement="top" title='<%#DataBinder.Eval(Container,"DataItem.Text") %>' src='<%#ThemesHelper.GetThemeFileUrl(DataBinder.Eval(Container,"DataItem.Image") == null ? "": DataBinder.Eval(Container,"DataItem.Image").ToString())%>' />
                        </asp:PlaceHolder>
                    </label>
                </div>
            </ItemTemplate>
        </asp:Repeater>
      </div>
    </div>
</div>
