﻿<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE stylesheet [
  <!ENTITY space "<xsl:text> </xsl:text>">
  <!ENTITY cr "<xsl:text>
</xsl:text>">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html"/>
	<xsl:param name="ControlID" />
	<xsl:param name="Options" />
    <xsl:param name="ManifestPath" />
  <xsl:template match="/*">
    <xsl:apply-templates select="root" />
  </xsl:template>
	<xsl:template match="root">
            <ul class="wsc-nav-side fa-ul">
        <xsl:apply-templates select="node">
          <xsl:with-param name="nodeType">root</xsl:with-param>
        </xsl:apply-templates>
            </ul>
    </xsl:template>

  <xsl:template match="node">
    <xsl:param name="nodeType" />
		<li>
      <xsl:variable name="nodeClass">
        <xsl:value-of select="$nodeType"/> 
        <xsl:if test="@selected = 1"> current active</xsl:if>
        <xsl:if test="@first = 1"> mmFirst</xsl:if>
        <xsl:if test="@last = 1"> mmLast</xsl:if>
        <xsl:if test="node"> sub dropdown</xsl:if>
      </xsl:variable>
            
      <xsl:attribute name="class">
        <xsl:value-of select="$nodeClass"/>
      </xsl:attribute>

        <xsl:choose>
            <xsl:when test="@enabled = 1">
                <i class="fa-li fa fa-angle-right"></i>
                <a href="{@url}" class="enabled-link">
                    <xsl:value-of select="@text" />
                </a>
            </xsl:when>
            <xsl:otherwise>
                <i class="fa-li fa fa-angle-right"></i>
                <a href="#_" class="disabled-link">
                    <xsl:value-of select="@text" />
                </a>
            </xsl:otherwise>
        </xsl:choose>

            
    </li>
	</xsl:template>
</xsl:stylesheet>
