<%@ Control language="vb" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Containers.Container" %>
<%@ Register TagPrefix="dnn" TagName="ICON" Src="~/Admin/Containers/Icon.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TITLE" Src="~/Admin/Containers/Title.ascx" %>
<%@ Register TagPrefix="dnn" TagName="VISIBILITY" Src="~/Admin/Containers/Visibility.ascx" %>
<div class="c_head h1_title_center_container">
    <div class="head_title">
        <div class="c_icon">
            <dnn:ICON runat="server" id="dnnICON" />
            <h1 class="c_title wsc_title center">
                <dnn:TITLE runat="server" id="dnnTITLE" CssClass="TitleHead" />
            </h1>
        </div>
        <div class="clear_float">
        </div>
    </div>
    <div class="c_content">
        <div id="ContentPane" runat="server" class="Normal c_contentpane center">
        </div>
    </div>
</div>
