<%@ Control language="vb" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Containers.Container" %>
<%@ Register TagPrefix="dnn" TagName="ICON" Src="~/Admin/Containers/Icon.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TITLE" Src="~/Admin/Containers/Title.ascx" %>
<%@ Register TagPrefix="dnn" TagName="VISIBILITY" Src="~/Admin/Containers/Visibility.ascx" %>
<div class="c_head boxed_container">
    <div class="tile">
    <div class="head_title">
        <div class="c_icon tile-image big-illustration">
            <dnn:ICON runat="server" id="dnnICON" />
            <h5 class="c_title wsc_title">
                <dnn:TITLE runat="server" id="dnnTITLE" CssClass="TitleHead" />
            </h5>
        </div>
        <div class="clear_float">
        </div>
    </div>
    <div class="c_content">
        <div id="ContentPane" runat="server" class="Normal c_contentpane">
        </div>
    </div>
    </div>
</div>
